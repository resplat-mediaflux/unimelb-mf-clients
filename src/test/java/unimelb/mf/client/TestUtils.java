package unimelb.mf.client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class TestUtils {

    public static final String PROPERTIES_FILE = System.getProperty("user.home")
            + "/.junit/unimelb-mf-clients-ssh-test.properties";

    public static final String MFLUX_CONFIG_FILE = System.getProperty("user.home") + "/.Arcitecta/mflux.cfg.local";

    public static final Properties properties = new Properties();

    static {
        try {
            Reader r = new BufferedReader(new FileReader(new File(PROPERTIES_FILE)));
            try {
                properties.load(r);
            } finally {
                r.close();
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    public static final List<String> putArgs = new ArrayList<String>();

    public static final List<String> getArgs = new ArrayList<String>();

    static {

    }

}
