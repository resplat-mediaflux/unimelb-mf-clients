package unimelb.mf.model.asset;

import arc.xml.XmlDoc;

public class SymlinkAsset {

    public static String TYPE = "posix/symlink";
    public static String LINK_TYPE = "posix-symlink"; // since 4.10.016

    public static boolean isSymlinkAsset(XmlDoc.Element ae) throws Throwable {
        return getSymlinkTarget(ae) != null;
    }

    public static String getSymlinkTarget(XmlDoc.Element ae) throws Throwable {
        if (ae.elementExists("link")) {
            if (LINK_TYPE.equals(ae.value("link/@type"))) {
                return ae.value("link/referent");
            }
        } else if (TYPE.equals(ae.value("type"))) {
            // TODO remove support for old style symlink
            String url = ae.value("content/url");
            if (url != null && url.startsWith("file:")) {
                boolean managed = ae.booleanValue("content/url/@managed");
                if (!managed) {
                    // old style symlink asset...
                    return url.substring(5);
                }
            }
        }
        return null;
    }

}
