package unimelb.mf.client.sync.cli;

import java.io.BufferedReader;
import java.io.Console;
import java.io.FileReader;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import arc.xml.XmlDoc;
import unimelb.mf.client.session.MFConfigBuilder;
import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.sync.MFSyncApp;
import unimelb.mf.client.sync.settings.upstream.DirectoryUploadJob;
import unimelb.mf.client.util.collection.CollectionPath;
import unimelb.utils.PathUtils;

public class MFInstrumentUpload extends MFSyncApp {

    public static final String PROG = "unimelb-mf-instrument-upload";

    public static final String DEFAULT_CONFIG_FILE_NAME = "mf-instrument-upload-config.xml";
    public static final Path DEFAULT_CONFIG_PATH = Paths.get(System.getProperty("user.home"), ".Arcitecta",
            DEFAULT_CONFIG_FILE_NAME);

    public MFInstrumentUpload() {
        super();
        settings().setCsumCheck(false);
        settings().setVerbose(true);
        settings().setDaemon(false);
    }

    @Override
    protected void preExecute() throws Throwable {
        if (logger() == null) {
            setLogger(createDefaultLogger(settings(), applicationName()));
        }
    }

    protected void printUsage() {
        // @formatter:off
        System.out.println();
        System.out.println("USAGE:");
        System.out.println(String.format("    %s [--config config.xml] src-dirs", PROG));
        System.out.println();
        System.out.println("DESCRIPTION:");
        System.out.println("    Upload local files to Mediaflux.  If the file pre-exists in Mediaflux and is the same as that being uploaded, the Mediaflux asset is not modified. However, if the files differ, a new version of the asset will be created. In Daemon mode, the process will only upload new files since the process last executed.");
        System.out.println();
        System.out.println("OPTIONS:");
        System.out.println("    --config <config.xml>                     A single configuration file including all required settings (Mediaflux server details, user credentials, application settings). If supplied, all other configuration options are ignored. If not supplied, defaults to \"" + DEFAULT_CONFIG_PATH.toString() + "\"");
        System.out.println("    --help                                    Prints usage.");
        System.out.println();
        System.out.println("POSITIONAL ARGUMENTS:");
        System.out.println("    src-dirs                                  Source directories to upload.");
        System.out.println();
        System.out.println("EXAMPLES:");
        System.out.println(String.format("    %s --config ./unimelb-mf-instrument-upload-config.xml /data/user1/acquisition_20181208_153005", PROG));
        System.out.println();
        // @formatter:on
    }

    protected void parseArgs(String[] args) throws Throwable {

        // must have args
        if (args == null || args.length == 0) {
            throw new IllegalArgumentException("Missing arguments.");
        }

        List<Path> srcDirs = new ArrayList<Path>();

        Path configPath = null;

        for (int i = 0; i < args.length;) {
            if ("--help".equalsIgnoreCase(args[i]) || "-h".equalsIgnoreCase(args[i])) {
                printUsage();
                System.exit(args.length > 1 ? 1 : 0);
            } else if ("--config".equalsIgnoreCase(args[i])) {
                if (configPath != null) {
                    throw new IllegalArgumentException("More than one --config arguments supplied. Expects only one.");
                }
                configPath = Paths.get(args[i + 1]);
                if (!Files.exists(configPath)) {
                    throw new IllegalArgumentException("Config file: " + configPath + " does not exist.");
                }
                i += 2;
            } else {
                Path f = Paths.get(args[i]);
                if (!Files.exists(f)) {
                    throw new IllegalArgumentException("Source directory: '" + args[i] + "' does not exist.");
                }
                if (!Files.isDirectory(f)) {
                    throw new IllegalArgumentException("'" + args[i] + "' is not a directory.");
                }
                if (Files.isSymbolicLink(f)) {
                    throw new IllegalArgumentException("'" + args[i] + "' is a symbolic link.");
                }
                srcDirs.add(f);
                i++;
            }
        }

        if (configPath == null) {
            if (Files.exists(DEFAULT_CONFIG_PATH)) {
                configPath = DEFAULT_CONFIG_PATH;
            } else {
                throw new IllegalArgumentException("Missing --config file.");
            }
        }

        /*
         * parse MF config
         */
        MFConfigBuilder mfConfig = new MFConfigBuilder();
        if (mfConfig.app() == null) {
            mfConfig.setApp(applicationName());
        }
        mfConfig.loadFromXmlFile(configPath.toFile());

        /*
         * authenticate to get session
         */
        MFSession session = MFSession.create(mfConfig);

        /*
         * associate with session
         */
        setSession(session);

        /*
         * parse settings
         */
        List<Destination> destinations = new ArrayList<Destination>();
        Reader r = new BufferedReader(new FileReader(configPath.toFile()));
        try {
            XmlDoc.Element pe = new XmlDoc().parse(r);
            if (pe == null) {
                throw new IllegalArgumentException("Failed to parse configuration XML file: " + configPath);
            }
            this.settings().loadFromXml(pe, configPath.toString(), this.session());
            List<XmlDoc.Element> des = pe.elements("sync/destination");
            if (des == null || des.isEmpty()) {
                throw new IllegalArgumentException(
                        "Failed to parse XML config file. properties/sync/destination is not found.");
            }
            for (XmlDoc.Element de : des) {
                destinations.add(new Destination(de));
            }
        } finally {
            r.close();
        }

        if (destinations.isEmpty()) {
            throw new IllegalArgumentException("Failed to parse XML config file. No destination is supplied.");
        }

        for (Path srcDir : srcDirs) {
            List<String> dstCollections = dstCollectionFor(srcDir, destinations);
            if (dstCollections == null || dstCollections.isEmpty()) {
                throw new IllegalArgumentException(
                        "Fail to identify destination namespace for source directory: " + srcDir + ".");
            }
            if (dstCollections.size() == 1) {
                this.settings().addJob(
                        new DirectoryUploadJob(srcDir, null, null, new CollectionPath(dstCollections.get(0)), true));
            } else {
                Collection<String> selectedCollections = selectDstCollection(dstCollections);
                for (String collectionPath : selectedCollections) {
                    this.settings().addJob(
                            new DirectoryUploadJob(srcDir, null, null, new CollectionPath(collectionPath), true));
                }
            }
        }
    }

    private Collection<String> selectDstCollection(List<String> dstCollections) {
        Set<Integer> selectedIndexes = new LinkedHashSet<Integer>();
        boolean selectAll = false;
        Console console = System.console();
        console.printf("Select destination:%n");
        for (int i = 0; i < dstCollections.size(); i++) {
            console.printf("%d. %s%n", i + 1, dstCollections.get(i));
        }
        console.printf("Select one or more (comma-separated) destinations. a/all to select all.%n");
        while (selectedIndexes.isEmpty() && !selectAll) {
            String selection = console.readLine("Your selection? ");
            if (selection != null) {
                selection = selection.trim();
                if (!selection.isEmpty()) {
                    if ("a".equalsIgnoreCase(selection) || "all".equalsIgnoreCase(selection)) {
                        selectAll = true;
                    }
                    try {
                        String[] tokens = selection.split(",");
                        for (String token : tokens) {
                            if (token != null && !token.trim().isEmpty()) {
                                int idx = Integer.parseInt(token.trim());
                                if (idx < 1 || idx > dstCollections.size()) {
                                    throw new IllegalArgumentException("Invalid index: " + idx
                                            + ". Expects value from 1 to " + dstCollections.size());
                                }
                                selectedIndexes.add(idx);
                            }
                        }
                    } catch (Throwable e) {
                        console.printf("Invalid selection: %s%n", e.getMessage() == null ? "" : e.getMessage());
                    }
                }
            }
        }
        Set<String> selectedNss = new LinkedHashSet<String>();
        if (selectAll) {
            selectedNss.addAll(dstCollections);
        } else {
            for (Integer idx : selectedIndexes) {
                selectedNss.add(dstCollections.get(idx));
            }
        }
        return selectedNss;
    }

    private List<String> dstCollectionFor(Path srcDir, List<Destination> destinations) {
        Set<String> dstCollections = new LinkedHashSet<String>();
        for (Destination destination : destinations) {
            if (destination.sourceMatches(srcDir)) {
                dstCollections.addAll(destination.namespaces());
            }
        }
        return new ArrayList<String>(dstCollections);
    }

    @Override
    public final String applicationName() {
        return PROG;
    }

    public static void main(String[] args) throws Throwable {
        MFInstrumentUpload app = new MFInstrumentUpload();
        try {
            app.parseArgs(args);
        } catch (IllegalArgumentException iae) {
            System.err.println();
            System.err.println("Invalid arguments: " + iae.getMessage());
            System.err.println();
            app.printUsage();
            System.exit(1);
        }
        if (app.settings().daemon()) {
            Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {

                @Override
                public void run() {
                    app.interrupt();
                }
            }));
            new Thread(app).start();
        } else {
            app.execute();
        }
    }

    static class Destination {
        private Set<String> _nss;
        private Path _rootDir;

        Destination(XmlDoc.Element de) throws Throwable {
            _nss = new LinkedHashSet<String>();
            Collection<String> nss = de.values("namespace");
            if (nss != null) {
                _nss.addAll(nss);
            } else {
                throw new IllegalArgumentException(
                        "Failed to parse XML configuration: destination/namespace is not found.");
            }
            if (de.elementExists("rootDir")) {
                if (de.count("rootDir") > 1) {
                    throw new IllegalArgumentException(
                            "Failed to parse XML configuration: multiple destination/rootDir are found. Expects at most one.");
                }
                _rootDir = Paths.get(de.value("rootDir"));
                if (!Files.exists(_rootDir) || !Files.isDirectory(_rootDir)) {
                    throw new IllegalArgumentException("Failed to parse XML configuration: destination/rootDir: '"
                            + _rootDir + "' does not exist or it is not a directory.");
                }
            }
        }

        public Collection<String> namespaces() {
            return Collections.unmodifiableSet(_nss);
        }

        public Path rootDir() {
            return _rootDir;
        }

        public boolean sourceMatches(Path src) {
            if (_rootDir == null) {
                return true;
            }
            String rootPath = PathUtils.toSystemIndependent(_rootDir.toAbsolutePath().toString());
            String srcPath = PathUtils.toSystemIndependent(src.toAbsolutePath().toString());
            return srcPath.startsWith(rootPath + "/");
        }
    }

}
