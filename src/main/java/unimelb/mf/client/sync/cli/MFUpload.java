package unimelb.mf.client.sync.cli;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import unimelb.mf.client.session.MFConfig;
import unimelb.mf.client.session.MFConfigBuilder;
import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.sync.MFSyncApp;
import unimelb.mf.client.sync.settings.upstream.DirectoryUploadJob;
import unimelb.mf.client.sync.settings.upstream.FileSetUploadJob;
import unimelb.mf.client.util.collection.CollectionPath;

public class MFUpload extends MFSyncApp {

    public static final String PROG = "unimelb-mf-upload";

    public MFUpload() {
        super();
        settings().setCsumCheck(false);
        settings().setVerbose(true);
        settings().setDaemon(false);
    }

    @Override
    protected void preExecute() throws Throwable {
        if (logger() == null) {
            setLogger(createDefaultLogger(settings(), applicationName()));
        }
    }

    protected void printUsage() {
        // @formatter:off
        System.out.println();
        System.out.println("USAGE:");
        System.out.printf("    %s [OPTIONS] --dest <dest-collection-path> [src-dir1 [src-dir2...]] [src-file1 [src-file2...]]%n", PROG);
        System.out.printf("    %s --config <mf-upload-config.xml>%n", PROG);
        System.out.println();
        System.out.println("DESCRIPTION:");
        System.out.println("    Upload local files to Mediaflux.  If the file pre-exists in Mediaflux and is the same as that being uploaded, the Mediaflux asset is not modified. However, if the files differ, a new version of the asset will be created. In Daemon mode, the process will only upload new files since the process last executed.");
        System.out.println();
        System.out.println("OPTIONS:");
        System.out.println("    --config <config.xml>                     A single configuration file including all required settings (Mediaflux server details, user credentials, application settings). If supplied, all other configuration options are ignored.");
        System.out.println();
        System.out.println("    --mf.config <mflux.cfg>                   Path to the config file that contains Mediaflux server details and user credentials.");
        System.out.println("    --mf.host <host>                          Mediaflux server host.");
        System.out.println("    --mf.port <port>                          Mediaflux server port.");
        System.out.println("    --mf.transport <https|http|tcp/ip>        Mediaflux server transport, can be http, https or tcp/ip.");

//        System.out.println("    --mf.auth <domain,user,password>          Mediaflux user credentials.");
//        System.out.println("    --mf.token <token>                        Mediaflux secure identity token.");

        System.out.println("    --no-cluster-io                           Disable cluster I/O if applicable.");

        System.out.println("    --dest <dest-collection-path>             The destination collection in Mediaflux.");
        System.out.println("    --create-parents                          Create destination parent collection if it does not exist, including any necessary but nonexistent parent collections.");
        System.out.println("    --csum-check                              If enabled, computes the checksum from the uploaded file and compares with that computed by the server for the Mediaflux asset.");
        System.out.println("    --nb-queriers <n>                         Number of query threads. Defaults to " + unimelb.mf.client.sync.settings.Settings.DEFAULT_NUM_OF_QUERIERS + ". Maximum is " + Settings.MAX_NUM_OF_QUERIERS);
        System.out.println("    --nb-workers <n>                          Number of concurrent worker threads to upload data. Defaults to " + unimelb.mf.client.sync.settings.Settings.DEFAULT_NUM_OF_WORKERS + ". Maximum is " + Settings.MAX_NUM_OF_WORKERS);
        System.out.println("    --nb-retries <n>                          Retry times when error occurs. Defaults to " + unimelb.mf.client.sync.settings.Settings.DEFAULT_NB_RETRIES);
        System.out.println("    --batch-size <size>                       Size of the query result. Defaults to " + unimelb.mf.client.sync.settings.Settings.DEFAULT_BATCH_SIZE);
        System.out.println("    --daemon                                  Run as a daemon.");
        System.out.println("    --daemon-port <port>                      Daemon listener port if running as a daemon. Defaults to " + unimelb.mf.client.sync.settings.Settings.DEFAULT_DAEMON_LISTENER_PORT);
        System.out.println("    --daemon-scan-interval <seconds>          Time interval (in seconds) between scans of source directories. Defaults to " + (unimelb.mf.client.sync.settings.Settings.DEFAULT_DAEMON_SCAN_INTERVAL / 1000) + " seconds.");
        System.out.println("    --exclude-parent                          Exclude parent directory at the destination (Upload the contents of the directory) if the source path ends with trailing slash.");
		System.out.println("    --aggregate-transmission                  Aggregate transmission to improve the performance for large number of small files if file size is less than 1MB.");
		System.out.println("    --split                                   Split large files (size>1GB) into chunks and upload them in parallel. Ignored if single worker thread.");
        System.out.println("    --log-dir <dir>                           Path to the directory for log files. No logging if not specified.");
        System.out.println("    --log-file-size-mb <n>                    Log file size limit in MB. Defaults to " + unimelb.mf.client.sync.settings.Settings.DEFAULT_LOG_FILE_SIZE_MB + "MB");
        System.out.println("    --log-file-count <n>                      Log file count. Defaults to " + unimelb.mf.client.sync.settings.Settings.DEFAULT_LOG_FILE_COUNT);
        System.out.println("    --notify <email-addresses>                When completes, send email notification to the recipients(comma-separated email addresses if multiple). Not applicable for daemon mode.");
        System.out.println("    --sync-delete-assets                      Delete assets that do not have corresponding local files exist.");
        System.out.println("    --hard-delete-assets                      Force the asset deletion (see --sync-delete-assets) process to hard delete assets.  Otherwise, the behaviour is controlled by server properties (whether a deletion is a soft or hard action).");
        System.out.println("    --follow-symlinks                         Follow symbolic links. If not specified, it will not follow symbolic links, instead, it will create special symbolic link assets in Mediaflux. When exported as NFS share these symoblic assets will be represented as symbolic links. If downloaded using the unimelb-mf-download tool on Linux/MacOS platforms, they can be restored as symbolic links.");
        System.out.println("    --worm                                    Set the WORM state for the uploaded assets.");
        System.out.println("    --worm-can-add-versions                   Allow to add new versions of metadata and content when in the WORM state. Ignored if --worm option is not specified.");
        System.out.println("    --worm-no-move                            Disallow to move the uploaded assets. Ignored if --worm option is not specified.");
		System.out.println("    --save-file-attrs                         If specified, save local file attributes, such as owner uid, gid, ctime and ACls, into Mediaflux asset metadata.");
		System.out.println("    --preserve-modified-time                  If specified, preserve the local file's modified time as corresponding asset modified time in Mediaflux.");
        System.out.println("    --quiet                                   Do not print progress messages.");
        System.out.println("    --help                                    Prints usage.");
        System.out.println("    --version                                 Prints version.");
        System.out.println();
        System.out.println("POSITIONAL ARGUMENTS:");
        System.out.println("    src-dir                                   Source directory to upload.");
        System.out.println("    src-file                                  Source file to upload.");
        System.out.println();
        System.out.println("EXAMPLES:");
        System.out.printf("    %s --mf.config ~/.Arcitecta/mflux.cfg --nb-workers 4  --dest /projects/proj-1128.1.59 ~/Documents/foo ~/Documents/bar%n", PROG);
        System.out.printf("    %s --config ~/.Arcitecta/mf-upload-config.xml%n", PROG);
        System.out.println();
        // @formatter:on
    }

    protected void parseArgs(String[] args) throws Throwable {

        // must have args
        if (args == null || args.length == 0) {
            throw new IllegalArgumentException("Missing arguments.");
        }

        if (args.length == 1 && "--config".equalsIgnoreCase(args[0])) {
            throw new IllegalArgumentException("Missing --config file.");
        }

        for (String arg : args) {
            if ("--help".equalsIgnoreCase(arg) || "-h".equalsIgnoreCase(arg)) {
                printUsage();
                System.exit(args.length > 1 ? 1 : 0);
            }
            if ("--version".equalsIgnoreCase(arg)) {
                printVersion();
                System.exit(args.length > 1 ? 1 : 0);
            }
        }

        if (args.length == 2 && "--config".equalsIgnoreCase(args[0])) {

            // settings from single configuration file (.xml)

            Path configPath = (args[1].indexOf('/') < 0 && args[1].indexOf(File.separatorChar) < 0)
                    ? Paths.get(System.getProperty("user.dir"), args[1])
                    : Paths.get(args[1]);
            if (!Files.isRegularFile(configPath)) {
                throw new IllegalArgumentException(
                        "--config file: " + args[1] + " does not exist or is not a regular file.");
            }
            MFConfigBuilder mfconfig = new MFConfigBuilder().setConsoleLogon(true);
            mfconfig.loadFromXmlFile(configPath.toFile());
            if (mfconfig.app() == null) {
                mfconfig.setApp(applicationName());
            }

            MFSession session = MFSession.create(mfconfig);

            setSession(session);

            settings().loadFromXmlFile(configPath, session);

        } else {

            // settings from all sort of arguments...

            MFConfigBuilder mfconfig = new MFConfigBuilder().setConsoleLogon(true);
            mfconfig.findAndLoadFromConfigFile();
            mfconfig.loadFromSystemEnv();
            if (mfconfig.app() == null) {
                mfconfig.setApp(applicationName());
            }

            Map<Path, Boolean> dirs = new LinkedHashMap<>();
            Set<Path> files = new LinkedHashSet<>();
            String rootCollectionPath = null;

            try {
                for (int i = 0; i < args.length;) {
                    if ("--config".equalsIgnoreCase(args[i])) {
                        throw new IllegalArgumentException("Unexpected arguments conflict with --config argument.");
                    } else if ("--dest".equalsIgnoreCase(args[i]) || "--namespace".equalsIgnoreCase(args[i])) {
                        if (rootCollectionPath == null) {
                            rootCollectionPath = args[i + 1];
                            if (rootCollectionPath != null && !rootCollectionPath.startsWith("/")) {
                                System.err
                                        .println("Warning: prepending missing / to asset collection path: '" + rootCollectionPath + "'.");
                                rootCollectionPath = "/" + rootCollectionPath;
                            }
                        } else {
                            throw new IllegalArgumentException("Expects only one --dest argument. Found multiple.");
                        }
                        i += 2;
                        continue;
                    }

                    int n = parseMFOptions(args, i, mfconfig);
                    if (n > 0) {
                        i += n;
                        continue;
                    }

                    n = parseUploadOptions(args, i);
                    if (n > 0) {
                        i += n;
                        continue;
                    }
                    // TODO
                    boolean trailingSlash = args[i].endsWith("/") || args[i].endsWith(File.separator);
                    Path f = args[i].indexOf('/') < 0 && args[i].indexOf(File.separatorChar) < 0
                            ? Paths.get(System.getProperty("user.dir"), args[i]).toAbsolutePath()
                            : Paths.get(args[i]).toAbsolutePath();
                    if (!Files.exists(f)) {
                        throw new IllegalArgumentException("Input file/directory: '" + args[i] + "' does not exist.");
                    }
                    if (Files.isDirectory(f)) {
                        if (Files.isSymbolicLink(f) && !settings().followSymlinks()) {
                            throw new IllegalArgumentException("'" + f
                                    + "' is a symbolic link to directory. To upload the linked directory, turn on --follow-symlinks.");
                        }
                        dirs.put(f, trailingSlash);
                    } else {
                        files.add(f);
                    }
                    i++;
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                throw new IllegalArgumentException(e);
            }

            if (rootCollectionPath == null) {
                throw new IllegalArgumentException("Missing destination path: --dest argument.");
            }
            if (dirs.isEmpty() && files.isEmpty()) {
                throw new IllegalArgumentException("Missing input file/directory.");
            }

            /*
             * authenticate to get session
             */
            MFSession session = MFSession.create(mfconfig);

            /*
             * associate with session
             */
            setSession(session);
            
            CollectionPath rootCollection = new CollectionPath(rootCollectionPath);
            rootCollection.resolve(session);

            if (!settings().createParentCollections() && !rootCollection.exists(session)) {
                throw new IllegalArgumentException("Remote collection: '" + rootCollectionPath + "' does not exist.");
            }
            /*
             * add jobs
             */
            // dirs
            Set<Path> ds = dirs.keySet();
            for (Path dir : ds) {
                boolean trailingSlash = dirs.get(dir);
                settings().addJob(
                        new DirectoryUploadJob(dir, null, null, rootCollection, !settings().excludeParent() || !trailingSlash));
            }
            // files
            if (!files.isEmpty()) {
                settings().addJob(new FileSetUploadJob(files, rootCollection));
            }

        }
    }

    protected int parseMFOptions(String[] args, int i, MFConfigBuilder mfconfig) {
        if ("--mf.config".equalsIgnoreCase(args[i])) {
            try {
                mfconfig.loadFromConfigFile(args[i + 1]);
            } catch (Throwable e) {
                e.printStackTrace();
                throw new IllegalArgumentException("Failed to parse --mf.config file: " + args[i + 1], e);
            }
            return 2;
        } else if ("--mf.host".equalsIgnoreCase(args[i])) {
            mfconfig.setHost(args[i + 1]);
            return 2;
        } else if ("--mf.port".equalsIgnoreCase(args[i])) {
            mfconfig.setPort(Integer.parseInt(args[i + 1]));
            return 2;
        } else if ("--mf.transport".equalsIgnoreCase(args[i])) {
            mfconfig.setTransport(args[i + 1]);
            return 2;
        } else if ("--mf.auth".equalsIgnoreCase(args[i])) {
            throw new IllegalArgumentException(
                    "--mf.auth argument is no longer supported. Please specify user credentials in file: "
                            + MFConfig.DEFAULT_MFLUX_CFG_FILE
                            + " or the config file specified with --mf.config argument.");
            // @formatter:off
//            String auth = args[i + 1];
//            String[] parts = auth.split(",");
//            if (parts.length != 3) {
//                throw new IllegalArgumentException("Invalid mf.auth: " + auth);
//            }
//            mfconfig.setUserCredentials(parts[0], parts[1], parts[2]);
//            return 2;
            // @formatter:on
        } else if ("--mf.token".equalsIgnoreCase(args[i])) {
            throw new IllegalArgumentException(
                    "--mf.token argument is no longer supported. Please specify the token in file: "
                            + MFConfig.DEFAULT_MFLUX_CFG_FILE
                            + " or the config file specified with --mf.config argument.");
            // @formatter:off
//            mfconfig.setToken(args[i + 1]);
//            return 2;
            // @formatter:on
        } else if ("--no-cluster-io".equalsIgnoreCase(args[i])) {
            mfconfig.setUseClusterIO(false);
            return 1;
        } else {
            return 0;
        }
    }

    protected int parseUploadOptions(String[] args, int i) {
        if ("--follow-symlinks".equalsIgnoreCase(args[i])) {
            settings().setFollowSymlinks(true);
            return 1;
        } else if ("--create-parents".equalsIgnoreCase(args[i]) || "--create-namespaces".equalsIgnoreCase(args[i])) {
            settings().setCreateParentCollections(true);
            return 1;
        } else if ("--csum-check".equalsIgnoreCase(args[i])) {
            settings().setCsumCheck(true);
            return 1;
        } else if ("--batch-size".equalsIgnoreCase(args[i])) {
            try {
                int batchSize = Integer.parseInt(args[i + 1]);
                if (batchSize <= 0) {
                    throw new IllegalArgumentException("Invalid --batch-size " + args[i + 1]
                            + " Expects a positive integer, found " + args[i + 1]);
                }
                settings().setBatchSize(batchSize);
                return 2;
            } catch (NumberFormatException nfe) {
                throw new IllegalArgumentException(
                        "Invalid --batch-size " + args[i + 1] + " Expects a positive integer, found " + args[i + 1],
                        nfe);
            }
        } else if ("--nb-queriers".equalsIgnoreCase(args[i])) {
            try {
                int nbQueriers = Integer.parseInt(args[i + 1]);
                if (nbQueriers <= 0 || nbQueriers > Settings.MAX_NUM_OF_QUERIERS) {
                    throw new IllegalArgumentException("Invalid --nb-queriers: " + args[i + 1]
                            + ". Expects an integer between 1 and " + Settings.MAX_NUM_OF_QUERIERS);
                }
                settings().setNumberOfQueriers(nbQueriers);
                return 2;
            } catch (NumberFormatException nfe) {
                throw new IllegalArgumentException(
                        "Invalid --nb-queriers " + args[i + 1] + " Expects a positive integer, found " + args[i + 1],
                        nfe);
            }
        } else if ("--nb-workers".equalsIgnoreCase(args[i])) {
            try {
                int nbWorkers = Integer.parseInt(args[i + 1]);
                if (nbWorkers <= 0 || nbWorkers > Settings.MAX_NUM_OF_WORKERS) {
                    throw new IllegalArgumentException("Invalid --nb-workers: " + args[i + 1]
                            + ". Expects an integer between 1 and " + Settings.MAX_NUM_OF_WORKERS);
                }
                settings().setNumberOfWorkers(nbWorkers);
                return 2;
            } catch (NumberFormatException nfe) {
                throw new IllegalArgumentException(
                        "Invalid --nb-workers " + args[i + 1] + " Expects a positive integer, found " + args[i + 1],
                        nfe);
            }
        } else if ("--nb-retries".equalsIgnoreCase(args[i])) {
            try {
                int nbRetries = Integer.parseInt(args[i + 1]);
                settings().setMaxNumberOfRetries(nbRetries);
                return 2;
            } catch (NumberFormatException nfe) {
                throw new IllegalArgumentException(
                        "Invalid --nb-retries " + args[i + 1] + " Expects a positive integer, found " + args[i + 1],
                        nfe);
            }
        } else if ("--daemon".equalsIgnoreCase(args[i])) {
            settings().setDaemon(true);
            return 1;
        } else if ("--daemon-port".equalsIgnoreCase(args[i])) {
            try {
                int port = Integer.parseInt(args[i + 1]);
                if (port < 0 || port > 65535) {
                    throw new IllegalArgumentException("Invalid --daemon-port " + args[i + 1]
                            + " Expects a positive integer, found " + args[i + 1]);
                }
                settings().setDaemonListenerPort(port);
                return 2;
            } catch (NumberFormatException nfe) {
                throw new IllegalArgumentException("Invalid --daemon-port " + args[i + 1]
                        + " Expects a positive integer in the range of [0, 65535], found " + args[i + 1], nfe);
            }
        } else if ("--daemon-scan-interval".equalsIgnoreCase(args[i])) {
            try {
                int intervalSeconds = Integer.parseInt(args[i + 1]);
                if (intervalSeconds <= 0) {
                    throw new IllegalArgumentException("Invalid --daemon-scan-interval " + args[i + 1]
                            + " Expects a positive integer, found " + args[i + 1]);
                }
                settings().setDaemonScanInterval(((long) intervalSeconds) * 1000L);
                return 2;
            } catch (NumberFormatException nfe) {
                throw new IllegalArgumentException("Invalid --daemon-scan-interval " + args[i + 1]
                        + " Expects a positive integer, found " + args[i + 1], nfe);
            }
        } else if ("--exclude-parent".equalsIgnoreCase(args[i])) {
            settings().setExcludeParent(true);
            return 1;
        } else if ("--aggregate-transmission".equalsIgnoreCase(args[i])) {
            // TODO remove this arg
//            settings().enableAggregateUpload();
            return 1;
        } else if ("--split".equalsIgnoreCase(args[i])) {
            // TODO remove this arg
//            settings().enableSplit();
            return 1;
        } else if ("--log-dir".equalsIgnoreCase(args[i])) {
            Path logDir = Paths.get(args[i + 1]).toAbsolutePath();
            if (!Files.exists(logDir)) {
                throw new IllegalArgumentException("Logging directory: " + args[i + 1] + " does not exist.");
            }
            if (!Files.isDirectory(logDir)) {
                throw new IllegalArgumentException(args[i + 1] + " is not a directory.");
            }
            settings().setLogDirectory(logDir);
            return 2;
        } else if ("--log-file-size-mb".equalsIgnoreCase(args[i])) {
            String size = args[i + 1];
            if (size.toLowerCase().endsWith("m")) {
                size = size.substring(0, size.length() - 1);
            }
            if (size.toLowerCase().endsWith("mb")) {
                size = size.substring(0, size.length() - 2);
            }
            int fileSizeMB = Integer.parseInt(size);
            settings().setLogFileSizeMB(fileSizeMB);
            return 2;
        } else if ("--log-file-count".equalsIgnoreCase(args[i])) {
            int fileCount = Integer.parseInt(args[i + 1]);
            settings().setLogFileCount(fileCount);
            return 2;
        } else if ("--notify".equalsIgnoreCase(args[i])) {
            String[] emails = args[i + 1].indexOf(',') != -1 ? args[i + 1].split(",") : new String[] { args[i + 1] };
            settings().addRecipients(emails);
            return 2;
        } else if ("--sync-delete-assets".equalsIgnoreCase(args[i])) {
            settings().setDeleteAssets(true);
            return 1;
        } else if ("--hard-delete-assets".equalsIgnoreCase(args[i])) {
            settings().setHardDestroyAssets(true);
            return 1;
        } else if ("--worm".equalsIgnoreCase(args[i])) {
            settings().setWorm(true);
            return 1;
        } else if ("--worm-can-add-versions".equalsIgnoreCase(args[i])) {
            settings().setWormCanAddVersions(true);
            return 1;
        } else if ("--worm-no-move".equalsIgnoreCase(args[i])) {
            settings().setWormCanMove(false);
            return 1;
        } else if ("--save-file-attrs".equalsIgnoreCase(args[i])) {
            settings().setSaveFileAttributes(true);
            return 1;
        } else if ("--preserve-modified-time".equalsIgnoreCase(args[i])) {
            settings().setPreserveModifiedTime(true);
            return 1;
        } else if ("--quiet".equalsIgnoreCase(args[i])) {
            settings().setVerbose(false);
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public final String applicationName() {
        return PROG;
    }

    public static void main(String[] args) throws Throwable {
        MFUpload app = new MFUpload();
        try {
            app.parseArgs(args);
        } catch (IllegalArgumentException iae) {
            System.err.println();
            System.err.println("Invalid arguments: " + iae.getMessage());
            System.err.println();
            app.printUsage();
            System.exit(1);
        }
        if (app.settings().daemon()) {
            Runtime.getRuntime().addShutdownHook(new Thread(app::interrupt));
            new Thread(app).start();
        } else {
            app.execute();
        }
    }

}
