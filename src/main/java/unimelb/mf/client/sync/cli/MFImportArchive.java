package unimelb.mf.client.sync.cli;

import java.nio.file.Path;
import java.util.Arrays;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;

import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;
import unimelb.mf.client.session.MFConfigBuilder;
import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.sync.task.FileSetArchiveCreateTask;
import unimelb.utils.LoggingUtils;

@Command(name = "mf-import-archive", abbreviateSynopsis = true, usageHelpWidth = 120, synopsisHeading = "\nUSAGE:\n  ", descriptionHeading = "\nDESCRIPTION:\n  ", description = "Import local files or directory into Mediaflux as an archive asset. The destination archive file format can be .zip, .tar or .aar.", parameterListHeading = "\nPARAMETERS:\n", optionListHeading = "\nOPTIONS:\n", sortOptions = false, version = MFImportArchive.VERSION, separator = " ")
public class MFImportArchive implements Callable<Integer> {

    public static final String VERSION = "0.0.1";

    @Option(names = { "-c",
            "--config" }, required = false, paramLabel = "<mflux.cfg>", description = "Mediaflux configuration file. If not specified, it will try $HOME/.Arcitecta/mflux.cfg on MacOS/Linux or %%USERPROFILE%%\\.Arcitecta\\mflux.cfg on Windows.")
    private Path cfgFile;

    @Option(names = { "-a",
            "--asset-path" }, required = true, paramLabel = "<asset-path>", description = "Path to the destination archive asset in Mediaflux.")
    private String assetPath;

    @Option(names = {
            "--log-dir" }, required = false, paramLabel = "<log-directory>", description = "The log directory. Logging is disabled unless this is specified.")
    private Path logDir;

    @Option(names = {
            "--compress" }, required = false, negatable = true, description = "Compress the destination archive. By default, the archive is not compressed.")
    private boolean compress = false;

    @Option(names = {
            "--analyze" }, required = false, negatable = true, description = "Analyze the asset content after the archive asset is created.")
    private boolean analyze = false;

    @Option(names = { "-h", "--help" }, usageHelp = true, description = "Print usage information")
    private boolean printHelp;

    @Option(names = { "--version" }, versionHelp = true, description = "Print version information")
    private boolean printVersion;

    @Option(names = { "--quiet" }, defaultValue = "false", description = "Quiet mode. ")
    private boolean quiet;

    @Parameters(description = "Source files or directories to import into Mediaflux.", index = "0", arity = "1..", paramLabel = "SRC_FILES")
    private Path[] srcFiles;

    @Override
    public Integer call() throws Exception {
        try {
            MFConfigBuilder mfconfig = new MFConfigBuilder().setConsoleLogon(true);
            mfconfig.findAndLoadFromConfigFile();
            mfconfig.loadFromSystemEnv();
            if (this.cfgFile != null) {
                mfconfig.loadFromConfigFile(cfgFile);
            }
            mfconfig.setApp(MFUpload.PROG);

            MFSession session = MFSession.create(mfconfig);

            Logger logger = createLogger(this.logDir, "mf-import-archive", this.quiet ? Level.WARNING : Level.ALL,
                    100000000, 2);

            new FileSetArchiveCreateTask(session, logger, Arrays.asList(this.srcFiles), this.assetPath,
                    this.compress ? 6 : 0, this.analyze).execute();

            return 0;
        } catch (Throwable e) {
            throw (e instanceof Exception) ? (Exception) e : new RuntimeException(e);
        }
    }

    private static Logger createLogger(Path logDir, String logFileName, Level logLevel, int logFileSize,
            int logFileCount) throws Throwable {
        Logger logger = logDir == null ? LoggingUtils.createConsoleLogger()
                : LoggingUtils.createFileAndConsoleLogger(logDir, logFileName, logFileSize, logFileCount);
        if (logLevel != null) {
            logger.setLevel(logLevel);
        } else {
            logger.setLevel(Level.WARNING);
        }
        return logger;
    }

    public static void main(String[] args) {
        System.exit(new CommandLine(new MFImportArchive()).execute(args));
    }

}