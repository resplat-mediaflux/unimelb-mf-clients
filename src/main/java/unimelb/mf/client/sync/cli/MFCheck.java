package unimelb.mf.client.sync.cli;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import unimelb.mf.client.session.MFConfig;
import unimelb.mf.client.session.MFConfigBuilder;
import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.sync.MFSyncApp;
import unimelb.mf.client.sync.check.DefaultCheckHandler;
import unimelb.mf.client.sync.settings.Action;
import unimelb.mf.client.sync.settings.Action.Direction;
import unimelb.mf.client.sync.settings.downstream.CollectionDownloadCheckJob;
import unimelb.mf.client.sync.settings.upstream.DirectoryUploadCheckJob;
import unimelb.mf.client.util.collection.CollectionPath;
import unimelb.utils.LoggingUtils;

public class MFCheck extends MFSyncApp {

    public static final String PROG = "unimelb-mf-check";

    private MFConfigBuilder _mfconfig;
    private final Map<Path, String> _dirCollections;
    private Action.Direction _direction;
    private Path _outputFile;
    private boolean _detailedOutput = false;
    private boolean _compressOutput = false;
    private DefaultCheckHandler _checkHandler;

    public MFCheck() {
        super();
        settings().setCsumCheck(true);
        settings().setVerbose(true);

        _dirCollections = new LinkedHashMap<>();

    }

    @Override
    protected void preExecute() {
        if (logger() == null) {
            Logger logger = LoggingUtils.createConsoleLogger();
            logger.setLevel(settings().verbose() ? Level.ALL : Level.WARNING);
            setLogger(logger);
        }
    }

    protected void printUsage() {
        // @formatter:off
        System.out.println();
        System.out.println("USAGE:");
        System.out.printf(
                "    %s [OPTIONS] --direction <up|down> --output <output.csv> <dir1> <collection1> [<dir2> <collection2>...]%n",
                PROG);
        System.out.println();
        System.out.println("DESCRIPTION:");
        System.out.println(
                "    Compare files in local directory with assets in remote Mediaflux collection and generates a list of the differences.");
        System.out.println();
        System.out.println("OPTIONS:");
        System.out.println(
                "    --mf.config <mflux.cfg>                   Path to the config file that contains Mediaflux server details and user credentials.");
        System.out.println("    --mf.host <host>                          Mediaflux server host.");
        System.out.println("    --mf.port <port>                          Mediaflux server port.");
        System.out.println(
                "    --mf.transport <https|http|tcp/ip>        Mediaflux server transport, can be http, https or tcp/ip.");

//        System.out.println("    --mf.auth <domain,user,password>          Mediaflux user credentials.");
//        System.out.println("    --mf.token <token>                        Mediaflux secure identity token.");

        System.out.println("    --direction <up|down>                     Direction: up/down.");
        System.out.println("    -o, --output <output.csv>                 Output CSV file.");
        System.out.println(
                "    --detailed-output                         Include all files checked. Otherwise, only the missing or invalid files are included in the output.");
        System.out.println(
                "    --compress-output                         Compress output CSV file to GZIP format(.csv.gz).");
        System.out.println(
                "    --no-csum-check                           Files are equated if the name, size and CRC32 checksum are the same. With this argument, you can exclude the CRC32 checksum comparison.");
        System.out.println("    --nb-queriers <n>                         Number of query threads. Defaults to "
                + unimelb.mf.client.sync.settings.Settings.DEFAULT_NUM_OF_QUERIERS + ". Maximum is "
                + Settings.MAX_NUM_OF_QUERIERS);
        System.out.println(
                "    --nb-workers <n>                          Number of concurrent worker threads to read local file (to generate checksum) if needed. Defaults to "
                        + unimelb.mf.client.sync.settings.Settings.DEFAULT_NUM_OF_WORKERS + ". Maximum is "
                        + Settings.MAX_NUM_OF_WORKERS);
        System.out.println("    --nb-retries <n>                          Retry times when error occurs. Defaults to "
                + unimelb.mf.client.sync.settings.Settings.DEFAULT_NB_RETRIES);
        System.out.println("    --batch-size <size>                       Size of the query result. Defaults to "
                + unimelb.mf.client.sync.settings.Settings.DEFAULT_BATCH_SIZE);
        System.out.println("    --follow-symlinks                         Follow symbolic links.");
        System.out.println("    --quiet                                   Do not print progress messages.");
        System.out.println("    --help                                    Prints usage.");
        System.out.println("    --version                                 Prints version.");
        System.out.println();
        System.out.println("POSITIONAL ARGUMENTS:");
        System.out.println("    <dir>                                     Local directory path.");
        System.out.println("    <collection>                               Remote Mediaflux collection path.");
        System.out.println();
        System.out.println("EXAMPLES:");
        System.out.printf(
                "    %s --mf.config ~/.Arcitecta/mflux.cfg --direction down --output ~/Documents/foo-download-check.csv ~/Documents/foo /projects/proj-1.2.3/foo%n",
                PROG);
        System.out.println();
        // @formatter:on
    }

    protected void parseArgs(String[] args) throws Throwable {
        if (_mfconfig == null) {
            _mfconfig = new MFConfigBuilder().setConsoleLogon(true);
            _mfconfig.findAndLoadFromConfigFile();
            _mfconfig.loadFromSystemEnv();
            if (_mfconfig.app() == null) {
                _mfconfig.setApp(applicationName());
            }
        }
        if (args != null) {
            try {
                for (int i = 0; i < args.length;) {
                    if ("--help".equalsIgnoreCase(args[i]) || "-h".equalsIgnoreCase(args[i])) {
                        printUsage();
                        System.exit(0);
                    }
                    if ("--version".equalsIgnoreCase(args[i])) {
                        printVersion();
                        System.exit(0);
                    }
                    if ("--follow-symlinks".equalsIgnoreCase(args[i])) {
                        settings().setFollowSymlinks(true);
                        i++;
                    }
                    int n = parseMFOptions(args, i);
                    if (n > 0) {
                        i += n;
                        continue;
                    }
                    n = parseCheckOptions(args, i);
                    if (n > 0) {
                        i += n;
                        continue;
                    }
                    Path dir = Paths.get(args[i]);
                    if (!Files.exists(dir)) {
                        throw new IllegalArgumentException("Directory: '" + dir.toString() + "' does not exist.");
                    }
                    if (!Files.isDirectory(dir)) {
                        throw new IllegalArgumentException("'" + dir.toString() + "' is not a directory.");
                    }
                    if (Files.isSymbolicLink(dir) && !settings().followSymlinks()) {
                        throw new IllegalArgumentException("'" + dir
                                + "' is a symbolic link to directory. To upload the linked directory, turn on --follow-symlinks.");
                    }
                    if (i + 1 >= args.length) {
                        throw new IllegalArgumentException("Missing remote collection path.");
                    }
                    String ns = args[i + 1];
                    if (ns == null || ns.isEmpty()) {
                        throw new IllegalArgumentException("Invalid collection path: " + ns);
                    }
                    if (!ns.startsWith("/")) {
                        System.err.println("Warning: prepending missing / to collection path: '" + ns + "'.");
                        ns = "/" + ns;
                    }
                    _dirCollections.put(dir, ns);
                    i += 2;
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                throw new IllegalArgumentException(e);
            }
        }

        if (_direction == null) {
            throw new IllegalArgumentException("Missing --direction argument.");
        }
        if (_outputFile == null) {
            throw new IllegalArgumentException("Missing --output argument.");
        }
        if (_dirCollections.isEmpty()) {
            throw new IllegalArgumentException("Missing local directory and remote collection arguments.");
        }

        /*
         * test MF authentication
         */
        MFSession session = MFSession.create(_mfconfig);

        /*
         * set MF session
         */
        setSession(session);

        /*
         * add jobs
         */
        Set<Path> dirs = _dirCollections.keySet();
        for (Path dir : dirs) {
            CollectionPath collectionPath = new CollectionPath(_dirCollections.get(dir));
            if (_direction == Direction.DOWN) {
                CollectionDownloadCheckJob cdcj = new CollectionDownloadCheckJob(collectionPath, dir, false);
                cdcj.srcCollection().resolve(session);
                if (!cdcj.srcCollection().exists(session)) {
                    throw new IllegalArgumentException("Remote collection: '" + collectionPath + "' does not exist.");
                }
                settings().addJob(cdcj);
            } else {
                DirectoryUploadCheckJob ducj = new DirectoryUploadCheckJob(dir, null, null, collectionPath, false);
                ducj.dstCollection().resolve(session);
                if (!ducj.dstCollection().exists(session)) {
                    throw new IllegalArgumentException("Remote collection: '" + collectionPath + "' does not exist.");
                }
                settings().addJob(ducj);
            }
        }

        _checkHandler = DefaultCheckHandler.create(_outputFile, _detailedOutput, _compressOutput);
        settings().setCheckHandler(_checkHandler);
    }

    protected int parseMFOptions(String[] args, int i) {
        if ("--mf.config".equalsIgnoreCase(args[i])) {
            try {
                _mfconfig.loadFromConfigFile(args[i + 1]);
            } catch (Throwable e) {
                throw new IllegalArgumentException("Invalid --mf.config: " + args[i + 1], e);
            }
            return 2;
        } else if ("--mf.host".equalsIgnoreCase(args[i])) {
            _mfconfig.setHost(args[i + 1]);
            return 2;
        } else if ("--mf.port".equalsIgnoreCase(args[i])) {
            _mfconfig.setPort(Integer.parseInt(args[i + 1]));
            return 2;
        } else if ("--mf.transport".equalsIgnoreCase(args[i])) {
            _mfconfig.setTransport(args[i + 1]);
            return 2;
        } else if ("--mf.auth".equalsIgnoreCase(args[i])) {
            throw new IllegalArgumentException(
                    "--mf.auth argument is no longer supported. Please specify user credentials in file: "
                            + MFConfig.DEFAULT_MFLUX_CFG_FILE
                            + " or the config file specified with --mf.config argument.");
            // @formatter:off
//            String auth = args[i + 1];
//            String[] parts = auth.split(",");
//            if (parts.length != 3) {
//                throw new IllegalArgumentException("Invalid mf.auth: " + auth);
//            }
//            _mfconfig.setUserCredentials(parts[0], parts[1], parts[2]);
//            return 2;
            // @formatter:on
        } else if ("--mf.token".equalsIgnoreCase(args[i])) {
            throw new IllegalArgumentException(
                    "--mf.token argument is no longer supported. Please specify the token in file: "
                            + MFConfig.DEFAULT_MFLUX_CFG_FILE
                            + " or the config file specified with --mf.config argument.");
            // @formatter:off
//            _mfconfig.setToken(args[i + 1]);
//            return 2;
            // @formatter:on
        } else {
            return 0;
        }
    }

    protected int parseCheckOptions(String[] args, int i) throws Throwable {
        if ("--direction".equalsIgnoreCase(args[i])) {
            _direction = Action.Direction.fromString(args[i + 1]);
            if (_direction == null) {
                throw new IllegalArgumentException("Invalid --direction " + args[i + 1]);
            }
            return 2;
        } else if ("--output".equalsIgnoreCase(args[i]) || "-o".equalsIgnoreCase(args[i])) {
            _outputFile = Paths.get(args[i + 1]).toAbsolutePath();
            Path dir = _outputFile.getParent();
            if (dir != null) {
                if (!Files.exists(dir)) {
                    Files.createDirectories(dir);
                }
            }
            _outputFile = renameOutputCsvFile(_outputFile);
            return 2;
        } else if ("--detailed-output".equalsIgnoreCase(args[i])) {
            _detailedOutput = true;
            return 1;
        } else if ("--compress-output".equalsIgnoreCase(args[i])) {
            _compressOutput = true;
            return 1;
        } else if ("--no-csum-check".equalsIgnoreCase(args[i])) {
            settings().setCsumCheck(false);
            return 1;
        } else if ("--batch-size".equalsIgnoreCase(args[i])) {
            try {
                int batchSize = Integer.parseInt(args[i + 1]);
                if (batchSize <= 0) {
                    throw new IllegalArgumentException("Invalid --batch-size " + args[i + 1]
                            + " Expects a positive integer, found " + args[i + 1]);
                }
                settings().setBatchSize(batchSize);
                return 2;
            } catch (NumberFormatException nfe) {
                throw new IllegalArgumentException(
                        "Invalid --batch-size " + args[i + 1] + " Expects a positive integer, found " + args[i + 1],
                        nfe);
            }
        } else if ("--nb-queriers".equalsIgnoreCase(args[i])) {
            try {
                int nbQueriers = Integer.parseInt(args[i + 1]);
                if (nbQueriers <= 0 || nbQueriers > Settings.MAX_NUM_OF_QUERIERS) {
                    throw new IllegalArgumentException("Invalid --nb-queriers: " + args[i + 1]
                            + ". Expects an integer between 1 and " + Settings.MAX_NUM_OF_QUERIERS);
                }
                settings().setNumberOfQueriers(nbQueriers);
                return 2;
            } catch (NumberFormatException nfe) {
                throw new IllegalArgumentException(
                        "Invalid --nb-queriers " + args[i + 1] + " Expects a positive integer, found " + args[i + 1],
                        nfe);
            }
        } else if ("--nb-workers".equalsIgnoreCase(args[i])) {
            try {
                int nbWorkers = Integer.parseInt(args[i + 1]);
                if (nbWorkers <= 0 || nbWorkers > Settings.MAX_NUM_OF_WORKERS) {
                    throw new IllegalArgumentException("Invalid --nb-workers: " + args[i + 1]
                            + ". Expects an integer between 1 and " + Settings.MAX_NUM_OF_WORKERS);
                }
                settings().setNumberOfWorkers(nbWorkers);
                return 2;
            } catch (NumberFormatException nfe) {
                throw new IllegalArgumentException(
                        "Invalid --nb-workers " + args[i + 1] + " Expects a positive integer, found " + args[i + 1],
                        nfe);
            }
        } else if ("--nb-retries".equalsIgnoreCase(args[i])) {
            try {
                int nbRetries = Integer.parseInt(args[i + 1]);
                settings().setMaxNumberOfRetries(nbRetries);
                return 2;
            } catch (NumberFormatException nfe) {
                throw new IllegalArgumentException(
                        "Invalid --nb-retries " + args[i + 1] + " Expects a positive integer, found " + args[i + 1],
                        nfe);
            }
        } else if ("--quiet".equalsIgnoreCase(args[i])) {
            settings().setVerbose(false);
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    protected void postExecute() {
        super.postExecute();
        try {
            _checkHandler.writeSummary();
            _checkHandler.printSummary();
            if (!_checkHandler.allChecked()) {
                StringBuilder sb = new StringBuilder("WARNING: checked ");
                if (_checkHandler.numberOfCheckedAssets() > 0) {
                    sb.append(_checkHandler.numberOfCheckedAssets()).append(" assets");
                } else {
                    sb.append(_checkHandler.numberOfCheckedFiles()).append(" files");
                }
                if (_checkHandler.total() > 0) {
                    sb.append(" out of total of ").append(_checkHandler.total()).append(". ");
                }
                sb.append("Please check for errors in the log file.");
                System.err.println(sb.toString());
            }
        } finally {
            try {
                _checkHandler.close();
            } catch (Throwable e) {
                logger().log(Level.SEVERE, e.getMessage(), e);
            }
        }
    }

    private static Path renameOutputCsvFile(Path file) throws Throwable {
        Path dirPath = file.toAbsolutePath().getParent();
        if (dirPath == null) {
            throw new Exception("Null parent directory for file: " + file);
        }
        String dir = dirPath.toString();
        Path fileName = file.getFileName();
        if (fileName == null) {
            throw new Exception("Null file name for file: " + file);
        }
        String name = fileName.toString();
        if (!name.toLowerCase().endsWith(".csv")) {
            return renameOutputCsvFile(Paths.get(dir, name + ".csv"));
        }
        String timestamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        StringBuilder sb = new StringBuilder();
        if (Files.exists(file)) {
            sb.append(name, 0, name.length() - 4);
            sb.append("-").append(timestamp);
            sb.append(".csv");
            return Paths.get(dir, sb.toString());
        } else {
            return file;
        }
    }

    void closeHandler() throws Throwable {
        if (_checkHandler != null) {
            _checkHandler.close();
        }
    }

    public static void main(String[] args) throws Throwable {
        MFCheck app = new MFCheck();
        try {
            app.parseArgs(args);
            app.execute();
        } catch (IllegalArgumentException iae) {
            System.err.println();
            System.err.println("Invalid arguments: " + iae.getMessage());
            System.err.println();
            app.printUsage();
            System.exit(1);
        } finally {
            app.closeHandler();
        }
    }

    @Override
    public final String applicationName() {
        return PROG;
    }

}
