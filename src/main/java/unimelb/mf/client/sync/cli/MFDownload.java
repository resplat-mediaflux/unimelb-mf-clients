package unimelb.mf.client.sync.cli;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedHashSet;
import java.util.Set;

import unimelb.mf.client.session.MFConfig;
import unimelb.mf.client.session.MFConfigBuilder;
import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.sync.MFSyncApp;
import unimelb.mf.client.sync.settings.downstream.AssetSetDownloadJob;
import unimelb.mf.client.sync.settings.downstream.CollectionDownloadJob;
import unimelb.mf.client.sync.task.AssetDownloadTask.Unarchive;
import unimelb.mf.client.util.AssetUtils;
import unimelb.mf.client.util.collection.CollectionPath;

public class MFDownload extends MFSyncApp {

    public static final String PROG = "unimelb-mf-download";

    public MFDownload() {
        super();
        settings().setOverwrite(false);
        settings().setUnarchive(Unarchive.NONE);
        settings().setCsumCheck(false);
        settings().setVerbose(true);
        settings().setDaemon(false);
    }

    @Override
    protected void preExecute() throws Throwable {
        if (logger() == null) {
            setLogger(createDefaultLogger(settings(), applicationName()));
        }
    }

    protected void printUsage() {
        // @formatter:off
        System.out.println();
        System.out.println("USAGE:");
        System.out.printf("    %s [OPTIONS] --out <dst-dir> <src-asset-or-collection-path> [<src-asset-or-collection-path>...]%n", PROG);
        System.out.printf("    %s --config <mf-download-config.xml>%n", PROG);
        System.out.println();
        System.out.println("DESCRIPTION:");
        System.out.println("    Download assets (files)  from Mediaflux to the local file system.  Pre-existing files in the local file system can be skipped or overwritten. In Daemon mode, the process will only download new assets (files)  since the process last executed.");
        System.out.println();
        System.out.println("OPTIONS:");
        System.out.println("    --config <config.xml>                     A single configuration file including all required settings (Mediaflux server details, user credentials, application settings). If conflicts with all the other options.");
        System.out.println();
        System.out.println("    --mf.config <mflux.cfg>                   Path to the config file that contains Mediaflux server details and user credentials.");
        System.out.println("    --mf.host <host>                          Mediaflux server host.");
        System.out.println("    --mf.port <port>                          Mediaflux server port.");
        System.out.println("    --mf.transport <https|http|tcp/ip>        Mediaflux server transport, can be http, https or tcp/ip.");

//        System.out.println("    --mf.auth <domain,user,password>          Mediaflux user credentials.");
//        System.out.println("    --mf.token <token>                        Mediaflux secure identity token.");

        System.out.println("    --no-cluster-io                           Disable cluster I/O if applicable.");

        System.out.println("    -o, --out <dst-dir>                       The output/destination directory.");
        System.out.println("    --overwrite                               Overwrite if the destination file exists but has a different size.");
        System.out.println("    --unarchive                               Extract Arcitecta .aar files.");
        System.out.println("    --csum-check                              Generate the CRC32 checksum during the file download and compare it with the remote checksum after the download is complete. If both the --csum-check and --overwrite options are enabled, local files will be overwritten even if their sizes match the remote files.");
        System.out.println("    --no-symlinks                             Do not restore symbolic links. If not specified, it will try to create (restore) symbolic links. Note: creating symbolic links works only the platforms that support symbolic links, such as Linux or MacOS.");
        System.out.println("    --nb-queriers <n>                         Number of query threads. Defaults to " + unimelb.mf.client.sync.settings.Settings.DEFAULT_NUM_OF_QUERIERS + ". Maximum is " + Settings.MAX_NUM_OF_QUERIERS);
        System.out.println("    --nb-workers <n>                          Number of concurrent worker threads to download data. Defaults to " + unimelb.mf.client.sync.settings.Settings.DEFAULT_NUM_OF_WORKERS + ". Maximum is " + Settings.MAX_NUM_OF_WORKERS);
        System.out.println("    --nb-retries <n>                          Retry times when error occurs. Defaults to " + unimelb.mf.client.sync.settings.Settings.DEFAULT_NB_RETRIES);
        System.out.println("    --batch-size <size>                       Size of the query result. Defaults to " + unimelb.mf.client.sync.settings.Settings.DEFAULT_BATCH_SIZE);
        System.out.println("    --daemon                                  Run as a daemon.");
        System.out.println("    --daemon-port <port>                      Daemon listener port if running as a daemon. Defaults to " + unimelb.mf.client.sync.settings.Settings.DEFAULT_DAEMON_LISTENER_PORT);
        System.out.println("    --daemon-scan-interval <seconds>          Time interval (in seconds) between scans of source collection. Defaults to " + (unimelb.mf.client.sync.settings.Settings.DEFAULT_DAEMON_SCAN_INTERVAL / 1000) + " seconds.");
        System.out.println("    --exclude-parent                          Exclude parent directory at the destination (Download the contents of the directory) if the source path ends with trailing slash.");
        System.out.println("    --include-metadata                        Download Mediaflux asset metadata as XML file (*.meta.xml)");
        System.out.println("    --aggregate-transmission                  Aggregate transmission to improve the performance for large number of small files.");
        System.out.println("    --log-dir <dir>                           Path to the directory for log files. No logging if not specified.");
        System.out.println("    --log-file-size-mb <n>                    Log file size limit in MB. Defaults to " + unimelb.mf.client.sync.settings.Settings.DEFAULT_LOG_FILE_SIZE_MB + "MB");
        System.out.println("    --log-file-count <n>                      Log file count. Defaults to " + unimelb.mf.client.sync.settings.Settings.DEFAULT_LOG_FILE_COUNT);
        System.out.println("    --notify <email-addresses>                When completes, send email notification to the recipients(comma-separated email addresses if multiple). Not applicable for daemon mode.");
        System.out.println("    --sync-delete-files                       Delete local files that do not have corresponding assets exist on the server side.");
        System.out.println("    --quiet                                   Do not print progress messages.");
        System.out.println("    --help                                    Prints usage.");
        System.out.println("    --version                                 Prints version.");
        System.out.println();
        System.out.println("POSITIONAL ARGUMENTS:");
        System.out.println("    <src-asset-or-collection-path>            The source asset path or asset collection/namespace path.");
        System.out.println();
        System.out.println("EXAMPLES:");
        System.out.printf("    %s --mf.config ~/.Arcitecta/mflux.cfg --nb-workers 2  --out ~/Downloads /projects/proj-test-1128.1.59/foo /projects/proj-test-1128.1.59/bar%n", PROG);
        System.out.printf("    %s --mf.config ~/.Arcitecta/mflux.cfg --out ~/Downloads /projects/proj-test-1128.1.15/sample.zip%n", PROG);
        System.out.printf("    %s --config ~/.Arcitecta/mf-download-config.xml%n", PROG);
        System.out.println();
        // @formatter:on
    }

    protected void parseArgs(String[] args) throws Throwable {
        // must have args
        if (args == null || args.length == 0) {
            throw new IllegalArgumentException("Missing arguments.");
        }

        if (args.length == 1 && "--config".equalsIgnoreCase(args[0])) {
            throw new IllegalArgumentException("Missing --config file.");
        }

        for (String arg : args) {
            if ("--help".equalsIgnoreCase(arg) || "-h".equalsIgnoreCase(arg)) {
                printUsage();
                System.exit(args.length > 1 ? 1 : 0);
            }
            if ("--version".equalsIgnoreCase(arg)) {
                printVersion();
                System.exit(args.length > 1 ? 1 : 0);
            }
        }

        if (args.length == 2 && "--config".equalsIgnoreCase(args[0])) {

            // settings from single configuration file (.xml)
            Path configPath = (args[1].indexOf('/') < 0 && args[1].indexOf(File.separatorChar) < 0)
                    ? Paths.get(System.getProperty("user.dir"), args[1])
                    : Paths.get(args[1]);
            if (!Files.isRegularFile(configPath)) {
                throw new IllegalArgumentException(
                        "--config file: " + args[1] + " does not exist or is not a regular file.");
            }
            MFConfigBuilder mfconfig = new MFConfigBuilder().setConsoleLogon(true);
            mfconfig.loadFromXmlFile(configPath.toFile());
            if (mfconfig.app() == null) {
                mfconfig.setApp(applicationName());
            }

            MFSession session = MFSession.create(mfconfig);

            setSession(session);

            settings().loadFromXmlFile(configPath, session);

        } else {

            MFConfigBuilder mfconfig = new MFConfigBuilder().setConsoleLogon(true);
            mfconfig.findAndLoadFromConfigFile();
            mfconfig.loadFromSystemEnv();
            if (mfconfig.app() == null) {
                mfconfig.setApp(applicationName());
            }

            Set<String> srcPaths = new LinkedHashSet<>();
            Path rootDir = null;
            try {
                for (int i = 0; i < args.length;) {
                    if ("--config".equalsIgnoreCase(args[i])) {
                        throw new IllegalArgumentException("Unexpected arguments conflict with --config argument.");
                    } else if ("--out".equalsIgnoreCase(args[i]) || "-o".equalsIgnoreCase(args[i])) {
                        if (rootDir != null) {
                            throw new IllegalArgumentException("Expects only one --out directory. Found multiple.");
                        }
                        if (args[i + 1].indexOf('/') < 0 && args[i + 1].indexOf(File.separatorChar) < 0) {
                            rootDir = Paths.get(System.getProperty("user.dir"), args[i + 1]).toAbsolutePath();
                        } else {
                            rootDir = Paths.get(args[i + 1]).toAbsolutePath();
                        }
                        if (!Files.isDirectory(rootDir)) {
                            throw new IllegalArgumentException(
                                    "Directory: '" + args[i + 1] + "' does not exist or it is not a directory.");
                        }
                        i += 2;
                    }
                    int n = parseMFOptions(args, i, mfconfig);
                    if (n > 0) {
                        i += n;
                        continue;
                    }
                    n = parseDownloadOptions(args, i);
                    if (n > 0) {
                        i += n;
                        continue;
                    }
                    String srcPath = args[i];
                    if (srcPath != null && !srcPath.isEmpty()) {
                        // prepend slash
                        if (!srcPath.startsWith("/")) {
                            System.err.println(
                                    "Warning: prepending missing / to asset/collection path: '" + srcPath + "'.");
                            srcPath = "/" + srcPath;
                        }
                        srcPaths.add(srcPath);
                    }
                    i++;
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                throw new IllegalArgumentException(e);
            }

            if (rootDir == null) {
                throw new IllegalArgumentException("Missing output directory: --out argument.");
            }
            if (srcPaths.isEmpty()) {
                throw new IllegalArgumentException("Missing Mediaflux asset/collection paths.");
            }

            /*
             * test MF authentication
             */
            MFSession session = MFSession.create(mfconfig);

            /*
             * set MF session
             */
            setSession(session);

            /*
             * add jobs
             */
            Set<String> assets = new LinkedHashSet<>();
            for (String srcPath : srcPaths) {
                boolean trailingSlash = srcPath.length() > 1 && srcPath.endsWith("/");
                if (trailingSlash) {
                    // remove trailing slash
                    srcPath = srcPath.replaceAll("/+$", "");
                }

                // assume srcPath is a collection/namespace, try resolve its details
                CollectionPath srcCollection = new CollectionPath(srcPath);
                srcCollection.resolve(session);

                if (srcCollection.exists(session)) {
                    if (trailingSlash && settings().excludeParent()) {
                        settings().addJob(new CollectionDownloadJob(srcCollection, rootDir, false));
                    } else {
                        settings().addJob(new CollectionDownloadJob(srcCollection, rootDir, true));
                    }
                } else if (AssetUtils.assetExists(session, "path=" + srcPath)) {
                    assets.add(srcPath);
                } else {
                    throw new IllegalArgumentException("Asset or collection: '" + srcPath + "' does not exist.");
                }
            }
            if (!assets.isEmpty()) {
                settings().addJob(new AssetSetDownloadJob(assets, rootDir));
            }
        }
    }

    protected int parseMFOptions(String[] args, int i, MFConfigBuilder mfconfig) {
        if ("--mf.config".equalsIgnoreCase(args[i])) {
            try {
                mfconfig.loadFromConfigFile(args[i + 1]);
            } catch (Throwable e) {
                throw new IllegalArgumentException("Invalid --mf.config: " + args[i + 1], e);
            }
            return 2;
        } else if ("--mf.host".equalsIgnoreCase(args[i])) {
            mfconfig.setHost(args[i + 1]);
            return 2;
        } else if ("--mf.port".equalsIgnoreCase(args[i])) {
            mfconfig.setPort(Integer.parseInt(args[i + 1]));
            return 2;
        } else if ("--mf.transport".equalsIgnoreCase(args[i])) {
            mfconfig.setTransport(args[i + 1]);
            return 2;
        } else if ("--mf.auth".equalsIgnoreCase(args[i])) {
            throw new IllegalArgumentException(
                    "--mf.auth argument is no longer supported. Please specify user credentials in file: "
                            + MFConfig.DEFAULT_MFLUX_CFG_FILE
                            + " or the config file specified with --mf.config argument.");
            // @formatter:off
//            String auth = args[i + 1];
//            String[] parts = auth.split(",");
//            if (parts.length != 3) {
//                throw new IllegalArgumentException("Invalid mf.auth: " + auth);
//            }
//            mfconfig.setUserCredentials(parts[0], parts[1], parts[2]);
//            return 2;
            // @formatter:on
        } else if ("--mf.token".equalsIgnoreCase(args[i])) {
            throw new IllegalArgumentException(
                    "--mf.token argument is no longer supported. Please specify the token in file: "
                            + MFConfig.DEFAULT_MFLUX_CFG_FILE
                            + " or the config file specified with --mf.config argument.");
            // @formatter:off
//            mfconfig.setToken(args[i + 1]);
//            return 2;
            // @formatter:on
        } else if ("--no-cluster-io".equalsIgnoreCase(args[i])) {
            mfconfig.setUseClusterIO(false);
            return 1;
        } else {
            return 0;
        }
    }

    protected int parseDownloadOptions(String[] args, int i) {
        if ("--overwrite".equalsIgnoreCase(args[i])) {
            settings().setOverwrite(true);
            return 1;
        } else if ("--unarchive".equalsIgnoreCase(args[i])) {
            settings().setUnarchive(Unarchive.ALL);
            return 1;
        } else if ("--csum-check".equalsIgnoreCase(args[i])) {
            settings().setCsumCheck(true);
            return 1;
        } else if ("--no-symlinks".equalsIgnoreCase(args[i])) {
            settings().setRestoreSymlinks(false);
            return 1;
        } else if ("--batch-size".equalsIgnoreCase(args[i])) {
            try {
                int batchSize = Integer.parseInt(args[i + 1]);
                if (batchSize <= 0) {
                    throw new IllegalArgumentException("Invalid --batch-size " + args[i + 1]
                            + " Expects a positive integer, found " + args[i + 1]);
                }
                settings().setBatchSize(batchSize);
                return 2;
            } catch (NumberFormatException nfe) {
                throw new IllegalArgumentException(
                        "Invalid --batch-size " + args[i + 1] + " Expects a positive integer, found " + args[i + 1],
                        nfe);
            }
        } else if ("--nb-queriers".equalsIgnoreCase(args[i])) {
            try {
                int nbQueriers = Integer.parseInt(args[i + 1]);
                if (nbQueriers <= 0 || nbQueriers > Settings.MAX_NUM_OF_QUERIERS) {
                    throw new IllegalArgumentException("Invalid --nb-queriers: " + args[i + 1]
                            + ". Expects an integer between 1 and " + Settings.MAX_NUM_OF_QUERIERS);
                }
                settings().setNumberOfQueriers(nbQueriers);
                return 2;
            } catch (NumberFormatException nfe) {
                throw new IllegalArgumentException(
                        "Invalid --nb-queriers " + args[i + 1] + " Expects a positive integer, found " + args[i + 1],
                        nfe);
            }
        } else if ("--nb-workers".equalsIgnoreCase(args[i])) {
            try {
                int nbWorkers = Integer.parseInt(args[i + 1]);
                if (nbWorkers <= 0 || nbWorkers > Settings.MAX_NUM_OF_WORKERS) {
                    throw new IllegalArgumentException("Invalid --nb-workers: " + args[i + 1]
                            + ". Expects an integer between 1 and " + Settings.MAX_NUM_OF_WORKERS);
                }
                settings().setNumberOfWorkers(nbWorkers);
                return 2;
            } catch (NumberFormatException nfe) {
                throw new IllegalArgumentException(
                        "Invalid --nb-workers " + args[i + 1] + " Expects a positive integer, found " + args[i + 1],
                        nfe);
            }
        } else if ("--nb-retries".equalsIgnoreCase(args[i])) {
            try {
                int nbRetries = Integer.parseInt(args[i + 1]);
                settings().setMaxNumberOfRetries(nbRetries);
                return 2;
            } catch (NumberFormatException nfe) {
                throw new IllegalArgumentException(
                        "Invalid --nb-retries " + args[i + 1] + " Expects a positive integer, found " + args[i + 1],
                        nfe);
            }
        } else if ("--daemon".equalsIgnoreCase(args[i])) {
            settings().setDaemon(true);
            return 1;
        } else if ("--daemon-port".equalsIgnoreCase(args[i])) {
            try {
                int port = Integer.parseInt(args[i + 1]);
                if (port < 0 || port > 65535) {
                    throw new IllegalArgumentException("Invalid --daemon-port " + args[i + 1]
                            + " Expects a positive integer, found " + args[i + 1]);
                }
                settings().setDaemonListenerPort(port);
                return 2;
            } catch (NumberFormatException nfe) {
                throw new IllegalArgumentException("Invalid --daemon-port " + args[i + 1]
                        + " Expects a positive integer in the range of [0, 65535], found " + args[i + 1], nfe);
            }
        } else if ("--exclude-parent".equalsIgnoreCase(args[i])) {
            settings().setExcludeParent(true);
            return 1;
        } else if ("--include-metadata".equalsIgnoreCase(args[i])) {
            settings().setIncludeMetadata(true);
            return 1;
        } else if ("--aggregate-transmission".equalsIgnoreCase(args[i])) {
            settings().enableAggregateDownload();
            return 1;
        } else if ("--daemon-scan-interval".equalsIgnoreCase(args[i])) {
            try {
                int intervalSeconds = Integer.parseInt(args[i + 1]);
                if (intervalSeconds <= 0) {
                    throw new IllegalArgumentException("Invalid --daemon-scan-interval " + args[i + 1]
                            + " Expects a positive integer, found " + args[i + 1]);
                }
                settings().setDaemonScanInterval(((long) intervalSeconds) * 1000L);
                return 2;
            } catch (NumberFormatException nfe) {
                throw new IllegalArgumentException("Invalid --daemon-scan-interval " + args[i + 1]
                        + " Expects a positive integer, found " + args[i + 1], nfe);
            }
        } else if ("--log-dir".equalsIgnoreCase(args[i])) {
            Path logDir = Paths.get(args[i + 1]).toAbsolutePath();
            if (!Files.exists(logDir)) {
                throw new IllegalArgumentException("Logging directory: " + args[i + 1] + " does not exist.");
            }
            if (!Files.isDirectory(logDir)) {
                throw new IllegalArgumentException(args[i + 1] + " is not a directory.");
            }
            settings().setLogDirectory(logDir);
            return 2;
        } else if ("--log-file-size-mb".equalsIgnoreCase(args[i])) {
            String size = args[i + 1];
            if (size.toLowerCase().endsWith("m")) {
                size = size.substring(0, size.length() - 1);
            }
            if (size.toLowerCase().endsWith("mb")) {
                size = size.substring(0, size.length() - 2);
            }
            int fileSizeMB = Integer.parseInt(size);
            settings().setLogFileSizeMB(fileSizeMB);
            return 2;
        } else if ("--log-file-count".equalsIgnoreCase(args[i])) {
            int fileCount = Integer.parseInt(args[i + 1]);
            settings().setLogFileCount(fileCount);
            return 2;
        } else if ("--notify".equalsIgnoreCase(args[i])) {
            String[] emails = args[i + 1].indexOf(',') != -1 ? args[i + 1].split(",") : new String[] { args[i + 1] };
            settings().addRecipients(emails);
            return 2;
        } else if ("--sync-delete-files".equalsIgnoreCase(args[i])) {
            settings().setDeleteFiles(true);
            return 1;
        } else if ("--quiet".equalsIgnoreCase(args[i])) {
            settings().setVerbose(false);
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public final String applicationName() {
        return PROG;
    }

    public static void main(String[] args) throws Throwable {
        MFDownload app = new MFDownload();
        try {
            app.parseArgs(args);
        } catch (IllegalArgumentException iae) {
            System.err.println();
            System.err.println("Invalid arguments: " + iae.getMessage());
            System.err.println();
            app.printUsage();
            System.exit(1);
        }
        if (app.settings().daemon()) {
            Runtime.getRuntime().addShutdownHook(new Thread(app::interrupt));
            new Thread(app).start();
        } else {
            app.execute();
        }
    }

}
