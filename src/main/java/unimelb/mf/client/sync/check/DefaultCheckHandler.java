package unimelb.mf.client.sync.check;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.atomic.AtomicLong;
import java.util.zip.GZIPOutputStream;

import unimelb.utils.PathUtils;

public class DefaultCheckHandler implements CheckHandler {

    private final PrintStream _outputPS;

    private final AtomicLong _total = new AtomicLong(0);

    private final AtomicLong _nbFileChecked = new AtomicLong(0);
    private final AtomicLong _nbFilePassed = new AtomicLong(0);
    private final AtomicLong _nbFileFailed = new AtomicLong(0);
    private final AtomicLong _nbFileMissing = new AtomicLong(0);
    private final AtomicLong _nbFileContentMismatch = new AtomicLong(0);

    private final AtomicLong _nbAssetChecked = new AtomicLong(0);
    private final AtomicLong _nbAssetPassed = new AtomicLong(0);
    private final AtomicLong _nbAssetFailed = new AtomicLong(0);
    private final AtomicLong _nbAssetMissing = new AtomicLong(0);
    private final AtomicLong _nbAssetContentMismatch = new AtomicLong(0);

    private final Path _outputFile;

    private final boolean _detailedOutput;

    public static DefaultCheckHandler create(Path outputFile, boolean detailedOutput,
                                             boolean compressOutput) throws Throwable {
        if (outputFile == null) {
            throw new IllegalArgumentException("Output file is null.", new NullPointerException());
        }
        Path outputDir = outputFile.getParent();
        if (outputDir == null) {
            throw new Exception("Failed to resolve parent directory of " + outputFile);
        }
        Path fileName = outputFile.getFileName();
        if (fileName == null) {
            throw new Exception("Unable to resolve file name: " + outputFile);
        }
        return new DefaultCheckHandler(outputDir, fileName.toString(), detailedOutput, compressOutput);
    }

    public DefaultCheckHandler(Path outputDir, String fileName, boolean detailedOutput,
                               boolean compressOutput) throws Throwable {
        _detailedOutput = detailedOutput;
        if (!Files.exists(outputDir)) {
            throw new IllegalArgumentException("Directory: '" + outputDir.toString() + "' does not exist.",
                    new FileNotFoundException(outputDir.toString()));
        }
        if (!Files.isDirectory(outputDir)) {
            throw new IllegalArgumentException("'" + outputDir.toString() + "' is not a directory.");
        }
        if (fileName == null) {
            throw new IllegalArgumentException("Output CSV file name is null.",
                    new NullPointerException("Output CSV file name is null."));
        }
        if (fileName.toLowerCase().endsWith(".csv.gz")) {
            compressOutput = true;
        } else if (fileName.toLowerCase().endsWith(".csv")) {
            if (compressOutput) {
                fileName += ".gz";
            }
        } else {
            fileName += compressOutput ? ".csv.gz" : ".csv";
        }
        try {
            // test file name...
            Paths.get(PathUtils.toSystemIndependent(outputDir.toString()), fileName).toFile().getCanonicalPath();
        } catch (IOException ioe) {
            throw new IllegalArgumentException("Invalid file name: " + fileName, ioe);
        }

        _outputFile = Paths.get(outputDir.toString(), fileName);
        _outputPS =
                new PrintStream(compressOutput ?
                        new GZIPOutputStream(new BufferedOutputStream(Files.newOutputStream(_outputFile))) :
                        new BufferedOutputStream(Files.newOutputStream(_outputFile)));
        // Header
        _outputPS.println(
                "SRC_PATH,SRC_LENGTH,SRC_CHECKSUM,DST_PATH,DST_LENGTH,DST_CHECKSUM,DST_EXISTS?,MATCH?");
    }

    public void writeSummary() {
        _outputPS.println(",,,,,,,");
        _outputPS.println(",,,,,,,");
        long nbCheckedFiles = numberOfCheckedFiles();
        if (nbCheckedFiles > 0) {
            _outputPS.printf(",,Number of checked files (total):,%d,,,,%n", nbCheckedFiles);
            _outputPS.printf(",,Number of files (passed):,%d,,,,%n", numberOfPassedFiles());
            _outputPS.printf(",,Number of files (missing):,%d,,,,%n", numberOfMissingFiles());
            _outputPS.printf(",,Number of files (content mismatch):,%d,,,,%n", numberOfContentMismatchFiles());
            _outputPS.println(",,,,,,,");
        }
        long nbCheckedAssets = numberOfCheckedAssets();
        if (nbCheckedAssets > 0) {
            _outputPS.printf(",,Number of checked assets (total):,%d,,,,%n", nbCheckedAssets);
            _outputPS.printf(",,Number of assets (passed):,%d,,,,%n", numberOfPassedAssets());
            _outputPS.printf(",,Number of assets (missing):,%d,,,,%n", numberOfMissingAssets());
            _outputPS.printf(",,Number of assets (content mismatch):,%d,,,,%n", numberOfContentMismatchAssets());
            _outputPS.println(",,,,,,,");
        }
    }

    public void printSummary() {
        System.out.println();
        long nbCheckedFiles = numberOfCheckedFiles();
        long nbCheckedAssets = numberOfCheckedAssets();
        long total = total();

        if (nbCheckedFiles > 0) {
            System.out.printf("    %,13d files [passed]%n", numberOfPassedFiles());
            System.out.printf("    %,13d files [missing]%n", numberOfMissingFiles());
            System.out.printf("    %,13d files [content mismatch]%n", numberOfContentMismatchFiles());
            System.out.printf("    %,13d files [checked]%n", nbCheckedFiles);
            if (total > 0 && total > nbCheckedFiles + nbCheckedAssets) {
                System.out.printf("    %,13d files [total]%n", total);
            }
            System.out.println();
        }

        if (nbCheckedAssets > 0) {
            System.out.printf("    %,13d assets [passed]%n", numberOfPassedAssets());
            System.out.printf("    %,13d assets [missing]%n", numberOfMissingAssets());
            System.out.printf("    %,13d assets [content mismatch]%n", numberOfContentMismatchAssets());
            System.out.printf("    %,13d assets [checked]%n", nbCheckedAssets);
            if (total > 0 && total > nbCheckedFiles + nbCheckedAssets) {
                System.out.printf("    %,13d assets [total]%n", total);
            }
            System.out.println();
        }
        System.out.printf("    Output file: %s%n", _outputFile.toString());
    }

    @Override
    public void checked(CheckResult result) {
        if (Item.Type.FILE.equals(result.srcType())) {
            _nbFileChecked.getAndIncrement();
            if (result.match()) {
                _nbFilePassed.getAndIncrement();
                if (_detailedOutput) {
                    result.println(_outputPS);
                }
            } else {
                _nbFileFailed.getAndIncrement();
                if (!result.exists()) {
                    _nbFileMissing.getAndIncrement();
                    System.out.printf("Missing: %s:%s (source %s:%s)%n", result.dstType().toString().toLowerCase(),
                            result.dstPath(), result.srcType().toString().toLowerCase(),
                            result.srcPath());
                } else {
                    _nbFileContentMismatch.getAndIncrement();
                    System.out.printf("Content mismatch: %s:%s (source %s:%s)%n",
                            result.dstType().toString().toLowerCase(),
                            result.dstPath(), result.srcType().toString().toLowerCase(),
                            result.srcPath());
                }
                result.println(_outputPS);
            }
        } else {
            _nbAssetChecked.getAndIncrement();
            if (result.match()) {
                _nbAssetPassed.getAndIncrement();
                if (_detailedOutput) {
                    result.println(_outputPS);
                }
            } else {
                _nbAssetFailed.getAndIncrement();
                if (!result.exists()) {
                    _nbAssetMissing.getAndIncrement();
                    System.out.printf("Missing: %s:%s (source %s:%s)%n", result.dstType().toString().toLowerCase(),
                            result.dstPath(), result.srcType().toString().toLowerCase(),
                            result.srcPath());
                } else {
                    _nbAssetContentMismatch.getAndIncrement();
                    System.out.printf("Content mismatch: %s:%s (source %s:%s)%n",
                            result.dstType().toString().toLowerCase(),
                            result.dstPath(), result.srcType().toString().toLowerCase(),
                            result.srcPath());
                }
                result.println(_outputPS);
            }
        }
    }

    public long numberOfCheckedFiles() {
        return _nbFileChecked.get();
    }

    public long numberOfPassedFiles() {
        return _nbFilePassed.get();
    }

    public long numberOfFailedFiles() {
        return _nbFileFailed.get();
    }

    public long numberOfMissingFiles() {
        return _nbFileMissing.get();
    }

    public long numberOfContentMismatchFiles() {
        return _nbFileContentMismatch.get();
    }

    public long numberOfCheckedAssets() {
        return _nbAssetChecked.get();
    }

    public long numberOfPassedAssets() {
        return _nbAssetPassed.get();
    }

    public long numberOfFailedAssets() {
        return _nbAssetFailed.get();
    }

    public long numberOfMissingAssets() {
        return _nbAssetMissing.get();
    }

    public long numberOfContentMismatchAssets() {
        return _nbAssetContentMismatch.get();
    }

    public void close() throws IOException {
        if (_outputPS != null) {
            _outputPS.close();
        }
    }

    @Override
    public long total() {
        return _total.get();
    }

    @Override
    public void addTotal(long delta) {
        _total.getAndAdd(delta);
    }

    public boolean allChecked() {
        return total() == numberOfCheckedFiles() + numberOfCheckedAssets();
    }
}
