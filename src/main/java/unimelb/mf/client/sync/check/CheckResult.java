package unimelb.mf.client.sync.check;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.util.Objects;
import java.util.logging.Logger;

import unimelb.utils.ChecksumUtils.ChecksumType;
import unimelb.utils.PathUtils;

public class CheckResult {

    private final Item.Type _srcType;
    private final String _srcPath;
    private final Long _srcLength;
    private final String _srcChecksum;
    private final ChecksumType _srcChecksumType;
    private final String _srcSymlinkTarget;
    private final Item.Type _dstType;
    private final String _dstPath;
    private final Long _dstLength;
    private final String _dstChecksum;
    private final ChecksumType _dstChecksumType;
    private final String _dstSymlinkTarget;
    private final boolean _dstExists;
    private final boolean _csumCheck;

    public CheckResult(Path filePath, Long fileLength, String fileChecksum, String fileSymlinkTarget, String assetPath,
                       Long assetContentSize, String assetContentChecksum, String assetSymlinkTarget, boolean dstExists,
                       boolean csumCheck) {
        _srcType = Item.Type.FILE;
        _srcPath = filePath == null ? null : filePath.toAbsolutePath().toString();
        _srcLength = fileLength;
        _srcChecksum = fileChecksum == null ? null : fileChecksum.toLowerCase();
        _srcChecksumType = ChecksumType.CRC32;
        _srcSymlinkTarget = fileSymlinkTarget;
        _dstType = Item.Type.ASSET;
        _dstPath = assetPath;
        _dstLength = assetContentSize;
        _dstChecksum = assetContentChecksum == null ? null : assetContentChecksum.toLowerCase();
        _dstChecksumType = ChecksumType.CRC32;
        _dstSymlinkTarget = assetSymlinkTarget;
        _dstExists = dstExists;
        _csumCheck = csumCheck;
    }

    public CheckResult(String assetPath, Long assetContentSize, String assetContentChecksum, String assetSymlinkTarget,
                       Path filePath, Long fileLength, String fileChecksum, String fileSymlinkTarget, boolean dstExists,
                       boolean csumCheck) {
        _srcType = Item.Type.ASSET;
        _srcPath = assetPath;
        _srcLength = assetContentSize;
        _srcChecksum = assetContentChecksum == null ? null : assetContentChecksum.toLowerCase();
        _srcChecksumType = ChecksumType.CRC32;
        _srcSymlinkTarget = assetSymlinkTarget;
        _dstType = Item.Type.FILE;
        _dstPath = filePath == null ? null : filePath.toAbsolutePath().toString();
        _dstLength = fileLength;
        _dstChecksum = fileChecksum == null ? null : fileChecksum.toLowerCase();
        _dstChecksumType = ChecksumType.CRC32;
        _dstSymlinkTarget = fileSymlinkTarget;
        _dstExists = dstExists;
        _csumCheck = csumCheck;
    }

    protected CheckResult(Item.Type srcType, String srcPath, Long srcLength, String srcChecksum,
                          ChecksumType srcChecksumType, String srcSymlinkTarget, Item.Type dstType, String dstPath,
                          Long dstLength, String dstChecksum, ChecksumType dstChecksumType, String dstSymlinkTarget,
                          boolean dstExists, boolean csumCheck) {
        _srcType = srcType;
        _srcPath = srcPath;
        _srcLength = srcLength;
        _srcChecksum = srcChecksum;
        _srcChecksumType = srcChecksumType;
        _srcSymlinkTarget = srcSymlinkTarget;
        _dstType = dstType;
        _dstPath = dstPath;
        _dstLength = dstLength;
        _dstChecksum = dstChecksum;
        _dstChecksumType = dstChecksumType;
        _dstSymlinkTarget = dstSymlinkTarget;
        _dstExists = dstExists;
        _csumCheck = csumCheck;
    }

    public final Item.Type srcType() {
        return _srcType;
    }

    public final String srcPath() {
        return _srcPath;
    }

    public final Item.Type dstType() {
        return _dstType;
    }

    public final String dstPath() {
        return _dstPath;
    }

    public final boolean exists() {
        return _dstExists;
    }

    public final boolean sizesMatch() {
        return _srcLength != null && _dstLength != null && _srcLength.equals(_dstLength);
    }

    public final boolean namesMatch() {
        return PathUtils.fileNamesMatch(_srcPath, _dstPath);
    }

    public final Boolean checksumsMatch() {
        if (_csumCheck) {
            if (_srcLength != null && _srcLength == 0 && _dstLength != null && _dstLength == 0) {
                // file sizes are 0
                return true;
            } else {
                return Objects.equals(_srcChecksumType, _dstChecksumType)
                        && _srcChecksum != null && _dstChecksum != null
                        && _srcChecksum.equalsIgnoreCase(_dstChecksum);
            }
        } else {
            return null;
        }
    }

    public final boolean contentsMatch() {
        return sizesMatch() && (!_csumCheck || checksumsMatch());
    }

    public final boolean match() {
        if (_srcSymlinkTarget != null || _dstSymlinkTarget != null) {
            return exists() && namesMatch() && Objects.equals(_srcSymlinkTarget, _dstSymlinkTarget);
        } else {
            return exists() && namesMatch() && contentsMatch();
        }
    }

    public final String toCSVRecord() {
        return String.format("\"%s:%s\",%s,%s,\"%s:%s\",%s,%s,%s,%s",
                srcType().toString().toLowerCase(), srcPath(),
                _srcLength != null ? Long.toString(_srcLength) : "",
                _srcChecksum != null ? _srcChecksum : "",
                dstType().toString().toLowerCase(), dstPath(),
                _dstLength != null ? Long.toString(_dstLength) : "",
                _dstChecksum != null ? _dstChecksum : "",
                exists() ? "true" : "false",
                match() ? "true" : "false");
    }

    @Override
    public final String toString() {
        return toCSVRecord();
    }

    public final void log(Logger logger) {
        logger.info(toCSVRecord());
    }

    public final void println(PrintWriter pw) {
        pw.println(toCSVRecord());
    }

    public final void println(PrintStream ps) {
        ps.println(toCSVRecord());
    }

}
