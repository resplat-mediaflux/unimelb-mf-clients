package unimelb.mf.client.sync.check;

public interface Item extends HasContent, HasContextPath {

    public static enum Type {
        ASSET, FILE
    }

    Type type();

    default String typeName() {
        return type().name().toLowerCase();
    }
}
