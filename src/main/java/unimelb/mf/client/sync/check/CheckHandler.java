package unimelb.mf.client.sync.check;

public interface CheckHandler {

	long total();

	void addTotal(long increment);

	void checked(CheckResult result);

}
