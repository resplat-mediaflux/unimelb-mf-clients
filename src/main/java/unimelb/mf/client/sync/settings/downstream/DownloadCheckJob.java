package unimelb.mf.client.sync.settings.downstream;

import java.nio.file.Path;

import unimelb.mf.client.sync.settings.Action;

public abstract class DownloadCheckJob extends DownJob {

    protected DownloadCheckJob(Path dstDir) {
        super(dstDir);
    }

    @Override
    public final Action action() {
        return Action.CHECK_DOWNLOAD;
    }

}
