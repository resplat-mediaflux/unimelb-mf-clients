package unimelb.mf.client.sync.settings.downstream;

import java.nio.file.Path;

import unimelb.mf.client.sync.settings.Action;

public abstract class DownloadJob extends DownJob {

    protected DownloadJob(Path dstDir) {
        super(dstDir);
    }

    @Override
    public final Action action() {
        return Action.DOWNLOAD;
    }

}
