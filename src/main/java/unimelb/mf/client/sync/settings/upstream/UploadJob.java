package unimelb.mf.client.sync.settings.upstream;

import unimelb.mf.client.sync.settings.Action;
import unimelb.mf.client.util.collection.CollectionPath;

public abstract class UploadJob extends UpJob {

    protected UploadJob(CollectionPath dstCollectionPath) {
        super(dstCollectionPath);
    }

    @Override
    public final Action action() {
        return Action.UPLOAD;
    }

}
