package unimelb.mf.client.sync.settings.upstream;

import java.nio.file.Path;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import unimelb.mf.client.util.collection.CollectionPath;
import unimelb.utils.PathUtils;

public class DirectoryUploadCheckJob extends UploadCheckJob {

    private Path _srcDir;
    private Set<String> _includes;
    private Set<String> _excludes;

    public DirectoryUploadCheckJob(Path srcDir, Collection<String> includes, Collection<String> excludes,
            CollectionPath dstCollectionPath, boolean isParentCollection) {
        super(isParentCollection
                ? new CollectionPath(
                        PathUtils.joinSystemIndependent(dstCollectionPath.path(), srcDir.toFile().getName()), null)
                : dstCollectionPath);
        _srcDir = srcDir.normalize().toAbsolutePath();
        if (includes != null && !includes.isEmpty()) {
            _includes = new LinkedHashSet<String>(includes);
        }
        if (excludes != null && !excludes.isEmpty()) {
            _excludes = new LinkedHashSet<String>(excludes);
        }
    }

    public final Path srcDirectory() {
        return _srcDir;
    }

    @Override
    public boolean accept(Object o) {
        return DirectoryUploadJob.accept(_srcDir, _includes, _excludes, o);
    }

    @Override
    public String transformPath(Path srcFile) {
        return dstAssetPath(this.dstCollection().path(), this.srcDirectory(), srcFile);
    }

}
