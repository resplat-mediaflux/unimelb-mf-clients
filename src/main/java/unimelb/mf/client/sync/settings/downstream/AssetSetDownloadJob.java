package unimelb.mf.client.sync.settings.downstream;

import java.nio.file.Path;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;

import unimelb.utils.PathUtils;

public class AssetSetDownloadJob extends DownloadJob {

    private final Collection<String> _srcAssetPaths;

    public AssetSetDownloadJob(Collection<String> srcAssetPaths, Path dstDir) {
        super(dstDir);
        _srcAssetPaths = new LinkedHashSet<>();
        if (srcAssetPaths != null && !srcAssetPaths.isEmpty()) {
            _srcAssetPaths.addAll(srcAssetPaths);
        }
    }

    public Collection<String> srcAssetPaths() {
        return Collections.unmodifiableCollection(_srcAssetPaths);
    }

    @Override
    public Path transformPath(String srcAssetPath) {
        return dstFilePath(dstDirectory(), PathUtils.getLastComponent(srcAssetPath));
    }

}
