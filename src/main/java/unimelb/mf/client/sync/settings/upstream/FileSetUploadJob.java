package unimelb.mf.client.sync.settings.upstream;

import java.nio.file.Path;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import unimelb.mf.client.util.collection.CollectionPath;

public class FileSetUploadJob extends UploadJob {

    private final Set<Path> _srcFiles;

    public FileSetUploadJob(Collection<Path> srcFiles, CollectionPath dstCollectionPath) {
        super(dstCollectionPath);
        _srcFiles = new LinkedHashSet<>();
        if (srcFiles != null && !srcFiles.isEmpty()) {
            for (Path srcFile : srcFiles) {
                _srcFiles.add(srcFile.normalize().toAbsolutePath());
            }
        }
    }

    public final Set<Path> srcFiles() {
        return Collections.unmodifiableSet(_srcFiles);
    }

    public final boolean hasSrcFiles() {
        return !_srcFiles.isEmpty();
    }

    @Override
    public String transformPath(Path srcFile) {
        return dstAssetPath(this.dstCollection().path(), srcFile);
    }

}
