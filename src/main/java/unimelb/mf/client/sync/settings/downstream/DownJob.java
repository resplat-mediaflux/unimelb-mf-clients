package unimelb.mf.client.sync.settings.downstream;

import java.nio.file.Path;
import java.nio.file.Paths;

import unimelb.mf.client.sync.settings.Job;
import unimelb.mf.client.sync.settings.PathTransformer;
import unimelb.utils.PathUtils;

public abstract class DownJob implements Job, PathTransformer<String, Path> {

	private final Path _dstDir;

	protected DownJob(Path dstDir) {
		_dstDir = dstDir == null ? null : dstDir.normalize().toAbsolutePath();
	}

	public final Path dstDirectory() {
		return _dstDir;
	}

	public static Path dstFilePath(Path dstDir, String srcCollectionPath, String srcAssetPath) {
		String relativePath = PathUtils.getRelativePathSI(srcCollectionPath, srcAssetPath);
		return Paths
				.get(PathUtils.joinSystemIndependent(PathUtils.toSystemIndependent(dstDir.toAbsolutePath().toString()),
						relativePath == null ? srcAssetPath : relativePath));
	}

	static Path dstFilePath(Path dstDir, String srcAssetPath) {
		return Paths.get(PathUtils.toSystemIndependent(dstDir.toAbsolutePath().toString()),
				PathUtils.getFileName(srcAssetPath));
	}

}
