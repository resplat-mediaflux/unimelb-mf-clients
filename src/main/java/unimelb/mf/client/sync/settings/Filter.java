package unimelb.mf.client.sync.settings;

public interface Filter {

    boolean accept(Object o);
    
}
