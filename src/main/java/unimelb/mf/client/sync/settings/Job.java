package unimelb.mf.client.sync.settings;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;

import arc.xml.XmlDoc;
import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.sync.settings.Action.Direction;
import unimelb.mf.client.sync.settings.downstream.CollectionDownloadCheckJob;
import unimelb.mf.client.sync.settings.downstream.CollectionDownloadJob;
import unimelb.mf.client.sync.settings.upstream.DirectoryUploadCheckJob;
import unimelb.mf.client.sync.settings.upstream.DirectoryUploadJob;
import unimelb.mf.client.util.AssetNamespaceUtils;
import unimelb.mf.client.util.AssetUtils;
import unimelb.mf.client.util.collection.CollectionPath;

public interface Job extends Filter {

    Action action();

    default boolean accept(Object o) {
        return o != null;
    }

    // static String parentAssetNamespace(String assetNamespace) {
    // if (assetNamespace == null || assetNamespace.isEmpty() ||
    // "/".equals(assetNamespace)) {
    // return null;
    // }
    // return PathUtils.getParentPath(assetNamespace);
    // }

    public static Job parse(XmlDoc.Element je, String source, MFSession session) throws Throwable {

        // TODO support project id
        String src = (source == null || source.isEmpty()) ? "" : "(" + source + ")";
        Action action = Action.fromString(je.value("@action"));
        if (action == null) {
            throw new IllegalArgumentException(
                    "Invalid job configuration" + src + ": Missing or invalid @action attribute.");
        }

        String collectionPath = je.stringValue("collection", je.value("namespace"));
        if (collectionPath == null) {
            throw new IllegalArgumentException("Invalid job configuration" + src + ": Missing collection element."
                    + (source == null ? "" : (" Source: " + source)));
        }
        boolean isParentCollection = false;
        if (je.elementExists("collection")) {
            if (session != null && !AssetUtils.collectionAssetExists(collectionPath, session)) {
                throw new IllegalArgumentException("Invalid job configuration" + src + ": Collection asset: '"
                        + collectionPath + "' does not exist.");
            }
            isParentCollection = action.direction() != Direction.DOWN ? je.booleanValue("collection/@parent", false)
                    : false;
        } else {
            if (session != null && !AssetNamespaceUtils.assetNamespaceExists(session, collectionPath)) {
                throw new IllegalArgumentException("Invalid job configuration" + src + ": Asset namespace: '"
                        + collectionPath + "' does not exist.");
            }
            isParentCollection = action.direction() != Direction.DOWN ? je.booleanValue("namespace/@parent", false)
                    : false;
        }
        String dir = je.value("directory");
        if (dir == null) {
            throw new IllegalArgumentException("Invalid job configuration" + src + ": Missing directory element.");
        }
        Path directory = Paths.get(dir).toAbsolutePath();
        if (!Files.isDirectory(directory)) {
            throw new IllegalArgumentException("Invalid job configuration" + src + ": directory: '" + directory
                    + "' does not exist or it is not a directory.");
        }
        boolean isParentDir = action.direction() != Direction.UP ? je.booleanValue("directory/@parent", false) : false;

        Collection<String> includes = je.values("include");
        Collection<String> excludes = je.values("exclude");

        CollectionPath collection = new CollectionPath(collectionPath);
        if (action.direction() == Action.Direction.UP) {
            if (action.type() == Action.Type.TRANSFER) {
                return new DirectoryUploadJob(directory, includes, excludes, collection, isParentCollection);
            } else {
                return new DirectoryUploadCheckJob(directory, includes, excludes, collection, isParentCollection);
            }
        } else {
            if (action.type() == Action.Type.TRANSFER) {
                return new CollectionDownloadJob(collection, directory, isParentDir);
            } else {
                return new CollectionDownloadCheckJob(collection, directory, isParentDir);
            }
        }
    }

}
