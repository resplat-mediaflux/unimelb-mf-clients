package unimelb.mf.client.sync.settings.downstream;

import java.nio.file.Path;
import java.nio.file.Paths;

import unimelb.mf.client.util.collection.CollectionPath;
import unimelb.utils.PathUtils;

public class CollectionDownloadJob extends DownloadJob {

    private final CollectionPath _srcCollectionPath;

    public CollectionDownloadJob(CollectionPath srcCollectionPath, Path dstDir, boolean isParentDir) {
        super(isParentDir
                ? Paths.get(dstDir.normalize().toAbsolutePath().toString(),
                        PathUtils.getLastComponent(srcCollectionPath.path()))
                : dstDir);
        _srcCollectionPath = srcCollectionPath;
    }

    public final CollectionPath srcCollection() {
        return _srcCollectionPath;
    }

    @Override
    public Path transformPath(String assetPath) {
        return dstFilePath(dstDirectory(), _srcCollectionPath.path(), assetPath);
    }

}
