package unimelb.mf.client.sync.settings;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import arc.xml.XmlDoc;
import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.sync.check.CheckHandler;
import unimelb.mf.client.sync.task.AssetDownloadTask;
import unimelb.mf.client.sync.task.AssetDownloadTask.Unarchive;
import unimelb.mf.client.task.MFApp;

public class Settings implements MFApp.Settings {

    public static final int DEFAULT_DAEMON_LISTENER_PORT = 9761;
    public static final long DEFAULT_DAEMON_SCAN_INTERVAL = 60000L;
    public static final int DEFAULT_NUM_OF_QUERIERS = 1;
    public static final int DEFAULT_NUM_OF_WORKERS = 1;
    public static final int DEFAULT_BATCH_SIZE = 1000;
    public static final int DEFAULT_NB_RETRIES = 2;
    public static final long DEFAULT_RETRY_WAIT_TIME = 100L;
    public static final int DEFAULT_LOG_FILE_SIZE_MB = 100; // 100MB
    public static final int MIN_LOG_FILE_SIZE_MB = 1;// 1MB
    public static final int MAX_LOG_FILE_SIZE_MB = 2000;// 2000MB
    public static final int DEFAULT_LOG_FILE_COUNT = 2;
    public static final int MIN_LOG_FILE_COUNT = 1;
    public static final int MAX_LOG_FILE_COUNT = 10;
    public static final long DEFAULT_AGGREGATE_UPLOAD_THRESHOLD = 1000000L;
    public static final long DEFAULT_AGGREGATE_DOWNLOAD_THRESHOLD = 1000000L;
    public static final long DEFAULT_SPLIT_THRESHOLD = 1000000000L; // 1GB

    private List<Job> _jobs;
    private Path _logDir = null;
    private int _logFileSizeMB = DEFAULT_LOG_FILE_SIZE_MB;
    private int _logFileCount = DEFAULT_LOG_FILE_COUNT;

    private int _nbQueriers = DEFAULT_NUM_OF_QUERIERS;
    private int _nbWorkers = DEFAULT_NUM_OF_WORKERS;

    private boolean _daemon = false;
    private int _daemonListenerPort = DEFAULT_DAEMON_LISTENER_PORT;
    private long _daemonScanInterval = DEFAULT_DAEMON_SCAN_INTERVAL;

    private int _batchSize = DEFAULT_BATCH_SIZE;

    private boolean _createparentCollections;
    private boolean _csumCheck;

    private boolean _includeMetadata = false; // download metadata xml

    private long _aggregateDownloadThreshold = 0L;

    private long _aggregateUploadThreshold = 0L;

    private long _splitThreshold = Long.MAX_VALUE;

    private int _maxNumberOfRetries = DEFAULT_NB_RETRIES; // Number of retries...

    private long _retryWaitTime = DEFAULT_RETRY_WAIT_TIME; // milliseconds

    /*
     * download settings
     */
    private boolean _overwrite = false;
    private AssetDownloadTask.Unarchive _unarchive = AssetDownloadTask.Unarchive.NONE;

    /*
     * upload settings;
     */
    private boolean _excludeEmptyFolder = true;

    /*
     * check settings
     */
    private CheckHandler _checkHandler = null;

    private boolean _verbose = false;

    private Set<String> _recipients;

    private boolean _deleteFiles = false;

    private boolean _deleteAssets = false;

    private boolean _hardDestroyAssets = false;

    // follow symbolic links when uploading
    private boolean _followSymlinks = false;

    // try restoring symbolic links when downloading
    private boolean _restoreSymlinks = true;

    // exclude parent if the source path ends with trailing slash
    private boolean _excludeParent = false;

    private boolean _worm = false;

    private boolean _wormCanAddVersions = false;

    private boolean _wormCanMove = true;

    private boolean _saveFileAttributes = false;

    // preserve local file mtime when uploading to Mediaflux.
    private boolean _preserveModifiedTime = false;

    public Settings() {
        _jobs = new ArrayList<>();
    }

    public int batchSize() {
        return _batchSize;
    }

    public void setBatchSize(int batchSize) {
        if (batchSize < 1) {
            batchSize = DEFAULT_BATCH_SIZE;
        }
        _batchSize = batchSize;
    }

    public Path logDirectory() {
        return _logDir;
    }

    public void setLogDirectory(Path logDir) {
        _logDir = logDir;
    }

    public List<Job> jobs() {
        if (_jobs != null && !_jobs.isEmpty()) {
            return Collections.unmodifiableList(_jobs);
        }
        return null;
    }

    public void addJob(Job... jobs) {
        if (_jobs == null) {
            _jobs = new ArrayList<>();
        }
        if (jobs != null) {
            Collections.addAll(_jobs, jobs);
        }
    }

    public void clearJobs() {
        if (_jobs != null) {
            _jobs.clear();
        }
    }

    public void setJobs(Collection<Job> jobs) {
        clearJobs();
        if (jobs != null) {
            for (Job job : jobs) {
                addJob(job);
            }
        }
    }

    public boolean daemon() {
        return _daemon;
    }

    public void setDaemon(boolean daemon) {
        _daemon = daemon;
    }

    public int daemonListenerPort() {
        return _daemonListenerPort;
    }

    public void setDaemonListenerPort(int port) {
        _daemonListenerPort = port;
    }

    public int numberOfWorkers() {
        return _nbWorkers;
    }

    public void setNumberOfWorkers(int nbWorkers) {
        if (nbWorkers <= 1) {
            _nbWorkers = 1;
        } else {
            _nbWorkers = Math.min(nbWorkers, MAX_NUM_OF_WORKERS);
        }
    }

    public int numberOfQueriers() {
        return _nbQueriers;
    }

    public void setNumberOfQueriers(int nbQueriers) {
        if (nbQueriers <= 1) {
            _nbQueriers = 1;
        } else {
            _nbQueriers = Math.min(nbQueriers, MAX_NUM_OF_QUERIERS);
        }
    }

    // public void validate(MFSession session) throws Throwable {
    // for (Job job : _jobs) {
    // /*
    // * check if namespace is specified
    // */
    // if (job.namespace() == null) {
    // throw new IllegalArgumentException("Asset namespace is null.", new
    // NullPointerException());
    // }
    // /*
    // * check if parent namespace exists
    // */
    // String parentNS = job.parentNamespace();
    // boolean parentNSExists =
    // !AssetNamespaceUtils.assetNamespaceExists(session,
    // PathUtils.getParentPath(parentNS));
    // if (!parentNSExists) {
    // throw new IllegalArgumentException("Asset namespace: '" + parentNS + "'
    // does not exist.");
    // }
    //
    // /*
    // * check if directory is specified
    // */
    // if (job.directory() == null) {
    // throw new IllegalArgumentException("Source directory is null.", new
    // NullPointerException());
    // }
    // /*
    // * check if parent directory exists
    // */
    // Path parentDir = job.directory().toAbsolutePath().getParent();
    // if (!Files.isDirectory(parentDir)) {
    // throw new IllegalArgumentException("'" + parentDir + "' does not exist or
    // it is not a directory.");
    // }
    // }
    // }

    public boolean createParentCollections() {
        return _createparentCollections;
    }

    public void setCreateParentCollections(boolean createParentCollections) {
        _createparentCollections = createParentCollections;
    }

    public boolean csumCheck() {
        return _csumCheck;
    }

    public void setCsumCheck(boolean csumCheck) {
        _csumCheck = csumCheck;
    }

    /**
     * Currently for download only.
     *
     * @return
     */
    public boolean overwrite() {
        return _overwrite;
    }

    /**
     * Currently for download only.
     *
     * @param overwrite
     */
    public void setOverwrite(boolean overwrite) {
        _overwrite = overwrite;
    }

    /**
     * Currently for download only.
     *
     * @return
     */
    public AssetDownloadTask.Unarchive unarchive() {
        return _unarchive;
    }

    /**
     * Currently for download only.
     *
     * @param unarchive
     */
    public void setUnarchive(AssetDownloadTask.Unarchive unarchive) {
        _unarchive = unarchive;
    }

    /**
     * For check only.
     *
     * @return
     */
    public CheckHandler checkHandler() {
        return _checkHandler;
    }

    /**
     * For check only.
     *
     * @param ch
     */
    public void setCheckHandler(CheckHandler ch) {
        _checkHandler = ch;
    }

    public int maxNumberOfRetries() {
        return _maxNumberOfRetries;
    }

    public void setMaxNumberOfRetries(int nbRetries) {
        _maxNumberOfRetries = Math.max(nbRetries, 0);
    }

    /**
     * Get retry wait time in milliseconds
     * 
     * @return the wait time before retrying
     */
    public long retryWaitTime() {
        return _retryWaitTime;
    }

    /**
     * Set retry wait time in milliseconds.
     * 
     * @param retryWaitTime milliseconds
     */
    public void setRetryWaitTime(int retryWaitTime) {
        _retryWaitTime = Math.max(retryWaitTime, 0);
    }

    public boolean excludeEmptyFolder() {
        return _excludeEmptyFolder;
    }

    public void setExcludeEmptyFolder(boolean excludeEmptyFolder) {
        _excludeEmptyFolder = excludeEmptyFolder;
    }

    public boolean verbose() {
        return _verbose;
    }

    public void setVerbose(boolean verbose) {
        _verbose = verbose;
    }

    public long daemonScanInterval() {
        return _daemonScanInterval;
    }

    public void setDaemonScanInterval(long interval) {
        _daemonScanInterval = interval;
    }

    public boolean hasRecipients() {
        return _recipients != null && !_recipients.isEmpty();
    }

    public Collection<String> recipients() {
        return _recipients == null ? null : Collections.unmodifiableCollection(_recipients);
    }

    public void addRecipients(String... emails) {
        if (emails != null && emails.length > 0) {
            if (_recipients == null) {
                _recipients = new LinkedHashSet<>();
            }
            for (String email : emails) {
                _recipients.add(email.toLowerCase());
            }
        }
    }

    public boolean hasJobs() {
        return _jobs != null && !_jobs.isEmpty();
    }

    public boolean hasOnlyCheckJobs() {
        if (hasJobs()) {
            for (Job job : _jobs) {
                if (job.action().type() != Action.Type.CHECK) {
                    return false;
                }
            }
        }
        return true;
    }

    public void loadFromXml(XmlDoc.Element pe, String source, MFSession session) throws Throwable {
        String src = (source == null || source.isEmpty()) ? "" : "(" + source + ")";
        XmlDoc.Element se = pe.element("sync/settings");
        if (se == null) {
            throw new Exception(
                    "Failed to parse configuration" + src + ": Element properties/sync/settings is not found.");
        }

        if (se.elementExists("numberOfQueriers")) {
            int nbQueriers = se.intValue("numberOfQueriers");
            if (nbQueriers <= 0 || nbQueriers > Settings.MAX_NUM_OF_WORKERS) {
                throw new IllegalArgumentException(
                        "Failed to parse numberOfQueriers from XML configuration. Expects integer between 0 and "
                                + Settings.MAX_NUM_OF_QUERIERS + ". Found: " + nbQueriers);
            }
            setNumberOfQueriers(nbQueriers);
        }
        if (se.elementExists("numberOfWorkers")) {
            int nbWorkers = se.intValue("numberOfWorkers");
            if (nbWorkers <= 0 || nbWorkers > Settings.MAX_NUM_OF_WORKERS) {
                throw new IllegalArgumentException(
                        "Failed to parse numberOfWorkers from XML configuration. Expects integer between 0 and "
                                + Settings.MAX_NUM_OF_WORKERS + ". Found: " + nbWorkers);
            }
            setNumberOfWorkers(nbWorkers);
        }
        if (se.elementExists("batchSize")) {
            setBatchSize(se.intValue("batchSize"));
        }
        if (se.elementExists("maxRetries")) {
            setMaxNumberOfRetries(se.intValue("maxRetries"));
        }
        if (se.elementExists("csumCheck")) {
            setCsumCheck(se.booleanValue("csumCheck", false));
        }
        if (se.elementExists("followSymlinks")) {
            setFollowSymlinks(se.booleanValue("followSymlinks", false));
        }
        if (se.elementExists("restoreSymlinks")) {
            setRestoreSymlinks(se.booleanValue("restoreSymlinks", true));
        }
        if (se.elementExists("overwrite")) {
            setOverwrite(se.booleanValue("overwrite", false));
        }
        if (se.elementExists("unarchive")) {
            setUnarchive(Unarchive.fromString(se.value("unarchive")));
        }
        if (se.elementExists("daemon")) {
            setDaemon(se.booleanValue("daemon/@enabled", false));
            if (se.elementExists("daemon/listenerPort")) {
                setDaemonListenerPort(se.intValue("daemon/listenerPort"));
            }
            if (se.elementExists("daemon/scanInterval")) {
                setDaemonScanInterval(se.intValue("daemon/scanInterval"));
            }
        }
        if (se.elementExists("worm")) {
            setWorm(se.booleanValue("worm/enable", false));
            setWormCanAddVersions(se.booleanValue("worm/canAddVersions", false));
            setWormCanMove(se.booleanValue("worm/canMove", true));
        }
        if (se.elementExists("logDirectory")) {
            Path logDir = Paths.get(se.value("logDirectory"));
            if (!Files.exists(logDir)) {
                throw new FileNotFoundException(logDir.toString());
            }
            if (!Files.isDirectory(logDir)) {
                throw new Exception(logDir.toString() + " is not a directory!");
            }
            setLogDirectory(logDir);
        }
        if (se.elementExists("logFileSizeMB")) {
            setLogFileSizeMB(se.intValue("logFileSizeMB"));
        }
        if (se.elementExists("logFileCount")) {
            setLogFileCount(se.intValue("logFileCount"));
        }
        if (se.elementExists("excludeEmptyFolder")) {
            setExcludeEmptyFolder(se.booleanValue("excludeEmptyFolder"));
        }
        if (se.elementExists("verbose")) {
            setVerbose(se.booleanValue("verbose"));
        }
        if (se.elementExists("notification/email")) {
            Collection<String> emails = se.values("notification/email");
            if (emails != null) {
                for (String email : emails) {
                    addRecipients(email);
                }
            }
        }

        // Add jobs
        List<XmlDoc.Element> jes = pe.elements("sync/job");
        if (jes != null) {
            for (XmlDoc.Element je : jes) {
                addJob(Job.parse(je, source, session));
            }
        }
    }

    public void loadFromXmlFile(Path xmlFile, MFSession session) throws Throwable {
        if (xmlFile != null) {
            try (Reader r = new BufferedReader(new FileReader(xmlFile.toFile()))) {
                XmlDoc.Element pe = new XmlDoc().parse(r);
                if (pe == null) {
                    throw new IllegalArgumentException("Failed to parse configuration XML file: " + xmlFile);
                }
                loadFromXml(pe, xmlFile.toString(), session);
            }
        }
    }

    public boolean hasDownloadJobs() {
        if (_jobs != null && !_jobs.isEmpty()) {
            for (Job job : _jobs) {
                if (job.action() == Action.DOWNLOAD) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean hasUploadJobs() {
        if (_jobs != null && !_jobs.isEmpty()) {
            for (Job job : _jobs) {
                if (job.action() == Action.UPLOAD) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Delete local files.
     *
     * @return
     */
    public boolean deleteFiles() {
        return _deleteFiles;
    }

    public void setDeleteFiles(boolean deleteFiles) {
        _deleteFiles = deleteFiles;
    }

    /**
     * Destroy remote assets.
     *
     * @return
     */
    public boolean deleteAssets() {
        return _deleteAssets;
    }

    public void setDeleteAssets(boolean deleteAssets) {
        _deleteAssets = deleteAssets;
    }

    public boolean hardDestroyAssets() {
        return _hardDestroyAssets;
    }

    public void setHardDestroyAssets(boolean hardDestroy) {
        _hardDestroyAssets = hardDestroy;
    }

    public boolean needToDeleteFiles() {
        return (hasDownloadJobs()) && deleteFiles();
    }

    public boolean needToDeleteAssets() {
        return (hasUploadJobs()) && deleteAssets();
    }

    public boolean followSymlinks() {
        return _followSymlinks;
    }

    public void setFollowSymlinks(boolean followSymlinks) {
        _followSymlinks = followSymlinks;
    }

    /**
     * restore symbolic links when downloading
     *
     * @return
     */
    public boolean restoreSymlinks() {
        return _restoreSymlinks;
    }

    /**
     * set whether or not to restore symbolic links when downloading.
     *
     * @param restoreSymlinks restore symbolic links
     */
    public void setRestoreSymlinks(boolean restoreSymlinks) {
        _restoreSymlinks = restoreSymlinks;
    }

    public int logFileSizeMB() {
        return _logFileSizeMB;
    }

    public void setLogFileSizeMB(int fileSizeMB) {
        if (fileSizeMB < MIN_LOG_FILE_SIZE_MB || fileSizeMB > MAX_LOG_FILE_SIZE_MB) {
            throw new IllegalArgumentException("Expects integer value between " + MIN_LOG_FILE_SIZE_MB + " and "
                    + MAX_LOG_FILE_SIZE_MB + ". Found " + fileSizeMB);
        }
        _logFileSizeMB = fileSizeMB;
    }

    public int logFileCount() {
        return _logFileCount;
    }

    public void setLogFileCount(int fileCount) {
        if (fileCount < MIN_LOG_FILE_COUNT || fileCount > MAX_LOG_FILE_COUNT) {
            throw new IllegalArgumentException("Expects integer value between " + MIN_LOG_FILE_COUNT + " and "
                    + MAX_LOG_FILE_COUNT + ". Found " + fileCount);
        }
        _logFileCount = fileCount;
    }

    public boolean excludeParent() {
        return _excludeParent;
    }

    public void setExcludeParent(boolean excludeParent) {
        _excludeParent = excludeParent;
    }

    public boolean worm() {
        return _worm;
    }

    public void setWorm(boolean worm) {
        _worm = worm;
    }

    public boolean wormCanAddVersions() {
        return _wormCanAddVersions;
    }

    public void setWormCanAddVersions(boolean wormCanAddVersions) {
        _wormCanAddVersions = wormCanAddVersions;
    }

    public boolean wormCanMove() {
        return _wormCanMove;
    }

    public void setWormCanMove(boolean wormCanMove) {
        _wormCanMove = wormCanMove;
    }

    public boolean saveFileAttributes() {
        return _saveFileAttributes;
    }

    public void setSaveFileAttributes(boolean saveFileAttributes) {
        _saveFileAttributes = saveFileAttributes;
    }

    /**
     * If set to true, when uploading a file, it will preserve the modified time of
     * the local file (by calling asset.modify.time.set service after the asset is
     * created in Mediaflux).
     * 
     * @return
     */
    public boolean preserveModifiedTime() {
        return _preserveModifiedTime;
    }

    public void setPreserveModifiedTime(boolean preserveModifiedTime) {
        _preserveModifiedTime = preserveModifiedTime;
    }

    public boolean includeMetadata() {
        return _includeMetadata;
    }

    public void setIncludeMetadata(boolean includeMetadata) {
        _includeMetadata = includeMetadata;
    }

    public long aggregateDownloadThreshold() {
        return _aggregateDownloadThreshold;
    }

    public void setAggregateDownloadThreshold(Long aggregateDownloadThreshold) {
        _aggregateDownloadThreshold = aggregateDownloadThreshold;
    }

    public void enableAggregateDownload() {
        setAggregateDownloadThreshold(DEFAULT_AGGREGATE_DOWNLOAD_THRESHOLD);
    }

    public void disableAggregateDownload() {
        setAggregateDownloadThreshold(0L);
    }

    public long aggregateUploadThreshold() {
        return _aggregateUploadThreshold;
    }

    public void setAggregateUploadThreshold(Long aggregateUploadThreshold) {
        _aggregateUploadThreshold = aggregateUploadThreshold;
    }

    public void enableAggregateUpload() {
        setAggregateUploadThreshold(DEFAULT_AGGREGATE_UPLOAD_THRESHOLD);
    }

    public void disableAggregateUpload() {
        setAggregateUploadThreshold(0L);
    }

    public long splitThreshold() {
        return _splitThreshold;
    }

    public void setSplitThreshold(long splitThreshold) {
        _splitThreshold = splitThreshold;
    }

    public void enableSplit() {
        _splitThreshold = DEFAULT_SPLIT_THRESHOLD;
    }

    public void disableSplit() {
        _splitThreshold = 0;
    }

}
