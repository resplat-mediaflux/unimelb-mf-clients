package unimelb.mf.client.sync.settings.upstream;

import java.io.File;
import java.nio.file.Path;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import unimelb.mf.client.util.collection.CollectionPath;
import unimelb.utils.PathPattern;
import unimelb.utils.PathUtils;

public class DirectoryUploadJob extends UploadJob {

    private Path _srcDir;
    private Set<String> _includes;
    private Set<String> _excludes;

    public DirectoryUploadJob(Path srcDir, Collection<String> includes, Collection<String> excludes,
            CollectionPath dstCollectionPath, boolean isParentCollection) {
        super(isParentCollection
                ? new CollectionPath(PathUtils.joinSystemIndependent(dstCollectionPath.path(),
                        srcDir.normalize().toAbsolutePath().toFile().getName()), dstCollectionPath.type())
                : dstCollectionPath);
        _srcDir = srcDir.normalize().toAbsolutePath();
        if (includes != null && !includes.isEmpty()) {
            _includes = new LinkedHashSet<String>(includes);
        }
        if (excludes != null && !excludes.isEmpty()) {
            _excludes = new LinkedHashSet<String>(excludes);
        }
    }

    public final Path srcDirectory() {
        return _srcDir;
    }

    @Override
    public boolean accept(Object o) {
        return accept(_srcDir, _includes, _excludes, o);
    }

    @Override
    public String transformPath(Path srcFile) {
        return dstAssetPath(this.dstCollection().path(), this.srcDirectory(), srcFile);
    }

    static boolean accept(Path srcDir, Set<String> includes, Set<String> excludes, Object o) {
        if (o != null) {
            String path = null;
            if (o instanceof Path) {
                path = PathUtils.toSystemIndependent(((Path) o).toAbsolutePath().toString());
            } else if (o instanceof File) {
                path = PathUtils.toSystemIndependent(((File) o).getAbsolutePath());
            } else {
                path = o.toString();
            }
            String src = PathUtils.toSystemIndependent(srcDir.toAbsolutePath().toString());
            if (PathUtils.isOrIsDescendant(path, src)) {
                boolean haveIncludePatterns = includes != null && !includes.isEmpty();
                boolean haveExcludePatterns = excludes != null && !excludes.isEmpty();
                if (!haveIncludePatterns && !haveExcludePatterns) {
                    return true;
                }
                String relativePath = PathUtils.getRelativePathSI(src, path);
                if (haveIncludePatterns) {
                    if (haveExcludePatterns) {
                        return PathPattern.matchesAny(relativePath, includes)
                                && !PathPattern.matchesAny(relativePath, excludes);
                    } else {
                        return PathPattern.matchesAny(relativePath, includes);
                    }
                } else {
                    if (haveExcludePatterns) {
                        return !PathPattern.matchesAny(relativePath, excludes);
                    } else {
                        return true;
                    }
                }
            }
        }
        return false;
    }

}
