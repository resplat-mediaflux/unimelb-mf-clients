package unimelb.mf.client.sync.settings;

public interface PathTransformer<S, D> {
    D transformPath(S src);
}
