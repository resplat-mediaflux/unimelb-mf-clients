package unimelb.mf.client.sync.settings.upstream;

import java.nio.file.Path;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import unimelb.mf.client.util.collection.CollectionPath;

public class FileSetUploadCheckJob extends UploadCheckJob {

    private Set<Path> _srcFiles;

    public FileSetUploadCheckJob(Collection<Path> srcFiles, CollectionPath dstCollectionPath) {
        super(dstCollectionPath);
        _srcFiles = new LinkedHashSet<Path>();
        if (srcFiles != null && !srcFiles.isEmpty()) {
            for (Path srcFile : srcFiles) {
                _srcFiles.add(srcFile.normalize().toAbsolutePath());
            }
        }
    }

    public final Set<Path> srcFiles() {
        return Collections.unmodifiableSet(_srcFiles);
    }

    public final int nbSrcFiles() {
        return _srcFiles.size();
    }

    @Override
    public String transformPath(Path srcFile) {
        return dstAssetPath(this.dstCollection().path(), srcFile);
    }

}
