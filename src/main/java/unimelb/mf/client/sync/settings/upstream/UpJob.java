package unimelb.mf.client.sync.settings.upstream;

import java.nio.file.Path;

import unimelb.mf.client.sync.settings.Job;
import unimelb.mf.client.sync.settings.PathTransformer;
import unimelb.mf.client.util.collection.CollectionPath;
import unimelb.utils.PathUtils;

public abstract class UpJob implements Job, PathTransformer<Path, String> {

    private CollectionPath _dstCollectionPath;

    protected UpJob(CollectionPath dstCollectionPath) {
        _dstCollectionPath = dstCollectionPath;
    }

    public final CollectionPath dstCollection() {
        return _dstCollectionPath;
    }
    
    public static String dstAssetPath(String dstCollectionPath, Path srcDir, Path srcFile) {
        return PathUtils.joinSystemIndependent(dstCollectionPath, PathUtils.getRelativePathSI(srcDir, srcFile));
    }

    static String dstAssetPath(String dstCollectionPath, Path srcFile) {
        return PathUtils.joinSystemIndependent(dstCollectionPath, srcFile.toFile().getName());
    }

}
