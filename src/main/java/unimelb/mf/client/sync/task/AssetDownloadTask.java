package unimelb.mf.client.sync.task;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.logging.Logger;
import java.util.zip.CRC32;
import java.util.zip.CheckedOutputStream;

import arc.archive.ArchiveInput;
import arc.archive.ArchiveRegistry;
import arc.mf.client.ServerClient;
import arc.mf.client.archive.Archive;
import arc.mime.NamedMimeType;
import arc.streams.LongInputStream;
import arc.streams.SizedInputStream;
import arc.streams.StreamCopy;
import arc.streams.StreamUtil;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlStringWriter;
import unimelb.io.ProgressMonitorInputStream;
import unimelb.mf.client.archive.MFArchive;
import unimelb.mf.client.session.MFOutputConsumer;
import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.sync.settings.PathTransformer;
import unimelb.mf.client.task.AbstractMFTask;
import unimelb.mf.model.asset.SymlinkAsset;
import unimelb.utils.FileUtils;
import unimelb.utils.PathUtils;

public class AssetDownloadTask extends AbstractMFTask {

    public static final String METADATA_FILE_SUFFIX = ".meta.xml";

    public enum Unarchive {
        AAR, ALL, NONE;

        public static Unarchive fromString(String sv) {
            Unarchive[] vs = values();
            for (Unarchive v : vs) {
                if (v.name().equalsIgnoreCase(sv)) {
                    return v;
                }
            }
            return null;
        }
    }

    private final XmlDoc.Element _ae;
    private final String _assetPath;
    private final String _assetId;

    private final Path _dstPath;

    private final Unarchive _unarchive;

    private final boolean _restoreSymlinks;

    private final boolean _csumCheck;

    private final boolean _includeMetadata;

    private final DataTransferListener<String, Path> _dl;

    public AssetDownloadTask(MFSession session, Logger logger, XmlDoc.Element ae, Path dstPath, Unarchive unarchive,
            boolean restoreSymlinks, boolean csumCheck, boolean includeMetadata, DataTransferListener<String, Path> dl)
            throws Throwable {
        super(session, logger);
        _ae = ae;
        _assetPath = ae.value("path");
        _assetId = ae.value("@id");
        _dstPath = dstPath.toAbsolutePath();
        _unarchive = unarchive == null ? Unarchive.NONE : unarchive;
        _restoreSymlinks = restoreSymlinks;
        _csumCheck = csumCheck;
        _includeMetadata = includeMetadata;
        _dl = dl;
    }

    public String assetPath() {
        return _assetPath;
    }

    public String assetId() {
        return _assetId;
    }

    @Override
    public void execute() throws Throwable {
        if (_dl != null) {
            _dl.transferStarted(_assetPath, _dstPath);
        }

        if (_unarchive != Unarchive.NONE) {
            Archive.declareSupportForAllTypes();
        }

        if (_includeMetadata) {
            FileUtils.createParentDirectories(_dstPath);
            Path metaFilePath = Paths.get(_dstPath.toString() + METADATA_FILE_SUFFIX);
            boolean metaFileExists = Files.exists(metaFilePath);
            if (metaFileExists) {
                logWarning("Metadata file: '" + metaFilePath + "' already exists. Overwriting...");
            }
            try (BufferedWriter w = Files.newBufferedWriter(metaFilePath)) {
                logInfo((metaFileExists ? "Updating" : "Creating") + " metadata file: '" + metaFilePath + "'");
                w.write(_ae.toString());
                w.flush();
            }
        }

        boolean isSymlinkAsset = SymlinkAsset.isSymlinkAsset(_ae);

        if (!isSymlinkAsset && !_ae.elementExists("content")) {
            logWarning("Asset " + _assetId + ": '" + _assetPath + "' does not have content.");
            if (_dl != null) {
                _dl.transferFailed(_assetPath, _dstPath);
            }
            return;
        }

        // symlink
        if (isSymlinkAsset) {
            String target = SymlinkAsset.getSymlinkTarget(_ae);
            if (target != null && _restoreSymlinks) {
                if (Files.exists(_dstPath, LinkOption.NOFOLLOW_LINKS)) {
                    if (Files.isSymbolicLink(_dstPath)) {
                        if (target.equals(Files.readSymbolicLink(_dstPath).toString())) {
                            logInfo("Skipped asset" + (_assetId == null ? "" : (" " + _assetId)) + ": '" + _assetPath
                                    + "'. Already exists.");
                            if (_dl != null) {
                                _dl.transferSkipped(_assetPath, _dstPath);
                            }
                        } else {
                            try {
                                logInfo("Downloading symbolic link: " + _assetPath);
                                Path tmpSymlink = Files.createTempFile(_dstPath.getParent(),
                                        _dstPath.getFileName().toString(), ".tmp");
                                Files.createSymbolicLink(tmpSymlink, Paths.get(target));
                                Files.move(tmpSymlink, _dstPath, StandardCopyOption.ATOMIC_MOVE);
                                if (_dl != null) {
                                    _dl.transferCompleted(_assetPath, _dstPath);
                                }
                            } catch (Throwable e) {
                                logWarning(String.format("Cannot restore symbolic link: %s -> %s", _dstPath, target));
                                logError(e);
                                if (_dl != null) {
                                    _dl.transferFailed(_assetPath, _dstPath);
                                }
                            }
                        }
                    } else {
                        logWarning("Failed to restore symbolic link asset: '" + _assetPath + "'. Destination file: '"
                                + _dstPath + "' already exists. But it is not a symbolic link.");
                        if (_dl != null) {
                            _dl.transferFailed(_assetPath, _dstPath);
                        }
                    }
                } else {
                    try {
                        logInfo("Downloading symbolic link: " + _assetPath);
                        FileUtils.createParentDirectories(_dstPath);
                        Files.createSymbolicLink(_dstPath, Paths.get(target));
                        if (_dl != null) {
                            _dl.transferCompleted(_assetPath, _dstPath);
                        }
                    } catch (Throwable e) {
                        logWarning(String.format("Cannot create symbolic link: %s -> %s", _dstPath, target));
                        logError(e);
                        if (_dl != null) {
                            _dl.transferFailed(_assetPath, _dstPath);
                        }
                    }
                }
            } else {
                if (_dl != null) {
                    _dl.transferSkipped(_assetPath, _dstPath);
                }
            }
            return;
        }

        // regular file
        ServerClient.Output output = new MFOutputConsumer() {

            @Override
            protected void consume(LongInputStream is, Element re) throws Throwable {

                if (is == null) {
                    logWarning("Asset " + _assetId + ": '" + _assetPath + "' content stream is null.");
                    return;
                }
                String ctype = ctype(_ae);
                boolean unarchive = needToUnarchive(ctype);
                Path dstDir = unarchive ? Paths.get(PathUtils.removeFileExtension(_dstPath.toString())) : null;
                ProgressMonitorInputStream pis = new ProgressMonitorInputStream(is, bytesRead -> {
                    if (_dl != null) {
                        _dl.transferProgressed(_assetPath, unarchive ? dstDir : _dstPath, bytesRead);
                    }
                });
                CRC32 crc32 = _csumCheck ? new CRC32() : null;
                SizedInputStream sis = new SizedInputStream(pis, is.length());
                sis.setPropagateClose(false);
                try {
                    if (unarchive) {
                        Files.createDirectories(dstDir);
                        ArchiveInput ai = ArchiveRegistry.createInput(sis, new NamedMimeType(ctype));
                        try {
                            for (ArchiveInput.Entry e = ai.next(); e != null; e = ai.next()) {
                                Path f = Paths.get(dstDir.toString(), e.name());
                                if (e.isDirectory()) {
                                    Files.createDirectories(f);
                                } else {
                                    logInfo("Extracting file: '" + f + "'");
                                    Files.createDirectories(f.getParent());
                                    try (BufferedOutputStream bos = new BufferedOutputStream(
                                            Files.newOutputStream(f))) {
                                        StreamCopy.copy(e.stream(), bos);
                                    }
                                }
                            }
                        } finally {
                            ai.close();
                        }
                    } else {
                        FileUtils.createParentDirectories(_dstPath);
                        try (BufferedOutputStream bos = new BufferedOutputStream(
                                _csumCheck ? new CheckedOutputStream(Files.newOutputStream(_dstPath), crc32)
                                        : Files.newOutputStream(_dstPath))) {
                            logInfo("Downloading file: '" + _dstPath + "'");
                            StreamCopy.copy(sis, bos);
                        }
                    }
                    if (_csumCheck) {
                        Long assetCsum = _ae.longValue("content/csum[@base='10']", null);
                        long fileCsum = crc32.getValue();
                        if (assetCsum == null) {
                            logWarning("Cannot compare checksums. No asset content checksum found for asset: "
                                    + (_assetPath != null ? _assetPath : _assetId));
                            if (_dl != null) {
                                _dl.transferFailed(_assetPath, _dstPath);
                            }
                            return;
                        } else {
                            if (fileCsum != assetCsum) {
                                logWarning("Checksums do not match. 'asset:"
                                        + (_assetPath == null ? _assetId : _assetPath) + "' : (crc32: " + assetCsum
                                        + ") " + "'file:" + _dstPath + "' : (crc32: " + fileCsum + ")");
                                if (_dl != null) {
                                    _dl.transferFailed(_assetPath, _dstPath);
                                }
                                return;
                            }
                        }
                    }
                    if (_dl != null) {
                        _dl.transferCompleted(_assetPath, unarchive ? dstDir : _dstPath);
                    }
                } catch (Throwable t) {
                    if (_dl != null) {
                        _dl.transferFailed(_assetPath, unarchive ? dstDir : _dstPath);
                    }
                    t.printStackTrace();
                } finally {
                    // exhaust the input stream but do not close it. Closing it will cause the
                    // pooled network stream closed.
                    if (sis.remaining() > 0) {
                        StreamUtil.skipFully(sis, sis.remaining());
                    }
                    // TODO check with Arcitecta to make sure: sis/is does not need to be closed.
                }
            }
        };

        XmlStringWriter w = new XmlStringWriter();
        w.add("id", _assetId != null ? _assetId : ("path=" + _assetPath));

        session().execute("asset.get", w.document(), output, this);
    }

    private static String ctype(XmlDoc.Element ae) throws Throwable {
        String ctype = ae.value("content/type");
        String name = ae.value("name");
        if ("application/x-gzip".equalsIgnoreCase(ctype) && name != null && (name.toLowerCase().endsWith(".tar.gz")
                || name.toLowerCase().endsWith(".gtar") || name.toLowerCase().endsWith(".tgz"))) {
            ctype = "application/x-gtar";
        }
        return ctype;
    }

    private boolean needToUnarchive(String ctype) throws Throwable {
        if (_unarchive != Unarchive.NONE) {
            if (ArchiveRegistry.isAnArchive(ctype)) {
                return _unarchive == Unarchive.ALL || MFArchive.TYPE_AAR.equals(ctype);
            }
        }
        return false;
    }

    public static void submit(List<XmlDoc.Element> aes, PathTransformer<String, Path> pathTransformer,
            DataTransferListener<String, Path> dl, boolean overwrite, Unarchive unarchive, boolean restoreSymlinks,
            boolean csumCheck, boolean includeMetadata, MFSession session, Logger logger, ExecutorService workers)
            throws Throwable {
        if (aes != null) {
            for (XmlDoc.Element ae : aes) {
                AssetDownloadTask.submit(ae, pathTransformer, dl, overwrite, unarchive, restoreSymlinks, csumCheck,
                        includeMetadata, session, logger, workers);
            }
        }
    }

    public static void submit(XmlDoc.Element ae, PathTransformer<String, Path> pathTransformer,
            DataTransferListener<String, Path> dl, boolean overwrite, Unarchive unarchive, boolean restoreSymlinks,
            boolean csumCheck, boolean includeMetadata, MFSession session, Logger logger, ExecutorService workers)
            throws Throwable {

        String assetId = ae.value("@id");
        String assetPath = ae.value("path");
        long assetContentSize = ae.longValue("content/size", -1);
        Path file = pathTransformer.transformPath(assetPath);
        boolean isSymlinkAsset = SymlinkAsset.isSymlinkAsset(ae);
        if (!isSymlinkAsset && assetContentSize < 0) {
            dl.transferSkipped(assetPath, file);
            logger.info("Skipped asset " + assetId + ": '" + assetPath + "' No asset content found.");
            return;
        }

        if (Files.exists(file, LinkOption.NOFOLLOW_LINKS)) {
            if (overwrite) {
                if (isSymlinkAsset) {
                    if (!restoreSymlinks) {
                        dl.transferSkipped(assetPath, file);
                        logger.info("Skipped (symbolic link) asset " + assetId + ": '" + assetPath
                                + "'. Destination file: '" + file + "' already exists.");
                        return;
                    }
                    if (Files.isSymbolicLink(file)) {
                        if (Objects.equals(SymlinkAsset.getSymlinkTarget(ae),
                                Files.readSymbolicLink(file).toString())) {
                            dl.transferSkipped(assetPath, file);
                            logger.info("Skipped (symbolic link) asset " + assetId + ": '" + assetPath
                                    + "'. Destination (symbolic link) file: '" + file + "' already exists.");
                            return;
                        }
                    } else {
                        logger.warning("Failed to download (symbolic link) asset " + assetId + ": '" + assetPath
                                + "'. Destination file: '" + file + "' already exists and it is not a symbolic link.");
                        dl.transferFailed(assetPath, file);
                        return;
                    }
                } else {
                    long fileSize = Files.size(file);
                    if (fileSize == assetContentSize && !csumCheck) {
                        dl.transferSkipped(assetPath, file);
                        logger.info("Skipped asset " + assetId + ": '" + assetPath
                                + "' Destination file already exists and file sizes are match.");
                        return;
                    }
                }
            } else {
                dl.transferSkipped(assetPath, file);
                logger.info("Skipped asset " + assetId + ": '" + assetPath + "'. Already exists.");
                return;
            }
        }
        workers.submit(new AssetDownloadTask(session, logger, ae, file, unarchive, restoreSymlinks, csumCheck,
                includeMetadata, dl));
    }

}
