package unimelb.mf.client.sync.task;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

import arc.xml.XmlDoc;
import arc.xml.XmlStringWriter;
import unimelb.mf.client.file.PosixAttributes;
import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.session.MFSlicedFile;
import unimelb.mf.client.task.AbstractMFTask;
import unimelb.mf.client.util.AssetUtils;
import unimelb.utils.ChecksumUtils;

public class SlicedFileUploadTask extends AbstractMFTask {

    private Path _file;
    private int _nbSlices;
    private String _assetPath;
    private boolean _dstIsNamespace;
    private String _store;
    private boolean _csumCheck;
    private boolean _worm = false;
    private boolean _wormCanAddVersions = false;
    private boolean _wormCanMove = true;
    private int _retry;
    private DataTransferListener<Path, String> _ul;

    private String _assetId;
    private long _crc32;

    private ExecutorService _workers;

    public SlicedFileUploadTask(MFSession session, Logger logger, Path file, int nbSlices, String assetPath,
            boolean dstIsNamespace, String store, boolean csumCheck, boolean worm, boolean wormCanAddVersions,
            boolean wormCanMove, int retry, DataTransferListener<Path, String> ul, ExecutorService workers) {
        super(session, logger);
        _file = file;
        _nbSlices = nbSlices;
        _assetPath = assetPath;
        _dstIsNamespace = dstIsNamespace;
        _store = store; // Required by Cluster IO
        _csumCheck = csumCheck;
        _worm = worm;
        _wormCanAddVersions = wormCanAddVersions;
        _wormCanMove = wormCanMove;
        _retry = retry;
        _ul = ul;
        _workers = workers;
    }

    @Override
    public void execute() throws Throwable {
        try {
            _ul.transferStarted(_file, _assetPath);

            Future<Long> csumCheckResult = null;
            if (_csumCheck) {
                logger().log(Level.FINEST, "Calculating CRC32 checksum for file: '" + _file + "'");
                csumCheckResult = _workers.submit(new Callable<Long>() {

                    @Override
                    public Long call() throws Exception {
                        try {
                            return ChecksumUtils.getCRC32Value(_file);
                        } catch (Throwable t) {
                            throw (t instanceof Exception) ? (Exception) t : new Exception(t);
                        }
                    }
                });
            }

            final long fileSize = Files.size(_file);
            if (fileSize == 0) {
                _crc32 = 0;
                logWarning("File length of '" + _file.toString() + "' is 0.");
            }

            XmlStringWriter w = new XmlStringWriter();
            w.add("id", "path=" + _assetPath);
            w.add("create", new String[] { "in", _dstIsNamespace ? "namespace" : "collection" }, true);
            w.add("store", _store);

            PosixAttributes fileAttrs = PosixAttributes.read(_file);
            if (fileAttrs != null) {
                w.push("meta", new String[] { "action", "replace" });
                fileAttrs.save(w);
                w.pop();
            }
            setCurrentOperation("Uploading file: '" + _file + "' to asset: '" + _assetPath + "'");
            logInfo("Uploading file: '" + _file + "' (split into " + _nbSlices + " slices) to asset: '" + _assetPath
                    + "'");
            XmlDoc.Element re = session().execute("asset.set", w.document(),
                    new MFSlicedFile(_file.toFile(), _nbSlices), _store, _workers);
            boolean create = re.elementExists("id");
            boolean changedOrCreated = re
                    .booleanValue(create ? "id/@changed-or-created" : "version/@changed-or-created");

            w = new XmlStringWriter();
            w.add("id", "path=" + _assetPath);
            XmlDoc.Element ae = session().execute("asset.get", w.document()).element("asset");
            _assetId = ae.value("@id");
            int assetVersion = ae.intValue("@version");
            final long contentSize = ae.longValue("content/size");

            if (fileSize != contentSize) {
                logWarning("Asset (" + _assetId + ") content size: " + contentSize + " does not match file (" + _file
                        + ") size: " + fileSize + ".");
                if (_retry > 0) {
                    _retry--;
                    logWarning("Retry uploading " + _file);
                    execute();
                    return;
                } else {
                    String msg = "Uploaded asset(" + _assetId + ") content size: " + contentSize
                            + " does not match the local file (" + _file.toString() + ") size: " + fileSize;
                    throw new IOException(msg);
                }
            }

            if (_csumCheck) {
                long assetCRC32 = ae.longValue("content/csum[@base='16']", 0L, 16);
                _crc32 = csumCheckResult.get();
                if (_crc32 != assetCRC32) {
                    logWarning("CRC32 checksums do not match for file: '" + _file + "'(" + _crc32 + ") and asset: '"
                            + _assetPath + "'(" + assetCRC32 + ")");
                    if (_retry > 0) {
                        _retry--;
                        logWarning("Retry uploading " + _file);
                        execute();
                        return;
                    } else {
                        throw new IOException("CRC32 checksums do not match for file: '" + _file + "'(" + _crc32
                                + ") and asset: '" + _assetPath + "'(" + assetCRC32 + ")");
                    }
                }
            }

            if (fileSize != contentSize) {
                logWarning("Asset (" + _assetId + ") content size: " + contentSize + " does not match file (" + _file
                        + ") size: " + fileSize + ".");
                _crc32 = 0;
                if (_retry > 0) {
                    _retry--;
                    logWarning("Retry uploading " + _file);
                    execute();
                    return;
                } else {
                    String msg = "Uploaded asset(" + _assetId + ") content size: " + contentSize
                            + " does not match the local file (" + _file.toString() + ") size: " + fileSize;
                    throw new Exception(msg);
                }
            }

            if (_worm) {
                logInfo("Setting WORM state for asset(id= " + _assetId + "): '" + _assetPath + "' ...");
                AssetUtils.setWorm(session(), _assetId, _wormCanAddVersions, _wormCanMove);
            }
            _ul.transferProgressed(_file, _assetPath, fileSize);
            _ul.transferCompleted(_file, _assetPath);
            if (changedOrCreated) {
                logInfo(String.format("Uploaded file: '%s' to asset(id=%s): '%s'", _file.toString(), _assetId,
                        _assetPath) + (assetVersion > 1 ? " New version (" + assetVersion + ") created." : ""));
            } else {
                logInfo(String.format("Asset(id=%s,version=%d): '%s' exists and no change made.", _assetId,
                        assetVersion, _assetPath));
            }
        } catch (Throwable e) {
            _ul.transferFailed(_file, _assetPath);
            throw e;
        }
    }
}
