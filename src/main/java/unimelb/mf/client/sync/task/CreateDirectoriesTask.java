package unimelb.mf.client.sync.task;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.logging.Logger;

public class CreateDirectoriesTask implements Callable<Void> {

    private Logger _logger;
    private List<Path> _dirs;

    public CreateDirectoriesTask(List<Path> dirs, Logger logger) {
        _dirs = dirs;
        _logger = logger;
    }

    @Override
    public Void call() throws Exception {
        if (_dirs != null) {
            for (Path dir : _dirs) {
                if (!Files.exists(dir) || !Files.isDirectory(dir)) {
                    Files.createDirectories(dir);
                    _logger.info("Created directory: '" + dir + "'");
                }
            }
        }
        return null;
    }

}
