package unimelb.mf.client.sync.task;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.logging.Logger;
import java.util.zip.CRC32;
import java.util.zip.CheckedOutputStream;

import arc.archive.ArchiveInput;
import arc.archive.ArchiveRegistry;
import arc.mf.client.ServerClient;
import arc.mf.client.archive.Archive;
import arc.mime.NamedMimeType;
import arc.streams.LongInputStream;
import arc.streams.NonCloseInputStream;
import arc.streams.NullOutputStream;
import arc.streams.StreamCopy;
import arc.streams.StreamUtil;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlStringWriter;
import unimelb.mf.client.archive.MFArchive;
import unimelb.mf.client.session.MFOutputConsumer;
import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.sync.settings.PathTransformer;
import unimelb.mf.client.sync.task.AssetDownloadTask.Unarchive;
import unimelb.mf.client.task.AbstractMFTask;
import unimelb.utils.FileUtils;

public class AggregatedDownloadTask extends AbstractMFTask {

	private PathTransformer<String, Path> _pathTransformer;
	private Map<String, XmlDoc.Element> _assets;
	private boolean _overwrite;
	private Unarchive _unarchive;
	private boolean _csumCheck;
	private boolean _includeMetadata;
	private DataTransferListener<String, Path> _dl;

	protected AggregatedDownloadTask(MFSession session, Logger logger, PathTransformer<String, Path> pathTransformer,
			List<XmlDoc.Element> assets, boolean overwrite, Unarchive unarchive, boolean csumCheck,
			boolean includeMetadata, DataTransferListener<String, Path> dl) throws Throwable {
		super(session, logger);
		_pathTransformer = pathTransformer;
		_assets = new LinkedHashMap<>();
		if (assets != null) {
			for (XmlDoc.Element asset : assets) {
				String assetPath = asset.value("path");
				_assets.put(assetPath, asset);
			}
		}
		_overwrite = overwrite;
		_unarchive = unarchive;
		_csumCheck = csumCheck;
		_includeMetadata = includeMetadata;
		_dl = dl;
	}

	protected AggregatedDownloadTask(MFSession session, Logger logger, PathTransformer<String, Path> pathTransformer,
			Map<String, XmlDoc.Element> assets, boolean overwrite, Unarchive unarchive, boolean csumCheck,
			boolean includeMetadata, DataTransferListener<String, Path> dl) throws Throwable {
		super(session, logger);
		_pathTransformer = pathTransformer;
		_assets = assets;
		_overwrite = overwrite;
		_unarchive = unarchive;
		_csumCheck = csumCheck;
		_includeMetadata = includeMetadata;
		_dl = dl;
	}

	@Override
	public void execute() throws Throwable {
		if (_assets.isEmpty()) {
			return;
		}

		ServerClient.Output output = new MFOutputConsumer() {
			@Override
			protected void consume(LongInputStream in, Element re) throws Throwable {
				Archive.declareSupportForAllTypes();
				ArchiveInput ai = ArchiveRegistry.createInput(new NonCloseInputStream(in),
						new NamedMimeType(MFArchive.TYPE_AAR));
				try {
					try {
						for (ArchiveInput.Entry e = ai.next(); e != null; e = ai.next()) {
							if (!e.isDirectory()) {
								processEntry(e);
							}
						}
					} finally {
						// ArchiveInput.discardToEndOfStream(in);
						if (in.remaining() > 0) {
							StreamUtil.skipFully(in, in.remaining());
						}
					}
				} finally {
					ai.close();
				}
			}
		};

		XmlStringWriter w = new XmlStringWriter();
		w.add("where", constructQuery());
		w.add("for", "user");
		w.add("versions", "match");
		w.add("clevel", 0);
		w.add("format", "aarv2");
		w.add("parts", "content");
		session().execute("asset.archive.create", w.document(), null, output);

	}

	private void processEntry(ArchiveInput.Entry e) throws Throwable {
		String assetPath = "/" + e.name();
		XmlDoc.Element ae = _assets.get(assetPath);
		if (ae == null) {
			logWarning("Unexpected asset: '" + assetPath + "'");
			return;
		}
		Path file = _pathTransformer.transformPath(assetPath);
		if (!Files.exists(file, LinkOption.NOFOLLOW_LINKS) || _overwrite) {
			String ctype = ctype(ae);
			if (_unarchive != Unarchive.NONE && ArchiveRegistry.isAnArchive(ctype)
					&& (_unarchive == Unarchive.ALL || MFArchive.TYPE_AAR.equals(ctype))) {
				// unarchive
				Path dstDir = removeArchiveFileExtension(file);
				extractArchive(assetPath, e.stream(), ctype, dstDir);
			} else {
				Long assetChecksum = ae.longValue("content/csum[@base='10']", null);
				downloadFile(assetPath, assetChecksum, e.stream(), file);
			}
		} else {
			_dl.transferSkipped(assetPath, file);
		}
		if (_includeMetadata) {
			Path metaFile = Paths.get(file.toString() + AssetDownloadTask.METADATA_FILE_SUFFIX);
			boolean metaFileExists = Files.exists(metaFile, LinkOption.NOFOLLOW_LINKS);
			if (!metaFileExists || _overwrite) {
				if (metaFileExists && _overwrite) {
					logWarning("Metadata file: '" + metaFile + "' already exists. Overwriting...");
				}
				try (BufferedWriter w = Files.newBufferedWriter(metaFile)) {
					logInfo((metaFileExists ? "Updating" : "Creating") + " metadata file: '" + metaFile + "'");
					w.write(ae.toString());
					w.flush();
				}
			}
		}
	}

	private void downloadFile(String assetPath, Long assetChecksum, LongInputStream in, Path file) throws Throwable {
		try {
			_dl.transferStarted(assetPath, file);
			FileUtils.createParentDirectories(file);
			boolean doChecksumCheck = _csumCheck && assetChecksum != null;
			Long fileChecksum = null;
			try (BufferedOutputStream bos = new BufferedOutputStream(Files.newOutputStream(file));
					OutputStream os = doChecksumCheck ? new CheckedOutputStream(bos, new CRC32()) : bos) {
				logInfo("Downloading file: '" + file + "'");
				StreamCopy.copy(in, os);
				if (doChecksumCheck) {
					fileChecksum = ((CheckedOutputStream) os).getChecksum().getValue();
				}
			}
			if (!doChecksumCheck || Objects.equals(assetChecksum, fileChecksum)) {
				_dl.transferProgressed(assetPath, file, Files.size(file));
				_dl.transferCompleted(assetPath, file);
			} else {
				logError(String.format("asset (%s) checksum: %x does not match file (%s) checksum: %x", assetPath,
						assetChecksum, file.toString(), fileChecksum));
				_dl.transferFailed(assetPath, file);
			}
		} catch (Throwable t) {
			_dl.transferFailed(assetPath, file);
			throw t;
		}
	}

	private void extractArchive(String assetPath, LongInputStream in, String ctype, Path dstDir) throws Throwable {
		long assetCSize = in.length();
		Files.createDirectories(dstDir);
		Archive.declareSupportForAllTypes();
		ArchiveInput ai = ArchiveRegistry.createInput(in, new NamedMimeType(ctype));
		try {
			for (ArchiveInput.Entry e = ai.next(); e != null; e = ai.next()) {
				Path f = Paths.get(dstDir.toString(), e.name());
				if (e.isDirectory()) {
					Files.createDirectories(f);
				} else {
					if (!Files.exists(f) || _overwrite) {
						logInfo("Extracting file: '" + f + "'");
						try (BufferedOutputStream bos = new BufferedOutputStream(Files.newOutputStream(f))) {
							StreamCopy.copy(e.stream(), bos);
						}
					} else {
						logInfo("File: '" + f + "' already exists. No overwrite.");
						StreamCopy.copy(e.stream(), new NullOutputStream());
					}
				}
			}
			_dl.transferProgressed(assetPath, dstDir, assetCSize);
			_dl.transferCompleted(assetPath, dstDir);
		} catch (Throwable e) {
			_dl.transferFailed(assetPath, dstDir);
		} finally {
			ai.close();
		}
	}

	private static String ctype(XmlDoc.Element ae) throws Throwable {
		String ctype = ae.value("content/type");
		String name = ae.value("name");
		if ("application/x-gzip".equalsIgnoreCase(ctype) && name != null && (name.toLowerCase().endsWith(".tar.gz")
				|| name.toLowerCase().endsWith(".gtar") || name.toLowerCase().endsWith(".tgz"))) {
			ctype = MFArchive.TYPE_GZIPPED_TAR;
		}
		return ctype;
	}

	private static Path removeArchiveFileExtension(Path archiveFile) {
		String path = archiveFile.toString();
		String lcPath = path.toLowerCase();
		if (lcPath.endsWith(".gtar")) {
			return Paths.get(path.substring(0, path.length() - 5));
		} else if (lcPath.endsWith(".tar.gz")) {
			return Paths.get(path.substring(0, path.length() - 7));
		} else if (lcPath.endsWith(".zip") || lcPath.endsWith(".jar") || lcPath.endsWith(".tar")
				|| lcPath.endsWith(".aar")) {
			return Paths.get(path.substring(0, path.length() - 4));
		} else {
			return archiveFile;
		}
	}

	private String constructQuery() throws Throwable {
		StringBuilder sb = new StringBuilder();
		Collection<XmlDoc.Element> aes = _assets.values();
		for (XmlDoc.Element ae : aes) {
			String assetId = ae.value("@id");
			if (sb.length() > 0) {
				sb.append(" or ");
			}
			sb.append("id=").append(assetId);
		}
		return sb.toString();
	}

	public static void submit(Map<String, XmlDoc.Element> aes, PathTransformer<String, Path> pathTransformer,
			DataTransferListener<String, Path> dl, boolean overwrite, Unarchive unarchive, boolean csumCheck,
			boolean includeMetadata, MFSession session, Logger logger, ExecutorService workers) throws Throwable {
		AggregatedDownloadTask task = new AggregatedDownloadTask(session, logger, pathTransformer, aes, overwrite,
				unarchive, csumCheck, includeMetadata, dl);
		workers.submit(task);
	}
}
