package unimelb.mf.client.sync.task;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.logging.Logger;

import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.sync.check.AssetItem;
import unimelb.mf.client.sync.check.CheckHandler;
import unimelb.mf.client.sync.check.CheckResult;
import unimelb.mf.client.sync.settings.PathTransformer;
import unimelb.mf.client.task.AbstractMFTask;
import unimelb.utils.ChecksumUtils;
import unimelb.utils.ChecksumUtils.ChecksumType;

public class AssetSetCheckTask extends AbstractMFTask {

    private final List<AssetItem> _assets;
    private final PathTransformer<String, Path> _pathTransformer;
    private final boolean _csumCheck;
    private final CheckHandler _rh;
    private final ExecutorService _workers;

    public AssetSetCheckTask(MFSession session, Logger logger, List<AssetItem> assets,
                             PathTransformer<String, Path> pathTransformer, boolean csumCheck, CheckHandler rh,
                             ExecutorService workers) {
        super(session, logger);
        _assets = assets;
        _pathTransformer = pathTransformer;
        _csumCheck = csumCheck;
        _rh = rh;
        _workers = workers;
    }

    @Override
    public void execute() throws Throwable {
        for (AssetItem ai : _assets) {
            Path file = _pathTransformer.transformPath(ai.assetPath());

            // not exist
            if (!Files.exists(file)) {
                _rh.checked(new CheckResult(
                        ai.assetPath(), ai.length(), ai.checksum(ChecksumType.CRC32), ai.symlinkTarget(),
                        file, null, null, null,
                        false, _csumCheck));
                incCompletedOperations();
                continue;
            }

            // symbolic link
            if (ai.symlinkTarget() != null) {
                // symbolic link
                if (Files.isSymbolicLink(file)) {
                    String fileSymlinkTarget = Files.readSymbolicLink(file).toString();
                    if (ai.symlinkTarget().equals(fileSymlinkTarget)) {
                        _rh.checked(new CheckResult(ai.assetPath(), null, null, ai.symlinkTarget(),
                                file, null, null, fileSymlinkTarget,
                                true, _csumCheck));
                    } else {
                        _rh.checked(new CheckResult(ai.assetPath(), null, null, ai.symlinkTarget(),
                                file, null, null, fileSymlinkTarget,
                                true, _csumCheck));
                    }
                } else {
                    _rh.checked(new CheckResult(ai.assetPath(), null, null, ai.symlinkTarget(),
                            file, getFileSize(file), ChecksumUtils.getCRC32(file), null,
                            true, _csumCheck));
                }
                incCompletedOperations();
                continue;
            }

            // regular file
            final Long fileSize = getFileSize(file);
            String assetChecksum = ai.checksum(ChecksumType.CRC32);
            if (_csumCheck) {
                if (fileSize == null || fileSize == 0) {
                    _rh.checked(new CheckResult(ai.assetPath(), ai.length(), assetChecksum, ai.symlinkTarget(),
                            file, fileSize, null, null,
                            true, _csumCheck));
                } else {
                    _workers.submit(new FileCsumCalcTask(session(), logger(), file,
                            ChecksumUtils.ChecksumType.CRC32,
                            (f, checksum, checksumType) -> _rh.checked(new CheckResult(
                                    ai.assetPath(), ai.length(), assetChecksum, null,
                                    f, fileSize, checksum, null,
                                    true, _csumCheck))));
                }
            } else {
                _rh.checked(new CheckResult(ai.assetPath(), ai.length(), assetChecksum, ai.symlinkTarget(),
                        file, fileSize, null, null, true, _csumCheck));
            }
            incCompletedOperations();
        }
    }

    private static Long getFileSize(Path file) {
        try {
            return Files.size(file);
        } catch (Throwable e) {
            System.err.println("Error: fail to retrieve length of file: '" + file + "'");
            e.printStackTrace();
            return null;
        }
    }

}
