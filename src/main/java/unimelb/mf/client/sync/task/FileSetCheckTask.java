package unimelb.mf.client.sync.task;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.logging.Logger;

import arc.xml.XmlDoc;
import arc.xml.XmlStringWriter;
import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.sync.check.AssetItem;
import unimelb.mf.client.sync.check.CheckHandler;
import unimelb.mf.client.sync.check.CheckResult;
import unimelb.mf.client.sync.settings.upstream.UploadCheckJob;
import unimelb.mf.client.task.AbstractMFTask;
import unimelb.utils.ChecksumUtils;
import unimelb.utils.ChecksumUtils.ChecksumType;

public class FileSetCheckTask extends AbstractMFTask {

    private final UploadCheckJob _job;
    private final Map<Path, String> _fileAssets;
    private final Map<String, Path> _assetFiles;
    private final boolean _csumCheck;
    private final CheckHandler _rh;
    private final ExecutorService _workers;
    private final boolean _followSymlinks;

    public FileSetCheckTask(MFSession session, Logger logger, List<Path> files, boolean followSymlinks,
                            UploadCheckJob job, boolean csumCheck, CheckHandler rh, ExecutorService workers) {
        super(session, logger);
        _job = job;
        _workers = workers;
        _csumCheck = csumCheck;
        _rh = rh;
        _fileAssets = new LinkedHashMap<>();
        _assetFiles = new LinkedHashMap<>();

        if (files != null) {
            for (Path file : files) {
                String assetPath = job.transformPath(file);
                _fileAssets.put(file, assetPath);
                _assetFiles.put(assetPath, file);
            }
        }
        _followSymlinks = followSymlinks;
    }

    @Override
    public void execute() throws Throwable {
        if (_assetFiles.isEmpty()) {
            return;
        }
        setTotalOperations(_assetFiles.size());
        logger().info("Checking " + _assetFiles.size() + " files...");
        XmlStringWriter w1 = new XmlStringWriter();
        List<String> assetPaths = new ArrayList<>(_fileAssets.values());
        for (String assetPath : assetPaths) {
            w1.add("id", "path=" + assetPath);
        }
        List<XmlDoc.Element> ees = session().execute("asset.exists", w1.document()).elements("exists");

        int nbExists = 0;
        XmlStringWriter w2 = new XmlStringWriter();
        for (int i = 0; i < ees.size(); i++) {
            XmlDoc.Element ee = ees.get(i);
            String assetPath = assetPaths.get(i);
            boolean exists = ee.booleanValue();
            if (!exists) {
                Path file = _assetFiles.get(assetPath);
                Long fileSize = getFileSizeNE(file);
                String fileChecksum = _csumCheck ? getCRC32NE(file) : null;
                String fileSymlinkTarget = getFileSymlinkTarget(file);
                _rh.checked(new CheckResult(
                        file, fileSize, fileChecksum, fileSymlinkTarget,
                        assetPath, null, null, null,
                        false, _csumCheck));
                incCompletedOperations();
            } else {
                w2.add("id", "path=" + assetPath);
                nbExists++;
            }
        }
        if (nbExists > 0) {
            List<XmlDoc.Element> aes = session().execute("asset.get", w2.document()).elements("asset");
            for (XmlDoc.Element ae : aes) {

                AssetItem ai = new AssetItem(ae, _job.dstCollection().path());
                Path file = _assetFiles.get(ai.assetPath());
                if (file == null) {
                    throw new AssertionError("Could not find file matching asset: '" + ai.assetPath() + "'");
                }

                // symbolic link
                if (Files.isSymbolicLink(file) && !_followSymlinks) {
                    String fileSymlinkTarget = getFileSymlinkTarget(file);
                    _rh.checked(new CheckResult(file, null, null, fileSymlinkTarget,
                            ai.assetPath(), null, null, ai.symlinkTarget(),
                            true, _csumCheck));
                    continue;
                }

                // regular file
                Long fileSize = getFileSizeNE(file);
                String assetChecksum = ai.checksum(ChecksumType.CRC32);
                boolean bothZeroSized = fileSize != null && fileSize == 0 && ai.length() == 0;
                if (_csumCheck && !bothZeroSized) {
                    _workers.submit(new FileCsumCalcTask(session(), logger(), file, ChecksumType.CRC32,
                            (f, checksum, checksumType) -> _rh.checked(new CheckResult(
                                    f, fileSize, checksum, null,
                                    ai.assetPath(), ai.length(), assetChecksum, null,
                                    true, _csumCheck))));
                } else {
                    _rh.checked(new CheckResult(
                            file, fileSize, null, null,
                            ai.assetPath(), ai.length(), null, null,
                            true, _csumCheck));
                }
                incCompletedOperations();
            }
        }
    }

    private static String getFileSymlinkTarget(Path file) {
        if (Files.isSymbolicLink(file)) {
            try {
                Path target = Files.readSymbolicLink(file);
                if (target != null) {
                    return target.toString();
                } else {
                    return null;
                }
            } catch (Throwable e) {
                System.err.println("Failed to resolve target of symbolic link: " + file);
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }

    private static Long getFileSizeNE(Path file) {
        try {
            return Files.size(file);
        } catch (Throwable e) {
            System.err.println("Warning: fail to retrieve length of file: '" + file + "'");
            e.printStackTrace();
            return null;
        }
    }

    private static String getCRC32NE(Path file) {
        try {
            return ChecksumUtils.getCRC32(file);
        } catch (Throwable t) {
            System.err.println("Warning: fail to read file: '" + file + "' to calculate CRC32 checksum.");
            t.printStackTrace();
            return null;
        }
    }
}
