package unimelb.mf.client.sync.task;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.FileVisitOption;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.EnumSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import arc.archive.ArchiveOutput;
import arc.archive.ArchiveRegistry;
import arc.mf.client.ServerClient;
import arc.mf.client.archive.Archive;
import arc.streams.StreamCopy.AbortCheck;
import arc.xml.XmlStringWriter;
import unimelb.mf.client.archive.MFArchive;
import unimelb.mf.client.session.MFGeneratedInput;
import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.task.AbstractMFTask;
import unimelb.mf.client.util.AssetNamespaceUtils;
import unimelb.mf.client.util.AssetUtils;
import unimelb.mf.client.util.collection.CollectionType;
import unimelb.utils.PathUtils;

public class FileSetArchiveCreateTask extends AbstractMFTask {

    private Set<Path> _files;
    private MFArchive.Type _archiveType;
    private String _assetPath;
    private int _compressionLevel = 6;
    private boolean _analyze = false;

    public FileSetArchiveCreateTask(MFSession session, Logger logger, List<Path> files, String assetPath,
            int compressionLevel, boolean analyze) {
        super(session, logger);
        _files = new LinkedHashSet<>();
        if (files != null && !files.isEmpty()) {
            _files.addAll(files);
        }
        _archiveType = archiveTypeFromPath(assetPath, MFArchive.Type.ZIP);
        _assetPath = assetPath.toLowerCase().endsWith(_archiveType.extension) ? assetPath
                : (assetPath + "." + _archiveType.extension);
        _compressionLevel = compressionLevel;
        _analyze = analyze;
    }

    private static MFArchive.Type archiveTypeFromPath(String filePath, MFArchive.Type defaultType) {
        if (filePath != null) {
            String lcFileName = PathUtils.getFileName(filePath).toLowerCase();
            if (lcFileName.endsWith(".tar.gz") || lcFileName.endsWith(".gtar") || lcFileName.endsWith(".tgz")) {
                return MFArchive.Type.GZIPPED_TAR;
            } else if (lcFileName.endsWith(".zip")) {
                return MFArchive.Type.ZIP;
            } else if (lcFileName.endsWith(".tar")) {
                return MFArchive.Type.TAR;
            } else if (lcFileName.endsWith(".jar")) {
                return MFArchive.Type.JAR;
            } else if (lcFileName.endsWith(".aar")) {
                return MFArchive.Type.AAR;
            }
        }
        return defaultType;
    }

    @Override
    public void execute() throws Throwable {
        Archive.declareSupportForAllTypes();
        Path baseDir = _files.size() == 1 ? _files.iterator().next().getParent()
                : PathUtils.getLongestCommonParent(_files);
        String source = _files.size() == 1 ? _files.iterator().next().toString() : null;
        String store = getAssetStoreFor(session(), _assetPath);
        store = store == null ? null : ("asset:" + store);
        ServerClient.Input input = new MFGeneratedInput(_archiveType.mimeType, _archiveType.extension, source, -1,
                store) {

            @Override
            protected void consume(OutputStream out, AbortCheck ac) throws Throwable {
                ArchiveOutput ao = ArchiveRegistry.createOutput(out, _archiveType.mimeType, _compressionLevel, null);
                try {
                    for (Path f : _files) {
                        add(ao, f, baseDir);
                    }
                } finally {
                    ao.close();
                }
            }
        };

        CollectionType parentType = CollectionType.collectionTypeFor(PathUtils.getParentPath(_assetPath), session());

        XmlStringWriter w = new XmlStringWriter();
        w.add("id", "path=" + _assetPath);
        w.add("create", new String[] { "in", parentType.shortName() }, true);
        w.add("analyze", _analyze);
        session().execute("asset.set", w.document(), input);
        String assetId = AssetUtils.getAssetMetaByPath(session(), _assetPath).value("@id");
        System.out.println("Imported Mediaflux asset(id=" + assetId + "): " + _assetPath);
    }

    private void add(ArchiveOutput ao, Path f, Path baseDir) throws Throwable {
        if (!Files.isDirectory(f)) {
            addFile(ao, f, baseDir);
        } else {
            addDirectory(ao, f, baseDir);
        }
    }

    private void addFile(ArchiveOutput ao, Path f, Path baseDir) throws Throwable {
        String name;
        if (baseDir == null) {
            name = f.getFileName().toString();
        } else {
            String filePath = f.toString();
            String baseDirPath = baseDir.toString();
            if (filePath.startsWith(baseDirPath)) {
                name = filePath.substring(baseDirPath.length() + 1);
            } else {
                baseDir = PathUtils.getLongestCommonParent(f, baseDir);
                baseDirPath = baseDir == null ? null : baseDir.toString();
                name = baseDir == null ? filePath : filePath.substring(baseDirPath.length() + 1);
            }
        }
        logger().info("Adding '" + f + "'");
        ao.add(null, name, f.toFile());
    }

    private void addDirectory(ArchiveOutput ao, Path dir, Path baseDir) throws Throwable {
        Files.walkFileTree(dir, EnumSet.noneOf(FileVisitOption.class), Integer.MAX_VALUE,
                new SimpleFileVisitor<Path>() {

                    @Override
                    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
                        try {
                            addFile(ao, file, baseDir);
                        } catch (Throwable e) {
                            logger().log(Level.SEVERE, e.getMessage(), e);
                        }
                        return FileVisitResult.CONTINUE;
                    }

                    @Override
                    public FileVisitResult visitFileFailed(Path file, IOException ioe) {
                        logger().log(Level.SEVERE, "Failed to access file: " + file, ioe);
                        return FileVisitResult.CONTINUE;
                    }

                    @Override
                    public FileVisitResult postVisitDirectory(Path dir, IOException ioe) {
                        if (ioe != null) {
                            logger().log(Level.SEVERE, "[postVisitDirectory] " + dir + ": " + ioe.getMessage(), ioe);
                        }
                        return FileVisitResult.CONTINUE;
                    }

                    @Override
                    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                        return super.preVisitDirectory(dir, attrs);
                    }
                });
    }

    private static String getAssetStoreFor(MFSession session, String assetPath) throws Throwable {
        String closestExistingAssetNamespace = getClosestExistingAssetNamespace(session, assetPath);
        if (closestExistingAssetNamespace != null) {
            String store = AssetNamespaceUtils.getAssetNamespaceStore(session, closestExistingAssetNamespace);
            return store;
        }
        return null;
    }

    private static String getClosestExistingAssetNamespace(MFSession session, String assetPath) throws Throwable {
        if (assetPath != null) {
            Path path = Paths.get(assetPath.startsWith("/") ? assetPath : ("/" + assetPath));
            Path parentPath = path.getParent();
            while (parentPath != null) {
                boolean exists = AssetNamespaceUtils.assetNamespaceExists(session, parentPath.toString());
                if (exists) {
                    return parentPath.toString();
                } else {
                    parentPath = parentPath.getParent();
                }
            }
        }
        return null;
    }
}
