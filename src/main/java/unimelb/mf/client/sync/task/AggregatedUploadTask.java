package unimelb.mf.client.sync.task;

import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import arc.archive.ArchiveOutput;
import arc.archive.ArchiveRegistry;
import arc.mf.client.ServerClient;
import arc.mf.client.archive.Archive;
import arc.streams.SizedInputStream;
import arc.streams.StreamCopy.AbortCheck;
import arc.xml.XmlDoc;
import arc.xml.XmlStringWriter;
import unimelb.mf.client.archive.MFArchive;
import unimelb.mf.client.file.PosixAttributes;
import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.session.MFGeneratedInput;
import unimelb.mf.client.task.AbstractMFTask;
import unimelb.utils.ChecksumUtils;

public class AggregatedUploadTask extends AbstractMFTask {

    private Map<String, Path> _assetFiles;
    private String _store;
    private boolean _csumCheck;
    private boolean _worm = false;
    private boolean _wormCanAddVersions = false;
    private boolean _wormCanMove = true;
    private int _retry;
    private DataTransferListener<Path, String> _ul;

    protected AggregatedUploadTask(MFSession session, Logger logger, Map<String, Path> assetFiles, String store,
            boolean csumCheck, boolean worm, boolean wormCanAddVersions, boolean wormCanMove, int retry,
            DataTransferListener<Path, String> ul) {
        super(session, logger);
        _assetFiles = assetFiles;
        _store = store;
        _csumCheck = csumCheck;
        _worm = worm;
        _wormCanAddVersions = wormCanAddVersions;
        _wormCanMove = wormCanMove;
        _retry = retry;
        _ul = ul;
    }

    @Override
    public void execute() throws Throwable {

        if (_assetFiles == null || _assetFiles.isEmpty()) {
            return;
        }
        try {
            aggregateUpload(_retry);
            Set<String> assetPaths = _assetFiles.keySet();
            for (String assetPath : assetPaths) {
                Path f = _assetFiles.get(assetPath);
                _ul.transferProgressed(f, assetPath, Files.size(f));
                _ul.transferCompleted(f, assetPath);
            }
        } catch (Throwable e) {
            _assetFiles.forEach((assetPath, f) -> {
                _ul.transferFailed(f, assetPath);
            });
        }

    }

    private void aggregateUpload(int retry) throws Throwable {
        try {
            doAggregateUpload();
        } catch (Throwable e) {
            if (retry > 0) {
                Thread.sleep(100);
                aggregateUpload(retry - 1);
            } else {
                throw e;
            }
        }
    }

    private void doAggregateUpload() throws Throwable {

        ServerClient.Input input = new MFGeneratedInput(MFArchive.TYPE_AAR, MFArchive.Type.AAR.extension, null, -1,
                _store == null ? null : ("asset:" + _store)) {
            @Override
            protected void consume(OutputStream out, AbortCheck ac) throws Throwable {
                Archive.declareSupportForAllTypes();
                ArchiveOutput ao = ArchiveRegistry.createOutput(out, MFArchive.TYPE_AAR, 0, null);
                try {
                    Set<String> assetPaths = _assetFiles.keySet();
                    for (String assetPath : assetPaths) {
                        Path f = _assetFiles.get(assetPath);
                        try (InputStream is = Files.newInputStream(f);
                                BufferedInputStream bis = new BufferedInputStream(is);
                                SizedInputStream sis = new SizedInputStream(bis, Files.size(f))) {
                            logInfo("Uploading file: '" + f + "'");
                            ao.add(null, assetPath, sis);
                        }
                    }
                } finally {
                    ao.close();
                }
            }
        };

        XmlStringWriter w = new XmlStringWriter();
        w.add("mode", "live");
        w.add("threads", 1);
        w.add("update", true);
        w.add("store", _store);
        // w.add("analyze", true);
        session().execute("asset.import", w.document(), input);

        /*
         * Update asset posix attrs.
         */
        w = new XmlStringWriter();
        Set<String> assetPaths = _assetFiles.keySet();
        for (String assetPath : assetPaths) {
            Path f = _assetFiles.get(assetPath);
            PosixAttributes fileAttrs = PosixAttributes.read(f);
            w.push("service", new String[] { "name", "asset.set" });
            w.add("id", "path=" + assetPath);
            w.push("meta", new String[] { "action", "replace" });
            fileAttrs.save(w);
            w.pop();
            w.pop();
        }
        session().execute("service.execute", w.document());

        /*
         * set WORM
         */
        if (_worm) {
            for (String assetPath : assetPaths) {
                w = new XmlStringWriter();
                w.push("service", new String[] { "name", "asset.worm.set" });
                w.add("id", "path=" + assetPath);
                w.add("can-add-versions", _wormCanAddVersions);
                w.add("can-move", _wormCanMove);
                w.pop();
            }
            session().execute("service.execute", w.document());
        }

        /*
         * Checksum check
         */
        if (_csumCheck) {
            w = new XmlStringWriter();
            for (String assetPath : assetPaths) {
                w.add("id", "path=" + assetPath);
            }
            List<XmlDoc.Element> aes = session().execute("asset.get", w.document()).elements("asset");
            if (aes == null || aes.size() != _assetFiles.size()) {
                throw new Exception("The number of uploaded assets (" + (aes == null ? 0 : aes.size())
                        + ") does not the match number of source files (" + _assetFiles.size() + ").");
            }
            for (XmlDoc.Element ae : aes) {
                String assetPath = ae.value("path");
                Path f = _assetFiles.get(assetPath);
                if (f == null) {
                    throw new FileNotFoundException("Could not find source file for asset: '" + assetPath + "'");
                }
                Long assetCSize = ae.longValue("content/size", null);
                Long assetChecksum = ae.longValue("content/csum[@base='10']", null);
                long fileSize = Files.size(f);
                long fileChecksum = ChecksumUtils.getCRC32Value(f);
                boolean contentMatch = assetCSize != null && assetCSize == fileSize && assetChecksum != null
                        && assetChecksum == fileChecksum;
                if (!contentMatch) {
                    throw new IOException("Content mismatch: file(size=" + fileSize + ", crc32="
                            + Long.toHexString(fileChecksum) + ") vs. asset(size=" + assetCSize + ", crc32="
                            + (assetChecksum != null ? Long.toHexString(assetChecksum) : "null") + ")");
                }
            }
        }
    }

}
