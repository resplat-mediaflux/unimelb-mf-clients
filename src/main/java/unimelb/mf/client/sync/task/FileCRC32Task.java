package unimelb.mf.client.sync.task;

import java.nio.file.Path;
import java.util.logging.Logger;

import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.task.AbstractMFTask;
import unimelb.utils.ChecksumUtils;

public class FileCRC32Task extends AbstractMFTask {

    private final Path _file;
    private final String _assetPath;
    private final boolean _dstIsNamespace;
    private final String _dstStore;
    private final long _assetCRC32;
    private final boolean _worm;
    private final boolean _wormCanAddVersions;
    private final boolean _wormCanMove;

    private final boolean _saveFileAttrs;
    private final int _maxNumberOfRetries;
    private final long _retryWaitTime;
    private final boolean _preserveModifiedTime;

    private final DataTransferListener<Path, String> _ul;

    public FileCRC32Task(MFSession session, Logger logger, Path file, String assetPath, boolean dstIsNamespace,
            String store, long assetCRC32, boolean worm, boolean wormCanAddVersions, boolean wormCanMove,
            boolean saveFileAttrs, int maxNumberOfRetries, long retryWaitTime, boolean preserveModifiedTime,
            DataTransferListener<Path, String> ul) {
        super(session, logger);
        _file = file;
        _assetPath = assetPath;
        _dstIsNamespace = dstIsNamespace;
        _dstStore = store; // Required by Cluster IO
        _assetCRC32 = assetCRC32;
        _worm = worm;
        _wormCanAddVersions = wormCanAddVersions;
        _wormCanMove = wormCanMove;
        _saveFileAttrs = saveFileAttrs;
        _maxNumberOfRetries = maxNumberOfRetries;
        _retryWaitTime = retryWaitTime;
        _preserveModifiedTime = preserveModifiedTime;
        _ul = ul;
    }

    @Override
    public void execute() throws Throwable {

        Long fileCRC32;
        logInfo("Generating CRC32 checksum for file: '" + _file + "'");
        try {
            fileCRC32 = ChecksumUtils.getCRC32Value(_file);
        } catch (Throwable e) {
            logError("Cannot generate checksum: Failed to read file: '" + _file + "'", e);
            if (_ul != null) {
                _ul.transferFailed(_file, _assetPath);
            }
            return;
        }

        if (fileCRC32 != _assetCRC32) {
            logWarning("CRC32 checksum does not match! Uploading the local file to create a new version.");
            // NOTE: run upload task on the current thread instead of resubmit to the
            // workers queue, because the workers queue may have already been shutdown and
            // no longer accept new tasks.
            new FileUploadTask(session(), logger(), _file, _assetPath, _dstIsNamespace, _dstStore, true, _worm,
                    _wormCanAddVersions, _wormCanMove, _saveFileAttrs, _maxNumberOfRetries, _retryWaitTime, _preserveModifiedTime, _ul)
                    .execute();
        } else {
            if (_ul != null) {
                _ul.transferSkipped(_file, _assetPath);
            }
            logInfo("Asset '" + _assetPath + "' already exists and CRC32 checksums match. Skipped.");
        }

    }

}
