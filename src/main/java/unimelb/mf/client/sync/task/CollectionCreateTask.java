package unimelb.mf.client.sync.task;

import java.util.logging.Logger;

import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.task.AbstractMFTask;
import unimelb.mf.client.util.AssetNamespaceUtils;
import unimelb.mf.client.util.AssetUtils;

public class CollectionCreateTask extends AbstractMFTask {

    private String _collectionPath;
    private boolean _isAssetNamespace;

    /**
     * 
     * @param session
     * @param logger
     * @param assetNamespacePath
     */
    public CollectionCreateTask(MFSession session, Logger logger, String collectionPath, boolean isAssetNamespace) {
        super(session, logger);
        _collectionPath = collectionPath;
        _isAssetNamespace = isAssetNamespace;
    }

    @Override
    public void execute() throws Throwable {
        if (_isAssetNamespace) {
            logInfo("Creating asset namespace: '" + _collectionPath + "'");
            AssetNamespaceUtils.createAssetNamespace(session(), _collectionPath, true, true);
            logInfo("Created asset namespace: '" + _collectionPath + "'");
        } else {
            logInfo("Creating collection asset: '" + _collectionPath + "'");
            AssetUtils.createCollectionAsset(session(), _collectionPath, true, true);
            logInfo("Created collection asset: '" + _collectionPath + "'");
        }
    }

}
