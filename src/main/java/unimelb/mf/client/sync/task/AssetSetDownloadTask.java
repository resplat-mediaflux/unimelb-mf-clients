package unimelb.mf.client.sync.task;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.logging.Logger;

import arc.xml.XmlDoc;
import arc.xml.XmlStringWriter;
import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.sync.settings.downstream.AssetSetDownloadJob;
import unimelb.mf.client.sync.task.AssetDownloadTask.Unarchive;
import unimelb.mf.client.task.AbstractMFTask;

public class AssetSetDownloadTask extends AbstractMFTask {

    private AssetSetDownloadJob _job;
    private int _batchSize;
    private boolean _overwrite;
    private Unarchive _unarchive;
    private boolean _restoreSymlinks;
    private boolean _csumCheck;
    private boolean _includeMetadata;
    private DataTransferListener<String, Path> _dl;
    private ExecutorService _workers;

    public AssetSetDownloadTask(MFSession session, Logger logger, AssetSetDownloadJob job, int batchSize,
            boolean overwrite, Unarchive unarchive, boolean restoreSymlinks, boolean csumCheck, boolean includeMetadata, ExecutorService workers,
            DataTransferListener<String, Path> dl) {
        super(session, logger);
        _job = job;
        _batchSize = batchSize;
        _overwrite = overwrite;
        _unarchive = unarchive;
        _restoreSymlinks = restoreSymlinks;
        _workers = workers;
        _dl = dl;
    }

    @Override
    public void execute() throws Throwable {

        List<String> assetPaths = _job.srcAssetPaths() == null ? null : new ArrayList<String>(_job.srcAssetPaths());
        if (assetPaths != null) {

            int nbBatches = assetPaths.size() / _batchSize;
            int remainder = assetPaths.size() % _batchSize;
            if (remainder != 0) {
                nbBatches++;
            }

            for (int i = 0; i < nbBatches; i++) {

                XmlStringWriter w = new XmlStringWriter();
                for (int j = 0; j < ((remainder == 0 || i < nbBatches - 1) ? _batchSize : remainder); j++) {
                    String assetPath = assetPaths.get(i * _batchSize + j);
                    w.add("id", "path=" + assetPath);
                }
                XmlDoc.Element re = session().execute("asset.get", w.document());

                List<XmlDoc.Element> aes = re.elements("asset");
                AssetDownloadTask.submit(aes, _job, _dl, _overwrite, _unarchive, _restoreSymlinks, _csumCheck, _includeMetadata, session(), logger(),
                        _workers);

            }
        }
    }

}
