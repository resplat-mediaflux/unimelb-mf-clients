package unimelb.mf.client.sync.task;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.logging.Logger;

import arc.xml.XmlDoc;
import arc.xml.XmlStringWriter;
import unimelb.mf.client.file.PosixAttributes;
import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.task.AbstractMFTask;
import unimelb.mf.model.asset.SymlinkAsset;

public class SymlinkUploadTask extends AbstractMFTask {

    private final Path _file;
    private final String _assetPath;
    private final boolean _dstIsNamespace;
    private String _assetId;
    private final DataTransferListener<Path, String> _ul;

    /**
     * @param session   Meidaflux server session
     * @param logger    logger
     * @param file      symlink file
     * @param assetPath destination asset path
     */
    public SymlinkUploadTask(MFSession session, Logger logger, Path file, String assetPath, boolean dstIsNamespace,
            DataTransferListener<Path, String> ul) {
        super(session, logger);
        _file = file;
        _assetPath = assetPath;
        _dstIsNamespace = dstIsNamespace;
        _ul = ul;
    }

    @Override
    public void execute() throws Throwable {
        try {
            String target = Files.readSymbolicLink(_file).toString();
            XmlDoc.Element ae = getAssetIfExists(session(), _assetPath);
            if (ae != null && target != null && target.equals(SymlinkAsset.getSymlinkTarget(ae))) {
                if (_ul != null) {
                    _ul.transferSkipped(_file, _assetPath);
                }
                logInfo(String.format("Symbolic asset(id=%s,version=%d): '%s' exists. Skipped.", _assetId,
                        ae.intValue("@version"), _assetPath));
                return;
            }

            if (_ul != null) {
                _ul.transferStarted(_file, _assetPath);
            }

            XmlDoc.Element re = isNewStyleSymlinkSupported() ? createNewStyleSymlinkAsset(target)
                    : createOldStyleSymlinkAsset(target);
            boolean changedOrCreated = re.booleanValue("reply[@service='asset.set']/response/id/@changed-or-created");
            ae = re.element("reply[@service='asset.get']/response/asset");
            _assetId = ae.value("@id");
            int assetVersion = ae.intValue("@version");

            if (_ul != null) {
                _ul.transferCompleted(_file, _assetPath);
            }

            if (changedOrCreated) {
                logInfo(String.format("Uploaded symbolic link: '%s' to asset(id=%s): '%s'", _file.toString(), _assetId,
                        _assetPath) + (assetVersion > 1 ? " New version (" + assetVersion + ") created." : ""));
            } else {
                logInfo(String.format("Asset(id=%s,version=%d): '%s' exists and no change made.", _assetId,
                        assetVersion, _assetPath));
            }
        } catch (Throwable e) {
            if (_ul != null) {
                _ul.transferFailed(_file, _assetPath);
            }
            throw e;
        }
    }

    static XmlDoc.Element getAssetIfExists(MFSession session, String assetPath) throws Throwable {
        XmlStringWriter w = new XmlStringWriter();
        w.add("id", "path=" + assetPath);
        boolean exists = session.execute("asset.exists", w.document()).booleanValue("exists");
        if (exists) {
            return session.execute("asset.get", w.document()).element("asset");
        } else {
            return null;
        }
    }

    private boolean isNewStyleSymlinkSupported() throws Throwable {
        XmlStringWriter w = new XmlStringWriter();
        w.add("service", "asset.set");
        return session().execute("system.service.describe", w.document())
                .elementExists("service/definition/element[@name='link']");
    }

    private XmlDoc.Element createOldStyleSymlinkAsset(String target) throws Throwable {
        XmlStringWriter w2 = new XmlStringWriter();
        w2.push("service", new String[] { "name", "asset.set" });
        w2.add("id", "path=" + _assetPath);
        w2.add("create", new String[] { "in", _dstIsNamespace ? "namespace" : "collection" }, true);
        w2.add("type", SymlinkAsset.TYPE);
        w2.add("ctype", SymlinkAsset.TYPE);
        w2.add("lctype", SymlinkAsset.TYPE);
        PosixAttributes fileAttrs = PosixAttributes.read(_file);
        if (fileAttrs != null) {
            w2.push("meta", new String[] { "action", "replace" });
            fileAttrs.save(w2);
            w2.pop();
        }
        w2.add("url", new String[] { "by", "reference" }, "file:" + target);
        w2.pop();

        w2.push("service", new String[] { "name", "asset.get" });
        w2.add("id", "path=" + _assetPath);
        w2.pop();

        setCurrentOperation("Uploading symbolic link: '" + _file + "' to asset: '" + _assetPath + "'");
        logInfo("Uploading symobolic link: '" + _file + "' to asset: '" + _assetPath + "'");

        XmlDoc.Element re;
        try {
            re = session().execute("service.execute", w2.document());
        } catch (Throwable e) {
            // check if it's caused by AccessDeniedException
            if (e instanceof java.io.IOException) {
                Throwable c;
                while ((c = e.getCause()) != null) {
                    if (c instanceof java.nio.file.AccessDeniedException) {
                        // extra logging for AccessDeniedException
                        logError("AccessDeniedException: " + c.getMessage());
                        break;
                    }
                }
            }
            throw e;
        }
        return re;
    }

    private XmlDoc.Element createNewStyleSymlinkAsset(String target) throws Throwable {
        XmlStringWriter w2 = new XmlStringWriter();
        w2.push("service", new String[] { "name", "asset.set" });
        w2.add("id", "path=" + _assetPath);
        w2.add("create", new String[] { "in", _dstIsNamespace ? "namespace" : "collection" }, true);
        // w2.add("type", SymlinkAsset.TYPE);
        PosixAttributes fileAttrs = PosixAttributes.read(_file);
        if (fileAttrs != null) {
            w2.push("meta", new String[] { "action", "replace" });
            fileAttrs.save(w2);
            w2.pop();
        }
        w2.push("link");
        w2.add("posix-symlink", target);
        w2.pop();
        w2.pop();

        w2.push("service", new String[] { "name", "asset.get" });
        w2.add("id", "path=" + _assetPath);
        w2.pop();

        setCurrentOperation("Uploading symbolic link: '" + _file + "' to asset: '" + _assetPath + "'");
        logInfo("Uploading symobolic link: '" + _file + "' to asset: '" + _assetPath + "'");

        XmlDoc.Element re;
        try {
            re = session().execute("service.execute", w2.document());
        } catch (Throwable e) {
            // check if it's caused by AccessDeniedException
            if (e instanceof java.io.IOException) {
                Throwable c;
                while ((c = e.getCause()) != null) {
                    if (c instanceof java.nio.file.AccessDeniedException) {
                        // extra logging for AccessDeniedException
                        logError("AccessDeniedException: " + c.getMessage());
                        break;
                    }
                }
            }
            throw e;
        }
        return re;
    }

}