package unimelb.mf.client.sync.task;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.logging.Logger;
import java.util.zip.CRC32;
import java.util.zip.CheckedInputStream;

import arc.mf.client.ServerClient;
import arc.streams.StreamCopy.AbortCheck;
import arc.xml.XmlDoc;
import arc.xml.XmlStringWriter;
import unimelb.mf.client.file.PosixAttributes;
import unimelb.mf.client.session.MFGeneratedInput;
import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.task.AbstractMFTask;
import unimelb.mf.client.util.AssetUtils;
import unimelb.utils.PathUtils;

public class FileUploadTask extends AbstractMFTask {

    private final Path _file;
    private final String _assetPath;
    private final boolean _parentIsNamespace;
    private final String _store;
    private final boolean _csumCheck;
    private final boolean _worm;
    private final boolean _wormCanAddVersions;
    private final boolean _wormCanMove;
    private final boolean _saveFileAttrs;
    private int _maxNumberOfRetries;
    private final long _retryWaitTime; // Milliseconds
    long _bytesUploaded = 0L;
    private final DataTransferListener<Path, String> _ul;
    private long _crc32;
    private final boolean _preserveModifiedTime;
    private PosixAttributes _fileAttrs;

    /**
     * @param session            server session
     * @param logger             logger
     * @param file               file path
     * @param assetPath          asset path
     * @param store              asset store name
     * @param csumCheck          do checksum check?
     * @param worm               set WORM?
     * @param wormCanAddVersions WORM can add versions?
     * @param wormCanMove        WORM can move?
     * @param saveFileAttrs      save file attributes?
     * @param maxNumberOfRetries maximum number of retries
     * @param retryWaitTime      wait time before each retry.
     * @param ul                 upload listener
     */
    public FileUploadTask(MFSession session, Logger logger, Path file, String assetPath, boolean parentIsNamespace,
            String store, boolean csumCheck, boolean worm, boolean wormCanAddVersions, boolean wormCanMove,
            boolean saveFileAttrs, int maxNumberOfRetries, long retryWaitTime, boolean preserveModifiedTime,
            DataTransferListener<Path, String> ul) {
        super(session, logger);
        _file = file;
        _assetPath = assetPath;
        _parentIsNamespace = parentIsNamespace;
        _store = store; // Required by Cluster IO
        _csumCheck = csumCheck;
        _worm = worm;
        _wormCanAddVersions = wormCanAddVersions;
        _wormCanMove = wormCanMove;
        _saveFileAttrs = saveFileAttrs;
        _maxNumberOfRetries = maxNumberOfRetries;
        _retryWaitTime = retryWaitTime;
        _bytesUploaded = 0;
        _preserveModifiedTime = preserveModifiedTime;
        _ul = ul;
    }

    @Override
    public void execute() throws Throwable {
        try {
            if (_ul != null) {
                _ul.transferStarted(_file, _assetPath);
            }

            final long fileSize = Files.size(_file);
            if (fileSize == 0) {
                _crc32 = 0;
                logWarning("File length of '" + _file + "' is 0.");
            }

            XmlStringWriter w2 = new XmlStringWriter();
            w2.push("service", new String[] { "name", "asset.set" });
            w2.add("id", "path=" + _assetPath);
            w2.add("create", new String[] { "in", _parentIsNamespace ? "namespace" : "collection" }, true);
            // Required by cluster IO
            if (_store != null) {
                w2.add("store", _store);
            }

            if (_saveFileAttrs && getFileAttributes() != null) {
                w2.push("meta", new String[] { "action", "replace" });
                getFileAttributes().save(w2);
                w2.pop();
            }

            w2.pop();

            if (_preserveModifiedTime && getFileAttributes() != null && getFileAttributes().mtime() != null) {
                w2.push("service", new String[] { "name", "asset.modify.time.set" });
                w2.add("id", "path=" + _assetPath);
                w2.add("time-in-millisec", _fileAttrs.mtime());
                w2.pop();
            }

            w2.push("service", new String[] { "name", "asset.get" });
            w2.add("id", "path=" + _assetPath);
            w2.pop();

            String fileExt = PathUtils.getFileExtension(_file.toString());
            ServerClient.Input input = new MFGeneratedInput(null, fileExt, _file.toString(), fileSize,
                    _store == null ? null : ("asset:" + _store)) {
                @Override
                protected void consume(OutputStream out, AbortCheck ac) throws Throwable {
                    try (InputStream in = _csumCheck
                            ? new CheckedInputStream(new BufferedInputStream(Files.newInputStream(_file)), new CRC32())
                            : new BufferedInputStream(Files.newInputStream(_file))) {
                        byte[] buffer = new byte[8192];
                        int len;
                        while ((len = in.read(buffer)) != -1) {
                            out.write(buffer, 0, len);
                            _bytesUploaded += len;
                            if (_ul != null) {
                                _ul.transferProgressed(_file, _assetPath, len);
                            }
                            if ((ac != null && ac.hasBeenAborted()) || Thread.interrupted()) {
                                throw new InterruptedException("Upload aborted.");
                            }
                        }
                        if (_csumCheck) {
                            _crc32 = fileSize == 0 ? 0 : ((CheckedInputStream) in).getChecksum().getValue();
                        }
                    }
                }
            };

            setCurrentOperation("Uploading file: '" + _file + "' to asset: '" + _assetPath + "'");
            logInfo("Uploading file: '" + _file + "' to asset: '" + _assetPath + "'");

            XmlDoc.Element re = session().execute("service.execute", w2.document(), input, this);

            boolean changedOrCreated = re.booleanValue("reply[@service='asset.set']/response/id/@changed-or-created");
            XmlDoc.Element ae = re.element("reply[@service='asset.get']/response/asset");
            String assetId = ae.value("@id");
            int assetVersion = ae.intValue("@version");
            final long contentSize = ae.longValue("content/size");

            if (fileSize != contentSize) {
                throw new Exception("Uploaded asset(" + assetId + ") content size: " + contentSize
                        + " does not match the local file (" + _file + ") size: " + fileSize + ".");
            }

            if (_csumCheck) {
                long assetCRC32 = ae.longValue("content/csum[@base='16']", 0L, 16);
                if (_crc32 != assetCRC32) {
                    throw new Exception("CRC32 checksums do not match for file: '" + _file + "'(" + _crc32
                            + ") and asset: '" + _assetPath + "'(" + assetCRC32 + ")");
                }
            }
            if (_worm) {
                logInfo("Setting WORM state for asset(id= " + assetId + "): '" + _assetPath + "' ...");
                AssetUtils.setWorm(session(), assetId, _wormCanAddVersions, _wormCanMove);
            }
            if (_ul != null) {
                _ul.transferCompleted(_file, _assetPath);
            }
            if (changedOrCreated) {
                logInfo(String.format("Uploaded file: '%s' to asset(id=%s): '%s'", _file, assetId, _assetPath)
                        + (assetVersion > 1 ? " New version (" + assetVersion + ") created." : ""));
            } else {
                logInfo(String.format("Asset(id=%s,version=%d): '%s' exists and no change made.", assetId, assetVersion,
                        _assetPath));
            }
        } catch (Throwable e) {
            rewindProgress();
            if (_maxNumberOfRetries > 0) {
                _maxNumberOfRetries--;
                Thread.sleep(_retryWaitTime);
                logWarning("Retry uploading " + _file);
                execute();
            } else {
                if (_ul != null) {
                    _ul.transferFailed(_file, _assetPath);
                }
                throw e;
            }
        }
    }

    private void rewindProgress() {
        setCompletedOperations(0);
        if (_bytesUploaded > 0) {
            if (_ul != null) {
                _ul.transferProgressed(_file, _assetPath, -1 * _bytesUploaded);
            }
            _bytesUploaded = 0;
        }
        _crc32 = 0;
    }

    private PosixAttributes getFileAttributes() throws IOException {
        if (_fileAttrs == null) {
            _fileAttrs = PosixAttributes.read(_file);
        }
        return _fileAttrs;
    }
}
