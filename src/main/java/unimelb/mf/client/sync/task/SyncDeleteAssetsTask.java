package unimelb.mf.client.sync.task;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

import arc.xml.XmlDoc;
import arc.xml.XmlStringWriter;
import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.sync.settings.Settings;
import unimelb.mf.client.sync.settings.downstream.DownJob;
import unimelb.mf.client.sync.settings.upstream.DirectoryUploadJob;
import unimelb.mf.client.task.AbstractMFTask;
import unimelb.mf.client.util.AssetNamespaceUtils;
import unimelb.mf.client.util.AssetQueryUtils;
import unimelb.mf.client.util.AssetUtils;
import unimelb.utils.PathUtils;

public class SyncDeleteAssetsTask extends AbstractMFTask {

	private final Path _srcDir;
	private final String _dstDirPath;
	private final boolean _dstDirIsNamespace;
	private final boolean _hardDestroy;
	private final int _batchSize;
	private final boolean _verbose;
	private final AtomicInteger _counter;

	public SyncDeleteAssetsTask(MFSession session, Logger logger, Path srcDir, String dstDir, boolean dstDirIsNamespace,
			boolean hardDestroy, int batchSize, boolean verbose, AtomicInteger counter) {
		super(session, logger);
		_srcDir = srcDir;
		_dstDirPath = dstDir;
		_dstDirIsNamespace = dstDirIsNamespace;
		_hardDestroy = hardDestroy;
		_batchSize = batchSize;
		_verbose = verbose;
		_counter = counter;
	}

	public SyncDeleteAssetsTask(MFSession session, Logger logger, DirectoryUploadJob job, Settings settings,
			AtomicInteger counter) throws Throwable {
		this(session, logger, job.srcDirectory(), job.dstCollection().path(),
				job.dstCollection().resolve(session).isAssetNamespace(), settings.hardDestroyAssets(),
				settings.batchSize(), settings.verbose(), counter);
	}

	@Override
	public void execute() throws Throwable {
		if (_verbose) {
			logger().info("Checking assets in directory: '" + _dstDirPath + "'... looking for assets to delete...");
		}
		processAssets();
		if (_dstDirIsNamespace) {
			processEmptyAssetNamespaces();
		} else {
			processEmptyCollections();
		}
	}

	private void processAssets() throws Throwable {
		XmlStringWriter w = new XmlStringWriter();
		if (_dstDirIsNamespace) {
			w.add("namespace", _dstDirPath);
		} else {
			w.add("collection", "path=" + _dstDirPath);
		}
		w.add("where", "(asset has content or asset has link) and not(asset is collection)");
		w.add("as", "iterator");
		w.add("action", "get-path");
		String iteratorId = session().execute("asset.query", w.document()).value("iterator");
		String args = String.format("<id>%s</id><size>%d</size>", iteratorId, _batchSize);
		boolean complete = false;
		try {
			while (!complete && !Thread.interrupted()) {
				XmlDoc.Element re = session().execute("asset.query.iterate", args);
				complete = re.booleanValue("iterated/@complete");
				List<XmlDoc.Element> pes = re.elements("path");
				if (pes != null && !pes.isEmpty()) {
					Set<String> toDestroy = new LinkedHashSet<>();
					for (XmlDoc.Element pe : pes) {
						String assetPath = pe.value();
						String assetId = pe.value("@id");
						Path file = DownJob.dstFilePath(_srcDir, _dstDirPath, assetPath);
						if (!Files.exists(file)) {
							toDestroy.add(assetId);
							if (_verbose) {
								logger().info("Submitted asset: '" + assetPath + "' to destroy");
							}
						}
					}
					if (!toDestroy.isEmpty()) {
						destroyAssets(toDestroy);
						if (_counter != null) {
							_counter.getAndAdd(toDestroy.size());
						}
						if (_verbose) {
							logger().info(
									"Deleted " + toDestroy.size() + " asset" + (toDestroy.size() == 1 ? "." : "s."));
						}
					}
				}
			}
		} finally {
			AssetQueryUtils.destroyIterator(session(), iteratorId, true);
		}
	}

	private void destroyAssets(Collection<String> assetIds) throws Throwable {
		XmlStringWriter w1 = new XmlStringWriter();
		w1.add("members", false);
		for (String assetId : assetIds) {
			w1.add("id", assetId);
		}
		String service = _hardDestroy ? "asset.hard.destroy" : "asset.destroy";
		session().execute(service, w1.document());
	}

	private void processEmptyCollections() throws Throwable {
		XmlStringWriter w = new XmlStringWriter();
		w.add("collection", "path=" + _dstDirPath);
		w.add("where", "asset collection member count=0");
		w.add("as", "iterator");
		w.add("action", "get-path");
		String iteratorId = session().execute("asset.query", w.document()).value("iterator");
		String args = String.format("<id>%s</id><size>%d</size>", iteratorId, _batchSize);
		boolean complete = false;
		try {
			while (!complete && !Thread.interrupted()) {
				XmlDoc.Element re = session().execute("asset.query.iterate", args);
				complete = re.booleanValue("iterated/@complete");
				List<XmlDoc.Element> pes = re.elements("path");
				if (pes != null && !pes.isEmpty()) {
					for (XmlDoc.Element pe : pes) {
						String collectionAssetPath = pe.value();
						processEmptyCollection(collectionAssetPath, false);
					}
				}
			}
		} finally {
			AssetQueryUtils.destroyIterator(session(), iteratorId, true);
		}
	}

	private void processEmptyCollection(String collectionAssetPath, boolean checkIfEmpty) throws Throwable {
		if (!collectionAssetPath.startsWith(_dstDirPath + "/")) {
			// root collection should not be destroyed even if it's empty
			return;
		}
		Path dir = DownJob.dstFilePath(_srcDir, _dstDirPath, collectionAssetPath);
		if (Files.isDirectory(dir)) {
			// local directory exists. should not be destroyed
			return;
		}
		if (!AssetUtils.assetExists(collectionAssetPath, session())) {
			// collection no longer exists
			return;
		}
		if (checkIfEmpty) {
			if (!AssetUtils.isEmptyCollectionAsset(collectionAssetPath, session())) {
				// collection is not empty
				return;
			}
		}
		try {
			AssetUtils.destroyAsset(collectionAssetPath, session(), false, _hardDestroy);
			if (_verbose) {
				logger().info("Deleted empty asset namespace: '" + collectionAssetPath + "'");
			}
			String parentPath = PathUtils.getParentPath(collectionAssetPath);
			if (AssetUtils.assetExists(parentPath, session())) {
				processEmptyCollection(parentPath, true);
			}
		} catch (Throwable e) {
			logger().warning(
					"Failed to destory asset namespace: '" + collectionAssetPath + "'. " + "Error: " + e.getMessage());
		}
	}

	private void processEmptyAssetNamespaces() throws Throwable {
		long idx = 0;
		boolean complete = false;
		while (!complete && !Thread.interrupted()) {
			XmlStringWriter w = new XmlStringWriter();
			w.add("namespace", _dstDirPath);
			w.add("leaf-only", true);
			w.add("idx", idx);
			w.add("size", _batchSize);
			XmlDoc.Element re = session().execute("asset.namespace.empty.list", w.document());
			idx += _batchSize;
			Collection<String> namespaces = re.values("namespace");
			complete = namespaces == null || namespaces.isEmpty();
			if (!complete) {
				for (String namespacePath : namespaces) {
					boolean destroyed = processEmptyAssetNamespace(namespacePath, false);
					if (destroyed) {
						// move the cursor back one so that it will not miss the namespace(s) in the
						// next page
						idx -= 1;
					}
				}
			}
		}
	}

	private boolean processEmptyAssetNamespace(String namespacePath, boolean checkIfEmpty) throws Throwable {
		if (!namespacePath.startsWith(_dstDirPath + "/")) {
			// root namespace should not be destroyed even if it's empty
			return false;
		}
		Path dir = DownJob.dstFilePath(_srcDir, _dstDirPath, namespacePath);
		if (Files.isDirectory(dir)) {
			// corresponding local directory exists. should not be destroyed
			return false;
		}
		if (!AssetNamespaceUtils.assetNamespaceExists(session(), namespacePath)) {
			// asset namespace no longer exists
			return true;
		}
		if (checkIfEmpty) {
			if (!AssetNamespaceUtils.isEmpty(session(), namespacePath)) {
				// asset namespace is not empty
				return false;
			}
		}
		boolean destroyed = false;
		try {
			AssetNamespaceUtils.destroyAssetNamespace(session(), namespacePath);
			destroyed = true;
			if (_verbose) {
				logger().info("Deleted empty asset namespace: '" + namespacePath + "'");
			}
		} catch (Throwable e) {
			logger().warning(
					"Failed to destory asset namespace: '" + namespacePath + "'. " + "Error: " + e.getMessage());
			destroyed = false;
		}
		String pns = PathUtils.getParentPath(namespacePath);
		processEmptyAssetNamespace(pns, true);
		return destroyed;
	}
}
