package unimelb.mf.client.sync.task;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.logging.Logger;

import arc.xml.XmlDoc;
import arc.xml.XmlStringWriter;
import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.sync.settings.upstream.UploadJob;
import unimelb.mf.client.task.AbstractMFTask;

public class FileSetUploadTask extends AbstractMFTask {

    private final Map<Path, String> _fileAssets;
    private final Map<String, Path> _assetFiles;
    private final boolean _followSymlinks;

    private final boolean _dstIsNamespace;
    private final String _dstStore;

    private final boolean _csumCheck;
    private final boolean _worm;
    private final boolean _wormCanAddVersions;
    private final boolean _wormCanMove;

    private final boolean _saveFileAttrs;
    private final long _aggregateThreshold;
    private final long _splitThreshold;

    private int _maxNumberOfRetries = 0;
    private long _retryWaitTime = 0;
    private final boolean _preserveModifiedTime;

    private final ThreadPoolExecutor _workers;
    private final DataTransferListener<Path, String> _ul;

    /**
     * @param session            server session
     * @param logger             logger
     * @param files              files
     * @param job                upload job
     * @param csumCheck          do checksum check?
     * @param maxNumberOfRetries maximum number of retries
     * @param retryWaitTime      wait time before each retry
     * @param ul                 upload listener
     * @param workers            thread pool of workers
     */
    public FileSetUploadTask(MFSession session, Logger logger, List<Path> files, boolean followSymlinks, UploadJob job,
            boolean csumCheck, boolean worm, boolean wormCanAddVersions, boolean wormCanMove, boolean saveFileAttrs,
            long aggregateThreshold, long splitThreshold, int maxNumberOfRetries, long retryWaitTime,
            boolean preserveModifiedTime, DataTransferListener<Path, String> ul, ThreadPoolExecutor workers)
            throws Throwable {
        super(session, logger);
        _workers = workers;
        _csumCheck = csumCheck;
        _worm = worm;
        _wormCanAddVersions = wormCanAddVersions;
        _wormCanMove = wormCanMove;
        _saveFileAttrs = saveFileAttrs;
        _aggregateThreshold = aggregateThreshold;
        _splitThreshold = splitThreshold;

        _maxNumberOfRetries = maxNumberOfRetries;
        _retryWaitTime = retryWaitTime;
        _preserveModifiedTime = preserveModifiedTime;

        _ul = ul;
        _fileAssets = new LinkedHashMap<Path, String>();
        _assetFiles = new LinkedHashMap<String, Path>();

        _dstIsNamespace = job.dstCollection().resolve(session).isAssetNamespace();
        // destination content store, required by Cluster IO.
        _dstStore = job.dstCollection().resolve(session).store();

        if (files != null) {
            for (Path file : files) {
                String assetPath = job.transformPath(file);
                _fileAssets.put(file, assetPath);
                _assetFiles.put(assetPath, file);
            }
        }
        _followSymlinks = followSymlinks;
    }

    @Override
    public void execute() throws Throwable {
        if (_assetFiles.isEmpty()) {
            return;
        }
        setTotalOperations(_assetFiles.size());
        logger().info("Checking " + _assetFiles.size() + " files...");
        XmlStringWriter w1 = new XmlStringWriter();
        List<String> assetPaths = new ArrayList<String>(_fileAssets.values());
        for (String assetPath : assetPaths) {
            w1.add("id", "path=" + assetPath);
        }
        List<XmlDoc.Element> ees = session().execute("asset.exists", w1.document()).elements("exists");

        Map<String, Path> aggregateUploadFiles = new LinkedHashMap<>();

        int nbExists = 0;
        XmlStringWriter w2 = new XmlStringWriter();
        for (int i = 0; i < ees.size(); i++) {
            XmlDoc.Element ee = ees.get(i);
            String assetPath = assetPaths.get(i);
            boolean exists = ee.booleanValue();
            if (!exists) {
                Path file = _assetFiles.get(assetPath);
                try {
                    if (file == null) {
                        throw new AssertionError("Could not find file matching asset: '" + assetPath + "'");
                    }
                    boolean isSymbolicLink = Files.isSymbolicLink(file);
                    if (isSymbolicLink && !_followSymlinks) {
                        _workers.submit(
                                new SymlinkUploadTask(session(), logger(), file, assetPath, _dstIsNamespace, _ul));
                    } else {
                        long fileSize = Files.size(file);
                        if (_aggregateThreshold > 0 && fileSize <= _aggregateThreshold) {
                            aggregateUploadFiles.put(assetPath, file);
                        } else if (_workers.getMaximumPoolSize() > 1 && fileSize >= _splitThreshold) {
                            new SlicedFileUploadTask(session(), logger(), file, _workers.getMaximumPoolSize(),
                                    assetPath, _dstIsNamespace, _dstStore, _csumCheck, _worm, _wormCanAddVersions,
                                    _wormCanMove, _maxNumberOfRetries, _ul, _workers).execute();
                        } else {
                            _workers.submit(new FileUploadTask(session(), logger(), file, assetPath, _dstIsNamespace,
                                    _dstStore, _csumCheck, _worm, _wormCanAddVersions, _wormCanMove, _saveFileAttrs,
                                    _maxNumberOfRetries, _retryWaitTime, _preserveModifiedTime, _ul));
                        }
                    }
                    incCompletedOperations();
                } catch (Throwable e) {
                    if (_ul != null) {
                        _ul.transferFailed(file, assetPath);
                    }
                    logError("accessing " + file + ": " + e.getMessage(), e);
                }
            } else {
                w2.add("id", "path=" + assetPath);
                nbExists++;
            }
        }
        if (nbExists > 0) {
            List<XmlDoc.Element> aes = session().execute("asset.get", w2.document()).elements("asset");
            for (XmlDoc.Element ae : aes) {

                String assetPath = ae.value("path");
                Path file = _assetFiles.get(assetPath);
                try {
                    if (file == null) {
                        throw new AssertionError("Could not find file matching asset: '" + assetPath + "'");
                    }
                    boolean isSymbolicLink = Files.isSymbolicLink(file);
                    if (isSymbolicLink && !_followSymlinks) {
                        // update symlink asset.
                        _workers.submit(
                                new SymlinkUploadTask(session(), logger(), file, assetPath, _dstIsNamespace, _ul));
                    } else {
                        // update file asset.
                        Long contentSize = ae.longValue("content/size", null);
                        String assetCSumStr = ae.value("content/csum[@base='16']");
                        Long assetCSum = assetCSumStr == null ? null : Long.parseLong(assetCSumStr, 16);
                        long fileSize = Files.size(file);

                        boolean fileSizesMatch = contentSize != null && (contentSize.equals(fileSize));

                        if (!fileSizesMatch) {
                            // upload file if size does not match
                            if (_aggregateThreshold > 0 && fileSize <= _aggregateThreshold) {
                                aggregateUploadFiles.put(assetPath, file);
                            } else if (_workers.getMaximumPoolSize() > 1 && fileSize >= _splitThreshold) {
                                new SlicedFileUploadTask(session(), logger(), file, _workers.getMaximumPoolSize(),
                                        assetPath, _dstIsNamespace, _dstStore, _csumCheck, _worm, _wormCanAddVersions,
                                        _wormCanMove, _maxNumberOfRetries, _ul, _workers).execute();
                            } else {
                                _workers.submit(new FileUploadTask(session(), logger(), file, assetPath,
                                        _dstIsNamespace, _dstStore, _csumCheck, _worm, _wormCanAddVersions,
                                        _wormCanMove, _saveFileAttrs, _maxNumberOfRetries, _retryWaitTime,
                                        _preserveModifiedTime, _ul));
                            }
                        } else if (_csumCheck) {
                            if (assetCSum == null) {
                                // no asset checksum available. Upload file regardless.
                                if (_aggregateThreshold > 0 && fileSize <= _aggregateThreshold) {
                                    aggregateUploadFiles.put(assetPath, file);
                                } else if (_workers.getMaximumPoolSize() > 1 && fileSize >= _splitThreshold) {
                                    new SlicedFileUploadTask(session(), logger(), file, _workers.getMaximumPoolSize(),
                                            assetPath, _dstIsNamespace, _dstStore, _csumCheck, _worm,
                                            _wormCanAddVersions, _wormCanMove, _maxNumberOfRetries, _ul, _workers)
                                            .execute();
                                } else {
                                    _workers.submit(new FileUploadTask(session(), logger(), file, assetPath,
                                            _dstIsNamespace, _dstStore, _csumCheck, _worm, _wormCanAddVersions,
                                            _wormCanMove, _saveFileAttrs, _maxNumberOfRetries, _retryWaitTime,
                                            _preserveModifiedTime, _ul));
                                }
                            } else {
                                // compare crc32 then decide
                                _workers.submit(new FileCRC32Task(session(), logger(), file, assetPath, _dstIsNamespace,
                                        _dstStore, assetCSum, _worm, _wormCanAddVersions, _wormCanMove, _saveFileAttrs,
                                        _maxNumberOfRetries, _retryWaitTime, _preserveModifiedTime, _ul));
                            }
                        } else {
                            // skip
                            if (_ul != null) {
                                _ul.transferSkipped(file, assetPath);
                            }
                            logger().info("Asset '" + assetPath + "' already exists. Skipped.");
                        }
                    }
                    incCompletedOperations();
                } catch (Throwable e) {
                    logError("accessing " + file + ": " + e.getMessage(), e);
                    if (_ul != null) {
                        _ul.transferFailed(file, assetPath);
                    }
                }

                // @formatter:off
//                PosixAttributes contentAttrs = ae.elementExists("meta/" + PosixAttributes.DOC_TYPE)
//                        ? new PosixAttributes(ae.element("meta/" + PosixAttributes.DOC_TYPE))
//                        : null;
//                PosixAttributes fileAttrs = PosixAttributes.read(file);
//                if (contentAttrs != null && fileAttrs != null && fileAttrs.mtimeEquals(contentAttrs)) {
                //
//                }
                // @formatter:on
            }
        }

        if (!aggregateUploadFiles.isEmpty()) {
            _workers.submit(new AggregatedUploadTask(session(), logger(), aggregateUploadFiles, _dstStore, _csumCheck,
                    _worm, _wormCanAddVersions, _wormCanMove, _maxNumberOfRetries, _ul));
        }
    }

}
