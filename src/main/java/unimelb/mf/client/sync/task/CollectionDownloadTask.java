package unimelb.mf.client.sync.task;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.logging.Logger;

import arc.xml.XmlDoc;
import arc.xml.XmlStringWriter;
import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.sync.settings.downstream.CollectionDownloadJob;
import unimelb.mf.client.sync.task.AssetDownloadTask.Unarchive;
import unimelb.mf.client.task.AbstractMFTask;
import unimelb.mf.client.util.collection.CollectionDetails;

public class CollectionDownloadTask extends AbstractMFTask {

    private CollectionDownloadJob _job;
    private Long _minSTime;
    private int _batchSize;
    private boolean _overwrite;
    private Unarchive _unarchive;
    private boolean _restoreSymlinks;
    private boolean _csumCheck;
    private boolean _includeMetadata;
    private long _aggregateThreshold;
    private DataTransferListener<String, Path> _dl;
    private ExecutorService _workers;

    public CollectionDownloadTask(MFSession session, Logger logger, CollectionDownloadJob job, Long minSTime,
            int batchSize, boolean overwrite, Unarchive unarchive, boolean restoreSymlinks, boolean csumCheck,
            boolean includeMetadata, long aggregateThreshold, ExecutorService workers,
            DataTransferListener<String, Path> dl) {
        super(session, logger);
        _job = job;
        _minSTime = minSTime;
        _batchSize = batchSize;
        _overwrite = overwrite;
        _unarchive = unarchive;
        _restoreSymlinks = restoreSymlinks;
        _csumCheck = csumCheck;
        _includeMetadata = includeMetadata;
        _aggregateThreshold = aggregateThreshold;
        _workers = workers;
        _dl = dl;
    }

    @Override
    public void execute() throws Throwable {
        if (!Files.exists(_job.dstDirectory())) {
            Files.createDirectories(_job.dstDirectory());
            this.logInfo("Created directory: '" + _job.dstDirectory() + "'");
        }
        /*
         * download assets
         */
        CollectionDetails srcCollection = _job.srcCollection().resolve(session());
        XmlStringWriter w = new XmlStringWriter();
        if (srcCollection.isAssetNamespace()) {
            if (_minSTime != null) {
                w.add("where", String.format("namespace>='%s' and stime>%d", _job.srcCollection().path(), _minSTime));
            } else {
                w.add("namespace", _job.srcCollection().path());
            }
        } else {
            if (_minSTime != null) {
                w.add("where", String.format("asset in collection or subcollection of '%s' and stime>%d",
                        _job.srcCollection().path(), _minSTime));
            } else {
                w.add("collection", "path=" + _job.srcCollection().path());
            }
        }
        // exclude collection assets
        w.add("where", "not(asset is collection)");
        w.add("action", "get-meta");
        w.add("as", "iterator");
        long iteratorId = session().execute("asset.query", w.document()).longValue("iterator");
        try {
            w = new XmlStringWriter();
            w.add("id", iteratorId);
            w.add("size", _batchSize);
            String args = w.document();
            boolean complete = false;
            while (!complete && !Thread.interrupted()) {
                XmlDoc.Element re = session().execute("asset.query.iterate", args);
                complete = re.booleanValue("iterated/@complete");
                List<XmlDoc.Element> aes = re.elements("asset");
                if (aes != null && !aes.isEmpty()) {
                    if (_aggregateThreshold > 0) {
                        Map<String, XmlDoc.Element> aggregateDownloadAes = new LinkedHashMap<>();
                        List<XmlDoc.Element> normalDownloadAes = new ArrayList<>();
                        aes.forEach(ae -> {
                            try {
                                long csize = ae.longValue("content/size", 0);
                                if (csize <= _aggregateThreshold) {
                                    String assetPath = ae.value("path");
                                    aggregateDownloadAes.put(assetPath, ae);
                                } else {
                                    normalDownloadAes.add(ae);
                                }
                            } catch (Throwable t) {
                                logError(t);
                            }
                        });
                        if (!aggregateDownloadAes.isEmpty()) {
                            AggregatedDownloadTask.submit(aggregateDownloadAes, _job, _dl, _overwrite, _unarchive,
                                    _csumCheck, _includeMetadata, session(), logger(), _workers);
                        }
                        if (!normalDownloadAes.isEmpty()) {
                            AssetDownloadTask.submit(normalDownloadAes, _job, _dl, _overwrite, _unarchive,
                                    _restoreSymlinks, _csumCheck, _includeMetadata, session(), logger(), _workers);
                        }
                    } else {
                        AssetDownloadTask.submit(aes, _job, _dl, _overwrite, _unarchive, _restoreSymlinks, _csumCheck,
                                _includeMetadata, session(), logger(), _workers);
                    }
                }
            }
        } finally {
            w = new XmlStringWriter();
            w.add("id", iteratorId);
            w.add("ignore-missing", true);
            session().execute("asset.query.iterator.destroy", w.document());
        }

        /*
         * download empty asset namespaces (create empty directories)
         */
        if (srcCollection.isAssetNamespace()) {
            downloadEmptyAssetNamespaces(session(), srcCollection);
        }

        /*
         * download empty collection assets (create empty directories)
         */
        downloadEmptyCollectionAssets(session(), srcCollection);
    }

    private void downloadEmptyAssetNamespaces(MFSession session, CollectionDetails srcCollection) throws Throwable {
        int idx = 0;
        boolean complete = false;
        while (!complete && !Thread.interrupted()) {
            XmlStringWriter w = new XmlStringWriter();
            w.add("namespace", srcCollection.path());
            w.add("leaf-only", true);
            w.add("idx", idx);
            w.add("size", _batchSize);
            w.add("count", true);
            XmlDoc.Element re = session.execute("asset.namespace.empty.list", w.document());
            Collection<String> namespaces = re.values("namespace");
            if (namespaces == null || namespaces.isEmpty()) {
                complete = true;
            } else {
                List<Path> dirs = new ArrayList<Path>();
                for (String namespace : namespaces) {
                    if (!srcCollection.path().equals(namespace)) {
                        // resolve local directory path
                        Path dir = _job.transformPath(namespace);
                        dirs.add(dir);
                    }
                }
                if (!dirs.isEmpty()) {
                    // submit to worker thread pool
                    _workers.submit(new CreateDirectoriesTask(dirs, logger()));
                }

                long total = re.longValue("total");
                complete = (idx + _batchSize >= total);
                idx += _batchSize;
            }
        }
    }

    private void downloadEmptyCollectionAssets(MFSession session, CollectionDetails srcCollection) throws Throwable {
        XmlStringWriter w = new XmlStringWriter();
        StringBuilder sb = new StringBuilder();
        if (srcCollection.isAssetNamespace()) {
            sb.append(String.format("(namespace>='%s')", srcCollection.path()));
        } else {
            sb.append(String.format("(asset in collection or subcollection of '%s')", srcCollection.path()));
        }
        sb.append(" and (asset is collection) and (asset collection member count = 0)");
        if (_minSTime != null) {
            sb.append(String.format(" and (stime>%d)", _minSTime));
        }
        w.add("where", sb.toString());
        w.add("action", "get-path");
        w.add("as", "iterator");
        long iteratorId = session.execute("asset.query", w.document()).longValue("iterator");
        try {
            w = new XmlStringWriter();
            w.add("id", iteratorId);
            String args = w.document();
            boolean complete = false;
            while (!complete && !Thread.interrupted()) {
                XmlDoc.Element re = session.execute("asset.query.iterate", args);
                complete = re.booleanValue("iterated/@complete");
                Collection<String> paths = re.values("path");
                if (paths != null && !paths.isEmpty()) {
                    List<Path> dirs = new ArrayList<Path>();
                    for (String path : paths) {
                        if (!srcCollection.path().equals(path)) {
                            // resolve local directory path
                            Path dir = _job.transformPath(path);
                            dirs.add(dir);
                        }
                    }
                    if (!dirs.isEmpty()) {
                        // submit to worker thread pool
                        _workers.submit(new CreateDirectoriesTask(dirs, logger()));
                    }
                }
            }
        } finally {
            w = new XmlStringWriter();
            w.add("id", iteratorId);
            w.add("ignore-missing", true);
            session.execute("asset.query.iterator.destroy", w.document());
        }
    }

}
