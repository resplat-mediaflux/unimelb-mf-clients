package unimelb.mf.client.sync;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.BindException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.nio.file.FileVisitOption;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.EnumSet;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;

import arc.xml.XmlDoc;
import arc.xml.XmlStringWriter;
import unimelb.mf.client.App;
import unimelb.mf.client.sync.check.AssetItem;
import unimelb.mf.client.sync.check.CheckHandler;
import unimelb.mf.client.sync.check.CheckResult;
import unimelb.mf.client.sync.settings.Job;
import unimelb.mf.client.sync.settings.downstream.AssetSetDownloadJob;
import unimelb.mf.client.sync.settings.downstream.CollectionDownloadCheckJob;
import unimelb.mf.client.sync.settings.downstream.CollectionDownloadJob;
import unimelb.mf.client.sync.settings.downstream.DownloadCheckJob;
import unimelb.mf.client.sync.settings.downstream.DownloadJob;
import unimelb.mf.client.sync.settings.upstream.DirectoryUploadCheckJob;
import unimelb.mf.client.sync.settings.upstream.DirectoryUploadJob;
import unimelb.mf.client.sync.settings.upstream.FileSetUploadCheckJob;
import unimelb.mf.client.sync.settings.upstream.FileSetUploadJob;
import unimelb.mf.client.sync.settings.upstream.UploadCheckJob;
import unimelb.mf.client.sync.settings.upstream.UploadJob;
import unimelb.mf.client.sync.task.AssetSetCheckTask;
import unimelb.mf.client.sync.task.AssetSetDownloadTask;
import unimelb.mf.client.sync.task.CollectionCreateTask;
import unimelb.mf.client.sync.task.CollectionDownloadTask;
import unimelb.mf.client.sync.task.DataTransferListener;
import unimelb.mf.client.sync.task.FileSetCheckTask;
import unimelb.mf.client.sync.task.FileSetUploadTask;
import unimelb.mf.client.sync.task.SyncDeleteAssetsTask;
import unimelb.mf.client.sync.task.SyncDeleteFilesTask;
import unimelb.mf.client.task.AbstractMFApp;
import unimelb.mf.client.util.AssetQueryUtils;
import unimelb.mf.client.util.MailUtils;
import unimelb.mf.client.util.collection.CollectionDetails;
import unimelb.mf.model.asset.SymlinkAsset;
import unimelb.utils.ChecksumUtils.ChecksumType;
import unimelb.utils.FileUtils;
import unimelb.utils.LoggingUtils;
import unimelb.utils.TimeUtils;

public abstract class MFSyncApp extends AbstractMFApp<unimelb.mf.client.sync.settings.Settings> implements Runnable {

    private final unimelb.mf.client.sync.settings.Settings _settings;
    private ThreadPoolExecutor _workers;
    private ThreadPoolExecutor _queriers;
    private Timer _daemonTimer;
    private Thread _daemonListener;
    private ServerSocket _daemonListenerSocket;

    private final AtomicLong _nbFiles = new AtomicLong(0);
    private final AtomicLong _nbUploadedFiles = new AtomicLong(0);
    private final AtomicLong _nbUploadedZeroSizeFiles = new AtomicLong(0);
    private final AtomicLong _nbUploadedSymlinks = new AtomicLong(0);
    private final AtomicLong _nbSkippedFiles = new AtomicLong(0);
    private final AtomicLong _nbFailedFiles = new AtomicLong(0);
    private final AtomicLong _nbUploadedBytes = new AtomicLong(0);
    private final AtomicInteger _nbDeletedFiles = new AtomicInteger(0);

    private final AtomicLong _nbAssets = new AtomicLong(0);
    private final AtomicLong _nbDownloadedAssets = new AtomicLong(0);
    private final AtomicLong _nbDownloadedZeroSizeAssets = new AtomicLong(0);
    private final AtomicLong _nbDownloadedSymlinks = new AtomicLong(0);
    private final AtomicLong _nbSkippedAssets = new AtomicLong(0);
    private final AtomicLong _nbFailedAssets = new AtomicLong(0);
    private final AtomicLong _nbDownloadedBytes = new AtomicLong(0);
    private final AtomicInteger _nbDeletedAssets = new AtomicInteger(0);

    private final DataTransferListener<Path, String> _ul;
    private final DataTransferListener<String, Path> _dl;

    private Long _stimeLast = null;
    private Long _stime = null;

    private Long _execStartTime = null;

    protected MFSyncApp() {
        super();
        _settings = new unimelb.mf.client.sync.settings.Settings();
        _ul = new DataTransferListener<Path, String>() {

            @Override
            public void transferStarted(Path src, String dst) {
            }

            @Override
            public void transferFailed(Path src, String dst) {
                _nbFailedFiles.getAndIncrement();
                // TODO record failed files.
            }

            @Override
            public void transferCompleted(Path src, String dst) {
                _nbUploadedFiles.getAndIncrement();
                try {
                    if (Files.isSymbolicLink(src)) {
                        if (!_settings.followSymlinks()) {
                            _nbUploadedSymlinks.getAndIncrement();
                        }
                    } else if (Files.size(src) == 0) {
                        _nbUploadedZeroSizeFiles.getAndIncrement();
                    }
                } catch (IOException e) {
                    MFSyncApp.this.logger().log(Level.SEVERE, e.getMessage(), e);
                }
            }

            @Override
            public void transferSkipped(Path src, String dst) {
                _nbSkippedFiles.getAndIncrement();
            }

            @Override
            public void transferProgressed(Path src, String dst, long increment) {
                _nbUploadedBytes.getAndAdd(increment);
            }
        };
        _dl = new DataTransferListener<String, Path>() {

            @Override
            public void transferStarted(String src, Path dst) {
            }

            @Override
            public void transferFailed(String src, Path dst) {
                _nbFailedAssets.getAndIncrement();
                // TODO record failed files.
            }

            @Override
            public void transferCompleted(String src, Path dst) {
                _nbDownloadedAssets.getAndIncrement();
                try {
                    if (Files.isSymbolicLink(dst)) {
                        _nbDownloadedSymlinks.getAndIncrement();
                    } else if (Files.size(dst) == 0) {
                        _nbDownloadedZeroSizeAssets.getAndIncrement();
                    }
                } catch (IOException e) {
                    MFSyncApp.this.logger().log(Level.SEVERE, e.getMessage(), e);
                }
            }

            @Override
            public void transferSkipped(String src, Path dst) {
                _nbSkippedAssets.getAndIncrement();
            }

            @Override
            public void transferProgressed(String src, Path dst, long increment) {
                _nbDownloadedBytes.getAndAdd(increment);
            }
        };
    }

    @Override
    public String description() {
        return "The Mediaflux client application to upload, download or check data.";
    }

    protected static Logger createDefaultLogger(unimelb.mf.client.sync.settings.Settings settings, String appName)
            throws Throwable {
        Logger logger = settings.logDirectory() == null ? LoggingUtils.createConsoleLogger()
                : LoggingUtils.createFileAndConsoleLogger(settings.logDirectory(), appName,
                        settings.logFileSizeMB() * 1000000, settings.logFileCount());
        logger.setLevel(settings.verbose() ? Level.ALL : Level.WARNING);
        return logger;
    }

    protected void preExecute() throws Throwable {

    }

    @Override
    public void run() {
        try {
            execute();
        } catch (Throwable e) {
            if (e instanceof InterruptedException) {
                logger().log(Level.WARNING, e.getMessage());
                Thread.currentThread().interrupt();
            } else {
                logger().log(Level.SEVERE, e.getMessage(), e);
            }
        }
    }

    @Override
    public void execute() throws Throwable {

        preExecute();

        // take down the current system time.
        _execStartTime = System.currentTimeMillis();

        if (!settings().hasJobs()) {
            throw new Exception("No job found!");
        }
        if (settings().hasOnlyCheckJobs()) {
            // Check jobs do not need to run into daemon mode.
            settings().setDaemon(false);
        }

        _queriers = new ThreadPoolExecutor(settings().numberOfQueriers(), settings().numberOfQueriers(), 0,
                TimeUnit.MILLISECONDS, new ArrayBlockingQueue<>(100),
                r -> new Thread(r, applicationName() + ".querier"), new ThreadPoolExecutor.CallerRunsPolicy());

        _workers = new ThreadPoolExecutor(settings().numberOfWorkers(), settings().numberOfWorkers(), 0,
                TimeUnit.MILLISECONDS, new ArrayBlockingQueue<>(100), r -> new Thread(r, applicationName() + ".worker"),
                new ThreadPoolExecutor.CallerRunsPolicy());

        try {

            session().startPingServerPeriodically(60000);

            // starts the daemon regardless whether is recurring execution or
            // not. As the listener socket is useful for the one-off execution
            // during the execution. Note: the daemon timer will not started for
            // one-off execution.
            startDaemon();

            submitJobs();

            if (!settings().daemon() && (settings().needToDeleteAssets() || settings().needToDeleteFiles())) {
                // waiting until the threadpools are clear. This is
                // required for sync jobs.
                while (_queriers.getActiveCount() > 0 || !_queriers.getQueue().isEmpty()
                        || _workers.getActiveCount() > 0 || !_workers.getQueue().isEmpty()) {
                    Thread.sleep(1000);
                }
                if (settings().deleteAssets()) {
                    syncDeleteAssets();
                }
                if (settings().deleteFiles()) {
                    syncDeleteFiles();
                }
            }
        } catch (Throwable e) {
            logger().log(Level.SEVERE, e.getMessage(), e);
        } finally {
            if (!settings().daemon()) {
                // shutdown the thread pools and wait until they complete
                shutdown(true);
                // stop the daemon listener (socket). The daemon timer was not
                // started.
                stopDaemon();
                // post execution procedure to handle the results etc.
                postExecute();
            }
        }
    }

    public void startDaemon() {
        if (settings().daemon()) {
            // Recurring execution requires a timer.
            if (_daemonTimer == null) {
                _daemonTimer = new Timer();
                _daemonTimer.schedule(new TimerTask() {

                    @Override
                    public void run() {
                        if (!settings().hasJobs() || settings().hasOnlyCheckJobs()) {
                            logger().info("No transfer jobs found! Stopping...");
                            interrupt();
                        }
                        if (_queriers.getActiveCount() == 0 && _queriers.getQueue().isEmpty()
                                && _workers.getActiveCount() == 0 && _workers.getQueue().isEmpty()) {
                            try {
                                if (settings().deleteAssets()) {
                                    syncDeleteAssets();
                                }
                                if (settings().deleteFiles()) {
                                    syncDeleteFiles();
                                }
                                // waiting until the thread pools are clear.
                                // This is required for sync jobs, and the
                                // situation, where there are both download and
                                // upload jobs
                                while (_queriers.getActiveCount() > 0 || !_queriers.getQueue().isEmpty()
                                        || _workers.getActiveCount() > 0 || !_workers.getQueue().isEmpty()) {
                                    // wait until threadpools are clear.
                                    Thread.sleep(1000);
                                }
                                submitJobs();
                            } catch (Throwable e) {
                                logger().log(Level.SEVERE, e.getMessage(), e);
                            }
                        }
                    }
                }, settings().daemonScanInterval(), settings().daemonScanInterval());
            }
        }
        if (_daemonListener == null) {
            // Both recurring execution and one-off execution can benefit from
            // the listener.
            _daemonListener = new Thread(() -> {
                try {
                    try {
                        _daemonListenerSocket = new ServerSocket(_settings.daemonListenerPort(), 0,
                                InetAddress.getByName(null));
                    } catch (BindException be) {
                        _daemonListenerSocket = new ServerSocket(0, 0, InetAddress.getByName(null));
                        String msg = "Local port: " + _settings.daemonListenerPort()
                                + " is already in use. Use available port: " + _daemonListenerSocket.getLocalPort()
                                + " instead.";
                        logger().warning(msg);
                    }
                    if (settings().daemon()) {
                        logger().info("Listening to local port: " + _daemonListenerSocket.getLocalPort()
                                + ". You can run command 'echo status | nc localhost "
                                + _daemonListenerSocket.getLocalPort() + "' to check the progress.");
                    }
                    try {
                        outerloop: while (!Thread.interrupted() && !_daemonListenerSocket.isClosed()) {
                            try (Socket client = _daemonListenerSocket.accept()) {
                                BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
                                while (!Thread.interrupted()) {
                                    String cmd = in.readLine();
                                    if ("stop".equalsIgnoreCase(cmd)) {
                                        interrupt();
                                        break outerloop;
                                    } else if ("status".equalsIgnoreCase(cmd)) {
                                        printSummary(new PrintStream(client.getOutputStream(), true));
                                        break;
                                    } else {
                                        break;
                                    }
                                }
                            }
                        }
                    } catch (SocketException se) {
                        if (settings().verbose()) {
                            logger().info("Listening socket closed!");
                        }
                    } finally {
                        _daemonListenerSocket.close();
                    }
                } catch (Throwable e) {
                    logger().log(Level.SEVERE, e.getMessage(), e);
                }
            }, applicationName().toLowerCase() + ".daemon.listener");
            _daemonListener.start();
        }
    }

    protected String generateSummary() {

        StringBuilder sb = new StringBuilder();
        long durationMillis = System.currentTimeMillis() - _execStartTime;
        List<Job> jobs = settings().jobs();
        if (jobs != null && !jobs.isEmpty()) {
            sb.append("\n");
            if (settings().hasDownloadJobs()) {
                sb.append("Download:\n");
                for (Job job : jobs) {
                    if (job instanceof CollectionDownloadJob) {
                        CollectionDownloadJob nsdj = (CollectionDownloadJob) job;
                        sb.append("    src(mediaflux): ").append(nsdj.srcCollection().path()).append("\n");
                        sb.append("    dst(directory): ").append(nsdj.dstDirectory().toString()).append("\n");
                    } else if (job instanceof AssetSetDownloadJob) {
                        AssetSetDownloadJob asdj = (AssetSetDownloadJob) job;
                        for (String assetPath : asdj.srcAssetPaths()) {
                            sb.append("        src(asset): ").append(assetPath).append("\n");
                            sb.append("         dst(file): ").append(asdj.transformPath(assetPath)).append("\n");
                        }
                    } else if (job instanceof CollectionDownloadCheckJob) {
                        CollectionDownloadCheckJob nsdcj = (CollectionDownloadCheckJob) job;
                        sb.append("    src(mediaflux): ").append(nsdcj.srcCollection().path()).append("\n");
                        sb.append("    dst(directory): ").append(nsdcj.dstDirectory().toString()).append("\n");
                    }
                }
            }
            if (settings().hasUploadJobs()) {
                sb.append("Upload:\n");
                for (Job job : jobs) {
                    if (job instanceof DirectoryUploadJob) {
                        DirectoryUploadJob uj = (DirectoryUploadJob) job;
                        sb.append("    src(directory): ").append(uj.srcDirectory().toString()).append("\n");
                        sb.append("    dst(mediaflux): ").append(uj.dstCollection()).append("\n");
                    } else if (job instanceof FileSetUploadJob) {
                        FileSetUploadJob uj = (FileSetUploadJob) job;
                        sb.append("    src(files):     ").append(uj.srcFiles().size()).append(" files").append("\n");
                        sb.append("    dst(mediaflux): ").append(uj.dstCollection()).append("\n");
                    } else if (job instanceof DirectoryUploadCheckJob) {
                        DirectoryUploadCheckJob ucj = (DirectoryUploadCheckJob) job;
                        sb.append("    src(directory): ").append(ucj.srcDirectory().toString()).append("\n");
                        sb.append("    dst(mediaflux): ").append(ucj.dstCollection()).append("\n");
                    } else if (job instanceof FileSetUploadCheckJob) {
                        FileSetUploadCheckJob ucj = (FileSetUploadCheckJob) job;
                        sb.append("    src(files):     ").append(ucj.srcFiles().size()).append(" files").append("\n");
                        sb.append("    dst(mediaflux): ").append(ucj.dstCollection()).append("\n");
                    }
                }
            }
            sb.append("\n");
        }
        sb.append("\n");
        sb.append("Summary:\n");
        if (_settings.daemon()) {
            sb.append(String.format("           Up time: %s", TimeUtils.humanReadableDuration(durationMillis)))
                    .append("\n");
        } else {
            sb.append(String.format("         Exec time: %s", TimeUtils.humanReadableDuration(durationMillis)))
                    .append("\n");
        }
        sb.append(String.format("    Worker threads: %d", _settings.numberOfWorkers())).append("\n");

        sb.append("\n");

        long totalFiles = _nbFiles.get();
        long totalProcessedFiles = _nbUploadedFiles.get() + _nbSkippedFiles.get() + _nbFailedFiles.get();
        if (totalFiles > 0 || totalProcessedFiles > 0) {
            if (_nbUploadedZeroSizeFiles.get() > 0 || _nbUploadedSymlinks.get() > 0) {
                StringBuilder sb1 = new StringBuilder();
                if (_nbUploadedSymlinks.get() > 0) {
                    sb1.append(_nbUploadedSymlinks.get()).append(" symlink");
                }
                if (_nbUploadedZeroSizeFiles.get() > 0) {
                    if (sb1.length() > 0) {
                        sb1.append(", ");
                    }
                    sb1.append(_nbUploadedZeroSizeFiles.get()).append(" zero-size");
                }
                sb.append(String.format("    Uploaded files: %,32d files(%s)", _nbUploadedFiles.get(), sb1.toString()))
                        .append("\n");
            } else {
                sb.append(String.format("    Uploaded files: %,32d files", _nbUploadedFiles.get())).append("\n");
            }
            sb.append(String.format("     Skipped files: %,32d files", _nbSkippedFiles.get())).append("\n");
            sb.append(String.format("      Failed files: %,32d files", _nbFailedFiles.get())).append("\n");
            if (!_settings.daemon()) {
                sb.append(String.format("       Total files: %,32d files", totalFiles)).append("\n");
            }
            sb.append(String.format("    Uploaded bytes: %,32d bytes", _nbUploadedBytes.get())).append("\n");
            if (!_settings.daemon()) {
                // @formatter:off
                sb.append(String.format("      Upload speed: %,32.3f MB/s",
                        (double) _nbUploadedBytes.get() / 1000.0 / ((double) durationMillis))).append("\n");
                // @formatter:on
            }
            sb.append("\n");
            if (!_settings.daemon()) {
                if (totalFiles != totalProcessedFiles) {
                    sb.append("\nWARNING: processed ").append(totalProcessedFiles).append(" files out of total of ")
                            .append(totalFiles).append(". Please check for errors in the log file.\n");
                }
            }
        }
        int deletedAssets = _nbDeletedAssets.get();
        if (deletedAssets > 0) {
            sb.append(String.format("    Deleted Assets: %,32d assets", _nbDeletedAssets.get())).append("\n");
            sb.append("\n");
        }

        long totalAssets = _nbAssets.get();
        long totalProcessedAssets = _nbDownloadedAssets.get() + _nbSkippedAssets.get() + _nbFailedAssets.get();
        if (totalAssets > 0 || totalProcessedAssets > 0) {
            if (_nbDownloadedZeroSizeAssets.get() > 0 || _nbDownloadedSymlinks.get() > 0) {
                StringBuilder sb1 = new StringBuilder();
                if (_nbDownloadedSymlinks.get() > 0) {
                    sb1.append(_nbDownloadedSymlinks.get()).append(" symlink");
                }
                if (_nbDownloadedZeroSizeAssets.get() > 0) {
                    if (sb1.length() > 0) {
                        sb1.append(", ");
                    }
                    sb1.append(_nbDownloadedZeroSizeAssets.get()).append(" zero-size");
                }
                sb.append(
                        String.format(" Downloaded assets: %,32d files(%s)", _nbDownloadedAssets.get(), sb1.toString()))
                        .append("\n");
            } else {
                sb.append(String.format(" Downloaded assets: %,32d files", _nbDownloadedAssets.get())).append("\n");
            }
            sb.append(String.format("    Skipped assets: %,32d files", _nbSkippedAssets.get())).append("\n");
            sb.append(String.format("     Failed assets: %,32d files", _nbFailedAssets.get())).append("\n");
            if (!_settings.daemon()) {
                sb.append(String.format("      Total assets: %,32d files", totalAssets)).append("\n");
            }
            sb.append(String.format(" Downloaded  bytes: %,32d bytes", _nbDownloadedBytes.get())).append("\n");
            if (!_settings.daemon()) {
                // @formatter:off
                sb.append(String.format("    Download speed: %,32.3f MB/s",
                        (double) _nbDownloadedBytes.get() / 1000.0 / ((double) durationMillis))).append("\n");
                // @formatter:on
            }
            sb.append("\n");
            if (!_settings.daemon()) {
                if (totalAssets != totalProcessedAssets) {
                    sb.append("\nWARNING: processed ").append(totalProcessedAssets).append(" assets out of total of ")
                            .append(totalAssets).append(". Please check for errors in the log file.\n");
                }
            }
        }
        int deletedFiles = _nbDeletedFiles.get();
        if (deletedFiles > 0) {
            sb.append(String.format("     Deleted files: %,32d files", _nbDeletedFiles.get())).append("\n");
            sb.append("\n");
        }
        return sb.toString();
    }

    protected void printSummary(PrintStream ps) {
        ps.print(generateSummary());
    }

    public void stopDaemon() {
        if (_daemonTimer != null) {
            _daemonTimer.cancel();
            _daemonTimer = null;
        }
        if (_daemonListener != null) {
            try {
                if (_daemonListenerSocket != null) {
                    _daemonListenerSocket.close();
                }
            } catch (IOException e) {
                logger().log(Level.SEVERE, e.getMessage(), e);
            } finally {
                _daemonListener.interrupt();
            }
        }
    }

    private void submitJobs() throws Throwable {
        if (_stime != null) {
            _stimeLast = _stime;
        }
        _stime = session().execute("server.clock.time").longValue("stime", null);
        List<Job> jobs = settings().jobs();
        if (jobs != null) {
            for (Job job : jobs) {
                switch (job.action()) {
                case DOWNLOAD:
                    submitDownloadJob((DownloadJob) job);
                    break;
                case UPLOAD:
                    submitUploadJob((UploadJob) job);
                    break;
                case CHECK_DOWNLOAD:
                    submitDownloadCheckJob((DownloadCheckJob) job);
                    break;
                case CHECK_UPLOAD:
                    submitUploadCheckJob((UploadCheckJob) job);
                    break;
                default:
                    break;
                }
            }
        }
    }

    private void submitDownloadCheckJob(DownloadCheckJob job) throws Throwable {
        if (job instanceof CollectionDownloadCheckJob) {
            CollectionDownloadCheckJob dcj = (CollectionDownloadCheckJob) job;

            final CollectionDetails srcCollection = dcj.srcCollection().resolve(session());
            long total = srcCollection.countFileAssets(session());
            _nbAssets.getAndAdd(total);
            settings().checkHandler().addTotal(total);

            XmlStringWriter w = new XmlStringWriter();
            if (srcCollection.isAssetNamespace()) {
                w.add("namespace", srcCollection.path());
            } else {
                w.add("collection", "path=" + srcCollection.path());
            }
            w.add("where", "(asset has content or asset has link) and not(asset is collection)");
            w.add("action", "get-meta");
            w.add("as", "iterator");
            String iteratorId = session().execute("asset.query", w.document()).value("iterator");

            try {
                boolean complete = false;
                long processed = 0;
                while (!complete) {
                    XmlDoc.Element re = session().execute("asset.query.iterate",
                            "<id>" + iteratorId + "</id><size>" + settings().batchSize() + "</size>");
                    complete = re.booleanValue("iterated/@complete");
                    List<XmlDoc.Element> aes = re.elements("asset");
                    if (aes != null && !aes.isEmpty()) {
                        long from = processed + 1;
                        long to = processed + aes.size();
                        logger().info("checking " + aes.size() + " assets (" + from + "~" + to + "/" + total + ")...");
                        List<AssetItem> ais = new ArrayList<>(aes.size());
                        for (XmlDoc.Element ae : aes) {
                            String assetPath = ae.value("path");
                            long assetContentSize = ae.longValue("content/size", -1);
                            String assetCsum = ae.value("content/csum[@base='16']");
                            String symlinkTarget = SymlinkAsset.getSymlinkTarget(ae);
                            AssetItem ai = new AssetItem(assetPath, dcj.srcCollection().path(), assetContentSize,
                                    assetCsum, ChecksumType.CRC32, symlinkTarget);
                            ais.add(ai);
                        }
                        _queriers.submit(new AssetSetCheckTask(session(), logger(), ais, dcj, settings().csumCheck(),
                                settings().checkHandler(), _workers));
                        processed += aes.size();
                    }
                }
            } finally {
                AssetQueryUtils.destroyIterator(session(), iteratorId, true);
            }
        } else {
            throw new UnsupportedOperationException("Unexpected job type: " + job.getClass().getName());
        }
    }

    private void submitUploadCheckJob(UploadCheckJob job) throws Throwable {
        if (job instanceof DirectoryUploadCheckJob) {
            DirectoryUploadCheckJob ucj = (DirectoryUploadCheckJob) job;
            List<Path> files = new ArrayList<>(settings().batchSize());
            Files.walkFileTree(ucj.srcDirectory(),
                    settings().followSymlinks() ? EnumSet.of(FileVisitOption.FOLLOW_LINKS)
                            : EnumSet.noneOf(FileVisitOption.class),
                    Integer.MAX_VALUE, new SimpleFileVisitor<Path>() {
                        @Override
                        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
                            try {
                                if (settings().followSymlinks() && Files.isSymbolicLink(file)
                                        && Files.isDirectory(file)) {
                                    // If follow symbolic links to directories, it should never get here. However,
                                    // to
                                    // catch unforeseeable exceptions, we throw exception here if it does happen.
                                    throw new IOException("Unexpected symbolic link: '" + file
                                            + "'. It's a symbolic link to a directory.");
                                }
                                if (ucj.accept(file)) {
                                    files.add(file);
                                    _nbFiles.getAndIncrement();
                                    settings().checkHandler().addTotal(1);
                                    if (files.size() >= settings().batchSize()) {
                                        // check files
                                        _queriers.submit(new FileSetCheckTask(session(), logger(),
                                                new ArrayList<>(files), settings().followSymlinks(), ucj,
                                                settings().csumCheck(), settings().checkHandler(), _workers));
                                        files.clear();
                                    }
                                }
                            } catch (Throwable e) {
                                logger().log(Level.SEVERE, e.getMessage(), e);
                            }
                            return FileVisitResult.CONTINUE;
                        }

                        @Override
                        public FileVisitResult visitFileFailed(Path file, IOException ioe) {
                            logger().log(Level.SEVERE, "Failed to access file: " + file, ioe);
                            if (ucj.accept(file)) {
                                String assetPath = ucj.transformPath(file);
                                CheckHandler ch = settings().checkHandler();
                                ch.checked(new CheckResult(file, null, null, null, assetPath, null, null, null, false,
                                        _settings.csumCheck()));
                            }
                            return FileVisitResult.CONTINUE;
                        }

                        @Override
                        public FileVisitResult postVisitDirectory(Path dir, IOException ioe) {
                            if (ioe != null) {
                                logger().log(Level.SEVERE, "[postVisitDirectory] " + dir + ": " + ioe.getMessage(),
                                        ioe);
                            }
                            return FileVisitResult.CONTINUE;
                        }

                        @Override
                        public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
                                throws IOException {
                            return super.preVisitDirectory(dir, attrs);
                        }
                    });
            if (!files.isEmpty()) {
                // check files
                _queriers.submit(new FileSetCheckTask(session(), logger(), new ArrayList<>(files),
                        settings().followSymlinks(), ucj, settings().csumCheck(), settings().checkHandler(), _workers));
                files.clear();
            }
        } else if (job instanceof FileSetUploadCheckJob) {
            FileSetUploadCheckJob ucj = (FileSetUploadCheckJob) job;

            int nfs = ucj.nbSrcFiles();
            _nbFiles.getAndAdd(nfs);
            settings().checkHandler().addTotal(nfs);

            _queriers.submit(new FileSetCheckTask(session(), logger(), new ArrayList<>(ucj.srcFiles()),
                    settings().followSymlinks(), ucj, settings().csumCheck(), settings().checkHandler(), _workers));
        } else {
            throw new UnsupportedOperationException("Unexpected job type: " + job.getClass().getName());
        }
    }

    private void submitDownloadJob(DownloadJob job) throws Throwable {
        if (job instanceof CollectionDownloadJob) {

            // add total assets
            CollectionDownloadJob cdj = (CollectionDownloadJob) job;
            logger().info("counting the number of assets to download...");
            long nba = cdj.srcCollection().resolve(session()).countFileAssets(session());
            _nbAssets.getAndAdd(nba);

            // submit
            _queriers.submit(new CollectionDownloadTask(session(), logger(), cdj, _stimeLast, settings().batchSize(),
                    settings().overwrite(), settings().unarchive(), settings().restoreSymlinks(),
                    settings().csumCheck(), settings().includeMetadata(), settings().aggregateDownloadThreshold(),
                    _workers, _dl));
        } else if (job instanceof AssetSetDownloadJob) {

            // add total assets
            AssetSetDownloadJob asj = (AssetSetDownloadJob) job;
            Collection<String> srcAssetPaths = asj.srcAssetPaths();
            if (srcAssetPaths != null) {
                _nbAssets.getAndAdd(srcAssetPaths.size());
            }

            _queriers.submit(new AssetSetDownloadTask(session(), logger(), (AssetSetDownloadJob) job,
                    settings().batchSize(), settings().overwrite(), settings().unarchive(),
                    settings().restoreSymlinks(), settings().csumCheck(), settings().includeMetadata(), _workers, _dl));
        } else {
            throw new UnsupportedOperationException("Unexpected job type: " + job.getClass().getName());
        }
    }

    private void submitUploadJob(UploadJob job) throws Throwable {

        // Resolve the dst collection details,
        // e.g. the content store associated with the dst collection asset or dst asset
        // namespace,
        // it is used by Input.setStore().
        final CollectionDetails dstCollection = job.dstCollection().createIfNotExist(session(),
                settings().createParentCollections());

        if (job instanceof DirectoryUploadJob) {
            DirectoryUploadJob duj = (DirectoryUploadJob) job;
            List<Path> files = new ArrayList<>(settings().batchSize());
            Files.walkFileTree(duj.srcDirectory(),
                    settings().followSymlinks() ? EnumSet.of(FileVisitOption.FOLLOW_LINKS)
                            : EnumSet.noneOf(FileVisitOption.class),
                    Integer.MAX_VALUE, new SimpleFileVisitor<Path>() {
                        @Override
                        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
                            try {
                                if (settings().followSymlinks() && Files.isSymbolicLink(file)
                                        && Files.isDirectory(file)) {
                                    // If follow symbolic links to directories, it should never get here. However,
                                    // to
                                    // catch unforeseeable exceptions, we throw exception here if it does happen.
                                    throw new IOException("Unexpected symbolic link: '" + file
                                            + "'. It's a symbolic link to a directory.");
                                }
                                if (duj.accept(file)) {
                                    if (attrs.isOther()) {
                                        // Only supports regular files, directories and symbolic links.
                                        // This should skip FIFO (named pipe) files
                                        if (logger() != null) {
                                            logger().log(Level.SEVERE,
                                                    "Cannot upload file: '" + file + "'. Unsupported file type.");
                                            _ul.transferFailed(file, duj.transformPath(file));
                                        }
                                    } else {
                                        files.add(file);
                                        _nbFiles.getAndIncrement();
                                        if (files.size() >= settings().batchSize()) {
                                            _queriers.submit(new FileSetUploadTask(session(), logger(),
                                                    new ArrayList<>(files), settings().followSymlinks(), duj,
                                                    settings().csumCheck(), settings().worm(),
                                                    settings().wormCanAddVersions(), settings().wormCanMove(),
                                                    settings().saveFileAttributes(),
                                                    settings().aggregateUploadThreshold(), settings().splitThreshold(),
                                                    settings().maxNumberOfRetries(), settings().retryWaitTime(),
                                                    settings().preserveModifiedTime(), _ul, _workers));
                                            files.clear();
                                        }
                                    }
                                }
                            } catch (Throwable e) {
                                logger().log(Level.SEVERE, e.getMessage(), e);
                            }
                            return FileVisitResult.CONTINUE;
                        }

                        @Override
                        public FileVisitResult visitFileFailed(Path file, IOException ioe) {
                            logger().log(Level.SEVERE, "Failed to access file: " + file, ioe);
                            if (duj.accept(file)) {
                                String assetPath = duj.transformPath(file);
                                _ul.transferFailed(file, assetPath);
                            }
                            return FileVisitResult.CONTINUE;
                        }

                        @Override
                        public FileVisitResult postVisitDirectory(Path dir, IOException ioe) {
                            if (ioe != null) {
                                logger().log(Level.SEVERE, ioe.getMessage(), ioe);
                            }
                            try {
                                if (FileUtils.isEmptyDirectory(dir)) {
                                    _workers.submit(new CollectionCreateTask(session(), logger(),
                                            duj.transformPath(dir), dstCollection.isAssetNamespace()));
                                }
                            } catch (IOException e) {
                                logger().log(Level.SEVERE, e.getMessage(), e);
                            }
                            return FileVisitResult.CONTINUE;
                        }

                        @Override
                        public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
                                throws IOException {
                            return super.preVisitDirectory(dir, attrs);
                        }
                    });
            if (!files.isEmpty()) {
                _queriers.submit(new FileSetUploadTask(session(), logger(), new ArrayList<>(files),
                        settings().followSymlinks(), duj, settings().csumCheck(), settings().worm(),
                        settings().wormCanAddVersions(), settings().wormCanMove(), settings().saveFileAttributes(),
                        settings().aggregateUploadThreshold(), settings().splitThreshold(),
                        settings().maxNumberOfRetries(), settings().retryWaitTime(), settings().preserveModifiedTime(),
                        _ul, _workers));
                files.clear();
            }
        } else if (job instanceof FileSetUploadJob) {
            FileSetUploadJob uj = (FileSetUploadJob) job;
            List<Path> srcFiles = uj.hasSrcFiles() ? new ArrayList<>(uj.srcFiles()) : null;
            if (srcFiles != null) {
                _nbFiles.getAndAdd(srcFiles.size());
                int batchSize = settings().batchSize();
                int nbBatches = srcFiles.size() / batchSize;
                int remainder = srcFiles.size() % batchSize;
                if (remainder != 0) {
                    nbBatches++;
                }

                for (int i = 0; i < nbBatches; i++) {
                    int from = i * batchSize;
                    int to = i * batchSize + ((remainder == 0 || i < nbBatches - 1) ? batchSize : remainder);
                    List<Path> files = srcFiles.subList(from, to);
                    _queriers.submit(new FileSetUploadTask(session(), logger(), new ArrayList<>(files),
                            settings().followSymlinks(), uj, settings().csumCheck(), settings().worm(),
                            settings().wormCanAddVersions(), settings().wormCanMove(), settings().saveFileAttributes(),
                            settings().aggregateUploadThreshold(), settings().splitThreshold(),
                            settings().maxNumberOfRetries(), settings().retryWaitTime(),
                            settings().preserveModifiedTime(), _ul, _workers));

                }
            }

        } else {
            throw new UnsupportedOperationException("Unexpected job type: " + job.getClass().getName());
        }
    }

    private void syncDeleteAssets() throws Throwable {
        List<Job> jobs = settings().jobs();
        for (Job job : jobs) {
            if (job instanceof DirectoryUploadJob) {
                _queriers.submit(new SyncDeleteAssetsTask(session(), logger(), (DirectoryUploadJob) job, settings(),
                        _nbDeletedAssets));
            }
        }
    }

    private void syncDeleteFiles() {
        List<Job> jobs = settings().jobs();
        for (Job job : jobs) {
            if (job instanceof CollectionDownloadJob) {
                _queriers.submit(new SyncDeleteFilesTask(session(), logger(), (CollectionDownloadJob) job, settings(),
                        _workers, _nbDeletedFiles));
            }
        }
    }

    public void interrupt() {
        if (_queriers != null && !_queriers.isShutdown()) {
            _queriers.shutdownNow();
        }
        if (_workers != null && !_workers.isShutdown()) {
            _workers.shutdownNow();
        }
        stopDaemon();

        // NOTE: We must discard the session because connection pooling is
        // enabled.
        session().discard();
    }

    public void shutdown(boolean wait) {
        try {
            if (_queriers != null) {
                if (!_queriers.isShutdown()) {
                    // now all jobs have been submitted
                    _queriers.shutdown();
                }
                if (wait) {
                    // wait until all tasks are submitted by queriers.
                    _queriers.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
                }
            }

            if (_workers != null) {
                if (!_workers.isShutdown()) {
                    // now all tasks have been submitted
                    _workers.shutdown();
                }
                if (wait) {
                    // wait until all tasks are processed by workers.
                    _workers.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
                }
            }

            // NOTE: We must discard the session because connection pooling is
            // enabled.
            session().discard();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            logger().info("Interrupted '" + Thread.currentThread().getName() + "' thread(id="
                    + Thread.currentThread().getId() + ").");
        } catch (Throwable e) {
            logger().log(Level.SEVERE, e.getMessage(), e);
        }
    }

    protected void postExecute() {
        if (!settings().hasOnlyCheckJobs()) {
            String summary = generateSummary();
            if (settings().logDirectory() != null) {
                logger().info(summary);
            }
            if (settings().logDirectory() == null || !settings().verbose()) {
                System.out.print(summary);
            }

            if (settings().hasRecipients()) {
                notifySummary(summary);
            }
        }
    }

    @Override
    public unimelb.mf.client.sync.settings.Settings settings() {
        return _settings;
    }

    public void notifySummary(String summary) {
        try {
            String subject = applicationName() + " results [" + new Date() + "]";
            notify(subject, summary);
        } catch (Throwable e) {
            logger().log(Level.SEVERE, e.getMessage(), e);
        }
    }

    public void notify(String subject, String message) throws Throwable {
        if (settings().hasRecipients()) {
            MailUtils.sendMail(session(), settings().recipients(), subject, message, true);
        }
    }

    public void printVersion() {
        System.out.printf("%s %s (build-time: %s)%n", this.applicationName(), App.version(), App.buildTime());
    }

}
