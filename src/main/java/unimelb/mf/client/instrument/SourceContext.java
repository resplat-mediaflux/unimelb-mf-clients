package unimelb.mf.client.instrument;

import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.EnumSet;
import java.util.concurrent.atomic.AtomicLong;

public class SourceContext {
    public final Path path;
    public final boolean exists;
    public final Long numberOfFiles;
    public final Long numberOfLinks;
    public final Long totalBytes;

    SourceContext(Path path, boolean exists, Long numberOfFiles, Long numberOfLinks, Long totalBytes) {
        this.path = path;
        this.exists = exists;
        this.numberOfFiles = numberOfFiles;
        this.numberOfLinks = numberOfLinks;
        this.totalBytes = totalBytes;
    }

    public static SourceContext resolve(Path dir, boolean followLinks) {
        boolean exists = Files.exists(dir);
        if (!exists) {
            return new SourceContext(dir, exists, null, null, null);
        }
        AtomicLong nbFiles = new AtomicLong(0);
        AtomicLong nbSymlinks = new AtomicLong(0);
        AtomicLong totalBytes = new AtomicLong(0);
        System.out.println("summing up the total size of files in '" + dir + "' ...");
        try {
            Files.walkFileTree(dir,
                    followLinks ? EnumSet.of(FileVisitOption.FOLLOW_LINKS) : EnumSet.noneOf(FileVisitOption.class),
                    Integer.MAX_VALUE, new SimpleFileVisitor<Path>() {
                        @Override
                        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
                            try {
                                if (Files.isRegularFile(file)) {
                                    nbFiles.getAndIncrement();
                                    totalBytes.getAndAdd(Files.size(file));
                                } else if (Files.isSymbolicLink(file)) {
                                    nbSymlinks.getAndIncrement();
                                }
                            } catch (Throwable e) {
                                System.err.println("failed to read file: '" + file + "': " + e.getMessage());
                            }
                            return FileVisitResult.CONTINUE;
                        }

                        @Override
                        public FileVisitResult visitFileFailed(Path file, IOException ioe) {
                            System.err.println("failed to access file: '" + file + "': " + ioe.getMessage());
                            return FileVisitResult.CONTINUE;
                        }
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new SourceContext(dir, exists, nbFiles.get(), nbSymlinks.get(), totalBytes.get());
    }

    public static SourceContext resolve(String dirPath, boolean followLinks) throws IOException {
        return resolve(Paths.get(dirPath), followLinks);
    }
}
