package unimelb.mf.client.instrument;

import java.io.PrintStream;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

import org.jline.utils.OSUtils;

import arc.utils.DateTime;
import arc.xml.XmlDoc;
import unimelb.utils.FileSizeUtils;
import unimelb.utils.StringUtils;

public class Upload {

    private long _shareableId;
    private long _uploadId;
    private Long _interactionId;
    private String _namespace;
    private Date _completedAt;
    private String _pathContext;
    private Set<String> _shareWith;
    private Set<String> _keywords;
    private Long _totalFileCount;
    private Long _totalFileSize;
    private SourceContext _srcContext;

    public Upload(XmlDoc.Element ue, long shareableId) throws Throwable {
        _shareableId = shareableId;
        _uploadId = ue.longValue("@id");
        _interactionId = ue.longValue("@interaction-id", null);
        _namespace = ue.value("namespace");
        _totalFileCount = ue.longValue("total-file-count", 0);
        _totalFileSize = ue.longValue("total-file-size", 0);
        _completedAt = ue.dateValue("completed-at");
        _pathContext = ue.value("path-context");
        String shareWith = ue.value("arg[@name='share-with']");
        _shareWith = shareWith == null ? null : parseValues(shareWith);
        String keywords = ue.value("arg[@name='keywords']");
        _keywords = keywords == null ? null : parseValues(keywords);
    }

    public final long shareableId() {
        return _shareableId;
    }

    public final long uploadId() {
        return _uploadId;
    }

    public final Long interactionId() {
        return _interactionId;
    }

    public final Date completedAt() {
        return _completedAt;
    }

    public final String namespace() {
        return _namespace;
    }

    public final String parentName() {
        if (_namespace != null) {
            int idx = _namespace.lastIndexOf('/');
            if (idx >= 0) {
                return _namespace.substring(idx + 1);
            }
        }
        return null;
    }

    public final String pathContext() {
        return _pathContext;
    }

    public final String shareWith() {
        return _shareWith == null ? null : String.join(",", _shareWith);
    }

    public final Collection<String> shareWithEmails() {
        return _shareWith == null ? null : Collections.unmodifiableCollection(_shareWith);
    }

    public final boolean hasShareWithEmail(String email) {
        return _shareWith != null && _shareWith.contains(email.toLowerCase());

    }

    public final boolean hasAnyShareWithEmail(Collection<String> emails) {
        if (_shareWith != null && !_shareWith.isEmpty()) {
            for (String email : emails) {
                if (_shareWith.contains(email.toLowerCase())) {
                    return true;
                }
            }
        }
        return false;
    }

    public final String keywords() {
        return _keywords == null ? null : String.join(",", _keywords);
    }

    public final Collection<String> keywordValues() {
        return _keywords == null ? null : Collections.unmodifiableCollection(_keywords);
    }

    public final boolean hasKeyword(String keyword) {
        return _keywords != null && _keywords.contains(keyword.toLowerCase());
    }

    public final boolean hasAnyKeyword(Collection<String> keywords) {
        if (_keywords != null && !_keywords.isEmpty()) {
            for (String keyword : keywords) {
                if (_keywords.contains(keyword)) {
                    return true;
                }
            }
        }
        return false;
    }

    public final Long totalFileCount() {
        return _totalFileCount;
    }

    public final Long totalFileSize() {
        return _totalFileSize;
    }

    public final String totalFileSizeHR() {
        return FileSizeUtils.toHumanReadable(_totalFileSize);
    }

    public final boolean pathContextMatches(Path path) {
        if (_pathContext != null && path != null) {
            String pctx = _pathContext.replaceFirst("/+$", "");
            String p = path.toAbsolutePath().toString().replaceFirst("/+$", "");
            if (OSUtils.IS_WINDOWS) {
                p = p.replace('\\', '/');
            }
            if (pctx.equals(p)) {
                return true;
            }
            if (p.matches("^[a-zA-Z]{1}:/.*")) {
                // Windows Path starts with drive letter such as C:/Downloads/abc
                if (pctx.equals(p.substring(2))) {
                    return true;
                }
            }
        }
        return false;
    }

    public final SourceContext sourceContext() {
        return _srcContext;
    }

    public final SourceContext resolveSourceContext() throws Throwable {
        _srcContext = null;
        if (_pathContext != null) {
            _srcContext = SourceContext.resolve(_pathContext, false);
        }
        return _srcContext;
    }

    public final void printLine(PrintStream ps, int indent) {
        indent = indent < 0 ? 0 : indent;
        String indentStr = indent > 0 ? StringUtils.stringOf(' ', indent) : "";
        ps.printf("%supload %d:%n", indentStr, _uploadId);
        indent += 4;
        indentStr = StringUtils.stringOf(' ', indent);
        if (_namespace != null) {
            ps.printf("%snamespace: %s%n", indentStr, _namespace);
        }
        if (_completedAt != null) {
            ps.printf("%scompleted-at: %s%n", indentStr, DateTime.string(_completedAt));
        }
        if (_pathContext != null) {
            ps.printf("%spath-context: %s%n", indentStr, _pathContext);
        }
        if (_shareWith != null) {
            ps.printf("%sshare-with: %s%n", indentStr, shareWith());
        }
        if (_keywords != null) {
            ps.printf("%skeywords: %s%n", indentStr, keywords());
        }
        if (_totalFileCount != null) {
            ps.printf("%sfile-count: %,d%n", indentStr, totalFileCount());
        }
        if (_totalFileSize != null) {
            ps.printf("%stotal-size: %,d bytes%n", indentStr, totalFileSize());
        }
        if (_srcContext != null) {

            ps.printf("%ssource:%n", indentStr);

            String indentStr2 = StringUtils.stringOf(' ', indent + 4);
            ps.printf("%spath: %s%n", indentStr2, _srcContext.path.toString());
            ps.printf("%sexists: %b%n", indentStr2, _srcContext.exists);
            if (_srcContext.numberOfFiles != null) {
                ps.printf("%sfile-count: %,d%n", indentStr2, _srcContext.numberOfFiles);
            }
            if (_srcContext.totalBytes != null) {
                ps.printf("%stotal-size: %,d bytes%n", indentStr2, _srcContext.totalBytes);
            }
            boolean match = _srcContext.numberOfFiles != null && _srcContext.numberOfFiles.equals(_totalFileCount)
                    && _srcContext.totalBytes != null && _srcContext.totalBytes.equals(_totalFileSize);
            ps.printf("%smatch: %b%n", indentStr2, match);
        }
    }

    public final void printCSVLine(PrintStream ps, boolean compared) {
        String completedAt = completedAt() == null ? ""
                : new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss").format(completedAt());
        String keywords = (_keywords == null || _keywords.isEmpty()) ? "" : String.join("; ", _keywords);
        String shareWith = (_shareWith == null || _shareWith.isEmpty()) ? "" : String.join("; ", _shareWith);
        ps.printf("%d,%d,\"%s\",%d,%d,%s,\"%s\",%s,\"%s\",", shareableId(), uploadId(), namespace(), totalFileCount(),
                totalFileSize(), completedAt, keywords, shareWith, pathContext());
        if (compared) {
            if (this.pathContext() == null || _srcContext == null || !_srcContext.exists) {
                ps.println(",,,");
            } else {
                boolean match = _srcContext.numberOfFiles != null && _srcContext.numberOfFiles.equals(_totalFileCount)
                        && _srcContext.totalBytes != null && _srcContext.totalBytes.equals(_totalFileSize);
                ps.printf("\"%s\",%s,%s,%b,", _srcContext.path.toAbsolutePath(),
                        _srcContext.numberOfFiles == null ? "" : Long.toString(_srcContext.numberOfFiles),
                        _srcContext.totalBytes == null ? "" : Long.toString(_srcContext.totalBytes), match);
            }
        }
        ps.println();
    }

    public static void printCSVHead(PrintStream ps, boolean compared) {
        ps.print(
                "Shareabe ID,Upload ID,Namespace,Number of Files,Total File Size,Completed At,Keywords,Shared With,Context Source Path,");
        if (compared) {
            ps.print("Local Path,Number of Files (local),Total File Size (local),Match?,");
        }
        ps.println();
    }

    public static void printCSVFoot(PrintStream ps, long nbFiles, long totalBytes, Long nbLocalFiles,
            Long totalLocalBytes, boolean compared) {
        ps.printf("Total,,,%d,%d,,,,,", nbFiles, totalBytes);
        if (compared) {
            ps.printf(",%s,%s,,", nbLocalFiles == null ? "" : Long.toString(nbLocalFiles),
                    totalLocalBytes == null ? "" : Long.toString(totalLocalBytes));
        }
        ps.println();
    }

    public static Set<String> parseValues(String s) {
        String[] vs = s.split("\\s*[,;]{1}\\s*");
        Set<String> set = new TreeSet<>();
        for (String v : vs) {
            set.add(v.toLowerCase());
        }
        return set;
    }

}
