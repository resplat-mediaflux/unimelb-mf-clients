package unimelb.mf.client.instrument.cli;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import arc.xml.XmlDoc;

public class DataMoverSettings {

    public static final Path FILE_PATH = Paths.get(System.getProperty("user.home"),
            ".Arcitecta/DataMover/settings.xml");

    public static XmlDoc.Element load() throws Throwable {
        try (Reader r = new BufferedReader(new FileReader(FILE_PATH.toFile()))) {
            return new XmlDoc().parse(r);
        }
    }

    public static String getToken() {
        if (!Files.exists(FILE_PATH)) {
            return null;
        }
        try {
            XmlDoc.Element settings = load();
            return settings.value("token");
        } catch (Throwable e) {
            return null;
        }
    }

}
