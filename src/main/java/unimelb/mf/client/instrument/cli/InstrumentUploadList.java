package unimelb.mf.client.instrument.cli;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.IExecutionExceptionHandler;
import picocli.CommandLine.Option;
import picocli.CommandLine.ParseResult;
import unimelb.mf.client.cli.MFCommand;
import unimelb.mf.client.instrument.Upload;
import unimelb.mf.client.instrument.UploadShareable;
import unimelb.mf.client.session.MFConfigBuilder;
import unimelb.mf.client.session.MFSession;
import unimelb.picocli.converters.DateTimeConverter;
import unimelb.picocli.converters.PathConverter;

@Command(name = "instrument-upload-list", abbreviateSynopsis = true, synopsisHeading = "\nUSAGE:\n  ", parameterListHeading = "\nPARAMETERS:\n", optionListHeading = "\nOPTIONS:\n", separator = " ", sortOptions = false)
public class InstrumentUploadList implements MFCommand {

    public static final String APPLICATION_NAME = "instrument-upload-list";

    @Option(names = {
            "--mf.config" }, paramLabel = "<mflux.cfg>", description = "Mediaflux server configuration file. Defaults to $HOME/.Arcitecta/mflux.cfg", required = false, converter = PathConverter.MustBeRegularFile.class)
    private Path mfCFG;

    @Option(names = { "-i", "--id" }, paramLabel = "<id>", description = "The instrument upload shareable id")
    private Long id;

    @Option(names = { "-t", "--token" }, paramLabel = "<token>", description = "The instrument upload shareable token")
    private String token;

    @Option(names = { "-s",
            "--share-with" }, paramLabel = "<email>", description = "The email address of the user", arity = "0..")
    private List<String> shareWith;

    @Option(names = { "-k",
            "--keywords" }, paramLabel = "<keyword>", description = "The keyword for the upload", arity = "0..")
    private List<String> keywords;

    @Option(names = { "-a",
            "--after" }, paramLabel = "<dd-MMM-yyyy>", description = "Include only the uploads completed after this date (inclusive).", converter = DateTimeConverter.class)
    private Date completedAfter;

    @Option(names = { "-b",
            "--before" }, paramLabel = "<dd-MMM-yyyy>", description = "Include only the uploads completed before this date (exclusive).", converter = DateTimeConverter.class)
    private Date completedBefore;

    @Option(names = { "-p", "--path" }, paramLabel = "<source_path>", description = "Context source path.")
    private String srcPath;

    @Option(names = { "-c", "--compare" }, description = "Compare with local source file/directory.")
    private boolean compare;

    @Option(names = { "-n", "--newest-to-oldest" }, description = "Sort newest to oldest.")
    private boolean newestToOldest;

    @Option(names = { "-o",
            "--output-csv" }, paramLabel = "<output.csv>", description = "Path to the output .csv file.")
    private Path outputCSVFile;

    @Option(names = { "-h", "--help" }, usageHelp = true, description = "display help information")
    private boolean printHelp;

    @Override
    public MFSession authenticate() throws Throwable {
        MFConfigBuilder mfcb = new MFConfigBuilder();
        mfcb.setApp(InstrumentUploadList.APPLICATION_NAME);
        if (this.mfCFG != null) {
            mfcb.loadFromConfigFile(mfCFG.toFile());
        } else {
            mfcb.findAndLoadFromConfigFile();
        }
        mfcb.setConsoleLogon(true);
        return MFSession.create(mfcb);
    }

    @Override
    public void execute(MFSession session) throws Throwable {

        Settings settings = new Settings();
        if (this.id != null) {
            settings.setShareableId(this.id);
        } else if (this.token != null) {
            settings.setShareableToken(this.token);
        } else {
            settings.setShareableToken(DataMoverSettings.getToken());
        }
        if (!settings.hasShareableIdOrToken()) {
            throw new IllegalArgumentException("No shareable id or token is specified!");
        }
        if (this.shareWith != null) {
            settings.setShareWith(shareWith);
        }
        if (this.keywords != null) {
            settings.setKeywords(this.keywords);
        }
        if (this.completedAfter != null) {
            settings.setAfter(completedAfter);
        }
        if (this.completedBefore != null) {
            settings.setBefore(completedBefore);
        }
        if (this.srcPath != null) {
            Path sp = Paths.get(this.srcPath);
            if (Files.exists(sp)) {
                settings.setSrcPath(sp);
            } else {
                settings.setSrcPath(sp);
            }
        }
        settings.setCompare(this.compare);
        settings.setNewestToOldest(this.newestToOldest);
        if (this.outputCSVFile != null) {
            settings.setOutputCSVFile(this.outputCSVFile);
        }

        Set<Upload> uploads = UploadShareable.findUploads(session, settings.shareableId(), settings.shareableToken(),
                settings.after(), settings.before(), settings.shareWith(), settings.keywords(), settings.srcPath(),
                settings.newestToOldest());
        if (!uploads.isEmpty()) {
            if (settings.compare()) {
                for (Upload upload : uploads) {
                    upload.resolveSourceContext();
                }
            }
            System.out.println();
            UploadShareable.printUploads(uploads, System.out);

            if (settings.outputCSVFile() != null) {
                UploadShareable.saveToCSVFile(session, uploads, settings.outputCSVFile(), settings.compare());
            }
        }

    }

    public static class Settings {
        private Long _id;
        private String _token;
        private Set<String> _shareWith;
        private Set<String> _keywords;
        private Date _before;
        private Date _after;
        private Path _srcPath;
        private boolean _compare = false;
        private boolean _newestToOldest = false;
        private Path _outputCSVFile;

        Long shareableId() {
            return _id;
        }

        String shareableToken() {
            return _token;
        }

        Collection<String> shareWith() {
            return _shareWith;
        }

        Collection<String> keywords() {
            return _keywords;
        }

        Date before() {
            return _before;
        }

        Date after() {
            return _after;
        }

        Path srcPath() {
            return _srcPath;
        }

        boolean compare() {
            return _compare;
        }

        boolean newestToOldest() {
            return _newestToOldest;
        }

        Path outputCSVFile() {
            return _outputCSVFile;
        }

        public Settings setShareableId(Long id) {
            _id = id;
            return this;
        }

        public Settings setShareableToken(String token) {
            _token = token;
            return this;
        }

        public Settings setShareWith(List<String> shareWith) {
            _shareWith = new LinkedHashSet<>();
            if (shareWith != null) {
                _shareWith.addAll(shareWith);
            }
            return this;
        }

        public Settings setKeywords(List<String> keywords) {
            _keywords = new LinkedHashSet<>();
            if (keywords != null) {
                _keywords.addAll(keywords);
            }
            return this;
        }

        public boolean hasShareableIdOrToken() {
            return _id != null || _token != null;
        }

        public Settings setBefore(Date before) {
            _before = before;
            return this;
        }

        public Settings setAfter(Date after) {
            _after = after;
            return this;
        }

        public Settings setSrcPath(Path srcPath) {
            _srcPath = srcPath;
            return this;
        }

        public Settings setCompare(boolean compare) {
            _compare = compare;
            return this;
        }

        public Settings setNewestToOldest(boolean newestToOldest) {
            _newestToOldest = newestToOldest;
            return this;
        }

        public Settings setOutputCSVFile(Path outputCSVFile) throws InterruptedException {
            String name = outputCSVFile.getFileName().toString();
            if (!name.toLowerCase().endsWith(".csv")) {
                name += ".csv";
                outputCSVFile = Paths.get(outputCSVFile.getParent().toString(), name);
            }
            while (Files.exists(outputCSVFile)) {
                Thread.sleep(50);
                name = name.substring(0, name.length() - 4) + "_" + currentTimeStamp() + ".csv";
                outputCSVFile = Paths.get(outputCSVFile.getParent().toString(), name);
            }
            _outputCSVFile = outputCSVFile;
            return this;
        }

        private static String currentTimeStamp() {
            return new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date());
        }
    }

    public static void main(String[] args) {
        CommandLine cl = new CommandLine(new InstrumentUploadList());
        cl.setExecutionExceptionHandler(new IExecutionExceptionHandler() {
            @Override
            public int handleExecutionException(Exception ex, CommandLine commandLine, ParseResult parseResult)
                    throws Exception {
                if (ex instanceof IllegalArgumentException) {
                    System.err.println("Error: " + ex.getMessage());
//                    ex.printStackTrace();
                    cl.usage(System.err);
                    return 1;
                } else {
                    throw ex;
                }
            }
        });
        System.exit(cl.execute(args));
    }
}