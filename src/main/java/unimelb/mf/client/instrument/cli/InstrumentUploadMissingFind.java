package unimelb.mf.client.instrument.cli;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.IExecutionExceptionHandler;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;
import picocli.CommandLine.ParseResult;
import unimelb.mf.client.cli.MFCommand;
import unimelb.mf.client.instrument.SourceContext;
import unimelb.mf.client.instrument.Upload;
import unimelb.mf.client.instrument.UploadShareable;
import unimelb.mf.client.session.MFConfigBuilder;
import unimelb.mf.client.session.MFSession;
import unimelb.picocli.converters.DateTimeConverter;
import unimelb.picocli.converters.PathConverter;
import unimelb.utils.FileSizeUtils;

@Command(name = "instrument-upload-missing-find", abbreviateSynopsis = true, synopsisHeading = "\nUSAGE:\n  ", parameterListHeading = "\nPARAMETERS:\n", optionListHeading = "\nOPTIONS:\n", separator = " ", sortOptions = false)
public class InstrumentUploadMissingFind implements MFCommand {

    public static final String APPLICATION_NAME = "instrument-upload-missing-find";

    @Option(names = {
            "--mf.config" }, paramLabel = "<mflux.cfg>", description = "Mediaflux server configuration file. Defaults to $HOME/.Arcitecta/mflux.cfg", required = false, converter = PathConverter.MustBeRegularFile.class)
    private Path mfCFG;

    @Option(names = { "-i", "--id" }, paramLabel = "<id>", description = "The instrument upload shareable id")
    private Long id;

    @Option(names = { "-t", "--token" }, paramLabel = "<token>", description = "The instrument upload shareable token.")
    private String token;

    @Option(names = { "-l",
            "--level" }, paramLabel = "<n>", description = "Level (or  level range) of sub-directories to search. For example, use 1-3 to specify the range from level 1 to level 3. Defaults to 1.", defaultValue = "1", converter = LevelRange.Converter.class)
    private LevelRange level;

    @Option(names = { "-a",
            "--after" }, paramLabel = "<dd-MMM-yyyy>", description = "Include only the data directories modified after this date (inclusive).", converter = DateTimeConverter.class)
    private Date modifiedAfter;

    @Option(names = { "-b",
            "--before" }, paramLabel = "<dd-MMM-yyyy>", description = "Include only the data directories modified before this date (exclusive).", converter = DateTimeConverter.class)
    private Date modifiedBefore;

    @Option(names = { "-o",
            "--output-csv" }, paramLabel = "<output.csv>", description = "Path to the output .csv file.")
    private Path outputCSVFile;

    @Option(names = { "-h", "--help" }, usageHelp = true, description = "display help information.")
    private boolean printHelp;

    @Parameters(paramLabel = "<parent-directories...>", description = "Parent directories to search.", arity = "1..")
    private List<Path> parentDirs;

    @Override
    public MFSession authenticate() throws Throwable {
        MFConfigBuilder mfcb = new MFConfigBuilder();
        mfcb.setApp(InstrumentUploadList.APPLICATION_NAME);
        if (this.mfCFG != null) {
            mfcb.loadFromConfigFile(mfCFG.toFile());
        } else {
            mfcb.findAndLoadFromConfigFile();
        }
        mfcb.setConsoleLogon(true);
        return MFSession.create(mfcb);
    }

    @Override
    public void execute(MFSession session) throws Throwable {

        Settings settings = new Settings();
        if (this.id != null) {
            settings.setShareableId(this.id);
        } else if (this.token != null) {
            settings.setShareableToken(this.token);
        } else {
            settings.setShareableToken(DataMoverSettings.getToken());
        }
        if (!settings.hasShareableIdOrToken()) {
            throw new IllegalArgumentException("No shareable id or token is specified!");
        }

        settings.setLevel(this.level);

        if (this.modifiedAfter != null) {
            settings.setAfter(this.modifiedAfter);
        }
        if (this.modifiedBefore != null) {
            settings.setBefore(this.modifiedBefore);
        }
        settings.setParentDirectories(this.parentDirs);
        if (settings.parentDirectories().isEmpty()) {
            throw new IllegalArgumentException("No parent directory path is specified!");
        }
        if (this.outputCSVFile != null) {
            settings.setOutputCSVFile(this.outputCSVFile);
        }

        List<Upload> uploads = UploadShareable.listUploads(session, settings.shareableId(), settings.shareableToken());

        Set<LocalDirectory> missingDirectories = new LinkedHashSet<>();
        Map<LocalDirectory, Upload> mismatchDirectories = new LinkedHashMap<>();

        checkDirectories(settings.parentDirectories(), settings.level(), settings.after(), settings.before(), uploads,
                missingDirectories, mismatchDirectories);

        if (missingDirectories.size() > 0 || mismatchDirectories.size() > 0) {

            printResult(missingDirectories, mismatchDirectories);

            if (settings.outputCSVFile() != null) {
                saveResult(missingDirectories, mismatchDirectories, settings.outputCSVFile());
            }
        } else {
            System.out.println("No missing or mismatch directories found.");
        }
    }

    private static void printResult(Set<LocalDirectory> missingDirectories,
            Map<LocalDirectory, Upload> mismatchDirectories) {
        System.out.println();
        if (!missingDirectories.isEmpty()) {
            long totalMissingFiles = 0;
            long totalMissingSize = 0;
            System.out.println("=== Missing Directories ===");
            for (LocalDirectory missingDir : missingDirectories) {
                System.out.printf("local_directory: %s%n", missingDir.path);
                System.out.printf("    file_count: %d%n", missingDir.nbFiles);
                System.out.printf("    size: %d bytes (%s)%n", missingDir.totalSize, missingDir.totalSizeHR());
                System.out.println();
                totalMissingFiles += missingDir.nbFiles;
                totalMissingSize += missingDir.totalSize;
            }
            System.out.printf("total_missing_directories: %d%n", missingDirectories.size());
            System.out.printf("total_missing_files: %d%n", totalMissingFiles);
            System.out.printf("total_missing_size: %d bytes (%s)%n", totalMissingSize,
                    FileSizeUtils.toHumanReadable(totalMissingSize));
            System.out.println();
        }

        if (!mismatchDirectories.isEmpty()) {
            long totalMismatchFiles = 0;
            long totalMismatchSize = 0;
            System.out.println("=== Mismatch Directories ===");
            Set<LocalDirectory> mismatchDirs = mismatchDirectories.keySet();
            for (LocalDirectory mismatchDir : mismatchDirs) {
                System.out.printf("local_directory: %s%n", mismatchDir.path);
                System.out.printf("    file_count: %d%n", mismatchDir.nbFiles);
                System.out.printf("    size: %d bytes (%s)%n", mismatchDir.totalSize, mismatchDir.totalSizeHR());
                Upload upload = mismatchDirectories.get(mismatchDir);
                System.out.printf("upload_namespace: %s/%s%n", upload.namespace(),
                        mismatchDir.path.getFileName().toString());
                System.out.printf("    file_count: %d%n", upload.totalFileCount());
                System.out.printf("    size: %d bytes (%s)%n", upload.totalFileSize(), upload.totalFileSizeHR());
                System.out.println();
                totalMismatchFiles += mismatchDir.nbFiles;
                totalMismatchSize += mismatchDir.totalSize;
            }
            System.out.printf("total_mismatch_directories: %d%n", mismatchDirectories.size());
            System.out.printf("total_mismatch_files: %d%n", totalMismatchFiles);
            System.out.printf("total_mismatch_size: %d bytes (%s)%n", totalMismatchSize,
                    FileSizeUtils.toHumanReadable(totalMismatchSize));
            System.out.println();
        }
    }

    private static void saveResult(Set<LocalDirectory> missingDirectories,
            Map<LocalDirectory, Upload> mismatchDirectories, Path outputCSVFile) throws IOException {

        Path parent = outputCSVFile.getParent();
        if (!Files.exists(parent)) {
            Files.createDirectories(parent);
        }
        try (OutputStream os = Files.newOutputStream(outputCSVFile);
                BufferedOutputStream bos = new BufferedOutputStream(os);
                PrintStream ps = new PrintStream(bos)) {
            System.out.print("writing to CSV file: '" + outputCSVFile + "'...");
            if (missingDirectories.size() > 0) {
                ps.println("Missing Directories,,,,,,");
                ps.println("Local Directory,Number of Files,Total File Size,,,,");
                long totalFileCount = 0;
                long totalFileSize = 0;
                for (LocalDirectory dir : missingDirectories) {
                    ps.printf("\"%s\",%d,%d,,,,%n", dir.path, dir.nbFiles, dir.totalSize);
                    totalFileCount += dir.nbFiles;
                    totalFileSize += dir.totalSize;
                }
                ps.printf("Total, %d, %d,,,,%n", totalFileCount, totalFileSize);
                ps.println(",,,,,,");
            }
            if (mismatchDirectories.size() > 0) {
                ps.println("Mismatch Directories,,,,,,");
                ps.println(
                        "Directory (Local),Number of Files (Local),Total File Size (Local),Upload Namespace (Mediaflux),Number of Files (Mediaflux),Total File Size (Mediaflux),");
                long totalLocalFileCount = 0;
                long totalLocalFileSize = 0;
                long totalUploadFileCount = 0;
                long totalUploadFileSize = 0;
                Set<LocalDirectory> mismatchDirs = mismatchDirectories.keySet();
                for (LocalDirectory mismatchDir : mismatchDirs) {
                    Upload upload = mismatchDirectories.get(mismatchDir);
                    ps.printf("\"%s\",%d,%d,\"%s\",%d,%d,%n", mismatchDir.path, mismatchDir.nbFiles,
                            mismatchDir.totalSize, upload.namespace(), upload.totalFileCount(), upload.totalFileSize());
                    totalLocalFileCount += mismatchDir.nbFiles;
                    totalLocalFileSize += mismatchDir.totalSize;
                    totalUploadFileCount += upload.totalFileCount();
                    totalUploadFileSize += upload.totalFileSize();
                }

                ps.printf("Total, %d, %d,,%d,%d,%n", totalLocalFileCount, totalLocalFileSize, totalUploadFileCount,
                        totalUploadFileSize);
                ps.println(",,,,,,");
            }
            System.out.println("done");
        }
    }

    private static void checkDirectories(Collection<Path> parentDirectories, LevelRange level, Date after, Date before,
            List<Upload> uploads, Set<LocalDirectory> missingDirs, Map<LocalDirectory, Upload> mismatchDirs)
            throws Throwable {
        for (Path parentDir : parentDirectories) {
            checkDirectories(parentDir, level, after, before, uploads, missingDirs, mismatchDirs);
        }
    }

    private static void checkDirectories(Path parentDir, LevelRange level, Date after, Date before,
            List<Upload> uploads, Set<LocalDirectory> missingDirs, Map<LocalDirectory, Upload> mismatchDirs)
            throws IOException {

        int rootDepth = parentDir.getNameCount();

        Files.walkFileTree(parentDir, new SimpleFileVisitor<Path>() {

            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                Objects.requireNonNull(dir);
                Objects.requireNonNull(attrs);
                int depth = dir.getNameCount();
                if (depth > rootDepth + level.max) {
                    return FileVisitResult.SKIP_SUBTREE;
                }
                if (depth >= rootDepth + level.min) {
                    if (lastModifiedTimeMatches(attrs.lastModifiedTime(), after, before)) {
                        checkDirectory(dir, uploads, missingDirs, mismatchDirs);
                    }
                }
                return FileVisitResult.CONTINUE;
            }

        });

    }

    private static void checkDirectory(Path dir, Collection<Upload> uploads, Set<LocalDirectory> missingDirs,
            Map<LocalDirectory, Upload> mismatchDirs) {
        SourceContext srcContext = SourceContext.resolve(dir, false);
        LocalDirectory localDir = new LocalDirectory(dir, srcContext.numberOfFiles, srcContext.totalBytes);
        for (Upload upload : uploads) {
            if (upload.pathContextMatches(dir)) {
                if (upload.totalFileCount() == localDir.nbFiles && upload.totalFileSize() == localDir.totalSize) {
                    // found one matching, remove the mismatch pair if there was.
                    if (mismatchDirs.containsKey(localDir)) {
                        mismatchDirs.remove(localDir);
                    }
                    return;
                } else {
                    mismatchDirs.put(localDir, upload);
                }
            }
        }
        if (!mismatchDirs.containsKey(localDir)) {
            missingDirs.add(localDir);
        }
    }

    private static boolean lastModifiedTimeMatches(FileTime lastModified, Date after, Date before) {
        if (after != null || before != null) {
            long lastModifiedMillis = lastModified.toMillis();
            if (after != null) {
                long afterMillis = after.getTime();
                if (lastModifiedMillis < afterMillis) {
                    return false;
                }
            }
            if (before != null) {
                long beforeMillis = before.getTime();
                if (lastModifiedMillis >= beforeMillis) {
                    return false;
                }
            }
        }
        return true;
    }

    public static class Settings {
        private Long _id;
        private String _token;
        private LevelRange _level;
        private Date _after;
        private Date _before;
        private Set<Path> _parentDirs = new LinkedHashSet<>();
        private Path _outputCSVFile;

        Long shareableId() {
            return _id;
        }

        String shareableToken() {
            return _token;
        }

        LevelRange level() {
            return _level;
        }

        Date before() {
            return _before;
        }

        Date after() {
            return _after;
        }

        Collection<Path> parentDirectories() {
            return Collections.unmodifiableCollection(_parentDirs);
        }

        Path outputCSVFile() {
            return _outputCSVFile;
        }

        public Settings setShareableId(Long id) {
            _id = id;
            return this;
        }

        public Settings setShareableToken(String token) {
            _token = token;
            return this;
        }

        public Settings setLevel(LevelRange level) {
            _level = level;
            return this;
        }

        public boolean hasShareableIdOrToken() {
            return _id != null || _token != null;
        }

        public Settings setBefore(Date before) {
            _before = before;
            return this;
        }

        public Settings setAfter(Date after) {
            _after = after;
            return this;
        }

        public Settings addParentDirectories(Collection<Path> parentDirs) throws IllegalArgumentException {
            if (parentDirs != null) {
                for (Path parentDir : parentDirs) {
                    if (Files.isDirectory(parentDir)) {
                        _parentDirs.add(parentDir.toAbsolutePath());
                    } else {
                        System.err.printf("'%s' does not exist or it is not a directory.%n", parentDir.toString());
                    }
                }
            }
            return this;
        }

        public Settings setParentDirectories(Collection<Path> parentDirs) {
            _parentDirs.clear();
            addParentDirectories(parentDirs);
            return this;
        }

        public Settings setOutputCSVFile(Path outputCSVFile) throws InterruptedException {
            String name = outputCSVFile.getFileName().toString();
            if (!name.toLowerCase().endsWith(".csv")) {
                name += ".csv";
                outputCSVFile = Paths.get(outputCSVFile.getParent().toString(), name);
            }
            while (Files.exists(outputCSVFile)) {
                Thread.sleep(50);
                name = name.substring(0, name.length() - 4) + "_" + currentTimeStamp() + ".csv";
                outputCSVFile = Paths.get(outputCSVFile.getParent().toString(), name);
            }
            _outputCSVFile = outputCSVFile;
            return this;
        }

        private static String currentTimeStamp() {
            return new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date());
        }

    }

    private static class LocalDirectory {
        public final Path path;
        public final long nbFiles;
        public final long totalSize;

        public LocalDirectory(Path path, long nbFiles, long totalSize) {
            this.path = path;
            this.nbFiles = nbFiles;
            this.totalSize = totalSize;
        }

        public String totalSizeHR() {
            return FileSizeUtils.toHumanReadable(this.totalSize);
        }

        @Override
        public int hashCode() {
            return this.path.hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            if (obj != null) {
                if (obj instanceof LocalDirectory) {
                    LocalDirectory ldo = (LocalDirectory) obj;
                    return Objects.equals(this.path, ldo.path);
                }
            }
            return false;
        }
    }

    private static class LevelRange {
        public static final int MIN_LEVEL = 1;
        public static final int MAX_LEVEL = 255;
        public final int min;
        public final int max;

        LevelRange(int min, int max) {
            this.min = min;
            this.max = max;
        }

        private static class Converter implements CommandLine.ITypeConverter<LevelRange> {
            @Override
            public LevelRange convert(String value) throws Exception {
                String v = value.trim();
                if (v.matches("^\\d+$")) {
                    int n = Integer.parseInt(v);
                    if (n < MIN_LEVEL || n > MAX_LEVEL) {
                        throw new IllegalArgumentException(
                                "invalid level: " + value + ". Expects integer between 1 and 255, found " + n);
                    }
                    return new LevelRange(n, n);
                } else if (v.matches("^\\d+-\\d+$")) {
                    int idx = v.indexOf('-');
                    int min = Integer.parseInt(v.substring(0, idx));
                    if (min < MIN_LEVEL || min > MAX_LEVEL) {
                        throw new IllegalArgumentException(
                                "invalid level: " + value + ". Expects integer between 1 and 255, found " + min);
                    }
                    int max = Integer.parseInt(v.substring(idx + 1));
                    if (max < MIN_LEVEL || max > MAX_LEVEL) {
                        throw new IllegalArgumentException(
                                "invalid level: " + value + ". Expects integer between 1 and 255, found " + max);
                    }
                    return new LevelRange(min, max);
                } else {
                    throw new IllegalArgumentException("invalid level: '" + value + "'");
                }
            }
        }

    }

    public static void main(String[] args) {
        CommandLine cl = new CommandLine(new InstrumentUploadMissingFind());
        cl.setExecutionExceptionHandler(new IExecutionExceptionHandler() {
            @Override
            public int handleExecutionException(Exception ex, CommandLine commandLine, ParseResult parseResult)
                    throws Exception {
                if (ex instanceof IllegalArgumentException) {
                    System.err.println("Error: " + ex.getMessage());
                    cl.usage(System.err);
                    return 1;
                } else {
                    throw ex;
                }
            }
        });
        System.exit(cl.execute(args));
    }
}
