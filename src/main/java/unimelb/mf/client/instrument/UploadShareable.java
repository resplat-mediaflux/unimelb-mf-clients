package unimelb.mf.client.instrument;

import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import arc.xml.XmlDoc;
import arc.xml.XmlStringWriter;
import unimelb.mf.client.session.MFSession;

public class UploadShareable {

    public static XmlDoc.Element describe(MFSession session, Long shareableId, String shareableToken) throws Throwable {
        XmlStringWriter w = new XmlStringWriter();
        if (shareableId != null) {
            w.add("id", shareableId);
        } else if (shareableToken != null) {
            w.add("token", shareableToken);
        }
        return session.execute("asset.shareable.describe", w.document()).element("shareable");
    }

    public static List<Upload> listUploads(MFSession session, long shareableId, Date completedAfter,
            Date completedBefore, String pathContext) throws Throwable {
        XmlStringWriter w = new XmlStringWriter();
        w.add("id", shareableId);
        if (completedAfter != null) {
            w.add("completed-after", completedAfter);
        }
        if (completedBefore != null) {
            w.add("completed-before", completedBefore);
        }
        if (pathContext != null) {
            w.add("path-context", pathContext);
        }
        List<XmlDoc.Element> ues = session.execute("vicnode.facility.shareable.upload.upload.list", w.document())
                .elements("upload");
        List<Upload> us = new ArrayList<>(ues == null ? 0 : ues.size());
        if (ues != null) {
            for (XmlDoc.Element ue : ues) {
                us.add(new Upload(ue, shareableId));
            }
        }
        return us;
    }

    public static List<Upload> listUploads(MFSession session, XmlDoc.Element shareable, Date completedAfter,
            Date completedBefore, String pathContext) throws Throwable {
        return listUploads(session, shareable.longValue("@id"), completedAfter, completedBefore, pathContext);
    }

    public static List<Upload> listUploads(MFSession session, String shareableToken, Date completedAfter,
            Date completedBefore, String pathContext) throws Throwable {
        return listUploads(session, describe(session, null, shareableToken), completedAfter, completedBefore,
                pathContext);
    }

    public static List<Upload> listUploads(MFSession session, Long shareableId, String shareableToken,
            Date completedAfter, Date completedBefore, String pathContext) throws Throwable {
        return shareableId != null ? listUploads(session, shareableId, completedAfter, completedBefore, pathContext)
                : listUploads(session, shareableToken, completedAfter, completedBefore, pathContext);
    }

    public static List<Upload> listUploads(MFSession session, Long shareableId, String shareableToken)
            throws Throwable {
        return listUploads(session, shareableId, shareableToken, null, null, null);
    }

    public static Set<Upload> findUploads(MFSession session, Long shareableId, String shareableToken,
            Date completedAfter, Date completedBefore, Collection<String> shareWith, Collection<String> keywords,
            Path srcPath, boolean desc) throws Throwable {
        // NOTE: filter path-context locally to avoid frequent service calls.
        List<Upload> uploads = listUploads(session, shareableId, shareableToken, completedAfter, completedBefore, null);
        Set<Upload> found = new TreeSet<>((a, b) -> {
            if (desc) {
                return Long.compare(b.uploadId(), a.uploadId());
            } else {
                return Long.compare(a.uploadId(), b.uploadId());
            }
        });
        if (uploads != null) {
            for (Upload upload : uploads) {
                if (srcPath != null && !upload.pathContextMatches(srcPath)) {
                    continue;
                }
                if (shareWith != null && !shareWith.isEmpty()) {
                    if (!upload.hasAnyShareWithEmail(shareWith)) {
                        continue;
                    }
                }
                if (keywords != null && !keywords.isEmpty()) {
                    if (!upload.hasAnyKeyword(keywords)) {
                        continue;
                    }
                }

                found.add(upload);
            }
        }
        return found;
    }

    public static void printUploads(Collection<Upload> uploads, PrintStream ps) throws Throwable {
        if (uploads != null && !uploads.isEmpty()) {
            for (Upload upload : uploads) {
                upload.printLine(ps, 4);
            }
        }
        ps.printf("    count: %d%n%n", uploads == null ? 0 : uploads.size());
    }

    public static void saveToCSVFile(MFSession session, Collection<Upload> uploads, Path csvFile, boolean compared)
            throws Throwable {
        if (uploads != null && !uploads.isEmpty()) {
            try (OutputStream os = Files.newOutputStream(csvFile);
                    BufferedOutputStream bos = new BufferedOutputStream(os);
                    PrintStream ps = new PrintStream(bos)) {
                System.out.print("writing to CSV file: '" + csvFile + "'... ");
                Upload.printCSVHead(ps, compared);
                long nbFiles = 0;
                long totalBytes = 0;
                Long nbLocalFiles = compared ? 0L : null;
                Long totalLocalBytes = compared ? 0L : null;
                for (Upload upload : uploads) {
                    upload.printCSVLine(ps, compared);
                    nbFiles += upload.totalFileCount();
                    totalBytes += upload.totalFileSize();
                    if (compared) {
                        SourceContext sctx = upload.sourceContext();
                        if (sctx != null && sctx.exists) {
                            if (sctx.numberOfFiles != null) {
                                nbLocalFiles += sctx.numberOfFiles;
                            }
                            if (sctx.totalBytes != null) {
                                totalLocalBytes += sctx.totalBytes;
                            }
                        }
                    }
                }
                Upload.printCSVFoot(ps, nbFiles, totalBytes, nbLocalFiles, totalLocalBytes, compared);
                System.out.println("done");
            }
        }
    }
}
