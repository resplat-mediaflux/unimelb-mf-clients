package unimelb.mf.client.task;

import java.io.PrintStream;

import unimelb.utils.HasLogger;

public interface AppCLI extends HasLogger {

    String applicationName();

    void execute(String[] args) throws Throwable;

    void printUsage(PrintStream ps);

}
