package unimelb.mf.client.task;

import java.util.logging.Logger;

import unimelb.mf.client.session.MFSession;
import unimelb.utils.HasLogger;

public interface MFApp<T extends MFApp.Settings> extends HasLogger {

    public static interface Settings {
        public static final int MAX_NUM_OF_QUERIERS = 4;
        public static final int MAX_NUM_OF_WORKERS = 8;

    }

    MFSession session();

    T settings();

    String applicationName();

    String description();

    void setLogger(Logger logger);

    void setSession(MFSession session);

    void execute() throws Throwable;

}
