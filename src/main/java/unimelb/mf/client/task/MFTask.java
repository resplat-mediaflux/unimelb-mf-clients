package unimelb.mf.client.task;

import java.util.concurrent.Callable;

import arc.utils.AbortableOperationHandler;
import unimelb.mf.client.session.MFSession;
import unimelb.utils.HasProgress;
import unimelb.utils.Loggable;

public interface MFTask extends Callable<Void>, Loggable, AbortableOperationHandler, HasProgress {

    MFSession session();

    void execute() throws Throwable;
}
