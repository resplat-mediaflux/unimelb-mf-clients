package unimelb.mf.client.task;

import java.io.PrintStream;
import java.util.List;
import java.util.logging.Logger;

import unimelb.mf.client.session.MFConfigBuilder;
import unimelb.mf.client.session.MFSession;
import unimelb.utils.LoggingUtils;

public abstract class AbstractMFAppCLI<T extends MFApp.Settings> extends AbstractMFApp<T> implements AppCLI {

    private T _settings;

    protected AbstractMFAppCLI(Logger logger, T settings) {
        if (logger == null) {
            setLogger(LoggingUtils.createConsoleLogger());
        } else {
            setLogger(logger);
        }
        _settings = settings;
    }

    @Override
    public T settings() {
        return _settings;
    }

    @Override
    public void execute(String[] args) throws Throwable {
        execute(args, true);
    }

    protected void execute(String[] args, boolean discardSession) throws Throwable {
        try {
            MFConfigBuilder config = new MFConfigBuilder().setConsoleLogon(true);
            config.findAndLoadFromConfigFile();
            if (config.app() == null) {
                config.setApp(applicationName());
            }
            List<String> remainArgs = config.parseArgs(args);

            MFSession session = MFSession.create(config);
            setSession(session);

            try {
                parseSettings(session, remainArgs);
                execute();
            } finally {
                if (discardSession) {
                    session.discard();
                }
            }
        } catch (IllegalArgumentException iae) {
            iae.printStackTrace();
            printUsage(System.out);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    protected void printMFArgs(PrintStream ps) {
        // @formatter:off
        ps.println("    --mf.config <mflux.cfg>               Path to the config file that contains Mediaflux server details and user credentials.");
        ps.println("    --mf.host <host>                      Mediaflux server host.");
        ps.println("    --mf.port <port>                      Mediaflux server port.");
        ps.println("    --mf.transport <https|http|tcp/ip>    Mediaflux server transport, can be http, https or tcp/ip.");
        ps.println("    --mf.auth <domain,user,password>      Mediaflux user credentials.");
        ps.println("    --mf.token <token>                    Mediaflux secure identity token.");
        // @formatter:on
    }

    protected abstract void parseSettings(MFSession session, List<String> args) throws Throwable;

}
