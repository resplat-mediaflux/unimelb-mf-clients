package unimelb.mf.client.ssh;

public abstract class SSHPutService extends SSHService<SSHPutConfiguration> {

    protected SSHPutService(SSHPutConfiguration cfg) {
        super(cfg);
    }

}
