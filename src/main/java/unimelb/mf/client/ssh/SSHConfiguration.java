package unimelb.mf.client.ssh;

import arc.xml.XmlWriter;

public abstract class SSHConfiguration {

    public static enum Action {
        GET, PUT
    }

    private String _host;
    private int _port = 22;
    private String _username;
    private String _password;
    private String _privateKey;
    private String _passphrase;
    private String _baseDir;
    private boolean _stopOnError = true;
    private int _retries = 0;

    SSHConfiguration(String host, int port, String username, String password, String privateKey, String passphrase,
            String baseDir, boolean stopOnError, int retries) {
        _host = host;
        _port = port;
        _username = username;
        _password = password;
        _privateKey = privateKey;
        _passphrase = passphrase;
        _baseDir = baseDir;
        _stopOnError = stopOnError;
        _retries = retries;
    }

    public String host() {
        return _host;
    }

    public int port() {
        return _port;
    }

    public String username() {
        return _username;
    }

    public String password() {
        return _password;
    }

    public String privateKey() {
        return _privateKey;
    }

    public String passphrase() {
        return _passphrase;
    }

    public String baseDirectory() {
        return _baseDir;
    }

    public int retries() {
        return _retries;
    }

    public boolean stopOnError() {
        return _stopOnError;
    }

    public void save(XmlWriter w) throws Throwable {
        w.add("host", host());
        w.add("port", port());
        w.add("user", username());
        if (password() != null) {
            w.add("password", password());
        }
        if (privateKey() != null) {
            w.add("private-key", privateKey());
        }
        if (passphrase() != null) {
            w.add("passphrase", passphrase());
        }
        if (!stopOnError() || retries() > 0) {
            w.add("on-error", new String[] { "retry", Integer.toString(retries()) },
                    stopOnError() ? "stop" : "continue");
        }
    }

    protected abstract Action action();

}
