package unimelb.mf.client.ssh;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import arc.xml.XmlWriter;
import unimelb.mf.model.asset.worm.Worm;

public class SSHGetConfiguration extends SSHConfiguration {

    private Boolean _readOnly = null;
    private Worm _worm = null;
    private Set<String> _srcPaths;
    private String _dstNamespace = null;

    SSHGetConfiguration(String host, int port, String username, String password, String privateKey, String passphrase,
            String baseDir, boolean stopOnError, int retries, Collection<String> srcPaths, String dstNamespace,
            Boolean readOnly, Worm worm) {
        super(host, port, username, password, privateKey, passphrase, baseDir, stopOnError, retries);
        _srcPaths = new LinkedHashSet<String>();
        if (srcPaths != null) {
            _srcPaths.addAll(srcPaths);
        }
        _dstNamespace = dstNamespace;
        _readOnly = readOnly;

    }

    @Override
    public final Action action() {
        return Action.GET;
    }

    public String dstNamespace() {
        return _dstNamespace;
    }

    public Collection<String> srcPaths() {
        return Collections.unmodifiableSet(_srcPaths);
    }

    public Boolean readOnly() {
        return _readOnly;
    }

    public Worm worm() {
        return _worm;
    }

    @Override
    public void save(XmlWriter w) throws Throwable {
        super.save(w);
        if (readOnly() != null) {
            w.add("read-only", readOnly());
        }
        if (worm() != null) {
            w.push("worm");
            worm().save(w);
            w.pop();
        }
        if (dstNamespace() != null) {
            w.add("namespace", dstNamespace());
        }
        Collection<String> srcPaths = srcPaths();
        if (srcPaths != null) {
            for (String srcPath : srcPaths) {
                w.add("path", srcPath);
            }
        }
    }

}
