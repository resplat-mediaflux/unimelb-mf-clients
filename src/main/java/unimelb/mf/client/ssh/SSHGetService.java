package unimelb.mf.client.ssh;

public abstract class SSHGetService extends SSHService<SSHGetConfiguration> {

    protected SSHGetService(SSHGetConfiguration cfg) {
        super(cfg);
    }
}
