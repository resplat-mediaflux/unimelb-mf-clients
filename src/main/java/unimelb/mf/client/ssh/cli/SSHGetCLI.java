package unimelb.mf.client.ssh.cli;

import java.io.PrintStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import unimelb.mf.client.ssh.SSHGetConfigurationBuilder;
import unimelb.mf.model.asset.worm.Worm;

public abstract class SSHGetCLI extends SSHCLI<SSHGetCLI.Settings> {

    protected SSHGetCLI() {
        super(new SSHGetCLI.Settings());
    }

    public static class Settings extends SSHCLI.Settings<SSHGetConfigurationBuilder> {

        private SSHGetConfigurationBuilder _config;

        public Settings() {
            _config = new SSHGetConfigurationBuilder();
        }

        @Override
        public SSHGetConfigurationBuilder config() {
            return _config;
        }

    }

    @Override
    protected int parseArgs(List<String> args, int i) {
        if (args.get(i).equalsIgnoreCase("--mf.namespace")) {
            settings().config().setDstNamespace(args.get(i + 1));
            return i + 2;
        } else if (args.get(i).equalsIgnoreCase("--mf.readonly")) {
            settings().config().setReadOnly(true);
            return i + 1;
        } else if (args.get(i).equalsIgnoreCase("--mf.worm")) {
            if (settings().config().worm() == null) {
                settings().config().setWorm(new Worm());
            }
            return i + 1;
        } else if (args.get(i).equalsIgnoreCase("--mf.worm.expiry")) {
            if (settings().config().worm() == null) {
                settings().config().setWorm(new Worm());
            }
            try {
                settings().config().worm().setExpiry(new SimpleDateFormat("d-MMM-yyyy").parse(args.get(i + 1)));
            } catch (ParseException pe) {
                throw new IllegalArgumentException("Invalid date value for --mf.worm.expiry: " + args.get(i + 1), pe);
            }
            return i + 2;
        } else if (args.get(i).equalsIgnoreCase("--ssh.path")) {
            settings().config().addSrcPath(args.get(i + 1));
            return i + 2;
        } else {
            throw new IllegalArgumentException("Unexpected argument: " + args.get(i));
        }
    }

    @Override
    protected void printMFArgs(PrintStream s) {
        //@formatter:off
        s.println("    --mf.namespace <dst-namespace>        Destination namespace on Mediaflux.");
        s.println("    --mf.readonly                         Set the assets to be read-only.");
        s.println("    --mf.worm                             Set the assets to WORM state.");
        s.println("    --mf.worm.expiry <d-MMM-yyyy>         Set the assets WORM expiry date.");
        //@formatter:on
    }

    @Override
    protected void printSshArgs(PrintStream s) {
        super.printSshArgs(s);
        //@formatter:off
        s.println(String.format("    --ssh.path <src-path>                 Source path on remote %s server.", protocol().toUpperCase()));
        //@formatter:on
    }

    @Override
    protected void printExamples(PrintStream s) {
        //@formatter:off
        s.println(String.format("    The command below imports files from %s server into the specified Mediaflux asset namespace:", protocol().toUpperCase()));
        s.println(String.format("         %s --mf.host mediaflux.your-domain.org --mf.port 443 --mf.transport 443 --mf.auth mf_domain,mf_user,MF_PASSWD --mf.namespace /path/to/dst-namespace --ssh.host ssh-server.your-domain.org --ssh.port 22 --ssh.user ssh_username --ssh.password SSH_PASSWD --ssh.path path/to/src-directory", applicationName()));
        s.println();
        //@formatter:on
    }
}
