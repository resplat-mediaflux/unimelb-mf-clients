package unimelb.mf.client.ssh.cli;

import unimelb.mf.client.ssh.SFTPGetService;
import unimelb.mf.client.ssh.SSHService;

public class SFTPGetCLI extends SSHGetCLI {

    @Override
    public final String applicationName() {
        return "sftp-get";
    }

    public static void main(String[] args) throws Throwable {
        execute(new SFTPGetCLI(), args);
    }

    @Override
    public String description() {
        return "Import files from remote SFTP server to Mediaflux using sftp.";
    }

    @Override
    protected SSHService<?> createService(Settings settings) {
        return new SFTPGetService(settings.config().build());
    }

    @Override
    protected final String protocol() {
        return "SFTP";
    }
}
