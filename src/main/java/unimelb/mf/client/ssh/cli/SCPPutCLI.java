package unimelb.mf.client.ssh.cli;

import unimelb.mf.client.ssh.SCPPutService;
import unimelb.mf.client.ssh.SSHService;

public class SCPPutCLI extends SSHPutCLI {

    @Override
    public final String applicationName() {
        return "scp-put";
    }

    public static void main(String[] args) throws Throwable {
        execute(new SCPPutCLI(), args);
    }

    @Override
    public String description() {
        return "Export Mediaflux assets to remote SSH server using scp.";
    }

    @Override
    protected final String protocol() {
        return "SSH";
    }

    @Override
    protected SSHService<?> createService(Settings settings) {
        return new SCPPutService(settings.config().build());
    }

}
