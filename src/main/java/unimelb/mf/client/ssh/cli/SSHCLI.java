package unimelb.mf.client.ssh.cli;

import java.io.PrintStream;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import arc.mf.client.ServerClient;
import arc.utils.ObjectUtil;
import arc.xml.XmlDoc;
import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.ssh.SSHConfigurationBuilder;
import unimelb.mf.client.ssh.SSHService;
import unimelb.mf.client.task.AbstractMFAppCLI;
import unimelb.mf.client.task.MFApp;
import unimelb.mf.client.util.XmlUtils;
import unimelb.mf.model.service.BackgroundService;
import unimelb.mf.model.task.Task;
import unimelb.utils.LoggingUtils;

@SuppressWarnings("rawtypes")
public abstract class SSHCLI<T extends SSHCLI.Settings> extends AbstractMFAppCLI<T>
        implements BackgroundService.StateListener {

    public static abstract class Settings<C extends SSHConfigurationBuilder> implements MFApp.Settings {
        public abstract C config();
    }

    protected boolean _async = false;

    private Timer _timer;

    private String _currentActivity = null;

    protected SSHCLI(T settings) {
        super(LoggingUtils.createConsoleLogger(), settings);

    }

    protected abstract SSHService<?> createService(T settings);

    @Override
    protected void parseSettings(MFSession session, List<String> args) throws Throwable {
        int n = args == null ? 0 : args.size();
        for (int i = 0; i < n;) {
            if (args.get(i).equals("--ssh.host")) {
                settings().config().setHost(args.get(i + 1));
                i += 2;
            } else if (args.get(i).equals("--ssh.port")) {
                settings().config().setPort(Integer.parseInt(args.get(i + 1)));
                i += 2;
            } else if (args.get(i).equals("--ssh.user")) {
                settings().config().setUsername(args.get(i + 1));
                i += 2;
            } else if (args.get(i).equals("--ssh.password")) {
                settings().config().setPassword(args.get(i + 1));
                i += 2;
            } else if (args.get(i).equals("--ssh.private-key")) {
                settings().config().setPrivateKey(args.get(i + 1));
                i += 2;
            } else if (args.get(i).equals("--ssh.passphrase")) {
                settings().config().setPassphrase(args.get(i + 1));
                i += 2;
            } else if (args.get(i).equals("--async")) {
                _async = true;
                i++;
            } else {
                i = parseArgs(args, i);
            }
        }
    }

    protected abstract int parseArgs(List<String> args, int idx);

    public void printUsage(PrintStream s) {
        // @formatter:off
        s.println();
        s.println("USAGE:");
        s.println(String.format(" %s <mediaflux-arguments> <%s-arguments>", applicationName(), protocol().toLowerCase()));
        s.println();
        s.println("DESCRIPTION:");
        s.println("    " + description());
        s.println();
        s.println("MEDIAFLUX ARGUMENTS:");
        super.printMFArgs(s);
        s.println("    --async                               Executes the job in the background. The background service can be checked by executing service.background.describe service in Mediaflux Aterm.");
        s.println();
        s.println(String.format("%s ARGUMENTS:", protocol().toUpperCase()));
        printSshArgs(s);
        s.println();
        s.println("EXAMPLES:");
        printExamples(s);
        // @formatter:on
    }

    protected void printSshArgs(PrintStream s) {
        //@formatter:off
        s.println(String.format("    --ssh.host <host>                     %s server host.", protocol().toUpperCase()));
        s.println(String.format("    --ssh.port <port>                     %s server port. Optional. Defaults to 22.", protocol().toUpperCase()));
        s.println(String.format("    --ssh.user <username>                 %s user name.", protocol().toUpperCase()));
        s.println(String.format("    --ssh.password <password>             %s user's password.", protocol().toUpperCase()));
        s.println(String.format("    --ssh.private-key <private-key>       %s user's private key.", protocol().toUpperCase()));
        s.println(String.format("    --ssh.passphrase <passphrase>         Passphrase for the %s user's private key.", protocol().toUpperCase()));
        //@formatter:on
    }

    protected abstract String protocol();

    protected abstract void printExamples(PrintStream s);

    private static String formatTime(double durationSeconds) {
        long seconds = (long) durationSeconds;
        long minutes = seconds / 60L;
        seconds = seconds % 60L;
        long hours = minutes / 60L;
        minutes = minutes % 60L;
        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }

    public static void execute(SSHCLI<?> cli, String[] args) throws Throwable {
        try {
            cli.execute(args);
        } catch (Throwable e) {
            e.printStackTrace();
            if (e instanceof IllegalArgumentException) {
                System.err.println("Error: " + e.getMessage());
                cli.printUsage(System.out);
            }
            System.exit(1);
        }
    }

    @Override
    public void execute(String[] args) throws Throwable {
        for (String arg : args) {
            if ("--async".equalsIgnoreCase(arg)) {
                _async = true;
                break;
            }
        }
        super.execute(args, _async);
    }

    @Override
    public void execute() throws Throwable {
        long id = createService(settings()).executeBackground(session());
        if (_async) {
            System.out.println("Background service ID: " + id);
            System.out.println();
            System.out.println("Run \n    service.background.describe :id " + id + "\n to check its status.");
            session().discard();
        } else {
            _timer = new Timer();
            _timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    try {
                        BackgroundService.describe(session(), id, SSHCLI.this);
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
            }, 0, 1000);
        }
    }

    @Override
    public void updated(BackgroundService bs) throws Throwable {
        if (bs.finished()) {
            _timer.cancel();
        }

        StringBuilder sb = new StringBuilder();
        sb.append(formatTime(bs.executionTime()));
        sb.append(" [service=").append(bs.name()).append(", id=").append(bs.id()).append(", state=").append(bs.state())
                .append(", completed=").append(bs.numberSubOperationsCompleted()).append("] ");
        String prefix = sb.toString();
        System.out.print(prefix);

        if (bs.currentActivity() != null && !ObjectUtil.equals(_currentActivity, bs.currentActivity())) {
            /*
             * current activity
             */
            _currentActivity = bs.currentActivity();
            System.out.println(_currentActivity);
        } else {
            /*
             * state
             */
            System.out.println(bs.state());
        }

        if (bs.state() == Task.State.COMPLETED) {
            /*
             * completed
             */
            XmlDoc.Element re = bs.getResult(session());
            List<XmlDoc.Element> fes = re.elements();
            if (fes != null) {
                for (XmlDoc.Element fe : fes) {
                    XmlUtils.print(System.err, fe);
                }
            }
        }

        if (bs.finished()) {
            session().discard();
        }
        if (bs.failed()) {
            /*
             * error
             */
            if (bs.exception() != null) {
                throw bs.exception();
            } else {
                throw new Exception(bs.error());
            }
        } else if (bs.aborted()) {
            /*
             * aborted
             */
            throw new ServerClient.ExAborted();
        }
    }

}
