package unimelb.mf.client.ssh.cli;

import unimelb.mf.client.ssh.SFTPPutService;
import unimelb.mf.client.ssh.SSHService;

public class SFTPPutCLI extends SSHPutCLI {

    protected SFTPPutCLI() {
        super();
    }

    @Override
    public final String applicationName() {
        return "sftp-put";
    }

    @Override
    public String description() {
        return "Export Mediaflux assets to remote SFTP server using sftp.";
    }

    @Override
    protected SSHService<?> createService(Settings settings) {
        return new SFTPPutService(settings.config().build());
    }

    @Override
    protected final String protocol() {
        return "SFTP";
    }

    public static void main(String[] args) throws Throwable {
        execute(new SFTPPutCLI(), args);
    }
}
