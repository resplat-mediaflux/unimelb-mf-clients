package unimelb.mf.client.ssh.cli;

import java.io.PrintStream;
import java.util.List;

import unimelb.mf.client.ssh.SSHPutConfigurationBuilder;

public abstract class SSHPutCLI extends SSHCLI<SSHPutCLI.Settings> {

    protected SSHPutCLI() {
        super(new SSHPutCLI.Settings());
    }

    public static class Settings extends SSHCLI.Settings<SSHPutConfigurationBuilder> {

        private SSHPutConfigurationBuilder _config;

        public Settings() {
            _config = new SSHPutConfigurationBuilder();
        }

        @Override
        public SSHPutConfigurationBuilder config() {
            return _config;
        }

    }

    @Override
    protected int parseArgs(List<String> args, int i) {
        if (args.get(i).equalsIgnoreCase("--mf.namespace")) {
            settings().config().addNamespace(args.get(i + 1));
            return i + 2;
        } else if (args.get(i).equalsIgnoreCase("--mf.unarchive")) {
            settings().config().setUnarchiveContents(true);
            return i + 1;
        } else if (args.get(i).equalsIgnoreCase("--ssh.directory")) {
            settings().config().setDstDirectory(args.get(i + 1));
            return i + 2;
        } else {
            throw new IllegalArgumentException("Unexpected argument: " + args.get(i));
        }
    }

    @Override
    protected void printMFArgs(PrintStream s) {
        super.printMFArgs(s);
        //@formatter:off
        s.println("    --mf.namespace <src-namespace>        Source namespace on Mediaflux.");
        s.println("    --mf.unarchive                        Unpack asset contents.");
        //@formatter:on
    }

    @Override
    protected void printSshArgs(PrintStream s) {
        super.printSshArgs(s);
        //@formatter:off
        s.println(String.format("    --ssh.directory <dst-directory>       Destination directory on remote %s server.", protocol().toUpperCase()));
        //@formatter:on
    }

    @Override
    protected void printExamples(PrintStream s) {
        //@formatter:off
        s.println(String.format("    The command below exports assets from the specified Mediaflux asset namespace to remote %s server:", protocol().toUpperCase()));
        s.println(String.format("        %s --mf.host mediaflux.your-domain.org --mf.port 443 --mf.transport 443 --mf.auth mf_domain,mf_user,MF_PASSWD --mf.namespace /path/to/src-namespace --ssh.host ssh-server.your-domain.org --ssh.port 22 --ssh.user ssh_username --ssh.password SSH_PASSWD --ssh.directory path/to/dst-directory", applicationName()));
        s.println();
        //@formatter:on
    }

}
