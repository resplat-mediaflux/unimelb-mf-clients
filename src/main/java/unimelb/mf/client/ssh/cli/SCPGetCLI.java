package unimelb.mf.client.ssh.cli;

import unimelb.mf.client.ssh.SCPGetService;
import unimelb.mf.client.ssh.SSHService;

public class SCPGetCLI extends SSHGetCLI {

    @Override
    public final String applicationName() {
        return "scp-get";
    }

    public static void main(String[] args) throws Throwable {
        execute(new SCPGetCLI(), args);
    }

    @Override
    public String description() {
        return "Import files from remote SSH server to Mediaflux using scp.";
    }

    @Override
    protected SSHService<?> createService(Settings settings) {
        return new SCPGetService(settings.config().build());
    }

    @Override
    protected final String protocol() {
        return "SSH";
    }

}
