package unimelb.mf.client.ssh;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import unimelb.mf.client.ssh.SSHConfiguration.Action;
import unimelb.mf.model.asset.worm.Worm;

public class SSHGetConfigurationBuilder
        extends SSHConfigurationBuilder<SSHGetConfiguration, SSHGetConfigurationBuilder> {

    protected Boolean readOnly = null;
    protected Worm worm = null;
    protected Set<String> srcPaths;
    protected String dstNamespace = null;

    public SSHGetConfigurationBuilder() {
        this.srcPaths = new LinkedHashSet<String>();
    }

    public void validate() throws IllegalArgumentException {
        super.validate();
        if (this.srcPaths.isEmpty()) {
            throw new IllegalArgumentException("Missing path on the remote SSH server.");
        }
        if (this.dstNamespace == null) {
            throw new IllegalArgumentException("Missing namespace path on the Mediaflux server.");
        }
    }

    @Override
    public SSHGetConfiguration build() {
        validate();
        return new SSHGetConfiguration(this.host, this.port, this.username, this.password, this.privateKey,
                this.passphrase, this.baseDirectory, this.stopOnError, this.retries, this.srcPaths, this.dstNamespace,
                this.readOnly, this.worm);
    }

    public SSHGetConfigurationBuilder setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
        return this;
    }

    public Worm worm() {
        return this.worm;
    }

    public SSHGetConfigurationBuilder setWorm(Worm worm) {
        this.worm = worm;
        return this;
    }

    public SSHGetConfigurationBuilder setSrcPath(String path) {
        this.srcPaths.clear();
        if (path != null) {
            this.srcPaths.add(path);
        }
        return this;
    }

    public SSHGetConfigurationBuilder setSrcPaths(Collection<String> paths) {
        this.srcPaths.clear();
        if (paths != null) {
            this.srcPaths.addAll(paths);
        }
        return this;
    }

    public SSHGetConfigurationBuilder addSrcPath(String path) {
        if (path != null) {
            this.srcPaths.add(path);
        }
        return this;
    }

    public SSHGetConfigurationBuilder setDstNamespace(String namespace) {
        this.dstNamespace = namespace;
        return this;
    }

    @Override
    public final Action action() {
        return Action.GET;
    }

}
