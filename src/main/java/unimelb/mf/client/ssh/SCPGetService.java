package unimelb.mf.client.ssh;

public class SCPGetService extends SSHGetService {

    public static final String SERVICE_NAME = "unimelb.scp.get";

    public SCPGetService(SSHGetConfiguration cfg) {
        super(cfg);
    }

    @Override
    public final String service() {
        return SERVICE_NAME;
    }

}
