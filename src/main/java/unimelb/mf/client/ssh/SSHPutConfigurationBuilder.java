package unimelb.mf.client.ssh;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import unimelb.mf.client.ssh.SSHConfiguration.Action;

public class SSHPutConfigurationBuilder
        extends SSHConfigurationBuilder<SSHPutConfiguration, SSHPutConfigurationBuilder> {

    protected String where;
    protected Set<String> namespaces;
    protected Map<String, Boolean> cids;
    protected Set<String> assetIds;
    protected String expr;
    protected Boolean unarchive;
    protected String directory;

    @Override
    public SSHPutConfiguration build() {
        validate();
        return new SSHPutConfiguration(this.host, this.port, this.username, this.password, this.privateKey,
                this.passphrase, this.baseDirectory, this.stopOnError, this.retries, this.where, this.namespaces,
                this.cids, this.assetIds, this.expr, this.unarchive, this.directory);
    }

    @Override
    public void validate() throws IllegalArgumentException {
        super.validate();
        if (this.directory == null) {
            throw new IllegalArgumentException("Missing destination directory on the remote SSH server.");
        }
        if (this.where == null && (this.namespaces == null || this.namespaces.isEmpty())
                && (this.cids == null || this.cids.isEmpty()) && (this.assetIds == null || this.assetIds.isEmpty())) {
            throw new IllegalArgumentException("Missing source asset namespaces (or selection query).");
        }
    }

    public void setCiteableIds(boolean recursive, String... cids) {
        if (this.cids != null) {
            this.cids.clear();
        }
        if (cids != null) {
            if (this.cids == null) {
                this.cids = new LinkedHashMap<String, Boolean>();
            }
            for (String cid : cids) {
                this.cids.put(cid, recursive);
            }
        }
    }

    public void addCiteableId(String cid, boolean recursive) {
        if (cid != null) {
            if (this.cids == null) {
                this.cids = new LinkedHashMap<String, Boolean>();
            }
            this.cids.put(cid, recursive);
        }
    }

    public void addCiteableId(String cid) {
        addCiteableId(cid, true);
    }

    public void setAssetIds(String... assetIds) {
        if (this.assetIds != null) {
            this.assetIds.clear();
        }
        if (assetIds != null) {
            if (this.assetIds == null) {
                this.assetIds = new LinkedHashSet<String>();
            }
            for (String assetId : assetIds) {
                this.assetIds.add(assetId);
            }
        }
    }

    public void addAssetId(String assetId) {
        if (assetId != null) {
            if (this.assetIds == null) {
                this.assetIds = new LinkedHashSet<String>();
            }
            this.assetIds.add(assetId);
        }
    }

    public void setNamespaces(String... namespaces) {
        if (this.namespaces != null) {
            this.namespaces.clear();
        }
        if (namespaces != null) {
            if (this.namespaces == null) {
                this.namespaces = new LinkedHashSet<String>();
            }
            for (String namespace : namespaces) {
                this.namespaces.add(namespace);
            }
        }
    }

    public void addNamespace(String namespace) {
        if (namespace != null) {
            if (this.namespaces == null) {
                this.namespaces = new LinkedHashSet<String>();
            }
            this.namespaces.add(namespace);
        }
    }

    public void setWhere(String where) {
        this.where = where;
    }

    public void setOutputPathExpression(String expr) {
        this.expr = expr;
    }

    public void setDstDirectory(String directory) {
        this.directory = directory;
    }

    public void setUnarchiveContents(Boolean unarchive) {
        this.unarchive = unarchive;
    }

    @Override
    public final Action action() {
        return Action.PUT;
    }

}
