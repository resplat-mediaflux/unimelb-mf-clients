package unimelb.mf.client.ssh;

public class SFTPPutService extends SSHPutService {

    public static final String SERVICE_NAME = "unimelb.sftp.put";

    public SFTPPutService(SSHPutConfiguration cfg) {
        super(cfg);
    }

    @Override
    public final String service() {
        return SERVICE_NAME;
    }
}
