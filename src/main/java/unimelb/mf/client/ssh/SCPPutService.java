package unimelb.mf.client.ssh;

public class SCPPutService extends SSHPutService {

    public static final String SERVICE_NAME = "unimelb.scp.put";

    public SCPPutService(SSHPutConfiguration cfg) {
        super(cfg);
    }

    @Override
    public final String service() {
        return SERVICE_NAME;
    }

}
