package unimelb.mf.client.ssh;

public class SFTPGetService extends SSHGetService {

    public static final String SERVICE_NAME = "unimelb.sftp.get";

    public SFTPGetService(SSHGetConfiguration cfg) {
        super(cfg);
    }

    @Override
    public final String service() {
        return SERVICE_NAME;
    }

}
