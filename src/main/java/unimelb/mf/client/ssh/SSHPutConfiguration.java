package unimelb.mf.client.ssh;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import arc.xml.XmlWriter;

public class SSHPutConfiguration extends SSHConfiguration {
    private String _where;
    private Set<String> _namespaces;
    private Map<String, Boolean> _cids;
    private Set<String> _assetIds;
    private String _expr;
    private Boolean _unarchive;
    private String _directory;

    SSHPutConfiguration(String host, int port, String username, String password, String privateKey, String passphrase,
            String baseDir, boolean stopOnError, int retries, String where, Collection<String> namespaces,
            Map<String, Boolean> cids, Collection<String> assetIds, String expr, Boolean unarchive, String directory) {
        super(host, port, username, password, privateKey, passphrase, baseDir, stopOnError, retries);
        _where = where;
        if (namespaces != null && !namespaces.isEmpty()) {
            _namespaces = new LinkedHashSet<String>();
            _namespaces.addAll(namespaces);
        }
        if (cids != null && !cids.isEmpty()) {
            _cids = new LinkedHashMap<String, Boolean>();
            _cids.putAll(cids);
        }
        if (assetIds != null && !assetIds.isEmpty()) {
            _assetIds = new LinkedHashSet<String>();
            _assetIds.addAll(assetIds);
        }
        _expr = expr;
        _unarchive = unarchive;
        _directory = directory;
    }

    @Override
    public final Action action() {
        return Action.PUT;
    }

    @Override
    public void save(XmlWriter w) throws Throwable {
        super.save(w);
        if (_cids != null && !_cids.isEmpty()) {
            Set<String> cids = _cids.keySet();
            for (String cid : cids) {
                w.add("cid", new String[] { "recursive", Boolean.toString(_cids.get(cid)) }, cid);
            }
        }
        if (_assetIds != null && !_assetIds.isEmpty()) {
            for (String assetId : _assetIds) {
                w.add("id", assetId);
            }
        }
        if (_namespaces != null && !_namespaces.isEmpty()) {
            for (String namespace : _namespaces) {
                w.add("namespace", namespace);
            }
        }
        if (_where != null) {
            w.add("where", _where);
        }
        if (_expr != null) {
            w.add("expr", _expr);
        }
        if (_unarchive != null) {
            w.add("unarchive", _unarchive);
        }
        w.add("directory", _directory);
    }

}
