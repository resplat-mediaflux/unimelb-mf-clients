package unimelb.mf.client.ssh;

import unimelb.mf.client.ssh.SSHConfiguration.Action;

@SuppressWarnings("unchecked")
public abstract class SSHConfigurationBuilder<T extends SSHConfiguration, B extends SSHConfigurationBuilder<T, B>> {

    protected String host;
    protected int port = 22;
    protected String username;
    protected String password;
    protected String privateKey;
    protected String passphrase;
    protected String baseDirectory;
    protected boolean stopOnError = true;;
    protected int retries = 0;

    protected SSHConfigurationBuilder() {

    }

    public B setHost(String host) {
        this.host = host;
        return (B) this;
    }

    public B setPort(int port) {
        this.port = port;
        return (B) this;
    }

    public B setUsername(String username) {
        this.username = username;
        return (B) this;
    }

    public B setPassword(String password) {
        this.password = password;
        return (B) this;
    }

    public B setPrivateKey(String privateKey, String passphrase) {
        this.privateKey = privateKey;
        this.passphrase = passphrase;
        return (B) this;
    }

    public B setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
        return (B) this;
    }

    public B setPassphrase(String passphrase) {
        this.passphrase = passphrase;
        return (B) this;
    }

    public B setBaseDirectory(String baseDirectory) {
        this.baseDirectory = baseDirectory;
        return (B) this;
    }

    public B setStopOnError(boolean stopOnError) {
        this.stopOnError = stopOnError;
        return (B) this;
    }

    public B setRetries(int retries) {
        this.retries = retries > 0 ? retries : 0;
        return (B) this;
    }

    public void validate() throws IllegalArgumentException {
        if (this.host == null) {
            throw new IllegalArgumentException("Missing SSH server host.");
        }
        if (this.port <= 0 || this.port > 65535) {
            throw new IllegalArgumentException("Invalid SSH server port: " + this.port);
        }
        if (this.username == null) {
            throw new IllegalArgumentException("Missing SSH username.");
        }
        if (this.password == null && this.privateKey == null) {
            throw new IllegalArgumentException("Missing SSH password or private key. Expects at least one.");
        }
    }

    public abstract Action action();

    public abstract T build();

}
