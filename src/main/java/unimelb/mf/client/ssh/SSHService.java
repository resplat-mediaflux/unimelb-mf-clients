package unimelb.mf.client.ssh;

import arc.mf.client.RequestOptions;
import arc.mf.client.ServerClient;
import arc.mf.client.ServerRoute;
import arc.utils.AbortableOperationHandler;
import arc.utils.CanAbort;
import arc.xml.XmlStringWriter;
import unimelb.mf.client.session.MFSession;

public abstract class SSHService<T extends SSHConfiguration> {

    private T _cfg;
    private RequestOptions _ops;
    private CanAbort _ca;

    protected SSHService(T cfg) {
        _cfg = cfg;
        _ops = new RequestOptions();
        _ops.setAbortHandler(new AbortableOperationHandler() {

            @Override
            public void finished(CanAbort ca) {
                _ca = null;
            }

            @Override
            public void started(CanAbort ca) {
                _ca = ca;
            }
        });
        _ops.setBackground(true);
    }

    public T configuration() {
        return _cfg;
    }

    public String args() {
        XmlStringWriter w = new XmlStringWriter();
        try {
            _cfg.save(w);
            return w.document();
        } catch (Throwable e) {
            if (e instanceof RuntimeException) {
                throw (RuntimeException) e;
            } else {
                throw new RuntimeException(e);
            }
        }
    }

    public RequestOptions options() {
        return _ops;
    }

    public void abort() throws Throwable {
        if (_ca != null) {
            _ca.abort();
            _ca = null;
        }
    }

    public abstract String service();

    public long executeBackground(MFSession session) throws Throwable {
        return session.execute((ServerRoute) null, service(), args(), (ServerClient.Input) null,
                (ServerClient.Output) null, options()).longValue("id");
    }

}
