package unimelb.mf.client.terminal;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.List;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import org.jline.builtins.Completers;
import org.jline.builtins.Completers.TreeCompleter;
import org.jline.reader.Candidate;
import org.jline.reader.Completer;
import org.jline.reader.EndOfFileException;
import org.jline.reader.LineReader;
import org.jline.reader.LineReaderBuilder;
import org.jline.reader.MaskingCallback;
import org.jline.reader.ParsedLine;
import org.jline.reader.Parser;
import org.jline.reader.UserInterruptException;
import org.jline.reader.impl.DefaultParser;
import org.jline.terminal.Terminal;
import org.jline.terminal.TerminalBuilder;
import org.jline.utils.AttributedStringBuilder;
import org.jline.utils.AttributedStyle;
import org.jline.utils.InfoCmp.Capability;

import arc.exception.ThrowableUtil;
import arc.xml.XmlDoc;
import arc.xml.XmlStringWriter;
import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.task.AbstractMFAppCLI;
import unimelb.mf.client.task.MFApp;
import unimelb.mf.client.terminal.MFTerminal.NameCompleter.Target;
import unimelb.mf.client.terminal.command.ChangeDirectoryCommand;
import unimelb.mf.client.terminal.command.ChangeNamespaceCommand;
import unimelb.mf.client.terminal.command.CreateDirectoryCommand;
import unimelb.mf.client.terminal.command.CreateNamespaceCommand;
import unimelb.mf.client.terminal.command.ListDirectoryCommand;
import unimelb.mf.client.terminal.command.ListNamespaceCommand;
import unimelb.mf.client.terminal.command.RemoveDirectoryOrFileCommand;
import unimelb.utils.PathUtils;

public class MFTerminal extends AbstractMFAppCLI<MFTerminal.Settings> {

    public static final String APP_NAME = "unimelb-mf-terminal";
    public static final String APP_DESCRIPTION = "Shell to manipulate data in Mediaflux.";

    public static final String DEFAULT_PROMPT_HOSTNAME = "Mediaflux";
    public static final String DEFAULT_RIGHT_PROMPT = null;

    public static class ANSI {

        public static final String SANE = "\u001B[0m";

        public static final String HIGH_INTENSITY = "\u001B[1m";
        public static final String LOW_INTENSITY = "\u001B[2m";

        public static final String ITALIC = "\u001B[3m";
        public static final String UNDERLINE = "\u001B[4m";
        public static final String BLINK = "\u001B[5m";
        public static final String RAPID_BLINK = "\u001B[6m";
        public static final String REVERSE_VIDEO = "\u001B[7m";
        public static final String INVISIBLE_TEXT = "\u001B[8m";

        public static final String BLACK = "\u001B[30m";
        public static final String RED = "\u001B[31m";
        public static final String GREEN = "\u001B[32m";
        public static final String YELLOW = "\u001B[33m";
        public static final String BLUE = "\u001B[34m";
        public static final String MAGENTA = "\u001B[35m";
        public static final String CYAN = "\u001B[36m";
        public static final String WHITE = "\u001B[37m";

        public static final String BACKGROUND_BLACK = "\u001B[40m";
        public static final String BACKGROUND_RED = "\u001B[41m";
        public static final String BACKGROUND_GREEN = "\u001B[42m";
        public static final String BACKGROUND_YELLOW = "\u001B[43m";
        public static final String BACKGROUND_BLUE = "\u001B[44m";
        public static final String BACKGROUND_MAGENTA = "\u001B[45m";
        public static final String BACKGROUND_CYAN = "\u001B[46m";
        public static final String BACKGROUND_WHITE = "\u001B[47m";

    }

    public static class NameCompleter implements org.jline.reader.Completer {

        public static enum Target {
            NAMESPACE, ASSET, ALL
        }

        private MFSession _session;
        private MFTerminal _shell;
        private Target _target;
        private Integer _size;

        public NameCompleter(MFSession session, MFTerminal shell, Target target, Integer size) {
            assert session != null;
            _session = session;
            _shell = shell;
            _target = target;
            _size = size;
        }

        public NameCompleter(MFSession session, MFTerminal shell, Target target) {
            this(session, shell, target, null);
        }

        @Override
        public void complete(LineReader reader, ParsedLine line, List<Candidate> candidates) {

            assert line != null;
            assert candidates != null;

            String token = line.word().substring(0, line.wordCursor());
            String ns = null;
            String name = null;
            boolean nameOnly = token == null || token.isEmpty() || token.indexOf("/") < 0;
            if (token.isEmpty()) {
                ns = _shell.currentNamespace();
            } else if (token.endsWith("/")) {
                ns = token;
            } else {
                ns = PathUtils.getParentPath(token);
                name = PathUtils.getFileName(token);
            }

            if (ns == null) {
                ns = _shell.currentNamespace();
            } else if (!ns.startsWith("/")) {
                ns = PathUtils.joinSystemIndependent(_shell.currentNamespace(), ns);
            }

            try {
                switch (_target) {
                case ASSET:
                    addAssetCandidates(ns, name, candidates, reader.getTerminal());
                    break;
                case NAMESPACE:
                    addNamespaceCandidates(ns, name, nameOnly, candidates, reader.getTerminal());
                    break;
                default:
                    addNamespaceAndAssetCandidates(ns, name, nameOnly, candidates, reader.getTerminal());
                    break;
                }
            } catch (Throwable e) {
                // ignore
                e.printStackTrace();
            }

        }

        private void addAssetCandidates(String ns, String prefix, List<Candidate> candidates, Terminal terminal)
                throws Throwable {
            StringBuilder sb = new StringBuilder();
            sb.append("namespace='").append(ns).append("'");
            if (prefix != null) {
                sb.append(" and name starts with '").append(prefix).append("'");
            }
            XmlStringWriter w = new XmlStringWriter();
            w.add("where", sb.toString());
            w.add("action", "get-path");
            if (_size != null) {
                w.add("size", _size);
            }
            XmlDoc.Element re = _session.execute("asset.query", w.document());
            Collection<String> paths = re.values("path");
            if (paths != null) {
                for (String path : paths) {
                    candidates.add(
                            new Candidate(path, displayValueOfAsset(path, terminal), null, null, null, null, true));
                }
            }
        }

        private void addNamespaceCandidates(String ns, String prefix, boolean nameOnly, List<Candidate> candidates,
                Terminal terminal) throws Throwable {
            XmlStringWriter w = new XmlStringWriter();
            w.add("namespace", ns);
            w.add("assets", false);
            if (prefix != null) {
                w.add("prefix", prefix);
            }

            XmlDoc.Element re = _session.execute("asset.namespace.list", w.document());
            List<XmlDoc.Element> nes = re.elements("namespace/namespace");
            String path = re.value("namespace/@path");
            long nbNamespaces = nes == null ? 0 : nes.size();

            candidates.add(new Candidate(".", displayValueOfNamespace(".", terminal), null, null, null, null,
                    nbNamespaces == 0));

            if (nbNamespaces > 0) {
                for (XmlDoc.Element ne : nes) {
                    String name = ne.value();
                    boolean leaf = ne.booleanValue("@leaf", false);
                    candidates.add(new Candidate(nameOnly ? name : Paths.get(path, name).toString(),
                            displayValueOfNamespace(name, terminal), null, null, null, null, leaf));
                }
            }
        }

        private void addNamespaceAndAssetCandidates(String ns, String prefix, boolean nameOnly,
                List<Candidate> candidates, Terminal terminal) throws Throwable {
            XmlStringWriter w = new XmlStringWriter();
            w.add("namespace", ns);
            w.add("assets", true);
            w.add("action", "get-name");
            if (_size != null) {
                w.add("size", _size);
            }
            XmlDoc.Element re = _session.execute("asset.namespace.list", w.document());
            List<XmlDoc.Element> nes = re.elements("namespace/namespace");
            String path = re.value("namespace/@path");
            long nbNamespaces = nes == null ? 0 : nes.size();
            Collection<String> assetNames = re.values("namespace/asset");

            candidates.add(new Candidate(".", displayValueOfNamespace(".", terminal), null, null, null, null,
                    nbNamespaces == 0));

            if (nes != null) {
                for (XmlDoc.Element ne : nes) {
                    String namespace = ne.value();
                    boolean leaf = ne.booleanValue("@leaf", false);
                    candidates.add(new Candidate(nameOnly ? namespace : Paths.get(path, namespace).toString(),
                            displayValueOfNamespace(namespace, terminal), null, null, null, null, leaf));
                }
            }
            if (assetNames != null) {
                for (String assetName : assetNames) {
                    candidates.add(new Candidate(nameOnly ? assetName : Paths.get(path, assetName).toString(),
                            displayValueOfAsset(assetName, terminal), null, null, null, null, true));
                }
            }
        }

        private String displayValueOfNamespace(String ns, Terminal terminal) {
            String name = PathUtils.getFileName(ns);
            if (!name.endsWith("/")) {
                name += "/";
            }
            AttributedStringBuilder sb = new AttributedStringBuilder();
            sb.styled(AttributedStyle.BOLD.foreground(AttributedStyle.BLUE), name);
            return sb.toAnsi();
        }

        private String displayValueOfAsset(String assetPath, Terminal terminal) {
            String name = PathUtils.getFileName(assetPath);
            return name;
        }

    }

    public static class Settings implements MFApp.Settings {

        private String _promptHostName = DEFAULT_PROMPT_HOSTNAME;
        private String _rightPrompt = DEFAULT_RIGHT_PROMPT;
        private boolean _showHelp = false;

        public void setShowHelp(boolean showHelp) {
            _showHelp = showHelp;
        }

        public boolean showHelp() {
            return _showHelp;
        }

        public String promptHostName() {
            return _promptHostName;
        }

        public String rightPrompt() {
            return _rightPrompt;
        }

        public void setPromptHostName(String promptHostName) {
            _promptHostName = promptHostName;
        }

        public void setRightPrompt(String rightPrompt) {
            _rightPrompt = rightPrompt;
        }

    }

    private Terminal _term;
    private Completer _completer;
    private Parser _parser;
    private LineReader _reader;
    private String _ns;
    private Path _dir;

    private Logger _termLogger;

    public MFTerminal() throws Throwable {
        super(Logger.getLogger("org.jline"), new Settings());
        _dir = Paths.get(System.getProperty("user.dir")).toAbsolutePath();
        this.logger().setLevel(Level.FINE);
        _termLogger = Logger.getLogger(APP_NAME);
    }

    @Override
    public String applicationName() {
        return APP_NAME;
    }

    @Override
    public String description() {
        return APP_DESCRIPTION;
    }

    public String prompt() {
        StringBuilder sb = new StringBuilder();
        String nsName = PathUtils.getFileName(_ns);
        if (nsName == null) {
            nsName = "/";
        }
        sb.append(settings().promptHostName()).append(":").append(nsName).append(" > ");
        return sb.toString();
    }

    public Terminal terminal() {
        return _term;
    }

    public String currentNamespace() {
        return _ns;
    }

    public void setCurrentNamespace(String namespace) {
        _ns = namespace;
    }

    public Path currentDirectory() {
        return _dir;
    }

    public void setCurrentDirectory(Path dir) {
        _dir = dir;
    }

    public void println(String x) {
        if (_term != null) {
            _term.writer().println(x);
        }
    }

    public void println() {
        if (_term != null) {
            _term.writer().println();
        }
    }

    public void printf(String format, Object... args) {
        if (_term != null) {
            _term.writer().printf(format, args);
        }
    }

    public void print(String s) {
        if (_term != null) {
            _term.writer().print(s);
        }
    }

    public PrintWriter writer() {
        if (_term != null) {
            return _term.writer();
        }
        return null;
    }

    @Override
    public void execute() throws Throwable {
        if (settings().showHelp()) {
            printUsage(System.out);
            return;
        }
        _ns = "/";

        // @formatter:off
        _term = TerminalBuilder.builder()
                .system(true)
                    .build();
        
        _termLogger.addHandler(new TerminalHandler(_term.writer()));

        _completer = new TreeCompleter(
                TreeCompleter.node("cd", TreeCompleter.node(new NameCompleter(session(), this, Target.NAMESPACE))),
                TreeCompleter.node("clear"),
                TreeCompleter.node("cls"),
                TreeCompleter.node("exit"),
                TreeCompleter.node("get"),
                TreeCompleter.node("lcd", TreeCompleter.node(new Completers.DirectoriesCompleter(_dir) {
                    @Override
                    protected Path getUserDir() {
                        return _dir;
                    }
                })),
                TreeCompleter.node("lls", TreeCompleter.node(new Completers.DirectoriesCompleter(_dir) {
                    @Override
                    protected Path getUserDir() {
                        return _dir;
                    }
                })),
                TreeCompleter.node("lmkdir", TreeCompleter.node(new Completers.DirectoriesCompleter(_dir) {
                    @Override
                    protected Path getUserDir() {
                        return _dir;
                    }
                })),
                TreeCompleter.node("lpwd"),
                TreeCompleter.node("lrm", TreeCompleter.node(new Completers.FileNameCompleter(){
                    @Override
                    protected Path getUserDir() {
                        return _dir;
                    }
                })),
                TreeCompleter.node("ls", TreeCompleter.node(new NameCompleter(session(), this, Target.NAMESPACE))),
                TreeCompleter.node("mkdir", TreeCompleter.node(new NameCompleter(session(), this, Target.NAMESPACE))),
                TreeCompleter.node("put"),
                TreeCompleter.node("pwd"),
                TreeCompleter.node("quit"),
                TreeCompleter.node("rm", TreeCompleter.node(new NameCompleter(session(), this, Target.ALL))),
                TreeCompleter.node("?")
                );

        _parser = new DefaultParser();
        
        _reader = LineReaderBuilder.builder()
                    .terminal(_term)
                    .completer(_completer)
                    .parser(_parser)
                        .build();
        // @formatter:on

        while (true) {

            String line = null;
            try {
                line = _reader.readLine(prompt(), settings().rightPrompt(), (MaskingCallback) null, null);
            } catch (UserInterruptException e) {
                // Ignore
            } catch (EndOfFileException e) {
                // exit
                return;
            }
            if (line == null) {
                continue;
            }
            line = line.trim();

            if ("exit".equalsIgnoreCase(line) || "quit".equalsIgnoreCase(line)) {
                break;
            }

            if ("clear".equalsIgnoreCase(line) || "cls".equalsIgnoreCase(line)) {
                _term.puts(Capability.clear_screen);
                _term.flush();
                continue;
            }

            ParsedLine pl = _reader.getParser().parse(line, 0);
            List<String> words = pl.words();
            String cmd = words.get(0);
            List<String> args = words.size() > 1 ? words.subList(1, words.size()) : null;
            if ("cd".equalsIgnoreCase(cmd)) {
                new ChangeNamespaceCommand().execute(this, args);
            } else if ("pwd".equalsIgnoreCase(cmd)) {
                _term.writer().println(_ns);
            } else if ("ls".equalsIgnoreCase(cmd)) {
                new ListNamespaceCommand().execute(this, args);
            } else if ("lcd".equalsIgnoreCase(cmd)) {
                new ChangeDirectoryCommand().execute(this, args);
            } else if ("lls".equalsIgnoreCase(cmd)) {
                new ListDirectoryCommand().execute(this, args);
            } else if ("lmkdir".equalsIgnoreCase(cmd)) {
                new CreateDirectoryCommand().execute(this, args);
            } else if ("lpwd".equalsIgnoreCase(cmd)) {
                _term.writer().println(_dir.toString());
            } else if ("lrm".equalsIgnoreCase(cmd)) {
                new RemoveDirectoryOrFileCommand().execute(this, args);
            } else if ("mkdir".equalsIgnoreCase(cmd)) {
                new CreateNamespaceCommand().execute(this, args);
            }

            _term.flush();
        }
    }

    boolean isDebugging() {
        // TODO remove or use.
        return logger().getLevel().intValue() <= Level.FINE.intValue();
    }

    public static void main(String[] args) throws Throwable {
        new MFTerminal().execute(args);
    }

    @Override
    protected void parseSettings(MFSession session, List<String> args) throws Throwable {
        for (int i = 0; i < args.size();) {
            if ("-h".equalsIgnoreCase(args.get(i)) || "--help".equalsIgnoreCase(args.get(i))) {
                settings().setShowHelp(true);
                i++;
            } else {
                throw new IllegalArgumentException("Unexpected argument: " + args.get(i));
            }
        }
    }

    @Override
    public void printUsage(PrintStream ps) {
        // @formatter:off
        ps.println();
        ps.println("USAGE:");
        ps.println(String.format("    %s [OPTIONS]", applicationName()));
        ps.println();
        ps.println("DESCRIPTION:");
        ps.println(String.format("    %s", description()));
        ps.println();
        ps.println("OPTIONS:");
        printMFArgs(ps);
        ps.println("    --help,-h                                 Show help.");
        // @formatter:on
    }

    public Logger terminalLogger() {
        return _termLogger;
    }

    public static boolean[] exists(MFSession session, String path) throws Throwable {
        XmlStringWriter w = new XmlStringWriter();
        w.push("service", new String[] { "name", "asset.exists" });
        w.add("id", "path=" + path);
        w.pop();
        w.push("service", new String[] { "name", "asset.namespace.exists" });
        w.add("namespace", path);
        w.pop();
        XmlDoc.Element re = session.execute("service.execute", w.document());
        return new boolean[] { re.booleanValue("reply[@service='asset.exists']/response/exists"),
                re.booleanValue("reply[@service='asset.namespace.exists']/response/exists") };
    }

    private static class TerminalHandler extends Handler {

        private volatile PrintWriter _w;

        TerminalHandler(PrintWriter w) {
            _w = w;
        }

        @Override
        public void publish(LogRecord record) {
            Level level = record.getLevel();
            if (level.intValue() >= Level.SEVERE.intValue()) {
                _w.printf("Error: %s\n", record.getMessage());
                if (record.getThrown() != null) {
                    _w.println(ThrowableUtil.stackTrace(record.getThrown()));
                }
            } else if (level.intValue() >= Level.WARNING.intValue()) {
                _w.printf("Warning: %s\n", record.getMessage());
            } else if (level.intValue() >= Level.INFO.intValue()) {
                _w.println(record.getMessage());
            }
        }

        @Override
        public void flush() {
            _w.flush();
        }

        @Override
        public void close() throws SecurityException {

        }

    }

}
