package unimelb.mf.client.terminal;

import java.io.PrintWriter;
import java.util.List;

public interface MFTerminalCommand {

    String name();

    void execute(MFTerminal term, List<String> args) throws Throwable;
    
    void printUsage(PrintWriter w);
    
    


}
