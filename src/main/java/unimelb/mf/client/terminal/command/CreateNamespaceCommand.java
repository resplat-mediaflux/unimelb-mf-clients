package unimelb.mf.client.terminal.command;

import java.io.PrintWriter;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import unimelb.mf.client.terminal.MFTerminal;
import unimelb.mf.client.terminal.MFTerminalCommand;
import unimelb.mf.client.util.AssetNamespaceUtils;

public class CreateNamespaceCommand implements MFTerminalCommand {

    public static final String NAME = "mkdir";

    @Override
    public void execute(MFTerminal t, List<String> args) throws Throwable {

        boolean parents = false;
        List<String> namespaces = new ArrayList<String>();
        if (args != null) {
            for (int i = 0; i < args.size();) {
                if ("-p".equals(args.get(i))) {
                    parents = true;
                    i++;
                } else {
                    String ns = args.get(i);
                    if (ns != null && !ns.isEmpty()) {
                        if (ns.startsWith("/")) {
                            ns = Paths.get(ns).normalize().toString();
                        } else {
                            ns = Paths.get(t.currentNamespace(), ns).normalize().toString();
                        }
                        if (AssetNamespaceUtils.assetNamespaceExists(t.session(), ns)) {
                            t.terminal().writer().printf("Error: namespace: '" + ns + "' already exists.");
                        } else {
                            namespaces.add(ns);
                        }
                    }
                    i++;
                }
            }
        }

        if (namespaces.isEmpty()) {
            printUsage(t.terminal().writer());
            return;
        }

        for (String ns : namespaces) {
            try {
                AssetNamespaceUtils.createAssetNamespace(t.session(), ns, parents);
            } catch (Throwable e) {
                String msg = e.getMessage();
                int idx = msg.indexOf('\n');
                if (idx > 0) {
                    msg = msg.substring(0, idx);
                }
                t.terminal().writer().println("Error: " + msg);
                t.logger().log(Level.FINE, msg, e);
            }
        }
    }

    public final void printUsage(PrintWriter w) {
        w.printf("usage: %s [-p] namespace-path\n", name());
    }

    @Override
    public final String name() {
        return NAME;
    }

}
