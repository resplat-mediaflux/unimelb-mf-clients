package unimelb.mf.client.terminal.command;

import java.io.File;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import arc.xml.XmlDoc;
import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.sync.task.AssetDownloadTask;
import unimelb.mf.client.sync.task.AssetDownloadTask.Unarchive;
import unimelb.mf.client.terminal.MFTerminal;
import unimelb.mf.client.terminal.MFTerminalCommand;
import unimelb.mf.client.util.AssetUtils;

public class GetCommand implements MFTerminalCommand {

    public static final String NAME = "get";

    public static class Options {
        private boolean _overwrite = false;
        private int _nbWorkers = 1;
        private Unarchive _unarchive = Unarchive.NONE;
        private boolean _restoreSymlinks = true;
        private boolean _csumCheck = false;
        private boolean _includeMetadata  = false;

        public void setNumberOfWorkers(int n) {
            _nbWorkers = n;
        }

        public int numberOfWorkers() {
            return _nbWorkers;
        }

        public void setUnarchive(Unarchive unarc) {
            _unarchive = unarc;
        }

        public Unarchive unarchive() {
            return _unarchive;
        }

        public void setOverwrite(boolean overwrite) {
            _overwrite = overwrite;
        }

        public boolean overwrite() {
            return _overwrite;
        }

        public void setRestoreSymlinks(boolean restoreSymlinks) {
            _restoreSymlinks = restoreSymlinks;
        }

        public boolean restoreSymlinks() {
            return _restoreSymlinks;
        }

        public boolean csumCheck() {
            return _csumCheck;
        }
        
        public void setInlcudeMetadata(boolean includeMetadata) {
        	_includeMetadata = includeMetadata;
        }
        
        public boolean includeMetadata() {
        	return _includeMetadata;
        }

    }

    @Override
    public void execute(MFTerminal term, List<String> args) throws Throwable {
        String remotePath = null;
        String localPath = null;
        // https://www.systutorials.com/docs/linux/man/1-sftp/

        if (args == null || args.isEmpty()) {
            printUsage(term.writer());
            return;
        }

        Options opts = new Options();

        for (int i = 0; i < args.size(); ) {
            String arg = args.get(i);
            if (arg == null || arg.trim().isEmpty()) {
                continue;
            }
            if (arg.matches("^-[a-zA-Z]+")) {
                // TODO add & parse options;
                if (arg.equals("-n")) {
                    try {
                        opts.setNumberOfWorkers(Integer.parseInt(args.get(i + 1)));
                    } catch (Throwable e) {
                        term.terminalLogger().log(Level.SEVERE, e.getMessage(), e);
                        return;
                    }
                    i += 2;
                } else {
                    for (int j = 1; i < arg.length(); j++) {
                        char c = arg.charAt(j);
                        switch (c) {
                            case 'x':
                                opts.setUnarchive(Unarchive.AAR);
                                break;
                            case 'X':
                                opts.setUnarchive(Unarchive.ALL);
                                break;
                            default:
                                term.printf("Error: invalid arguments. Unknown option: '%c'", c);
                                break;
                        }
                    }
                    i++;
                }
            } else {
                if (remotePath == null) {
                    if (arg.startsWith("/")) {
                        remotePath = Paths.get(arg).normalize().toString();
                    } else {
                        remotePath = Paths.get(term.currentNamespace(), arg).normalize().toString();
                    }
                    i++;
                } else if (localPath == null) {
                    localPath = arg;
                    i++;
                } else {
                    term.println("Error: Invalid arguments.");
                    printUsage(term.writer());
                    return;
                }
            }
        }

        if (remotePath == null) {
            printUsage(term.writer());
            return;
        }

        boolean[] exists = MFTerminal.exists(term.session(), remotePath);
        boolean assetExists = exists[0];
        boolean namespaceExists = exists[1];

        if (assetExists && namespaceExists) {
            term.println("Error: '" + remotePath
                    + "' exists as both asset and namespace. Cannot download to local file system.");
            return;
        }

        Path dstPath = null;
        if (assetExists) {
            String assetName = Paths.get(remotePath).normalize().toFile().getName();
            if (localPath == null) {
                dstPath = Paths.get(term.currentDirectory().toString(), assetName);
            } else {
                if (localPath.startsWith("/") || localPath.startsWith(File.separator)) {
                    if (localPath.endsWith("/") || localPath.endsWith(File.separator)) {
                        dstPath = Paths.get(localPath, assetName).normalize();
                    } else {
                        dstPath = Paths.get(localPath).normalize();
                        if (Files.exists(dstPath) && Files.isDirectory(dstPath)) {
                            dstPath = Paths.get(dstPath.toString(), assetName);
                        }
                    }
                } else {
                    if (localPath.endsWith("/") || localPath.endsWith(File.separator)) {
                        dstPath = Paths.get(term.currentDirectory().toString(), localPath, assetName).normalize();
                    } else {
                        dstPath = Paths.get(term.currentDirectory().toString(), localPath).normalize();
                        if (Files.exists(dstPath) && Files.isDirectory(dstPath)) {
                            dstPath = Paths.get(dstPath.toString(), assetName);
                        }
                    }
                }
            }
            if (Files.exists(dstPath) && !opts.overwrite()) {
                term.printf("'%s' already exists. Skipped.", dstPath.toString());
                return;
            }
            downloadAsset(term.session(), remotePath, dstPath, opts, term.terminalLogger());
        } else {
            // TODO
        }
    }

    private void downloadAsset(MFSession session, String assetPath, Path dstPath, Options options, Logger logger)
            throws Throwable {
        XmlDoc.Element ae = AssetUtils.getAssetMetaByPath(session, assetPath);
        new AssetDownloadTask(session, logger, ae, dstPath, options.unarchive(), options.restoreSymlinks(), options.csumCheck(), options.includeMetadata(), null)
                .execute();
    }

    public final void printUsage(PrintWriter w) {
        w.printf("usage: %s remote-path [local-path]\n", name());
    }

    @Override
    public final String name() {
        return NAME;
    }

}
