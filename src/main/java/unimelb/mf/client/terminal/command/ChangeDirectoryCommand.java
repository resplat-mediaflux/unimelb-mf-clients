package unimelb.mf.client.terminal.command;

import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import unimelb.mf.client.terminal.MFTerminalCommand;
import unimelb.mf.client.terminal.MFTerminal;

public class ChangeDirectoryCommand implements MFTerminalCommand {

    public static final String NAME = "lcd";

    @Override
    public void execute(MFTerminal t, List<String> args) throws Throwable {

        String dir = null;
        if (args != null && args.size() > 0) {
            dir = args.get(0);
            if (dir != null && !dir.isEmpty()) {
                if (dir.startsWith("/")) {
                    dir = Paths.get(dir).normalize().toString();
                } else {
                    dir = Paths.get(t.currentDirectory().toAbsolutePath().toString(), dir).normalize().toString();
                }
            }
        }
        if (dir == null || dir.isEmpty()) {
            dir = System.getProperty("user.home");
        }
        Path dirPath = Paths.get(dir).normalize().toAbsolutePath();
        if (!t.currentDirectory().toString().equals(dirPath.toString())) {
            if (!Files.exists(dirPath)) {
                t.terminal().writer().println("Error: Directory: '" + dir + "' does not exist.");
            }
            if (!Files.isDirectory(dirPath)) {
                t.terminal().writer().println("Error: '" + dir + "' is not a directory.");
            }
            t.setCurrentDirectory(dirPath);
        }
    }

    @Override
    public final String name() {
        return NAME;
    }

    @Override
    public void printUsage(PrintWriter w) {
        w.printf("usage: %s local-directory\n", name());
    }

}
