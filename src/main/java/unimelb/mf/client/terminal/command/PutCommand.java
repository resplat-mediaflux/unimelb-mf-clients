package unimelb.mf.client.terminal.command;

import java.io.PrintWriter;
import java.nio.file.Path;
import java.util.List;

import unimelb.mf.client.terminal.MFTerminal;
import unimelb.mf.client.terminal.MFTerminalCommand;

public class PutCommand implements MFTerminalCommand {

    public static final String NAME = "put";

    @Override
    public void execute(MFTerminal term, List<String> args) throws Throwable {
        // TODO Auto-generated method stub
        String dstNamespace;
        Path srcPath;
        // TODO
    }

    @Override
    public final String name() {
        return NAME;
    }

    @Override
    public final void printUsage(PrintWriter w) {
        w.printf("usage: %s local-path [remote-path]\n", name());
    }

}
