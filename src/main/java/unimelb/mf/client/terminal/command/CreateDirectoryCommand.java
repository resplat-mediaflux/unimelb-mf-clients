package unimelb.mf.client.terminal.command;

import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import unimelb.mf.client.terminal.MFTerminal;
import unimelb.mf.client.terminal.MFTerminalCommand;

public class CreateDirectoryCommand implements MFTerminalCommand {

    public static final String NAME = "lmkdir";

    @Override
    public void execute(MFTerminal t, List<String> args) throws Throwable {

        boolean parents = false;
        List<Path> dirs = new ArrayList<Path>();
        if (args != null) {
            for (int i = 0; i < args.size();) {
                if ("-p".equals(args.get(i))) {
                    parents = true;
                    i++;
                } else {
                    String dir = args.get(i);
                    if (dir != null && !dir.isEmpty()) {
                        if (dir.startsWith("/")) {
                            dir = Paths.get(dir).normalize().toString();
                        } else {
                            dir = Paths.get(t.currentDirectory().toAbsolutePath().toString(), dir).normalize()
                                    .toString();
                        }
                        Path dirPath = Paths.get(dir);
                        if (Files.exists(dirPath)) {
                            t.terminal().writer().printf("Error: file: '" + dir + "' already exists.");
                        } else {
                            dirs.add(dirPath);
                        }
                    }
                    i++;
                }
            }
        }

        if (dirs.isEmpty()) {
            printUsage(t.terminal().writer());
            return;
        }

        for (Path dir : dirs) {
            try {
                if (parents) {
                    Files.createDirectories(dir);
                } else {
                    Path parentDir = dir.getParent();
                    if (parentDir == null || !Files.exists(parentDir)) {
                        t.terminal().writer().println("Error: directory: '" + parentDir + "' does not exist.");
                        continue;
                    }
                    Files.createDirectory(dir);
                }
            } catch (Throwable e) {
                String msg = e.getMessage();
                int idx = msg.indexOf('\n');
                if (idx > 0) {
                    msg = msg.substring(0, idx);
                }
                t.terminal().writer().println("Error: " + msg);
                t.logger().log(Level.FINE, msg, e);
            }
        }
    }

    public void printUsage(PrintWriter w) {
        w.printf("usage: %s [-p] directory-path ...\n", name());
    }

    @Override
    public final String name() {
        return NAME;
    }

}
