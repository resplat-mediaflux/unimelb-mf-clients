package unimelb.mf.client.terminal.command;

import java.io.PrintWriter;
import java.nio.file.Paths;
import java.util.List;

import unimelb.mf.client.terminal.MFTerminalCommand;
import unimelb.mf.client.terminal.MFTerminal;
import unimelb.mf.client.util.AssetNamespaceUtils;

public class ChangeNamespaceCommand implements MFTerminalCommand {

    public static final String NAME = "cd";

    @Override
    public void execute(MFTerminal term, List<String> args) throws Throwable {

        String ns = null;
        if (args != null && !args.isEmpty()) {
            ns = args.get(0);
            if (ns != null && !ns.isEmpty()) {
                if (ns.startsWith("/")) {
                    ns = Paths.get(ns).normalize().toString();
                } else {
                    ns = Paths.get(term.currentNamespace(), ns).normalize().toString();
                }
            }
        }
        if (ns == null || ns.isEmpty()) {
            ns = "/";
        }
        if (!term.currentNamespace().equals(ns)) {
            if (AssetNamespaceUtils.assetNamespaceExists(term.session(), ns)) {
                term.setCurrentNamespace(ns);
            } else {
                term.terminal().writer().println("Error: asset namespace: '" + ns + "' does not exist.");
            }
        }
    }

    @Override
    public final String name() {
        return NAME;
    }

    @Override
    public void printUsage(PrintWriter w) {
        w.printf("usage: %s namespace-path\n", name());
    }

}
