package unimelb.mf.client.terminal.command;

import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import unimelb.mf.client.terminal.MFTerminal;
import unimelb.mf.client.terminal.MFTerminalCommand;
import unimelb.utils.FileUtils;

public class RemoveDirectoryOrFileCommand implements MFTerminalCommand {

    public static final String NAME = "lrm";

    @Override
    public void execute(MFTerminal term, List<String> args) throws Throwable {

        List<String> files = new ArrayList<String>();
        Set<String> opts = new LinkedHashSet<String>();

        if (args != null && args.size() > 0) {
            for (int i = 0; i < args.size();) {
                String arg = args.get(i);
                if (arg == null || arg.trim().isEmpty()) {
                    i++;
                } else {
                    arg = arg.trim();
                    if (arg.startsWith("-")) {
                        while (arg.startsWith("-")) {
                            arg = arg.substring(1);
                        }
                        while (!arg.isEmpty()) {
                            opts.add(arg.substring(0, 1));
                            // TODO process options...
                            arg = arg.substring(1);
                        }
                        i++;
                    } else {
                        if (arg.startsWith("/")) {
                            files.add(Paths.get(arg).normalize().toString());
                        } else {
                            files.add(Paths.get(term.currentDirectory().toAbsolutePath().toString(), arg).normalize()
                                    .toString());
                        }
                        i++;
                    }
                }
            }
        }

        if (files.isEmpty()) {
            printUsage(term.terminal().writer());
            return;
        }

        for (int i = 0; i < files.size(); i++) {
            Path path = Paths.get(files.get(i));
            if (!Files.exists(path)) {
                term.terminal().writer().printf("lrm: %s: No such file or directory.\n", path.toString());
                return;
            }
            if (Files.isDirectory(path)) {
                if (!opts.contains("r")) {
                    term.terminal().writer().printf("lrm: %s: is a directory. Use -r to remove a directory.\n",
                            path.toString());
                    return;
                }
                FileUtils.deleteDirectory(path);
            } else {
                Files.delete(path);
            }
        }
    }

    @Override
    public final String name() {
        return NAME;
    }

    @Override
    public final void printUsage(PrintWriter w) {
        w.printf("usage: %s [-r] local-path ...", name());
    }
}
