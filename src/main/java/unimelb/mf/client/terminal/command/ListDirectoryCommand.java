package unimelb.mf.client.terminal.command;

import java.io.File;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.jline.terminal.Terminal;

import arc.utils.DateTime;
import unimelb.mf.client.terminal.MFTerminal;
import unimelb.mf.client.terminal.MFTerminalCommand;

public class ListDirectoryCommand implements MFTerminalCommand {

    public static final String NAME = "lls";

    @Override
    public void execute(MFTerminal term, List<String> args) throws Throwable {

        List<String> dirs = new ArrayList<String>();
        Set<String> opts = new LinkedHashSet<String>();

        if (args != null && args.size() > 0) {
            for (int i = 0; i < args.size();) {
                String arg = args.get(i);
                if (arg == null || arg.trim().isEmpty()) {
                    i++;
                } else {
                    arg = arg.trim();
                    if (arg.startsWith("-")) {
                        while (arg.startsWith("-")) {
                            arg = arg.substring(1);
                        }
                        while (!arg.isEmpty()) {
                            opts.add(arg.substring(0, 1));
                            // TODO process options...
                            arg = arg.substring(1);
                        }
                        i++;
                    } else {
                        if (arg.startsWith("/")) {
                            dirs.add(Paths.get(arg).normalize().toString());
                        } else {
                            dirs.add(Paths.get(term.currentDirectory().toAbsolutePath().toString(), arg).normalize()
                                    .toString());
                        }
                        i++;
                    }
                }
            }
        }

        if (dirs.isEmpty()) {
            dirs.add(term.currentDirectory().toString());
        }

        for (int i = 0; i < dirs.size(); i++) {
            Path dirPath = Paths.get(dirs.get(i));

            if (i > 0) {
                term.terminal().writer().println();
            }
            term.terminal().writer().printf("%s:\n", dirPath.toString());

            int nbDirs = 0;
            File[] sdirs = dirPath.toFile().listFiles(f -> {
                return f.isDirectory();
            });
            if (sdirs != null) {
                nbDirs = sdirs.length;
                Arrays.sort(sdirs);
            }

            int nbFiles = 0;
            File[] files = dirPath.toFile().listFiles(f -> {
                return f.isFile();
            });
            if (files != null) {
                nbFiles = files.length;
                Arrays.sort(files);
            }

            if (nbFiles == 0 && nbDirs == 0) {
                return;
            }

            term.terminal().writer().printf("%-9s%20s  %20s  %s\n", "Type", "Size", "Modified", "Name");

            if (sdirs != null) {
                for (File d : sdirs) {
                    consume(d, term.terminal());
                }
            }
            if (files != null) {
                for (File f : files) {
                    consume(f, term.terminal());
                }
            }

            term.terminal().writer().printf("total %d directories, %d files\n", nbDirs, nbFiles);
        }

    }

    private void consume(File f, Terminal term) {
        try {
            boolean isDirectory = f.isDirectory();
            term.writer().printf("%-9s%20s  %20s  %s\n", isDirectory ? "directory" : "file",
                    isDirectory ? "" : Long.toString(f.length()),
                    new SimpleDateFormat(DateTime.DATE_TIME_FORMAT).format(new Date(f.lastModified())), f.getName());
        } catch (Throwable e) {
            term.writer().println("Error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public final String name() {
        return NAME;
    }

    @Override
    public final void printUsage(PrintWriter w) {
        w.printf("usage: %s directory-path ...", name());
    }

}
