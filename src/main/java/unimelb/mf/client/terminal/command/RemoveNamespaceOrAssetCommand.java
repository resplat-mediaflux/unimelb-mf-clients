package unimelb.mf.client.terminal.command;

import java.io.PrintWriter;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import arc.xml.XmlStringWriter;
import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.terminal.MFTerminal;
import unimelb.mf.client.terminal.MFTerminalCommand;

public class RemoveNamespaceOrAssetCommand implements MFTerminalCommand {

    public static final String NAME = "rm";

    @Override
    public void execute(MFTerminal term, List<String> args) throws Throwable {

        List<String> paths = new ArrayList<String>();
        Set<String> opts = new LinkedHashSet<String>();

        if (args != null && args.size() > 0) {
            for (int i = 0; i < args.size();) {
                String arg = args.get(i);
                if (arg == null || arg.trim().isEmpty()) {
                    i++;
                } else {
                    arg = arg.trim();
                    if (arg.startsWith("-")) {
                        while (arg.startsWith("-")) {
                            arg = arg.substring(1);
                        }
                        while (!arg.isEmpty()) {
                            opts.add(arg.substring(0, 1));
                            // TODO process options...
                            arg = arg.substring(1);
                        }
                        i++;
                    } else {
                        if (arg.startsWith("/")) {
                            paths.add(Paths.get(arg).normalize().toString());
                        } else {
                            paths.add(Paths.get(term.currentDirectory().toAbsolutePath().toString(), arg).normalize()
                                    .toString());
                        }
                        i++;
                    }
                }
            }
        }

        if (paths.isEmpty()) {
            printUsage(term.terminal().writer());
            return;
        }

        for (int i = 0; i < paths.size(); i++) {
            String path = paths.get(i);
            boolean[] exists = MFTerminal.exists(term.session(), path);
            boolean assetExists = exists[0];
            boolean namespaceExists = exists[1];
            if (!assetExists && !namespaceExists) {
                term.terminal().writer().printf("rm: %s: No such asset or namespace.\n", path);
                return;
            }

            if (assetExists && namespaceExists) {
                if (!opts.contains("a")) {
                    term.terminal().writer()
                            .printf("rm: %s: exists as both asset and namespace. Use -a to remove both.\n", path);
                    return;
                }
            }

            if (assetExists) {
                destroyAsset(term.session(), path);
            }

            if (namespaceExists) {
                if (!opts.contains("r")) {
                    term.terminal().writer()
                            .printf("rm: %s: is an asset namespace. Use -r to remove an asset namespace.\n", path);
                    return;
                }
                destroyNamespace(term.session(), path);
            }
        }
    }

    private static void destroyAsset(MFSession session, String path) throws Throwable {
        XmlStringWriter w = new XmlStringWriter();
        w.add("id", "path=" + path);
        session.execute("asset.destroy", w.document());
    }

    private static void destroyNamespace(MFSession session, String path) throws Throwable {
        XmlStringWriter w = new XmlStringWriter();
        w.add("namespace", path);
        session.execute("asset.namespace.destroy", w.document());
    }

    @Override
    public final String name() {
        return NAME;
    }

    @Override
    public final void printUsage(PrintWriter w) {
        w.printf("usage: %s [-ra] remote-path", name());
    }

}
