package unimelb.mf.client.terminal.command;

import java.io.PrintWriter;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import arc.xml.XmlDoc;
import arc.xml.XmlStringWriter;
import unimelb.mf.client.terminal.MFTerminal;
import unimelb.mf.client.terminal.MFTerminalCommand;
import unimelb.mf.client.util.AssetNamespaceUtils;

public class ListNamespaceCommand implements MFTerminalCommand {

    public static final String NAME = "ls";

    public static final int ASSET_QUERY_PAGE_SIZE = 1000;

    @Override
    public void execute(MFTerminal t, List<String> args) throws Throwable {

        List<String> nss = new ArrayList<String>();
        Set<String> opts = new LinkedHashSet<String>();

        if (args != null && args.size() > 0) {
            for (int i = 0; i < args.size();) {
                String arg = args.get(i);
                if (arg == null || arg.trim().isEmpty()) {
                    i++;
                } else {
                    arg = arg.trim();
                    if (arg.startsWith("-")) {
                        while (arg.startsWith("-")) {
                            arg = arg.substring(1);
                        }
                        while (!arg.isEmpty()) {
                            opts.add(arg.substring(0, 1));
                            // TODO process options...
                            arg = arg.substring(1);
                        }
                        i++;
                    } else {
                        if (arg.startsWith("/")) {
                            nss.add(Paths.get(arg).normalize().toString());
                        } else {
                            nss.add(Paths.get(t.currentNamespace(), arg).normalize().toString());
                        }
                        i++;
                    }
                }
            }
        }

        if (nss.isEmpty()) {
            nss.add(t.currentNamespace());
        }

        for (int i = 0; i < nss.size(); i++) {
            String ns = nss.get(i);

            if (i > 0) {
                t.terminal().writer().println();
            }

            t.terminal().writer().printf("%s:\n", ns);

            if (!AssetNamespaceUtils.assetNamespaceExists(t.session(), ns)) {
                t.terminal().writer().println("Error: asset namespace: '" + ns + "' does not exist.");
                return;
            }

            int idx = 1;
            long remaining = Long.MAX_VALUE;

            XmlStringWriter w = new XmlStringWriter();
            w.push("service", new String[] { "name", "asset.namespace.describe" });
            w.add("namespace", ns);
            w.add("levels", 1);
            w.pop();
            w.push("service", new String[] { "name", "asset.query" });
            addAssetQueryArgs(w, ns, idx, ASSET_QUERY_PAGE_SIZE);
            w.pop();
            XmlDoc.Element re = t.session().execute("service.execute", w.document());
            List<XmlDoc.Element> nes = re
                    .elements("reply[@service='asset.namespace.describe']/response/namespace/namespace");
            long totalNamespaces = nes == null ? 0L : nes.size();
            List<XmlDoc.Element> aes = re.elements("reply[@service='asset.query']/response/asset");
            long totalAssets = re.longValue("reply[@service='asset.query']/response/cursor/total");

            long nbNamespaces = nes == null ? 0 : nes.size();
            long nbAssets = aes == null ? 0 : aes.size();
            remaining = re.longValue("reply[@service='asset.query']/response/cursor/remaining", 0);

            if (nbNamespaces == 0 && nbAssets == 0) {
                return;
            }

            t.terminal().writer().printf("%-9s%20s%20s  %20s  %s\n", "Type", "ID", "Size", "Modified", "Name");

            if (nbNamespaces > 0) {
                for (XmlDoc.Element ne : nes) {
                    String id = ne.value("@id");
                    String name = ne.value("name");
                    String mtime = ne.stringValue("mmtime", ne.value("ctime"));
                    t.terminal().writer().printf("%-9s%20s%20s  %20s  %s\n", "namespace", id, "", mtime, name);
                }
            }

            if (nbAssets > 0) {
                for (XmlDoc.Element ae : aes) {
                    String id = ae.value("@id");
                    long csize = ae.longValue("csize", 0);
                    String mtime = ae.value("mtime");
                    String name = ae.value("name");
                    t.terminal().writer().printf("%-9s%20s%20s  %20s  %s\n", "asset", id, Long.toString(csize), mtime,
                            name);
                }
            }

            while (remaining > 0) {
                idx += ASSET_QUERY_PAGE_SIZE;
                XmlStringWriter w2 = new XmlStringWriter();
                addAssetQueryArgs(w2, ns, idx, ASSET_QUERY_PAGE_SIZE);
                XmlDoc.Element re2 = t.session().execute("asset.query", w2.document());
                aes = re2.elements("asset");
                remaining = re2.longValue("cursor/remaining", 0);
                if (aes != null) {
                    for (XmlDoc.Element ae : aes) {
                        String id = ae.value("@id");
                        long csize = ae.longValue("csize", 0);
                        String mtime = ae.value("mtime");
                        String name = ae.value("name");
                        t.terminal().writer().printf("%-9s%20s%20s  %20s  %s\n", "asset", id, Long.toString(csize),
                                mtime, name);
                    }
                }
            }
            t.terminal().writer().printf("total %d namespaces, %d assets\n", totalNamespaces, totalAssets);
        }
    }

    private static void addAssetQueryArgs(XmlStringWriter w, String ns, int idx, int size) throws Throwable {
        w.add("where", "namespace='" + ns + "'");
        w.add("xpath", new String[] { "ename", "csize" }, "content/size");
        w.add("xpath", new String[] { "ename", "mtime" }, "mtime");
        w.add("xpath", new String[] { "ename", "name" },
                "string.format('%s', choose(equals(xvalue('name'),null()), string.format('__asset_id__%s',xvalue('@id')),xvalue('name')))");
        w.add("action", "get-value");
        w.add("size", size);
        w.add("idx", idx);
        w.add("count", true);
    }

    @Override
    public final String name() {
        return NAME;
    }

    @Override
    public final void printUsage(PrintWriter w) {
        w.printf("usage: %s [remote-path] ...\n", name());
    }
}
