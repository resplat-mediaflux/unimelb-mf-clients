package unimelb.mf.client.session;

import arc.mf.client.MultiFactorAuthenticationHandler;
import unimelb.utils.AnsiColors;
import unimelb.utils.StringUtils;

public class DefaultMFAHandler implements MultiFactorAuthenticationHandler {

	/**
	 * Indicates the server has requested multi-factor verification.
	 *
	 * @param deviceModel         The model of the device the notification was sent
	 *                            to.
	 * @param deviceName          The name of the device the notification was sent
	 *                            to.
	 * @param remainingTimeInSecs The amount of time remaining in seconds before the
	 *                            notification request expires.
	 */
	public void requestedMultiFactor(String deviceModel, String deviceName, long remainingTimeInSecs) {
		System.out.printf("MFA notification sent to %s%s [%s]%s%n", AnsiColors.BLUE, deviceName, deviceModel, AnsiColors.RESET);
		System.out.printf("Remaining %s%03d%s seconds", AnsiColors.YELLOW, remainingTimeInSecs, AnsiColors.RESET);
	}

	/**
	 * Indicates that user has not yet responded to the request for verification.
	 *
	 * @param requestAtTime       The time (UTC) that the request was generated.
	 * @param remainingTimeInSecs The amount of time remaining in seconds before the
	 *                            notification request expires.
	 */
	public void multifactorIsStillWaiting(long requestAtTime, long remainingTimeInSecs) {
		System.out.printf("\r\rRemaining %s%03d%s seconds", AnsiColors.YELLOW, remainingTimeInSecs, AnsiColors.RESET);
	}

	/**
	 * Indicates the system is no longer waiting for multi-factor authentication -
	 * typically it was successful.
	 */
	public void multifactorIsNoLongerWaiting() {
		String line = StringUtils.repeat(" ", 80);
		System.out.printf("\r\r%s\r", line);
	}
}
