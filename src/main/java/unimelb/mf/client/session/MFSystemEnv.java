package unimelb.mf.client.session;

public class MFSystemEnv {

	public static final String ENV_MFLUX_HOST = "MFLUX_HOST";
	public static final String ENV_MFLUX_PORT = "MFLUX_PORT";
	public static final String ENV_MFLUX_TRANSPORT = "MFLUX_TRANSPORT";
	public static final String ENV_MFLUX_DOMAIN = "MFLUX_DOMAIN";
	public static final String ENV_MFLUX_USER = "MFLUX_USER";
	public static final String ENV_MFLUX_PASSWORD = "MFLUX_PASSWORD";
	public static final String ENV_MFLUX_TOKEN = "MFLUX_TOKEN";
	public static final String ENV_MFLUX_APP = "MFLUX_APP";

	public static String getMFHost() {
		return System.getenv(ENV_MFLUX_HOST);
	}

	public static Integer getMFPort() {
		String p = System.getenv(ENV_MFLUX_PORT);
		if (p != null) {
			return Integer.parseInt(p);
		}
		return null;
	}

	public static String getMFTransport() {
		return System.getenv(ENV_MFLUX_TRANSPORT);
	}

	public static String getMFToken() {
		return System.getenv(ENV_MFLUX_TOKEN);
	}

	public static String getMFApp() {
		return System.getenv(ENV_MFLUX_APP);
	}

	public static String getMFDomain() {
		return System.getenv(ENV_MFLUX_DOMAIN);
	}

	public static String getMFUser() {
		return System.getenv(ENV_MFLUX_USER);
	}

	public static String getMFPassword() {
		return System.getenv(ENV_MFLUX_PASSWORD);
	}
}
