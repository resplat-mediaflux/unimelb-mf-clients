package unimelb.mf.client.session;

import arc.mf.client.ServerClient;
import arc.streams.LongInputStream;

public abstract class MFInput extends ServerClient.Input {

    protected MFInput(String type, LongInputStream in, String source) throws Throwable {
        super(type, in, source);
    }

    public abstract boolean canReset();

    public boolean isUnused() {
        return !this.hasBeenRead();
    }
}
