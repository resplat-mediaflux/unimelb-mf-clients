package unimelb.mf.client.session;

import arc.mf.client.ServerClient;

public interface MFOutput extends ServerClient.Output {

    boolean hasBeenRead();

    default boolean isUnused() {
        return !this.hasBeenRead();
    }
}
