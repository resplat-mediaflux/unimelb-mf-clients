package unimelb.mf.client.session;

import java.util.Collections;
import java.util.List;

import arc.mf.client.RequestOptions;
import arc.mf.client.ServerClient;
import arc.mf.client.ServerRoute;
import arc.utils.AbortableOperationHandler;
import arc.utils.CanAbort;
import arc.xml.XmlDoc;

public class MFRequest {

    private final ServerRoute _route;
    private final String _service;
    private final String _args;
    private final List<ServerClient.Input> _inputs;
    private final ServerClient.Output _output;
    private RequestOptions _ops;
    private CanAbort _ca;

    protected MFRequest(ServerRoute route, String service, String args, List<ServerClient.Input> inputs,
            ServerClient.Output output, RequestOptions options) {
        _route = route;
        _service = service;
        _args = args;
        _inputs = inputs;
        _output = output;
        _ops = copy(options);
        if (_ops == null) {
            _ops = new RequestOptions();
        }
        final AbortableOperationHandler ah = _ops.abortHandler();
        _ops.setAbortHandler(new AbortableOperationHandler() {

            @Override
            public void finished(CanAbort ca) {
                _ca = null;
                if (ah != null) {
                    ah.finished(ca);
                }
            }

            @Override
            public void started(CanAbort ca) {
                _ca = ca;
                if (ah != null) {
                    ah.started(ca);
                }
            }
        });
    }

    public ServerRoute serverRoute() {
        return _route;
    }

    public String service() {
        return _service;
    }

    public String args() {
        return _args;
    }

    public List<ServerClient.Input> inputs() {
        if (_inputs == null) {
            return null;
        } else {
            return Collections.unmodifiableList(_inputs);
        }
    }

    public boolean hasInput() {
        return _inputs != null && !_inputs.isEmpty();
    }

    public ServerClient.Output output() {
        return _output;
    }

    public boolean hasOutput() {
        return _output != null;
    }

    public RequestOptions options() {
        if (_ops == null) {
            return null;
        } else {
            // Make a copy to guarantee immutability.
            return copy(_ops);
        }
    }

    public void abort() throws Throwable {
        if (_ca != null) {
            _ca.abort();
            _ca = null;
        }
    }

    public XmlDoc.Element execute(MFSession session) throws Throwable {
        return session.execute(serverRoute(), service(), args(), inputs(), output(), options());
    }

    private static RequestOptions copy(RequestOptions options) {
        if (options == null) {
            return null;
        }
        RequestOptions ops = new RequestOptions();
        ops.setAbortHandler(options.abortHandler());
        ops.setBackground(options.background());
        ops.setDescription(options.description());
        ops.setKey(options.key());
        ops.setRetainHours(options.retainHours());
        ops.setSequenceListener(options.sequenceListener());
        ops.setStatusHandler(options.statusHandler(), 1000);
        return ops;
    }

    public boolean canBeReExecuted() {
        boolean hasInput = _inputs != null && !_inputs.isEmpty();
        boolean hasOutput = _output != null;
        if (!hasInput && !hasOutput) {
            return true;
        }
        boolean inputUnused = !hasInput;
        if (hasInput) {
            for (ServerClient.Input input : _inputs) {
                if (input instanceof MFInput) {
                    if (((MFInput) input).isUnused()) {
                        inputUnused = true;
                        break;
                    }
                } else if (input instanceof MFGeneratedInput) {
                    if (((MFGeneratedInput) input).isUnused()) {
                        inputUnused = true;
                        break;
                    }
                } else {
                    if (input.stream() != null && input.length() > 0 && !input.hasBeenRead()) {
                        inputUnused = true;
                        break;
                    }
                }
            }
        }
        boolean outputUnused = !hasOutput;
        if (hasOutput) {
            if (_output instanceof MFOutput) {
                if (((MFOutput) _output).isUnused()) {
                    outputUnused = true;
                }
            }
        }
        return inputUnused && outputUnused;
    }

    public static MFRequest create(ServerRoute route, String service, String args, List<ServerClient.Input> inputs,
            ServerClient.Output output, RequestOptions options) {
        return new MFRequestBuilder().build(route, service, args, inputs, output, options);
    }
}
