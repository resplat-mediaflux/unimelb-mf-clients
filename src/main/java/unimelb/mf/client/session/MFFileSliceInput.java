package unimelb.mf.client.session;

import java.io.File;
import java.io.IOException;

import arc.streams.LongFileInputStream;
import arc.streams.SliceInputStream;

public class MFFileSliceInput extends MFInput {

    private final long _offset;

    public MFFileSliceInput(File file, long offset, long length) throws Throwable {
        super(null, getSliceInputStream(file, offset, length), null);
        _offset = offset;
    }

    public long offset() {
        return _offset;
    }

    @Override
    public boolean canReset() {
        return true;
    }

    private static SliceInputStream getSliceInputStream(File file, long offset, long length) throws Throwable {
        long fileLength = file.length();
        if (offset < 0 || offset > fileLength) {
            throw new IOException("Invalid file slice offset: " + offset + " for file: '" + file.getPath()
                    + "' (fileLength=" + fileLength + ", sliceLength=" + length + ")");
        }
        if (length == -1) {
            length = fileLength - offset;
        }
        if (length < 0 || length > fileLength - offset) {
            throw new IOException("Invalid file slice length: " + offset + " for file: '" + file.getPath()
                    + "' (fileLength=" + fileLength + ", sliceOffset=" + offset + ")");
        }
        LongFileInputStream fin = new LongFileInputStream(file);
        try {
            return new SliceInputStream(fin, offset, length);
        } catch (Throwable t) {
            try {
                fin.close();
            } catch (Throwable t2) {
            }
            throw t;
        }
    }

}
