package unimelb.mf.client.session;

import java.io.OutputStream;

import arc.mf.client.ServerClient;
import arc.streams.NonCloseOutputStream;
import arc.streams.StreamCopy.AbortCheck;
import unimelb.io.CountedOutputStream;

public abstract class MFGeneratedInput extends ServerClient.GeneratedInput {

	private CountedOutputStream _cout;

	public MFGeneratedInput(String type, String ext, String source, long length, String store) throws Throwable {
		super(type, ext, source, length, store);
	}

	@Override
	protected final synchronized void copyTo(OutputStream out, AbortCheck ac) throws Throwable {
		_cout = (out instanceof NonCloseOutputStream) ? new CountedOutputStream(out)
				: new CountedOutputStream(new NonCloseOutputStream(out));
		try {
			consume(_cout, ac);
		} finally {
			_cout.close();
		}
	}

	protected abstract void consume(OutputStream out, AbortCheck ac) throws Throwable;

	@Override
	public final synchronized boolean hasBeenRead() {
		return _cout != null && _cout.bytesCount() > 0;
	}

	public boolean isUnused() {
		return !hasBeenRead();
	}

}