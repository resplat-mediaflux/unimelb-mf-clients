package unimelb.mf.client.session;

import arc.mf.client.ServerClient;
import arc.xml.XmlDoc;
import arc.xml.XmlStringWriter;

import java.util.ArrayList;
import java.util.List;

public class SamlProvider {

    private String _id;
    private String _type;
    private String _label;
    private String _url;
    private String _shortName;
    private String _auth;

    public SamlProvider(XmlDoc.Element pe) throws Throwable {
        _id = pe.value("@id");
        _type = pe.value("type");
        _label = pe.value("label");
        _url = pe.value("url");
        _shortName = pe.value("shortname");
        _auth = pe.value("auth");
    }

    public String id() {
        return _id;
    }

    public String type() {
        return _type;
    }

    public String label() {
        return _label;
    }

    public String url() {
        return _url;
    }

    public String shortName() {
        return _shortName;
    }

    public String auth() {
        return _auth;
    }

    @Override
    public String toString() {
        return _label;
    }

    public static List<SamlProvider> getAll(ServerClient.Connection cxn, String domain) throws Throwable {
        XmlStringWriter w = new XmlStringWriter();
        w.add("domain", domain);
        XmlDoc.Element re = cxn.execute("authentication.domain.provider.list", w.document());
        List<XmlDoc.Element> pes = re.elements("provider");
        if (pes != null && !pes.isEmpty()) {
            List<SamlProvider> providers = new ArrayList<SamlProvider>(pes.size());
            for (XmlDoc.Element pe : pes) {
                providers.add(new SamlProvider(pe));
            }
            return providers;
        }
        return null;
    }

}
