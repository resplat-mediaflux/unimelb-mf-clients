package unimelb.mf.client.asset;

public interface HasAssetID {
    
    String assetId();

}
