package unimelb.mf.client.asset;

public interface HasPath {

    String path();

}
