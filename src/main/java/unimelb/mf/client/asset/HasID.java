package unimelb.mf.client.asset;

public interface HasID extends HasAssetID, HasCiteableID, HasPath {

}
