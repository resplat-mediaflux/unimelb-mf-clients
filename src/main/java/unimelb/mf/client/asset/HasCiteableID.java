package unimelb.mf.client.asset;

public interface HasCiteableID {

    String citeableId();

}
