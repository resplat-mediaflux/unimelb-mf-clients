package unimelb.mf.client.asset;

public class SimpleAsset implements HasID {

    private String _assetId;
    private String _cid;
    private String _path;

    public SimpleAsset(String assetId, String cid, String path) {
        _assetId = assetId;
        _cid = cid;
        _path = path;
    }

    @Override
    public String citeableId() {
        return _cid;
    }

    @Override
    public String assetId() {
        return _assetId;
    }

    @Override
    public String path() {
        return _path;
    }

    public void setAssetId(String assetId) {
        _assetId = assetId;
    }

    public void setCiteableId(String cid) {
        _cid = cid;
    }

    public void setPath(String path) {
        _path = path;
    }

}
