package unimelb.mf.client.asset;

public interface HasNamespace {

    String namespace();

}
