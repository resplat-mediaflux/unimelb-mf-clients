package unimelb.mf.client.asset;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;

import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlStringWriter;
import arc.xml.XmlWriter;
import unimelb.mf.client.session.MFSession;

public class AssetQuery implements Callable<XmlDoc.Element> {

    public static enum Action {
        GET_ID("get-id"), GET_META("get-meta"), GET_VALUE("get-value"), GET_CID("get-cid");
        public final String value;

        Action(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return this.value;
        }

    }

    public static class Settings {

        private Integer _idx;
        private Integer _size;
        private Boolean _count;
        private Action _action;
        private List<String> _wheres;

        public Settings() {

        }

        public AssetQuery.Settings setIdx(Integer idx) {
            _idx = idx;
            return this;
        }

        public AssetQuery.Settings setSize(Integer size) {
            _size = size;
            return this;
        }

        public AssetQuery.Settings setAction(Action action) {
            _action = action;
            return this;
        }

        public AssetQuery.Settings setCount(Boolean count) {
            _count = count;
            return this;
        }

        public AssetQuery.Settings setWhere(String where) {
            if (_wheres == null) {
                _wheres = new ArrayList<String>();
            } else {
                _wheres.clear();
            }
            _wheres.add(where);
            return this;
        }

        public AssetQuery.Settings addWhere(String where) {
            if (_wheres == null) {
                _wheres = new ArrayList<String>();
            }
            _wheres.add(where);
            return this;
        }

        public AssetQuery.Settings setWheres(String... wheres) {
            if (_wheres == null) {
                _wheres = new ArrayList<String>();
            } else {
                _wheres.clear();
            }
            for (String where : wheres) {
                _wheres.add(where);
            }
            return this;
        }

        public AssetQuery.Settings setWheres(Collection<String> wheres) {
            if (_wheres == null) {
                _wheres = new ArrayList<String>();
            } else {
                _wheres.clear();
            }
            for (String where : wheres) {
                _wheres.add(where);
            }
            return this;
        }

        public List<String> wheres() {
            return (_wheres == null || _wheres.isEmpty()) ? null : Collections.unmodifiableList(_wheres);
        }

        public Integer idx() {
            return _idx;
        }

        public Integer size() {
            return _size;
        }

        public Boolean count() {
            return _count;
        }

        public Action action() {
            return _action;
        }

        public void save(XmlWriter w) throws Throwable {
            if (_idx != null) {
                w.add("idx", _idx);
            }
            if (_size != null) {
                w.add("size", _size);
            }
            if (_count != null) {
                w.add("count", _count);
            }
            if (_action != null) {
                w.add("action", _action);
            }
            if (_wheres != null) {
                for (String where : _wheres) {
                    w.add("where", where);
                }
            }
        }
    }

    private MFSession _session;
    private Settings _settings;

    public AssetQuery(MFSession session, Settings settings) {
        _session = session;
        _settings = settings;
    }

    @Override
    public Element call() throws Exception {
        XmlStringWriter w = new XmlStringWriter();
        try {
            _settings.save(w);
            return _session.execute("asset.query", w.document());
        } catch (Throwable e) {
            if (e instanceof Exception) {
                throw (Exception) e;
            } else {
                throw new Exception(e.getMessage(), e);
            }
        }
    }

    public static XmlDoc.Element execute(MFSession session, AssetQuery.Settings settings) throws Exception {
        return new AssetQuery(session, settings).call();
    }
}
