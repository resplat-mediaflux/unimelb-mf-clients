package unimelb.mf.client.perf.cli;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import unimelb.mf.client.perf.MFPerf;
import unimelb.mf.client.perf.MFPerfSettings;
import unimelb.mf.client.session.MFConfigBuilder;
import unimelb.mf.client.session.MFSession;

public class MFPerfCLI {

    public static void main(String[] args) throws Throwable {

        try {
            Path configFile = null;
            if (args != null) {
                for (int i = 0; i < args.length;) {
                    if ("--config".equalsIgnoreCase(args[i])) {
                        if (configFile != null) {
                            throw new IllegalArgumentException(
                                    "Multiple --config configuration files specified. Expects only one.");
                        }
                        configFile = Paths.get(args[i + 1]);
                        if (!Files.exists(configFile)) {
                            throw new IllegalArgumentException("Cannot find file: " + args[i + 1]);
                        }
                        if (!Files.isRegularFile(configFile)) {
                            throw new IllegalArgumentException("File: " + args[i + 1] + " is not a regular file.");
                        }
                        i += 2;
                    } else {
                        throw new IllegalArgumentException("Unexpected argument: " + args[i]);
                    }
                }
            }
            if (configFile == null) {
                throw new IllegalArgumentException("No --config configuration file is specified.");
            }

            MFConfigBuilder mfconfig = new MFConfigBuilder();
            mfconfig.loadFromXmlFile(configFile.toFile());
            if (mfconfig.app() == null) {
                mfconfig.setApp(MFPerf.APPLICATION_NAME);
            }

            MFSession session = MFSession.create(mfconfig);
            session.startPingServerPeriodically(60000);

            MFPerfSettings settings = MFPerfSettings.parse(configFile.toFile());
            if (settings.testSettings().isEmpty()) {
                throw new IllegalArgumentException("Failed to parse settings from configuration file: "
                        + configFile.toString() + ". Missing test configuration.");
            }
            settings.validate(session);

            try {
                new MFPerf(session, settings).call();
            } finally {
                session.discard();
            }
        } catch (Throwable e) {
            e.printStackTrace();
            if (e instanceof IllegalArgumentException) {
                printUsage();
            }
        }

    }

    public static void printUsage() {
        System.out.println();
        System.out.println("USAGE:");
        System.out.println(String.format("    %s --config <config-file>", MFPerf.APPLICATION_NAME));
        System.out.println();
        System.out.println("DESCRIPTION:");
        System.out.println("    Test network transfer rates to/from the specified Mediaflux server.");
        System.out.println();
    }

}
