package unimelb.mf.client.perf;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;

import arc.mf.client.ServerClient;
import arc.xml.XmlStringWriter;
import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.util.AssetUtils;
import unimelb.mf.client.util.collection.CollectionType;
import unimelb.utils.LoggingUtils;

public class MFPerf implements Callable<Void> {

    public static final String APPLICATION_NAME = "unimelb-mf-perf";

    private MFSession _session;
    private MFPerfSettings _settings;
    private Logger _logger;

    public MFPerf(MFSession session, MFPerfSettings settings) throws Throwable {
        _session = session;
        _settings = settings;
        if (_settings.logDirectory() != null) {
            _logger = LoggingUtils.createFileAndConsoleLogger(_settings.logDirectory(), APPLICATION_NAME);
        } else {
            _logger = LoggingUtils.createConsoleLogger(APPLICATION_NAME);
        }
    }

    @Override
    public Void call() throws Exception {
        try {
            List<TestResult> results = new ArrayList<TestResult>();
            List<TestSettings> tss = _settings.testSettings();
            if (_settings.verbose()) {
                _logger.info("starting tests...");
            }
            for (TestSettings ts : tss) {
                results.addAll(new Test(_session, ts, _logger, _settings.verbose()).call());
            }
            if (_settings.resultAssetID() != null) {
                if (_settings.verbose()) {
                    _logger.info("updating result asset " + _settings.resultAssetID() + "...");
                }
                updateResultAsset(_session, results, _settings.resultAssetID(), _settings.resultAssetRetain());
            }
            if (_settings.resultFilePath() != null) {
                if (_settings.verbose()) {
                    _logger.info("updating result file " + _settings.resultFilePath() + "...");
                }
                saveResultsToFile(results, _settings.resultFilePath().toFile());
            }

            printResults(results);

            return null;
        } catch (Throwable e) {
            _logger.log(Level.SEVERE, e.getMessage(), e);
            if (e instanceof InterruptedException) {
                Thread.currentThread().interrupt();
            }
            if (e instanceof Exception) {
                throw (Exception) e;
            } else {
                throw new Exception(e);
            }
        }
    }

    private static void updateResultAsset(MFSession session, List<TestResult> results, String resultAssetID, int retain)
            throws Throwable {
        updateResultAsset(session, results, resultAssetID, retain, null);
    }

    private static void updateResultAsset(MFSession session, List<TestResult> results, String resultAssetID, int retain,
            Path tmpDir) throws Throwable {
        File tmpFile = tmpDir != null ? File.createTempFile(APPLICATION_NAME + "-result-", ".csv", tmpDir.toFile())
                : File.createTempFile(APPLICATION_NAME + "-result-", ".csv");
        try {
            if (AssetUtils.assetExists(session, resultAssetID)) {
                XmlStringWriter w = new XmlStringWriter();
                w.add("id", resultAssetID);
                session.execute("asset.get", w.document(), new ServerClient.FileOutput(tmpFile));
            }

            saveResultsToFile(results, tmpFile);

            XmlStringWriter w = new XmlStringWriter();
            w.add("id", resultAssetID);
            if (resultAssetID.startsWith("path=")) {
                String resultAssetPath = resultAssetID.substring(5);
                CollectionType parentType = CollectionType.collectionTypeFor(resultAssetPath, session);
                w.add("create", new String[] { "in", parentType.shortName() }, true);
            }
            session.execute("asset.set", w.document(), new ServerClient.FileInput(tmpFile));
            if (retain > 0) {
                pruneAsset(session, resultAssetID, retain);
            }
        } finally {
            Files.deleteIfExists(tmpFile.toPath());
        }
    }

    private static void saveResultsToFile(List<TestResult> results, File file) throws Throwable {
        boolean fileExists = file.exists();
        try (Writer w = new BufferedWriter(new FileWriter(file, fileExists))) {
            if (!fileExists) {
                TestResult.saveCSVHeader(w);
            }
            for (TestResult r : results) {
                r.saveCSV(w);
            }
        }
    }

    private static void pruneAsset(MFSession session, String assetId, int retain) throws Throwable {
        XmlStringWriter w = new XmlStringWriter();
        w.add("id", assetId);
        w.add("retain", retain);
        session.execute("asset.prune", w.document());
    }

    private static void printResults(List<TestResult> results) throws IOException {
        System.out.println();
        OutputStreamWriter w = new OutputStreamWriter(System.out);
        TestResult.saveCSVHeader(w);
        if (results != null && !results.isEmpty()) {
            for (TestResult r : results) {
                r.saveCSV(w);
            }
            w.flush();
        }
        System.out.println();
    }
}
