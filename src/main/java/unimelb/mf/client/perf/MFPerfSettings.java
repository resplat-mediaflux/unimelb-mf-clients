package unimelb.mf.client.perf;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import arc.xml.XmlDoc;
import unimelb.mf.client.session.MFSession;

public class MFPerfSettings {

    public static final int RESULT_ASSET_RETAIN = 10;

    private String _resultAssetID;
    private String _resultAssetPath;
    private int _resultAssetRetain = RESULT_ASSET_RETAIN;
    private Path _resultFilePath;
    private List<TestSettings> _testSettings;
    private boolean _verbose = false;
    private Path _logDir = null;

    MFPerfSettings() {
        _testSettings = new ArrayList<TestSettings>();
    }

    MFPerfSettings(XmlDoc.Element pe) throws Throwable {
        this();
        parse(pe);
    }

    public Path resultFilePath() {
        return _resultFilePath;
    }

    public String resultAssetID() {
        if (_resultAssetID != null) {
            return _resultAssetID;
        } else if (_resultAssetPath != null) {
            return "path=" + _resultAssetPath;
        } else {
            return null;
        }
    }

    public String resultAssetPath() {
        return _resultAssetPath;
    }

    public int resultAssetRetain() {
        return _resultAssetRetain;
    }

    public boolean verbose() {
        return _verbose;
    }

    public Path logDirectory() {
        return _logDir;
    }

    public MFPerfSettings setResultFilePath(Path path) {
        _resultFilePath = path;
        return this;
    }

    public MFPerfSettings setResultAssetID(String assetID) {
        _resultAssetID = assetID;
        return this;
    }

    public MFPerfSettings setResultAssetPath(String assetPath) {
        _resultAssetPath = assetPath;
        return this;
    }

    public MFPerfSettings setResultAssetRetain(int retain) {
        _resultAssetRetain = retain;
        return this;
    }

    public MFPerfSettings setResultFilePath(String path) {
        _resultFilePath = Paths.get(path);
        return this;
    }

    public MFPerfSettings setVerbose(boolean verbose) {
        _verbose = verbose;
        return this;
    }

    public MFPerfSettings setLogDirectory(Path logDir) {
        _logDir = logDir;
        return this;
    }

    public void addTestSettings(TestSettings ts) {
        _testSettings.add(ts);
    }

    public List<TestSettings> testSettings() {
        return Collections.unmodifiableList(_testSettings);
    }

    public void parse(XmlDoc.Element e) throws Throwable {
        _verbose = e.booleanValue("verbose", false);
        String logDir = e.value("logDirectory");
        if (logDir != null) {

        }
        if (e.elementExists("logDirectory")) {
            _logDir = Paths.get(e.value("logDirectory"));
            if (!Files.isDirectory(_logDir)) {
                throw new IllegalArgumentException(
                        "Log directory: " + _logDir.toString() + " does not exist or it is not a directory.");
            }
        }
        if (e.elementExists("result/asset/id")) {
            setResultAssetID(e.value("result/asset/id"));
        }
        if (e.elementExists("result/asset/path")) {
            setResultAssetPath(e.value("result/asset/path"));
        }
        if (e.elementExists("result/asset/retain")) {
            setResultAssetRetain(e.intValue("result/asset/retain"));
        }
        if (e.elementExists("result/file/path")) {
            setResultFilePath(Paths.get(e.value("result/file/path")));
        }
        List<XmlDoc.Element> tes = e.elements("test");
        if (tes != null && !tes.isEmpty()) {
            for (XmlDoc.Element te : tes) {
                addTestSettings(new TestSettings(te));
            }
        }
    }

    public void validate(MFSession session) throws Throwable {

        for (TestSettings ts : _testSettings) {
            ts.validate(session);
        }
    }

    public static MFPerfSettings parse(File xmlFile) throws Throwable {
        try (Reader r = new BufferedReader(new FileReader(xmlFile))) {
            XmlDoc.Element e = new XmlDoc().parse(r);
            if (!e.elementExists("perf")) {
                throw new IllegalArgumentException(
                        "No perf element found from XML file: " + xmlFile.getPath() + ". Invalid configuration.");
            }
            XmlDoc.Element pe = e.element("perf");
            return new MFPerfSettings(pe);
        }
    }

}
