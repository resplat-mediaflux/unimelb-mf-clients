package unimelb.mf.client.perf;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import arc.xml.XmlDoc;
import unimelb.mf.client.session.MFSession;

public class TestSettings {

	public static final int DEFAULT_NUMBER_OF_THREADS = 1;
	public static final int DEFAULT_NUMBER_OF_FILES = 1;
	public static final long DEFAULT_FILE_SIZE = 1048576L; // 1 MiB;

	private Set<Action> _actions;
	private int _nbThreads;
	private int _nbFiles;
	private boolean _useClusterIO;
	private boolean _useInMemoryFile;
	private long _fileSize;
	private Path _dir;
	private String _namespace;
	private boolean _destroyAssets = true;

	public TestSettings() {
		_actions = new LinkedHashSet<Action>();
		_nbThreads = DEFAULT_NUMBER_OF_THREADS;
		_nbFiles = DEFAULT_NUMBER_OF_FILES;
		_useClusterIO = false;
		_useInMemoryFile = true;
		_fileSize = DEFAULT_FILE_SIZE;
		_dir = null;
		_namespace = null;
		_destroyAssets = true;
	}

	public TestSettings(XmlDoc.Element e) throws Throwable {
		this();
		parse(e);
	}

	public boolean ping() {
		return _actions.contains(Action.PING);
	}

	public boolean upload() {
		return _actions.contains(Action.UPLOAD);
	}

	public boolean download() {
		return _actions.contains(Action.DOWNLOAD);
	}

	public int numberOfThreads() {
		return _nbThreads;
	}

	public int numberOfFiles() {
		return _nbFiles;
	}

	public boolean useClusterIO() {
		return _useClusterIO;
	}

	public boolean useInMemoryFile() {
		return _useInMemoryFile;
	}

	public long fileSize() {
		return _fileSize;
	}

	public Path directory() {
		return _dir;
	}

	public String namespace() {
		return _namespace;
	}

	public boolean destroyAssets() {
		return _destroyAssets;
	}

	public TestSettings addAction(Action action) {
		_actions.add(action);
		return this;
	}

	public TestSettings setNumberOfThreads(int nbThreads) {
		_nbThreads = nbThreads;
		return this;
	}

	public TestSettings setNumberOfFiles(int nbFiles) {
		_nbFiles = nbFiles;
		return this;
	}

	public TestSettings setUseClusterIO(boolean useClusterIO) {
		_useClusterIO = useClusterIO;
		return this;
	}

	public TestSettings setUseInMemoryFile(boolean useInMemoryFile) {
		_useInMemoryFile = useInMemoryFile;
		return this;
	}

	public TestSettings setFileSize(long fileSize) {
		_fileSize = fileSize;
		return this;
	}

	public TestSettings setDirectory(Path dir) {
		_dir = dir;
		return this;
	}

	public TestSettings setNamespace(String ns) {
		_namespace = ns;
		return this;
	}

	public TestSettings setDestroyAssets(boolean destroyAssets) {
		_destroyAssets = destroyAssets;
		return this;
	}

	public TestSettings parse(XmlDoc.Element e) throws Throwable {
		if (e != null) {
			if (e.elementExists("action")) {
				Collection<String> as = e.values("action");
				for (String a : as) {
					Action action = Action.fromString(a);
					if (a == null) {
						throw new IllegalArgumentException("Invalid action: " + a);
					}
					addAction(action);
				}
			}
			if (e.elementExists("numberOfThreads")) {
				setNumberOfThreads(e.intValue("numberOfThreads"));
			}
			if (e.elementExists("numberOfFiles")) {
				setNumberOfFiles(e.intValue("numberOfFiles"));
			}
			if (e.elementExists("useClusterIO")) {
				setUseClusterIO(e.booleanValue("useClusterIO"));
			}
			if (e.elementExists("useInMemoryFile")) {
				setUseInMemoryFile(e.booleanValue("useInMemoryFile"));
			}
			if (e.elementExists("fileSize")) {
				setFileSize(e.longValue("fileSize"));
			}
			if (e.elementExists("directory")) {
				Path dir = Paths.get(e.value("directory"));
				if (!Files.exists(dir)) {
					throw new IllegalArgumentException("Directory: " + dir.toString() + " does not exist.");
				}
				if (!Files.isDirectory(dir)) {
					throw new IllegalArgumentException(dir.toString() + " is not a directory.");
				}
				setDirectory(dir);
			}
			if (e.elementExists("namespace")) {
				setNamespace(e.value("namespace"));
			}
			if(e.elementExists("cleanUp")) {
				setDestroyAssets(e.booleanValue("cleanUp"));
			}

		}
		return this;
	}

	public void validate(MFSession session) {
		if (_actions.isEmpty()) {
			throw new IllegalArgumentException("Invalid configuration: missing action.");
		}
		if (_namespace == null && (_actions.contains(Action.DOWNLOAD) || _actions.contains(Action.UPLOAD))) {
			throw new IllegalArgumentException("Invalid configuration: missing namespace.");
		}
		if (_dir == null && !_useInMemoryFile) {
			throw new IllegalArgumentException("Invalid configuration: missing directory.");
		}
	}

}
