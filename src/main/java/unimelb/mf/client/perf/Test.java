package unimelb.mf.client.perf;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

import arc.mf.client.ServerClient;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlStringWriter;
import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.util.AssetNamespaceUtils;

public class Test implements Callable<List<TestResult>> {

    private TestSettings _settings;
    private MFSession _session;
    private Logger _logger;
    private boolean _verbose;

    public Test(MFSession session, TestSettings settings, Logger logger, boolean verbose) {
        _session = session;
        _settings = settings;
        _logger = logger;
        _verbose = verbose;
    }

    @Override
    public List<TestResult> call() throws Exception {
        try {
            _session.setUseClusterIO(_settings.useClusterIO());
            List<TestResult> results = new ArrayList<TestResult>();
            ExecutorService executor = Executors.newFixedThreadPool(_settings.numberOfThreads());
            if (_settings.ping()) {
                if (_verbose) {
                    _logger.info("starting ping test...");
                }
                TestResult pr = ping(executor);
                if (_verbose) {
                    _logger.info("completed ping test. Result:\n" + pr.toString());
                }
                results.add(pr);
            }
            if (_settings.upload() || _settings.download()) {
                String store = _settings.useClusterIO() ? AssetNamespaceUtils.getAssetNamespaceStore(_session, _settings.namespace())
                        : null;
                if (store != null && _verbose) {
                    _logger.info("cluster I/O enabled by specifying asset store: " + store);
                }
                List<String> assetIds = new ArrayList<String>();
                try {
                    if (_verbose) {
                        _logger.info("starting upload test...");
                    }
                    TestResult ur = upload(executor, assetIds, store);
                    if (_verbose) {
                        _logger.info("completed upload test. Result:\n" + ur.toString());
                    }
                    results.add(ur);
                    if (_settings.download()) {
                        if (_verbose) {
                            _logger.info("starting download test...");
                        }
                        TestResult dr = download(executor, assetIds);
                        if (_verbose) {
                            _logger.info("completed download test. Result:\n" + dr.toString());
                        }
                        results.add(dr);
                    }
                } finally {
                    if (!assetIds.isEmpty() && _settings.destroyAssets()) {
                        if (_verbose) {
                            _logger.info("cleaning up uploaded test assets in namespace: " + _settings.namespace());
                        }
                        destroyAssets(_session, assetIds);
                    }
                }
            }
            executor.shutdown();
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.MICROSECONDS);
            return results;
        } catch (Throwable e) {
            if (e instanceof InterruptedException) {
                Thread.currentThread().interrupt();
            }
            if (e instanceof Exception) {
                throw (Exception) e;
            } else {
                throw new Exception(e);
            }
        }
    }

    private TestResult download(ExecutorService executor, List<String> assetIds) throws Throwable {
        long totalSize = getTotalContentSize(_session, assetIds);
        List<Future<Void>> futures = new ArrayList<Future<Void>>(assetIds.size());
        long startTime = System.currentTimeMillis();
        for (String assetId : assetIds) {
            Future<Void> future = executor.submit(new Callable<Void>() {

                @Override
                public Void call() throws Exception {
                    try {
                        if (_verbose) {
                            _logger.info("downloading asset " + assetId + "...");
                        }
                        download(_session, assetId, _settings.useInMemoryFile() ? null : _settings.directory());
                        if (_verbose) {
                            _logger.info("downloaded asset " + assetId + ".");
                        }
                        return null;
                    } catch (Throwable e) {
                        if (e instanceof Exception) {
                            throw (Exception) e;
                        } else {
                            throw new Exception(e);
                        }
                    }
                }
            });
            futures.add(future);
        }
        for (Future<Void> future : futures) {
            future.get();
        }
        long endTime = System.currentTimeMillis();
        return new TestResult(Action.DOWNLOAD, totalSize, startTime, endTime, _settings.numberOfThreads(),
                assetIds.size(), _settings.fileSize(), _settings.useClusterIO(), _settings.useInMemoryFile(),
                _settings.useInMemoryFile() ? null : _settings.directory(), _settings.namespace());

    }

    public static long getTotalContentSize(MFSession session, List<String> assetIds) throws Throwable {
        XmlStringWriter w = new XmlStringWriter();
        w.add("display", "total");
        for (String assetId : assetIds) {
            w.add("id", assetId);
        }
        return session.execute("asset.content.size", w.document()).longValue("total");
    }

    public static void download(MFSession session, String assetId, Path dir) throws Throwable {
        XmlStringWriter w = new XmlStringWriter();
        w.add("id", assetId);
        ServerClient.Output output = null;
        File file = null;
        try {
            if (dir == null) {
                output = new ServerClient.NullOutput();
            } else {
                file = File.createTempFile("test", ".tmp", dir.toFile());
                output = new ServerClient.FileOutput(file);
            }
            session.execute(Action.DOWNLOAD.service(), w.document(), output);
        } finally {
            if (file != null) {
                Files.deleteIfExists(file.toPath());
            }
        }
    }

    private TestResult upload(ExecutorService executor, List<String> assetIds, String store) throws Throwable {
        if (_settings.useInMemoryFile()) {
            return upload(executor, _settings.fileSize(), _settings.numberOfFiles(), assetIds, store);
        } else {
            List<File> files = generateTmpFiles(_settings.directory(), _settings.fileSize(), _settings.numberOfFiles(),
                    _settings.numberOfThreads(), _logger);
            try {
                return upload(executor, files, assetIds, store);
            } finally {
                for (File file : files) {
                    if (_verbose) {
                        _logger.info("deleting temp file: " + file.getPath());
                    }
                    Files.delete(file.toPath());
                }
            }
        }
    }

    private TestResult upload(ExecutorService executor, long fileSize, int numberOfFiles, List<String> assetIds,
            String store) throws Throwable {
        long totalSize = fileSize * numberOfFiles;
        List<Future<String>> futures = new ArrayList<Future<String>>(numberOfFiles);
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < numberOfFiles; i++) {
            final String name = String.format("test%d-%d.tmp", i, System.currentTimeMillis());
            Future<String> future = executor.submit(new Callable<String>() {

                @Override
                public String call() throws Exception {
                    try {
                        if (_verbose) {
                            _logger.info("uploading file: " + name + "...");
                        }
                        String assetId = upload(_session, fileSize, name, _settings.namespace(), store);
                        if (_verbose) {
                            _logger.info("uploaded file: " + name + " to asset " + assetId + ".");
                        }
                        return assetId;
                    } catch (Throwable e) {
                        if (e instanceof Exception) {
                            throw (Exception) e;
                        } else {
                            throw new Exception(e);
                        }
                    }
                }
            });
            futures.add(future);
        }
        for (Future<String> future : futures) {
            assetIds.add(future.get());
        }
        long endTime = System.currentTimeMillis();
        return new TestResult(Action.UPLOAD, totalSize, startTime, endTime, _settings.numberOfThreads(), numberOfFiles,
                fileSize, _settings.useClusterIO(), true, null, _settings.namespace());

    }

    private TestResult upload(ExecutorService executor, Collection<File> files, List<String> assetIds, String store)
            throws Throwable {
        long totalSize = 0L;
        for (File file : files) {
            totalSize += file.length();
        }
        List<Future<String>> futures = new ArrayList<Future<String>>(files.size());
        long startTime = System.currentTimeMillis();
        for (File file : files) {
            Future<String> future = executor.submit(new Callable<String>() {

                @Override
                public String call() throws Exception {
                    try {
                        if (_verbose) {
                            _logger.info("uploading file: " + file.getName() + "...");
                        }
                        String assetId = upload(_session, file, _settings.namespace(), store);
                        if (_verbose) {
                            _logger.info("uploaded file: " + file.getName() + " to asset " + assetId + ".");
                        }
                        return assetId;
                    } catch (Throwable e) {
                        if (e instanceof Exception) {
                            throw (Exception) e;
                        } else {
                            throw new Exception(e);
                        }
                    }
                }

            });
            futures.add(future);
        }
        for (Future<String> future : futures) {
            assetIds.add(future.get());
        }
        long endTime = System.currentTimeMillis();
        return new TestResult(Action.UPLOAD, totalSize, startTime, endTime, _settings.numberOfThreads(), files.size(),
                _settings.fileSize(), _settings.useClusterIO(), false, _settings.directory(), _settings.namespace());
    }

    public static String upload(MFSession session, File file, String namespace, String store) throws Throwable {
        XmlStringWriter w = new XmlStringWriter();
        w.add("namespace", namespace);
        w.add("name", file.getName());
        if (store != null) {
            w.add("store", store);
        }
        ServerClient.Input input = new ServerClient.FileInput(file);
        if (store != null) {
            input.setStore("asset:" + store);
        }
        return session.execute(Action.UPLOAD.service(), w.document(), input).value("id");
    }

    public static String upload(MFSession session, long fileSize, String name, String namespace, String store)
            throws Throwable {
        XmlStringWriter w = new XmlStringWriter();
        w.add("namespace", namespace);
        w.add("name", name);
        if (store != null) {
            w.add("store", store);
        }
        ServerClient.Input input = new ServerClient.NullInput(fileSize);
        if (store != null) {
            input.setStore("asset:" + store);
        }
        return session.execute(Action.UPLOAD.service(), w.document(), input).value("id");
    }

    private TestResult ping(ExecutorService executor) throws Throwable {
        if (_settings.useInMemoryFile()) {
            return ping(executor, _settings.fileSize(), _settings.numberOfFiles());
        } else {
            List<File> files = generateTmpFiles(_settings.directory(), _settings.fileSize(), _settings.numberOfFiles(),
                    _settings.numberOfThreads(), _logger);
            try {
                return ping(executor, files);
            } finally {
                for (File file : files) {
                    Files.delete(file.toPath());
                }
            }

        }
    }

    private TestResult ping(ExecutorService executor, long fileSize, int numberOfFiles) throws Throwable {
        long totalSize = fileSize * numberOfFiles;
        List<Future<XmlDoc.Element>> futures = new ArrayList<Future<XmlDoc.Element>>(numberOfFiles);
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < numberOfFiles; i++) {
            Future<XmlDoc.Element> future = executor.submit(new Callable<XmlDoc.Element>() {

                @Override
                public Element call() throws Exception {
                    try {
                        return ping(_session, fileSize);
                    } catch (Throwable e) {
                        if (e instanceof Exception) {
                            throw (Exception) e;
                        } else {
                            throw new Exception(e);
                        }
                    }
                }
            });
            futures.add(future);
        }
        List<XmlDoc.Element> res = new ArrayList<XmlDoc.Element>(futures.size());
        for (Future<XmlDoc.Element> future : futures) {
            res.add(future.get());
        }
        long endTime = System.currentTimeMillis();
        return new TestResult(Action.PING, totalSize, startTime, endTime, _settings.numberOfThreads(), numberOfFiles,
                fileSize, _settings.useClusterIO(), true, null, null);
    }

    private TestResult ping(ExecutorService executor, Collection<File> files) throws Throwable {
        long totalSize = 0L;
        for (File file : files) {
            totalSize += file.length();
        }
        List<Future<XmlDoc.Element>> futures = new ArrayList<Future<XmlDoc.Element>>(files.size());
        long startTime = System.currentTimeMillis();
        for (File file : files) {
            Future<XmlDoc.Element> future = executor.submit(new Callable<XmlDoc.Element>() {

                @Override
                public Element call() throws Exception {
                    try {
                        return ping(_session, file);
                    } catch (Throwable e) {
                        if (e instanceof Exception) {
                            throw (Exception) e;
                        } else {
                            throw new Exception(e);
                        }
                    }
                }
            });
            futures.add(future);
        }
        List<XmlDoc.Element> res = new ArrayList<XmlDoc.Element>(futures.size());
        for (Future<XmlDoc.Element> future : futures) {
            res.add(future.get());
        }
        long endTime = System.currentTimeMillis();
        return new TestResult(Action.PING, totalSize, startTime, endTime, _settings.numberOfThreads(), files.size(),
                _settings.fileSize(), _settings.useClusterIO(), false, _settings.directory(), null);
    }

    public static XmlDoc.Element ping(MFSession session, File file) throws Throwable {
        return session.execute(Action.PING.service(), null, new ServerClient.FileInput(file));
    }

    public static XmlDoc.Element ping(MFSession session, long length) throws Throwable {
        return session.execute(Action.PING.service(), null, new ServerClient.NullInput(length));
    }

    private static List<File> generateTmpFiles(Path dir, long length, int nbFiles, int nbThreads, Logger logger) throws Throwable {
        List<File> testFiles = Collections.synchronizedList(new ArrayList<File>(nbFiles));
        AtomicInteger ai = new AtomicInteger(0);
        ExecutorService threadPool = Executors.newFixedThreadPool(nbThreads);
        for (int i = 0; i < nbFiles; i++) {
            threadPool.submit(new Callable<Void>() {

                @Override
                public Void call() throws Exception {
                    try {
                        File file = generateTmpFile(dir,
                                String.format("test%d-%d.tmp", ai.getAndIncrement(), System.currentTimeMillis()),
                                length, logger);
                        synchronized (testFiles) {
                            testFiles.add(file);
                        }
                        return null;
                    } catch (Throwable t) {
                        if (t instanceof Exception) {
                            throw (Exception) t;
                        } else {
                            throw new Exception(t);
                        }
                    }
                }
            });
        }
        threadPool.shutdown();
        threadPool.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
        return testFiles;
    }

    private static File generateTmpFile(Path dir, String name, long length, Logger logger) throws Throwable {
        Path f = Paths.get(dir.toString(), name);
        byte[] buffer = new byte[1024];
        Random random = new Random();
        long remaining = length;
        logger.info("generating temporary file: " + f.toString());
        try (OutputStream os = Files.newOutputStream(f); BufferedOutputStream bos = new BufferedOutputStream(os)) {
            while (remaining > 0) {
                random.nextBytes(buffer);
                if (remaining >= buffer.length) {
                    bos.write(buffer);
                    remaining -= buffer.length;
                } else {
                    bos.write(buffer, 0, (int) remaining);
                    remaining = 0;
                }
            }
            bos.flush();
            return f.toFile();
        }
    }

    private static void destroyAssets(MFSession session, Collection<String> assetIds) throws Throwable {
        XmlStringWriter w = new XmlStringWriter();
        for (String assetId : assetIds) {
            w.add("id", assetId);
        }
        session.execute("asset.destroy", w.document());
    }

}
