package unimelb.mf.client.perf;

public enum Action {

    PING("server.ping"), UPLOAD("asset.create"), DOWNLOAD("asset.get");

    private String _service;

    Action(String service) {
        _service = service;
    }

    public String service() {
        return _service;
    }

    public static Action fromString(String a) {
        Action[] vs = values();
        for (Action v : vs) {
            if (v.name().equalsIgnoreCase(a == null ? null : a.trim())) {
                return v;
            }
        }
        return null;
    }
}
