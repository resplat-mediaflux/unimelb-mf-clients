package unimelb.mf.client.perf;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Date;

import arc.utils.DateTime;

public class TestResult {

    public final Action action;
    public final long size;
    public final long startTime;
    public final long endTime;

    public final int numberOfThreads;
    public final int numberOfFiles;
    public final long fileSize;

    public final boolean useClusterIO;
    public final boolean useInMemoryFile;
    public final Path directory;
    public final String namespace;

    public TestResult(Action action, long size, long startTime, long endTime, int numberOfThreads, int numberOfFiles,
            long fileSize, boolean useClusterIO, boolean useInMemoryFile, Path directory, String namespace) {
        this.action = action;
        this.size = size;
        this.startTime = startTime;
        this.endTime = endTime;

        this.numberOfThreads = numberOfThreads;
        this.numberOfFiles = numberOfFiles;
        this.useClusterIO = useClusterIO;
        this.useInMemoryFile = useInMemoryFile;
        this.fileSize = fileSize;
        this.directory = directory;
        this.namespace = namespace;
    }

    public final long timeMillisecs() {
        return this.endTime - this.startTime;
    }

    public final double timeSeconds() {
        return (double) timeMillisecs() / 1000.0;
    }

    public final double rateBPS() {
        return ((double) this.size) / ((double) timeMillisecs() / 1000.0);
    }

    public final double rateKPS() {
        return rateBPS() / 1000.0;
    }

    public final double rateMPS() {
        return rateBPS() / 1000000.0;
    }

    public final double rateGPS() {
        return rateBPS() / 1000000000.0;
    }

    public void saveCSV(Writer w) throws IOException {
        StringBuilder sb = new StringBuilder();
        sb.append(this.action.name().toLowerCase()).append(",");
        sb.append(this.numberOfThreads).append(",");
        sb.append(this.useClusterIO).append(",");
        sb.append(this.useInMemoryFile).append(",");
        sb.append(this.fileSize).append(",");
        sb.append(this.numberOfFiles).append(",");
        sb.append(formatTime(this.startTime)).append(",");
        sb.append(formatTime(this.endTime)).append(",");
        sb.append(String.format("%.3f", this.timeSeconds())).append(",");
        sb.append(String.format("%.3f", this.rateMPS())).append(",");
        sb.append("\n");
        w.write(sb.toString());
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("\taction: %s,\n", this.action.name().toLowerCase()));
        sb.append(String.format("\tnumber-of-threads: %s,\n", this.numberOfThreads));
        sb.append(String.format("\tuse-cluster-io: %s\n", this.useClusterIO));
        sb.append(String.format("\tuse-in-memory-file: %s\n", this.useInMemoryFile));
        sb.append(String.format("\tfile-size: %d bytes\n", this.fileSize));
        sb.append(String.format("\tnumber-of-files: %d\n", this.numberOfFiles));
        sb.append(String.format("\tstart-time: %s\n", formatTime(this.startTime)));
        sb.append(String.format("\tend-time: %s\n", formatTime(this.endTime)));
        sb.append(String.format("\tduration(seconds): %.3f\n", this.timeSeconds()));
        sb.append(String.format("\trate(MB/s): %.3f\n", this.rateMPS()));
        return sb.toString();
    }

    public static void saveCSVHeader(Writer w) throws IOException {
        StringBuilder sb = new StringBuilder();
        sb.append("\"Action\"").append(",");
        sb.append("\"Number of Threads\"").append(",");
        sb.append("\"Use Cluster IO?\"").append(",");
        sb.append("\"Use In-Memory File?\"").append(",");
        sb.append("\"File Size\"").append(",");
        sb.append("\"Number of Files\"").append(",");
        sb.append("\"Start Time\"").append(",");
        sb.append("\"End Time\"").append(",");
        sb.append("\"Duration(Seconds)\"").append(",");
        sb.append("\"Rate(MB/s)\"").append(",");
        sb.append("\n");
        w.write(sb.toString());
    }

    static String formatTime(long timeMillisecs) {
        return new SimpleDateFormat(DateTime.DATE_TIME_MS_FORMAT).format(new Date(timeMillisecs));
    }

}
