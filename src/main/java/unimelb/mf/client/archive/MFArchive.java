package unimelb.mf.client.archive;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

import arc.archive.ArchiveInput;
import arc.archive.ArchiveOutput;
import arc.archive.ArchiveRegistry;
import arc.mime.MimeType;
import arc.mime.NamedMimeType;
import arc.streams.SizedInputStream;
import arc.streams.StreamCopy;
import unimelb.utils.PathUtils;

public class MFArchive {

    public static final String TYPE_AAR = "application/arc-archive";
    public static final String TYPE_ZIP = "application/zip";
    public static final String TYPE_JAR = "application/java-archive";
    public static final String TYPE_TAR = "application/x-tar";
    public static final String TYPE_GZIPPED_TAR = "application/x-gtar";

    public static enum Type {
        // @formatter:off
        AAR(TYPE_AAR, "aar"),
        ZIP(TYPE_ZIP, "zip"),
        JAR(TYPE_JAR, "jar"),
        TAR(TYPE_TAR, "tar"),
        GZIPPED_TAR(TYPE_GZIPPED_TAR, "tar.gz");
        // @formatter:on

        public final String mimeType;
        public final String extension;

        Type(String mimeType, String extension) {
            this.mimeType = mimeType;
            this.extension = extension;
        }

        public static Type fromFileExtension(String ext) {
            if (ext != null) {
                Type[] vs = values();
                for (Type v : vs) {
                    if ("tgz".equalsIgnoreCase(ext) || "gtar".equalsIgnoreCase(ext) || "tar.gz".equalsIgnoreCase(ext)) {
                        return GZIPPED_TAR;
                    } else if (v.extension.equalsIgnoreCase(ext)) {
                        return v;
                    }
                }
            }
            return null;
        }

        public static Type fromMimeType(String mimeType) {
            if (mimeType != null) {
                Type[] vs = values();
                for (Type v : vs) {
                    if (v.mimeType.equalsIgnoreCase(mimeType)) {
                        return v;
                    }
                }
            }
            return null;
        }

        public static Type fromFileName(String fileName) {
            if (fileName != null) {
                String ext = fileName.toLowerCase().endsWith(".tar.gz") ? "tar.gz"
                        : PathUtils.getFileExtension(fileName);
                return fromFileExtension(ext);
            }
            return null;
        }
    }

    public static void extract(Path inFile, Path outputDir, boolean overwrite, Logger logger) throws Throwable {
        extract(inFile.toFile(), null, outputDir, overwrite, logger);
    }

    public static void extract(File inFile, Path outputDir, boolean overwrite, Logger logger) throws Throwable {
        extract(inFile, null, outputDir, overwrite, logger);
    }

    public static void extract(File inFile, String mimeType, Path outputDir, boolean overwrite, Logger logger)
            throws Throwable {
        ArchiveInput ai = mimeType == null ? ArchiveRegistry.createInput(inFile)
                : ArchiveRegistry.createInput(inFile, new NamedMimeType(mimeType));
        extract(ai, outputDir, overwrite, logger);
    }

    public static void extract(InputStream in, long length, String mimeType, Path outputDir, boolean overwrite,
            Logger logger) throws Throwable {
        ArchiveInput ai = ArchiveRegistry.createInput(new SizedInputStream(in, length),
                mimeType == null ? null : new NamedMimeType(mimeType));
        extract(ai, outputDir, overwrite, logger);
    }

    public static void extract(ArchiveInput ai, Path outputDir, boolean overwrite, Logger logger) throws Throwable {
        try {
            ArchiveInput.Entry entry = null;
            while ((entry = ai.next()) != null) {
                String ename = entry.name();
                while (ename.startsWith("/")) {
                    ename = ename.substring(1);
                }
                Path output = Paths.get(outputDir.toString(), ename);
                if (entry.isDirectory()) {
                    if (logger != null) {
                        logger.info("Creating directory: '" + output.toString() + "'...");
                    }
                    Files.createDirectories(output);
                } else {
                    if (logger != null) {
                        logger.info("Extracting file: '" + output.toString() + "'...");
                    }
                    boolean exists = Files.exists(output);
                    if (exists) {
                        if (overwrite) {
                            if (logger != null) {
                                logger.info("File: '" + output.toString() + "' already exists. Overwriting.");
                            }
                            File outputFile = output.toFile();
                            StreamCopy.copy(entry.stream(), outputFile);
                        } else {
                            if (logger != null) {
                                logger.info("File: '" + output.toString() + "' already exists. Skipped.");
                            }
                        }
                    } else {
                        Path parent = output.getParent();
                        if (parent != null) {
                            Files.createDirectories(parent);
                        }
                        File outputFile = output.toFile();
                        outputFile.createNewFile();
                        StreamCopy.copy(entry.stream(), outputFile);
                    }
                }
                ai.closeEntry();
            }
        } finally {
            ai.close();
        }
    }

    public static void create(File of, int compressionLevel, Logger logger, Path... inputs) throws Throwable {
        create(of, mimeTypeStringOf(of), compressionLevel, logger, inputs);
    }

    public static void create(File of, String mimeType, int compressionLevel, Logger logger, Path... inputs)
            throws Throwable {
        mimeType = mimeType == null ? mimeTypeStringOf(of) : mimeType;
        if ("application/x-tar".equals(mimeType)) {
            compressionLevel = 0;
        }
        ArchiveOutput ao = ArchiveRegistry.createOutput(of, mimeType, compressionLevel, null);
        try {
            if (inputs != null) {
                for (Path input : inputs) {
                    add(ao, input.getParent(), input, logger);
                }
            }
        } finally {
            ao.close();
        }
    }

    public static void create(File of, int compressionLevel, Logger logger, Collection<Path> inputs) throws Throwable {
        create(of, mimeTypeStringOf(of), compressionLevel, logger, inputs);
    }

    public static void create(File of, String mimeType, int compressionLevel, Logger logger, Collection<Path> inputs)
            throws Throwable {
        mimeType = mimeType == null ? mimeTypeStringOf(of) : mimeType;
        if ("application/x-tar".equals(mimeType)) {
            compressionLevel = 0;
        }
        ArchiveOutput ao = ArchiveRegistry.createOutput(of, mimeType, compressionLevel, null);
        try {
            if (inputs != null) {
                for (Path input : inputs) {
                    add(ao, input.getParent(), input, logger);
                }
            }
        } finally {
            ao.close();
        }
    }

    public static void create(OutputStream os, String mimeType, int compressionLevel, Logger logger, Path... inputs)
            throws Throwable {
        ArchiveOutput ao = ArchiveRegistry.createOutput(os, mimeType, compressionLevel, null);
        try {
            if (inputs != null) {
                for (Path input : inputs) {
                    add(ao, input.getParent(), input, logger);
                }
            }
        } finally {
            ao.close();
        }
    }

    public static void add(ArchiveOutput ao, Path baseDir, Path input, Logger logger) throws Throwable {
        String entryName = entryNameFor(baseDir, input);
        if (Files.isDirectory(input)) {
            if (logger != null) {
                logger.info("Adding directory: '" + entryName + "'");
            }
            // ao.addDirectory(entryName);
            Files.walkFileTree(input, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    try {
                        add(ao, baseDir, file, logger);
                    } catch (Throwable e) {
                        if (e instanceof InterruptedException) {
                            Thread.currentThread().interrupt();
                            return FileVisitResult.TERMINATE;
                        }
                        logger.log(Level.SEVERE, e.getMessage(), e);
                    }
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFileFailed(Path file, IOException ioe) {
                    logger.log(Level.SEVERE, "Failed to access file: " + file, ioe);
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException ioe) {
                    if (ioe != null) {
                        logger.log(Level.SEVERE, ioe.getMessage(), ioe);
                    }
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                    return super.preVisitDirectory(dir, attrs);
                }
            });
        } else {
            if (logger != null) {
                logger.info("Adding file: '" + entryName + "'");
            }
            ao.add(null, entryName, input.toFile());
        }
    }

    public static String entryNameFor(Path baseDir, Path input) {
        if (baseDir == null) {
            return PathUtils.toSystemIndependent(input.toString());
        } else {
            Path b = baseDir.toAbsolutePath();
            Path i = input.toAbsolutePath();
            if (i.equals(b)) {
                return null;
            }
            if (i.startsWith(b)) {
                return PathUtils.toSystemIndependent(b.relativize(i).toString());
            } else {
                return PathUtils.toSystemIndependent(input.toString());
            }
        }
    }

    public static MimeType mimeTypeFromExtension(String ext) {
        if ("aar".equalsIgnoreCase(ext)) {
            return new NamedMimeType(TYPE_AAR);
        } else if ("jar".equalsIgnoreCase(ext)) {
            return new NamedMimeType(TYPE_JAR);
        } else if ("tar".equalsIgnoreCase(ext)) {
            return new NamedMimeType(TYPE_TAR);
        } else if ("tgz".equalsIgnoreCase(ext)) {
            return new NamedMimeType(TYPE_GZIPPED_TAR);
        } else if ("zip".equalsIgnoreCase(ext)) {
            return new NamedMimeType(TYPE_ZIP);
        } else {
            return null;
        }
    }

    public static MimeType mimeTypeFromName(String name) {
        if (name.toLowerCase().endsWith(".aar")) {
            return new NamedMimeType(TYPE_AAR);
        } else if (name.toLowerCase().endsWith(".jar")) {
            return new NamedMimeType(TYPE_JAR);
        } else if (name.toLowerCase().endsWith(".tar")) {
            return new NamedMimeType(TYPE_TAR);
        } else if (name.toLowerCase().endsWith(".tar.gz") || name.toLowerCase().endsWith(".tgz")) {
            return new NamedMimeType(TYPE_GZIPPED_TAR);
        } else if (name.toLowerCase().endsWith(".zip")) {
            return new NamedMimeType(TYPE_ZIP);
        } else {
            return null;
        }
    }

    public static String mimeTypeStringOf(File f) {
        MimeType mt = mimeTypeOf(f);
        if (mt == null) {
            return null;
        }
        return mt.name();
    }

    public static MimeType mimeTypeOf(File f) {
        return mimeTypeFromName(f.getName());
    }

    public static String mimeTypeStringOf(Path f) {
        MimeType mt = mimeTypeOf(f);
        if (mt == null) {
            return null;
        }
        return mt.name();
    }

    public static MimeType mimeTypeOf(Path f) {
        if (f != null) {
            Path fn = f.getFileName();
            if (fn != null) {
                return mimeTypeFromName(fn.toString());
            }
        }
        return null;
    }

    public static boolean isAnArchive(File archiveFile) {
        return mimeTypeOf(archiveFile) != null;
    }

}
