package unimelb.mf.client.archive.cli;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import arc.mf.client.archive.Archive;
import unimelb.mf.client.archive.MFArchive;
import unimelb.utils.LoggingUtils;

public class MFArchiveCLI {

    public static final String APP_NAME = "unimelb-mf-archive";

    public static void main(String[] args) throws Throwable {
        try {
            if (args == null || args.length < 2) {
                throw new IllegalArgumentException("Missing arguments.");
            }
            String action = args[0];
            if (!"create".equalsIgnoreCase(action) && !"extract".equalsIgnoreCase(action)) {
                throw new IllegalArgumentException("Invalid action: " + action + ". Expects create or extract.");
            }

            Archive.declareSupportForAllTypes();

            if (action.equalsIgnoreCase("extract")) {
                Path archiveFile = null;
                Path outputDir = null;
                boolean overwrite = false;
                for (int i = 1; i < args.length;) {
                    if ("--overwrite".equalsIgnoreCase(args[i])) {
                        overwrite = true;
                        i++;
                    } else {
                        if (archiveFile == null) {
                            archiveFile = Paths.get(args[i]);
                            i++;
                        } else if (outputDir == null) {
                            outputDir = Paths.get(args[i]);
                            i++;
                        } else {
                            throw new IllegalArgumentException("Unexpected argument: " + args[i]);
                        }
                    }
                }
                if (outputDir == null) {
                    // output to current working dir
                    outputDir = Paths.get(System.getProperty("user.dir"));
                }
                MFArchive.extract(archiveFile, outputDir, overwrite, LoggingUtils.createConsoleLogger());
            } else {
                File archiveFile = null;
                int compressionLevel = 6;
                List<Path> inputs = new ArrayList<Path>();
                for (int i = 1; i < args.length;) {
                    if ("--compress".equalsIgnoreCase(args[i])) {
                        if (i + 1 == args.length) {
                            throw new IllegalArgumentException("Invalid arguments.");
                        }
                        try {
                            compressionLevel = Integer.parseInt(args[i + 1]);
                        } catch (NumberFormatException nfe) {
                            throw new IllegalArgumentException("Invalid --compress value: " + args[i + 1], nfe);
                        }
                        i += 2;
                    } else {
                        if (archiveFile == null) {
                            archiveFile = new File(args[i]);
                            if (archiveFile.exists()) {
                                throw new IllegalArgumentException(
                                        "File '" + archiveFile.toString() + "' already exists.");
                            }
                            if (!MFArchive.isAnArchive(archiveFile)) {
                                throw new IllegalArgumentException("Output file '" + archiveFile.toString()
                                        + "' does not have valid file extension. Expects *.zip or *.aar etc.");
                            }
                            i++;
                        } else {
                            Path input = Paths.get(args[i]);
                            if (!Files.exists(input)) {
                                throw new IllegalArgumentException(input.toString() + " does not exist.",
                                        new FileNotFoundException(input.toString()));
                            }
                            inputs.add(input);
                            i++;
                        }
                    }
                }
                MFArchive.create(archiveFile, compressionLevel, LoggingUtils.createConsoleLogger(), inputs);
            }
        } catch (IllegalArgumentException iae) {
            iae.printStackTrace();
            printUsage(System.out);
        }
    }

    static void printUsage(PrintStream ps) {
        ps.println();
        ps.println("USAGE:");
        ps.println();
        ps.println("    Create archive:");
        ps.println();
        ps.println(String.format("        %s create [options] <archive-file> <input-files-or-directories>", APP_NAME));
        ps.println();
        ps.println("            Options:");
        ps.println("                --compress <0-9>  Compression level: 0-9.");
        ps.println();
        ps.println("    Extract archive:");
        ps.println();
        ps.println(String.format("        %s extract [options] <archive-file> [output-directory]", APP_NAME));
        ps.println();
        ps.println("            Options:");
        ps.println("                --overwrite       Overwrite files if exist.");
        ps.println();
    }

}
