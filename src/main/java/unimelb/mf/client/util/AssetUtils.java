package unimelb.mf.client.util;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Objects;

import arc.mf.client.ServerClient;
import arc.xml.XmlDoc;
import arc.xml.XmlStringWriter;
import unimelb.mf.client.asset.HasID;
import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.util.collection.CollectionType;

public class AssetUtils {

	public static XmlDoc.Element getAssetMeta(ServerClient.Connection connection, String assetId) throws Throwable {
		return getAssetMeta(connection, assetId, null, false);
	}

	public static XmlDoc.Element getAssetMeta(ServerClient.Connection connection, String assetId, String cid)
			throws Throwable {
		return getAssetMeta(connection, assetId, cid, false);
	}

	public static XmlDoc.Element getAssetMetaByCID(ServerClient.Connection connection, String cid) throws Throwable {
		return getAssetMeta(connection, null, cid, false);
	}

	public static XmlDoc.Element getAssetMeta(ServerClient.Connection connection, String assetId, String cid,
			boolean lock) throws Throwable {
		XmlStringWriter w = new XmlStringWriter();
		if (assetId != null) {
			w.add("id", assetId);
		} else if (cid != null) {
			w.add("cid", cid);
		} else {
			throw new IllegalArgumentException("Missing asset identifiers.");
		}
		if (lock) {
			w.add("lock", true);
		}
		return connection.execute("asset.get", w.document()).element("asset");
	}

	public static XmlDoc.Element getAssetMeta(MFSession session, String assetId) throws Throwable {
		return getAssetMeta(session, assetId, null, false);
	}

	public static XmlDoc.Element getAssetMetaByPath(MFSession session, String assetPath) throws Throwable {
		return getAssetMeta(session, "path=" + assetPath, null, false);
	}

	public static XmlDoc.Element getAssetMetaByCID(MFSession session, String cid) throws Throwable {
		return getAssetMeta(session, null, cid, false);
	}

	public static List<XmlDoc.Element> getAssetMetaByCIDs(MFSession session, String... cids) throws Throwable {
		XmlStringWriter w = new XmlStringWriter();
		for (String cid : cids) {
			w.push("service", new String[] { "name", "asset.get" });
			w.add("cid", cid);
			w.pop();
		}
		return session.execute("service.execute", w.document()).elements("reply/response/asset");
	}

	public static XmlDoc.Element getAssetMeta(MFSession session, String assetId, String cid, boolean lock)
			throws Throwable {
		XmlStringWriter w = new XmlStringWriter();
		if (assetId != null) {
			w.add("id", assetId);
		} else if (cid != null) {
			w.add("cid", cid);
		} else {
			throw new IllegalArgumentException("Missing asset identifiers.");
		}
		if (lock) {
			w.add("lock", true);
		}
		return session.execute("asset.get", w.document()).element("asset");
	}

	public static void lockAsset(MFSession session, String assetId) throws Throwable {
		XmlStringWriter w = new XmlStringWriter();
		w.add("id", assetId);
		session.execute("asset.lock", w.document());
	}

	public static void unlockAsset(MFSession session, String assetId) throws Throwable {
		XmlStringWriter w = new XmlStringWriter();
		w.add("id", assetId);
		session.execute("asset.unlock", w.document());
	}

	public static void unlockAsset(String assetPath, MFSession session) throws Throwable {
		XmlStringWriter w = new XmlStringWriter();
		w.add("id", "path=" + assetPath);
		session.execute("asset.unlock", w.document());
	}

	public static void unlockAssetByCID(MFSession session, String cid) throws Throwable {
		unlockAsset(session, idFromCID(session, cid));
	}

	public static String idFromCID(MFSession session, String cid) throws Throwable {
		XmlStringWriter w = new XmlStringWriter();
		w.add("cid", cid);
		return session.execute("asset.identifier.get", w.document()).value("id");
	}

	public static String cidFromID(MFSession session, String assetId) throws Throwable {
		XmlStringWriter w = new XmlStringWriter();
		w.add("id", assetId);
		return session.execute("asset.identifier.get", w.document()).value("id/@cid");
	}

	public static String idFromPath(String assetPath, MFSession session) throws Throwable {
		XmlStringWriter w = new XmlStringWriter();
		w.add("id", "path=" + assetPath);
		return session.execute("asset.identifier.get", w.document()).value("id");
	}

	public static void unlockAsset(MFSession session, HasID id) throws Throwable {
		if (id.assetId() != null) {
			unlockAsset(session, id.assetId());
		} else if (id.citeableId() != null) {
			unlockAssetByCID(session, id.citeableId());
		} else if (id.path() != null) {
			unlockAsset(id.path(), session);
		} else {
			throw new IllegalArgumentException("Missing asset identifier.");
		}
	}

	public static boolean assetExists(MFSession session, String assetId) throws Throwable {
		XmlStringWriter w = new XmlStringWriter();
		w.add("id", assetId);
		return session.execute("asset.exists", w.document()).booleanValue("exists");
	}

	public static boolean assetExists(String assetPath, MFSession session) throws Throwable {
		return assetExists(session, "path=" + assetPath);
	}

	public static void setWorm(MFSession session, String assetId, boolean canAddVersions, boolean canMove)
			throws Throwable {
		XmlStringWriter w = new XmlStringWriter();
		w.add("id", assetId);
		w.add("can-add-versions", canAddVersions);
		w.add("can-move", canMove);
		session.execute("asset.worm.set", w.document());
	}

	public static String getCollectionStore(MFSession session, String collectionAssetId) throws Throwable {
		XmlDoc.Element ae = getAssetMeta(session, collectionAssetId);
		return ae.value("collection/store");
	}

	public static String getCollectionStore(String collectionAssetPath, MFSession session) throws Throwable {
		return getCollectionStore(session, "path=" + collectionAssetPath);
	}

	public static boolean collectionAssetExists(String path, MFSession session) throws Throwable {
		if (assetExists(path, session)) {
			XmlDoc.Element ae = getAssetMetaByPath(session, path);
			return ae.elementExists("collection");
		}
		return false;
	}

	public static String createCollectionAsset(MFSession session, String assetPath, boolean createParents,
			boolean ignoreIfExists) throws Throwable {
		if (ignoreIfExists) {
			if (assetExists(assetPath, session)) {
				XmlDoc.Element ae = getAssetMetaByPath(session, assetPath);
				boolean isCollection = ae.elementExists("collection");
				if (isCollection) {
					return ae.value("@id");
				}
			}
		}

		XmlStringWriter w = new XmlStringWriter();
		String parentPath = AssetPathUtils.getParent(assetPath);
		CollectionType parentType = CollectionType.collectionTypeOf(parentPath, session);
		if (parentType == null) {
			if (!createParents) {
				// parent does not exist and not creating it
				throw new FileNotFoundException("parent collection: '" + parentPath + "' does not exist.");
			} else {
				parentType = CollectionType.collectionTypeFor(parentPath, session);
			}
		}

		if (parentType == CollectionType.COLLECTION_ASSET) {
			w.add("pid", new String[] { "create", Boolean.toString(createParents) }, "path=" + parentPath);
		} else {
			w.add("namespace", new String[] { "create", Boolean.toString(createParents) }, parentPath);
		}
		w.add("name", AssetPathUtils.getName(assetPath));
		boolean levelZeroRoot = "/".equals(parentPath);
		w.add("collection",
				new String[] { "unique-name-index", "true", "level-zero-root", Boolean.toString(levelZeroRoot) }, true);

		XmlDoc.Element ae = session.execute("asset.create", w.document());
		return ae.value("id");
	}

	public static long countFileAssetsInCollection(String collectionAssetPath, MFSession session) throws Throwable {
		XmlStringWriter w = new XmlStringWriter();
		w.add("collection", "path=" + collectionAssetPath);
		w.add("where", "(asset has content or asset has link) and not(asset is collection)");
		return session.execute("asset.count", w.document()).longValue("total");
	}

	public static void destroyAsset(String assetPath, MFSession session, boolean members, boolean hardDestroy)
			throws Throwable {
		destroyAsset(session, "path=" + assetPath, members, hardDestroy);
	}

	public static void destroyAsset(MFSession session, String assetId, boolean members, boolean hardDestroy)
			throws Throwable {
		XmlStringWriter w = new XmlStringWriter();
		w.add("id", assetId);
		w.add("members", members);
		session.execute(hardDestroy ? "asset.hard.destroy" : "asset.destroy", w.document());
	}

	public static boolean isEmptyCollectionAsset(String assetPath, MFSession session) throws Throwable {
		String assetId = AssetUtils.idFromPath(assetPath, session);
		XmlStringWriter w = new XmlStringWriter();
		w.add("where", "id=" + assetId);
		w.add("where", "asset collection member count=0");
		String id = session.execute("asset.query", w.document()).value("id");
		return id != null && Objects.equals(assetId, id);
	}
}
