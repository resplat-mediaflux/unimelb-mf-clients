package unimelb.mf.client.util;

import java.util.Stack;

import unimelb.mf.client.session.MFSession;

public class AssetPathUtils {

    public static String trimTrailingSlash(String path) {
        if ("/".equals(path)) {
            return path;
        }
        return path.replaceAll("/+$", "");
    }

    public static String getName(String path) {
        if ("/".equals(path)) {
            return path;
        }
        int idx = path.lastIndexOf('/');
        if (idx < 0) {
            return path;
        } else {
            return path.substring(idx + 1);
        }
    }

    public static String getParent(String path) {
        if ("/".equals(path)) {
            return null;
        }
        int idx = path.lastIndexOf('/');
        if (idx <= 0) {
            return "/";
        } else {
            return path.substring(0, idx);
        }
    }

    public static String join(String prefix, String... components) {
        StringBuilder sb = new StringBuilder(prefix);
        for (String c : components) {
            sb.append("/").append(c);
        }
        return sb.toString();
    }

    /**
     * Convert a relative path to an absolute path.
     *
     * @param path              The relative path.
     * @param currentWorkingDir The absolute path of the current working directory.
     * @return
     */
    public static String toAbsolutePath(String path, String currentWorkingDir) {
        if (currentWorkingDir == null) {
            currentWorkingDir = "/";
        }
        Stack<String> stack = new Stack<>();
        if (!path.startsWith("/")) {
            String[] components = currentWorkingDir.split("/");
            for (String c : components) {
                if (!c.isEmpty()) {
                    stack.push(c);
                }
            }
        }
        String[] components = path.split("/");
        for (String c : components) {
            if ("..".equals(c)) {
                if (!stack.isEmpty()) {
                    stack.pop();
                }
            } else if (".".equals(c)) {

            } else {
                if (!c.isEmpty()) {
                    stack.push(c);
                }
            }
        }
        StringBuilder sb = new StringBuilder();
        if (stack.isEmpty()) {
            return "/";
        } else {
            stack.forEach(c -> {
                sb.append("/").append(c);
            });
        }
        return sb.toString();
    }

    public static boolean exists(String path, MFSession session) throws Throwable {
        return AssetNamespaceUtils.assetNamespaceExists(session, path) || AssetUtils.assetExists(path, session);
    }

    public static boolean isTopLevel(String path) {
        return !"/".equals(path) && path.lastIndexOf("/") == 0 && path.length() > 1;
    }

    public static void main(String[] args) {
//        String cwd = "/a/b/c";
//        String[] parts = cwd.split("/");
//        System.out.println(parts.length);
//        for (String p : parts) {
//            System.out.println(p);
//        }
//        System.out.println(toAbsolutePath("/d/e/f/../../", cwd));
    }

}
