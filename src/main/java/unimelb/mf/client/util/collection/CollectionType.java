package unimelb.mf.client.util.collection;

import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.util.AssetNamespaceUtils;
import unimelb.mf.client.util.AssetPathUtils;
import unimelb.mf.client.util.AssetUtils;

public enum CollectionType {

    COLLECTION_ASSET("collection"), ASSET_NAMESPACE("namespace");

    private String _shortName;

    CollectionType(String shortName) {
        _shortName = shortName;
    }

    public String shortName() {
        return _shortName;
    }

    @Override
    public String toString() {
        return _shortName;
    }

    public static CollectionType collectionTypeFor(String path, MFSession session) throws Throwable {
        if ("/".equals(path)) {
            return CollectionType.ASSET_NAMESPACE;
        }
        if (AssetUtils.collectionAssetExists(path, session)) {
            return CollectionType.COLLECTION_ASSET;
        } else if (AssetNamespaceUtils.assetNamespaceExists(session, path)) {
            return CollectionType.ASSET_NAMESPACE;
        } else {
            String parentPath = AssetPathUtils.getParent(path);
            return collectionTypeFor(parentPath, session);
        }
    }

    /**
     * 
     * @param path    the collection path
     * @param session
     * @return the collection type at the specified path; null if collection does
     *         not exist.
     * @throws Throwable
     */
    public static CollectionType collectionTypeOf(String path, MFSession session) throws Throwable {
        if ("/".equals(path)) {
            return CollectionType.ASSET_NAMESPACE;
        }
        if (AssetUtils.collectionAssetExists(path, session)) {
            return CollectionType.COLLECTION_ASSET;
        } else if (AssetNamespaceUtils.assetNamespaceExists(session, path)) {
            return CollectionType.ASSET_NAMESPACE;
        } else {
            return null;
        }
    }
}
