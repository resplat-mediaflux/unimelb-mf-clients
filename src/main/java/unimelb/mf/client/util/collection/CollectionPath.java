package unimelb.mf.client.util.collection;

import java.util.Objects;

import arc.xml.XmlDoc;
import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.util.AssetNamespaceUtils;
import unimelb.mf.client.util.AssetPathUtils;
import unimelb.mf.client.util.AssetUtils;

public class CollectionPath {

    private final String _path;
    private CollectionType _type;
    private Boolean _exists;
    private CollectionDetails _details;

    public CollectionPath(String path, CollectionType type, Boolean exists) {
        _path = Objects.requireNonNull(path);
        _type = type;
        _exists = exists;
        _details = null;
    }

    public CollectionPath(CollectionDetails details) {
        _path = _details.path();
        _type = _details.type();
        _exists = true;
        _details = Objects.requireNonNull(details);
    }

    public CollectionPath(String path, CollectionType type) {
        this(path, type, null);
    }

    public CollectionPath(String path) {
        this(path, null, null);
    }

    public final String path() {
        return _path;
    }

    @Override
    public String toString() {
        return _path;
    }

    public CollectionType type() {
        return _type;
    }

    public Boolean exists() {
        return _exists;
    }

    public CollectionDetails resolve(MFSession session) throws Throwable {
        return resolve(session, false);
    }

    public synchronized CollectionDetails resolve(MFSession session, boolean refresh) throws Throwable {
        if (_details == null || _exists == null || _type == null || refresh) {
            boolean collectionAssetExists = false;
            boolean assetExists = AssetUtils.assetExists(_path, session);
            if (assetExists) {
                XmlDoc.Element ae = AssetUtils.getAssetMetaByPath(session, _path);
                boolean isCollectionAsset = ae.elementExists("collection");
                if (isCollectionAsset) {
                    _exists = true;
                    String assetId = ae.value("@id");
                    String store = ae.value("collection/store");
                    _details = new CollectionDetails(_path, assetId, CollectionType.COLLECTION_ASSET, store);
                    _type = _details.type();
                    collectionAssetExists = true;
                }
            }
            if (!collectionAssetExists) {
                boolean assetNamespaceExists = AssetNamespaceUtils.assetNamespaceExists(session, _path);
                if (assetNamespaceExists) {
                    _exists = true;
                    XmlDoc.Element ne = AssetNamespaceUtils.describeAssetNamespace(session, _path, false);
                    String id = ne.value("@id");
                    String store = ne.value("store");
                    _details = new CollectionDetails(_path, id, CollectionType.ASSET_NAMESPACE, store);
                    _type = _details.type();
                } else {
                    _exists = false;
                    _details = null;
                }
            }
        }
        return _details;
    }

    public boolean exists(MFSession session) throws Throwable {
        return exists(session, false);
    }

    public synchronized boolean exists(MFSession session, boolean refresh) throws Throwable {
        if (_exists == null || refresh) {
            if (_type != null) {
                if (_type == CollectionType.COLLECTION_ASSET) {
                    _exists = AssetUtils.collectionAssetExists(_path, session);
                } else {
                    _exists = AssetNamespaceUtils.assetNamespaceExists(session, _path);
                }
            } else {
                _exists = AssetUtils.collectionAssetExists(_path, session)
                        || AssetNamespaceUtils.assetNamespaceExists(session, _path);
            }
        }
        return _exists;
    }

    public CollectionType type(MFSession session) throws Throwable {
        return type(session, false);
    }

    public synchronized CollectionType type(MFSession session, boolean refresh) throws Throwable {
        if (_type == null || refresh) {
            resolve(session, true);
        }
        return _type;
    }

    public CollectionPath parent() {
        return new CollectionPath(AssetPathUtils.getParent(_path));
    }

    public synchronized CollectionDetails createIfNotExist(MFSession session, boolean createParents) throws Throwable {
    	if(!exists(session)) {
            CollectionUtils.createCollection(_path, createParents, true, session);        	
    	}
        return resolve(session, true);
    }
}
