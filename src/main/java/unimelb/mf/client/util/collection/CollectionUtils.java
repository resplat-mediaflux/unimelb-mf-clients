package unimelb.mf.client.util.collection;

import java.io.FileNotFoundException;

import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.util.AssetNamespaceUtils;
import unimelb.mf.client.util.AssetPathUtils;
import unimelb.mf.client.util.AssetUtils;

public class CollectionUtils {

    public static boolean collectionExists(String path, MFSession session) throws Throwable {
        return CollectionType.collectionTypeOf(path, session) != null;
    }

    public static String createCollection(String path, boolean createParents, boolean ignoreIfExists, MFSession session)
            throws Throwable {
        if (!createParents) {
            String directParent = AssetPathUtils.getParent(path);
            if (!collectionExists(directParent, session)) {
                throw new FileNotFoundException("parent collection: '" + directParent + "' not found.");
            }
        }
        CollectionType type = CollectionType.collectionTypeFor(path, session);
        if (type == CollectionType.COLLECTION_ASSET) {
            return AssetUtils.createCollectionAsset(session, path, createParents, ignoreIfExists);
        } else if (type == CollectionType.ASSET_NAMESPACE) {
            AssetNamespaceUtils.createAssetNamespace(session, path, createParents, ignoreIfExists);
            return AssetNamespaceUtils.getAssetNamespaceId(session, path);
        } else {
            throw new Exception("Unsupported conllection type: " + type);
        }
    }

}
