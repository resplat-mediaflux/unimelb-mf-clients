package unimelb.mf.client.util.collection;

import java.util.Objects;

import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.util.AssetNamespaceUtils;
import unimelb.mf.client.util.AssetUtils;

public class CollectionDetails {

    private final String _path;
    private final String _id;
    private final CollectionType _type;
    private final String _store;

    public CollectionDetails(String path, String id, CollectionType type, String store) {
        _path = Objects.requireNonNull(path);
        _id = id;
        _type = Objects.requireNonNull(type);
        _store = store;
    }

    public final String path() {
        return _path;
    }

    public final CollectionType type() {
        return _type;
    }

    public final String id() {
        return _id;
    }

    public final String store() {
        return _store;
    }

    public final boolean isCollectionAsset() {
        return _type == CollectionType.COLLECTION_ASSET;
    }

    public final boolean isAssetNamespace() {
        return _type == CollectionType.ASSET_NAMESPACE;
    }

    public long countFileAssets(MFSession session) throws Throwable {
        if (this.isAssetNamespace()) {
            return AssetNamespaceUtils.countFileAssets(session, _path);
        } else {
            return AssetUtils.countFileAssetsInCollection(_path, session);
        }
    }

}
