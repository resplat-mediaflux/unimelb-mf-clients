package unimelb.mf.client.util;

import arc.xml.XmlDoc;
import arc.xml.XmlStringWriter;
import unimelb.mf.client.session.MFSession;
import unimelb.utils.PathUtils;

public class AssetNamespaceUtils {

    public static void createAssetNamespace(MFSession session, String path, boolean createParents) throws Throwable {
        createAssetNamespace(session, path, createParents, true);
    }

    public static void createAssetNamespace(MFSession session, String path, boolean createParents, boolean ignoreIfExists)
            throws Throwable {
        if (ignoreIfExists) {
            if (assetNamespaceExists(session, path)) {
                return;
            }
        }
        XmlStringWriter w = new XmlStringWriter();
        w.add("namespace", new String[] { "all", Boolean.toString(createParents) }, path);
        session.execute("asset.namespace.create", w.document());
    }

    public static boolean assetNamespaceExists(MFSession session, String ns) throws Throwable {
        XmlStringWriter w = new XmlStringWriter();
        w.add("namespace", ns);
        return session.execute("asset.namespace.exists", w.document()).booleanValue("exists");
    }

    public static String getAssetNamespaceStore(MFSession session, String assetNamespacePath) throws Throwable {
        if (assetNamespacePath == null || assetNamespacePath.isEmpty()) {
            assetNamespacePath = "/";
        }
        XmlStringWriter w = new XmlStringWriter();
        w.add("namespace", assetNamespacePath);
        return session.execute("asset.namespace.store.name", w.document()).value("store");
    }

    public static String getParentAssetNamespaceStore(MFSession session, String assetNamespacePath) throws Throwable {
        String parentAssetNamespacePath = PathUtils.getParentPath(assetNamespacePath);
        return getAssetNamespaceStore(session, parentAssetNamespacePath);
    }

    public static String getParentAssetNamespace(String assetNamespace) {
        if (assetNamespace == null || assetNamespace.isEmpty() || "/".equals(assetNamespace)) {
            return null;
        }
        return PathUtils.getParentPath(assetNamespace);
    }

    public static long countAllAssets(MFSession session, String ns) throws Throwable {
        XmlStringWriter w = new XmlStringWriter();
        w.add("namespace", ns);
        return session.execute("asset.count", w.document()).longValue("total");
    }

    public static long countFileAssets(MFSession session, String assetNamespacePath) throws Throwable {
        XmlStringWriter w = new XmlStringWriter();
        w.add("namespace", assetNamespacePath);
        w.add("where", "(asset has content or asset has link) and not(asset is collection)");
        return session.execute("asset.count", w.document()).longValue("total");
    }

    public static boolean isEmpty(MFSession session, String ns) throws Throwable {
        XmlStringWriter w = new XmlStringWriter();
        w.add("namespace", ns);
        w.add("leaf-only", true);
        XmlDoc.Element re = session.execute("asset.namespace.empty.list", w.document());
        return re.longValue("total", 0) == 1 && re.count("namespace") == 1 && ns.equals(re.value("namespace"));
    }

    public static void destroyAssetNamespace(MFSession session, String ns) throws Throwable {
        XmlStringWriter w = new XmlStringWriter();
        w.add("namespace", ns);
        session.execute("asset.namespace.destroy", w.document());
    }

    public static XmlDoc.Element describeAssetNamespace(MFSession session, String nsPath, boolean listContents)
            throws Throwable {
        XmlStringWriter w = new XmlStringWriter();
        w.add("namespace", nsPath);
        w.add("list-contents", false);
        return session.execute("asset.namespace.describe", w.document()).element("namespace");
    }
    
    public static String getAssetNamespaceId(MFSession session, String assetNamespacePath) throws Throwable {
        XmlStringWriter w = new XmlStringWriter();
        w.add("namespace", assetNamespacePath);
        return session.execute("asset.namespace.identifier.get", w.document()).value("id");
    }
}
