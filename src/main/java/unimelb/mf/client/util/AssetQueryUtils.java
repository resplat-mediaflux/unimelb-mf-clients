package unimelb.mf.client.util;

import arc.xml.XmlStringWriter;
import unimelb.mf.client.session.MFSession;

public class AssetQueryUtils {
    
    public static void destroyIterator(MFSession session, String iteratorId, boolean ignoreMissing) throws Throwable {
        XmlStringWriter w = new XmlStringWriter();
        w.add("id", iteratorId);
        w.add("ignore-missing", ignoreMissing);
        session.execute("asset.query.iterator.destroy", w.document());
    }

}
