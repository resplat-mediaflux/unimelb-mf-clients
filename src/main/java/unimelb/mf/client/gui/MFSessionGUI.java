package unimelb.mf.client.gui;

import arc.mf.client.RemoteServer;
import unimelb.mf.client.session.MFConfig;
import unimelb.mf.client.session.MFSession;

public class MFSessionGUI extends MFSession {

    private ErrorDialog _ed;
    private LogonDialog _ld;

    protected MFSessionGUI(MFConfig config, RemoteServer rs, String sessionKey, LogonDialog ld, ErrorDialog ed) {
        super(config, rs, sessionKey);
        _ld = ld;
        _ed = ed;
    }

    public void setErrorDialog(ErrorDialog ed) {
        _ed = ed;
    }

    public void setLogonDialog(LogonDialog ld) {
        _ld = ld;
    }

    public void displayErrorDialog(String context, Throwable error) {
        if (_ed != null) {
            _ed.displayError(context, error);
        }
    }

    public void displyLogonDialog(LogonResponseHandler rh) {
        if (_ld != null) {
            _ld.show(rh);
        }
    }

    // TODO not completed...
}
