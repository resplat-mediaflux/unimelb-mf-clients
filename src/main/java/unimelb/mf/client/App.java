package unimelb.mf.client;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public final class App {

    private App() {
    }

    private static Properties _properties = new Properties();

    static void loadProperties() {
        if (_properties.isEmpty()) {
            try (InputStream in = App.class.getResourceAsStream("app.properties")) {
                _properties.load(in);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static String version() {
        loadProperties();
        return _properties.getProperty("version");
    }

    public static String buildTime() {
        loadProperties();
        return _properties.getProperty("buildTime");
    }

}
