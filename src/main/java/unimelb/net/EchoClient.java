package unimelb.net;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

public class EchoClient {

    public static final String APP_NAME = "unimelb-net-echo";

    public static void main(String[] args) throws Throwable {
        if (args == null || args.length != 3) {
            System.err.println("Error: Invalid arguments.");
            printUsage();
            System.exit(1);
        }
        try {
            String host = args[0];
            int port = Integer.parseInt(args[1]);
            String message = args[2];
            send(host, port, message);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private static void printUsage() {
        System.out.println();
        System.out.println("Usage: ");
        System.out.println(String.format("    %s <host> <port> <message>", APP_NAME));
        System.out.println();
        System.out.println("Examples:");
        System.out.println(String.format("    %s localhost 9761 status", APP_NAME));
        System.out.println();
    }

    public static void send(String host, int port, String message) throws Throwable {
        try (Socket socket = new Socket(host, port)) {
            send(socket.getOutputStream(), message);
            recv(socket.getInputStream());
        }
    }

    private static void send(OutputStream out, String message) {
        PrintWriter writer = new PrintWriter(out);
        writer.println(message);
        writer.flush();
    }

    private static void recv(InputStream in) throws Throwable {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        String line = null;
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
        }
    }

}
