package unimelb.utils;

import java.text.DecimalFormat;

public class FileSizeUtils {

    public static final long KB = 1000L;
    public static final long MB = KB * 1000L;
    public static final long GB = MB * 1000L;
    public static final long TB = GB * 1000L;
    public static final long PB = TB * 1000L;

    public static final long KiB = 1024L;
    public static final long MiB = KiB * 1024L;
    public static final long GiB = MiB * 1024L;
    public static final long TiB = GiB * 1024L;
    public static final long PiB = TiB * 1024L;

    public static final DecimalFormat FORMAT = new DecimalFormat("###.###");

    public static String toHumanReadable(long nBytes, boolean bi) {
        if (bi) {
            if (nBytes >= PiB) {
                return String.format("%s PiB", FORMAT.format(((double) nBytes) / ((double) PiB)));
            } else if (nBytes >= TiB) {
                return String.format("%s TiB", FORMAT.format(((double) nBytes) / ((double) TiB)));
            } else if (nBytes >= GiB) {
                return String.format("%s GiB", FORMAT.format(((double) nBytes) / ((double) GiB)));
            } else if (nBytes >= MiB) {
                return String.format("%s MiB", FORMAT.format(((double) nBytes) / ((double) MiB)));
            } else if (nBytes >= KiB) {
                return String.format("%s KiB", FORMAT.format(((double) nBytes) / ((double) KiB)));
            } else {
                return String.format("%d Bytes", nBytes);
            }
        } else {
            if (nBytes >= PB) {
                return String.format("%s PB", FORMAT.format(((double) nBytes) / ((double) PB)));
            } else if (nBytes >= TB) {
                return String.format("%s TB", FORMAT.format(((double) nBytes) / ((double) TB)));
            } else if (nBytes >= GB) {
                return String.format("%s GB", FORMAT.format(((double) nBytes) / ((double) GB)));
            } else if (nBytes >= MB) {
                return String.format("%s MB", FORMAT.format(((double) nBytes) / ((double) MB)));
            } else if (nBytes >= KB) {
                return String.format("%s KB", FORMAT.format(((double) nBytes) / ((double) KB)));
            } else {
                return String.format("%d Bytes", nBytes);
            }
        }
    }

    public static String toHumanReadable(long nBytes) {
        return toHumanReadable(nBytes, false);
    }

    public static void main(String[] args) {
//        System.out.println(toHumanReadable(12000000000L, false));
    }

}
