package unimelb.utils;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class FileListSummary implements FileListProgress {

    private final FileListOptions _options;

    private final long _matchedFileCount;
    private final Map<Long, Long> _matchedFileCountThresholded;
    private final long _matchedFileSizeTotal;
    private final long _matchedFileSizeMin;
    private final long _matchedFileSizeMax;

    private final long _fileCount;
    private final long _failedFileCount;
    private final long _regularFileCount;
    private final long _symlinkCount;
    private final long _nonRegularFileCount;
    private final Map<Long, Long> _fileCountThresholded;

    private final long _fileSizeTotal;
    private final long _fileSizeMin;
    private final long _fileSizeMax;

    private final long _directoryCount;
    private final long _failedDirectoryCount;

    public FileListSummary(FileListOptions options, long matchedFileCount, Map<Long, Long> matchedFileCountThresholded,
            long matchedFileSizeTotal, long matchedFileSizeMin, long matchedFileSizeMax, long fileCount,
            Map<Long, Long> fileCountThresholded, long failedFileCount, long regularFileCount, long symlinkCount,
            long nonRegularFileCount, long fileSizeTotal, long fileSizeMin, long fileSizeMax, long dirCount,
            long failedDirCount) {
        _options = options;
        _matchedFileCount = matchedFileCount;
        _matchedFileCountThresholded = (matchedFileCountThresholded == null || matchedFileCountThresholded.isEmpty())
                ? null
                : new TreeMap<Long, Long>(matchedFileCountThresholded);
        _matchedFileSizeTotal = matchedFileSizeTotal;
        _matchedFileSizeMin = matchedFileSizeMin;
        _matchedFileSizeMax = matchedFileSizeMax;
        _fileCount = fileCount;
        _failedFileCount = failedFileCount;
        _regularFileCount = regularFileCount;
        _symlinkCount = symlinkCount;
        _nonRegularFileCount = nonRegularFileCount;
        _fileCountThresholded = (fileCountThresholded == null || fileCountThresholded.isEmpty()) ? null
                : new TreeMap<Long, Long>(fileCountThresholded);
        _fileSizeTotal = fileSizeTotal;
        _fileSizeMin = fileSizeMin;
        _fileSizeMax = fileSizeMax;
        _directoryCount = dirCount;
        _failedDirectoryCount = failedDirCount;
    }

    @Override
    public final FileListOptions options() {
        return _options;
    }

    @Override
    public final long matchedFileCount() {
        return _matchedFileCount;
    }

    @Override
    public final long matchedFileSizeTotal() {
        return _matchedFileSizeTotal;
    }

    @Override
    public final long matchedFileSizeMin() {
        return _matchedFileSizeMin;
    }

    @Override
    public final long matchedFileSizeMax() {
        return _matchedFileSizeMax;
    }

    @Override
    public final long fileCount() {
        return _fileCount;
    }

    @Override
    public final long failedFileCount() {
        return _failedFileCount;
    }

    @Override
    public final long regularFileCount() {
        return _regularFileCount;
    }

    @Override
    public long symlinkCount() {
        return _symlinkCount;
    }

    @Override
    public long nonRegularFileCount() {
        return _nonRegularFileCount;
    }

    @Override
    public final long fileSizeTotal() {
        return _fileSizeTotal;
    }

    @Override
    public final long fileSizeMin() {
        return _fileSizeMin;
    }

    @Override
    public final long fileSizeMax() {
        return _fileSizeMax;
    }

    @Override
    public final long directoryCount() {
        return _directoryCount;
    }

    @Override
    public final long failedDirectoryCount() {
        return _failedDirectoryCount;
    }

    @Override
    public Map<Long, Long> matchedFileCountThresholded() {
        if (_matchedFileCountThresholded != null && !_matchedFileCountThresholded.isEmpty()) {
            return Collections.unmodifiableMap(_matchedFileCountThresholded);
        }
        return null;
    }

    @Override
    public Map<Long, Long> fileCountThresholded() {
        if (_fileCountThresholded != null && !_fileCountThresholded.isEmpty()) {
            return Collections.unmodifiableMap(_fileCountThresholded);
        }
        return null;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        // Options
        sb.append("\n");
        sb.append("Options:\n");
        sb.append(_options);
        // Results
        sb.append("\n");
        sb.append("Results:\n");
        if (_options.hasConstraints()) {
            sb.append(String.format("%50s: %,26d", "matched.file.count", _matchedFileCount)).append("\n");
            sb.append(String.format("%50s: %,26d", "matched.file.size.total", _matchedFileSizeTotal)).append("\n");
            sb.append(String.format("%50s: %,26d", "matched.file.size.min", _matchedFileSizeMin)).append("\n");
            sb.append(String.format("%50s: %,26d", "matched.file.size.max", _matchedFileSizeMax)).append("\n");
            if (_matchedFileCountThresholded != null) {
                saveThresholded(sb, "matched.file.count", _matchedFileCountThresholded, _matchedFileCount,
                        _matchedFileSizeMax);
            }
        }
        sb.append(String.format("%50s: %,26d", "file.count", _fileCount)).append("\n");
        sb.append(String.format("%50s: %,26d", "symlink.count", _symlinkCount)).append("\n");
        sb.append(String.format("%50s: %,26d", "non-regular.file.count", _nonRegularFileCount)).append("\n");
        if (_fileCountThresholded != null) {
            saveThresholded(sb, "file.count", _fileCountThresholded, _regularFileCount, _fileSizeMax);
        }
        sb.append(String.format("%50s: %,26d", "file.failed", _failedFileCount)).append("\n");
        sb.append(String.format("%50s: %,26d", "file.size.total", _fileSizeTotal)).append("\n");
        sb.append(String.format("%50s: %,26d", "file.size.min", _fileSizeMin)).append("\n");
        sb.append(String.format("%50s: %,26d", "file.size.max", _fileSizeMax)).append("\n");
        sb.append(String.format("%50s: %,26d", "directory.count", _directoryCount)).append("\n");
        sb.append(String.format("%50s: %,26d", "directory.failed", _failedDirectoryCount)).append("\n");
        return sb.toString();
    }

    private static void saveThresholded(StringBuilder sb, String titlePrefix, Map<Long, Long> thresholded, long total,
            long maxSize) {
        if (thresholded != null && !thresholded.isEmpty()) {
            List<Long> ts = new ArrayList<Long>(thresholded.keySet());
            for (int i = 0; i < ts.size(); i++) {
                long t = ts.get(i);
                long c = thresholded.get(t);
                if (i == 0) {
                    if (c > 0) {
                        if (t == 0) {
                            String title = String.format("%s(size = 0 B)", titlePrefix);
                            sb.append(String.format("%50s: %,26d", title, c)).append("\n");
                        } else {
                            String title = String.format("%s(0 < size <= %s)", titlePrefix,
                                    FileSizeUtils.toHumanReadable(t));
                            sb.append(String.format("%50s: %,26d", title, c)).append("\n");
                        }
                    }
                } else {
                    long pt = ts.get(i - 1);
                    long pc = thresholded.get(pt);
                    long n = c - pc;
                    if (n > 0) {
                        String title = String.format("%s(%s < size <= %s)", titlePrefix,
                                FileSizeUtils.toHumanReadable(pt + 1), FileSizeUtils.toHumanReadable(t));
                        sb.append(String.format("%50s: %,26d", title, n)).append("\n");
                    }
                }
                if (i == ts.size() - 1) {
                    long n = total - c;
                    if (n > 0) {
                        String title = String.format("%s(%s < size <= %s)", titlePrefix,
                                FileSizeUtils.toHumanReadable(t), FileSizeUtils.toHumanReadable(maxSize));
                        sb.append(String.format("%50s: %,26d", title, total - c)).append("\n");
                    }
                }
            }
        }
    }

    public void print(PrintStream o) {
        o.println(toString());
    }

}
