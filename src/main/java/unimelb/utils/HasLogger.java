package unimelb.utils;

import java.util.logging.Logger;

public interface HasLogger {

    Logger logger();
}
