package unimelb.utils;

public interface HasProgress {

    long totalOperations();

    long completedOperations();

    String currentOperation();

}
