package unimelb.utils;

import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class FileList implements Callable<FileListSummary>, FileListProgress {

    public static interface FileHandler {
        void handleFile(FileListProgress progress, Path file, long fileSize);
    }

    private static final Logger logger = Logger.getLogger(FileList.class.getName());
    static {
        ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new SimpleFormatter());
        handler.setLevel(Level.ALL);
        logger.addHandler(handler);
        logger.setLevel(Level.ALL);
    }

    private FileListOptions _options;

    private AtomicLong _matchedFileCount;
    private Map<Long, AtomicLong> _matchedFileCountThresholded;
    private AtomicLong _matchedFileSizeTotal;
    private AtomicLong _matchedFileSizeMin;
    private AtomicLong _matchedFileSizeMax;
    private AtomicLong _fileCount;
    private Map<Long, AtomicLong> _fileCountThresholded;
    private AtomicLong _failedFileCount;
    private AtomicLong _symlinkCount;
    private AtomicLong _regularFileCount;
    private AtomicLong _nonRegularFileCount;
    private AtomicLong _fileSizeTotal;
    private AtomicLong _fileSizeMin;
    private AtomicLong _fileSizeMax;
    private AtomicLong _dirCount;
    private AtomicLong _failedDirCount;

    private FileHandler _fh;

    public FileList(FileListOptions options, FileHandler fh) throws Throwable {
        _options = options;
        _fh = fh == null ? new FileHandler() {

            @Override
            public void handleFile(FileListProgress progress, Path file, long size) {
                System.out.println(String.format("%-19d '%s'", size, file));
            }
        } : fh;

        _matchedFileCount = new AtomicLong(0L);
        _matchedFileSizeTotal = new AtomicLong(0L);
        _matchedFileSizeMin = new AtomicLong(Long.MAX_VALUE);
        _matchedFileSizeMax = new AtomicLong(Long.MIN_VALUE);

        _fileCount = new AtomicLong(0L);
        _failedFileCount = new AtomicLong(0L);
        _regularFileCount = new AtomicLong(0L);
        _symlinkCount = new AtomicLong(0L);
        _nonRegularFileCount = new AtomicLong(0L);
        _fileSizeTotal = new AtomicLong(0L);
        _fileSizeMin = new AtomicLong(Long.MAX_VALUE);
        _fileSizeMax = new AtomicLong(Long.MIN_VALUE);

        _dirCount = new AtomicLong(0L);
        _failedDirCount = new AtomicLong(0L);

        if (_options.hasThresholds()) {
            _fileCountThresholded = new TreeMap<Long, AtomicLong>();
            if (_options.hasConstraints()) {
                _matchedFileCountThresholded = new TreeMap<Long, AtomicLong>();
            }
            List<Long> thresholds = _options.thresholds();
            for (long threshold : thresholds) {
                _fileCountThresholded.put(threshold, new AtomicLong(0L));
                if (_matchedFileCountThresholded != null) {
                    if (!((_options.sizeFrom() != null && threshold < _options.sizeFrom())
                            || (_options.sizeTo() != null && threshold > _options.sizeTo()))) {
                        _matchedFileCountThresholded.put(threshold, new AtomicLong(0L));
                    }
                }
            }
        }
    }

    @Override
    public FileListOptions options() {
        return _options;
    }

    @Override
    public long matchedFileCount() {
        return _matchedFileCount.get();
    }

    @Override
    public Map<Long, Long> matchedFileCountThresholded() {
        if (_matchedFileCountThresholded == null || _matchedFileCountThresholded.isEmpty()) {
            return null;
        }
        final Map<Long, Long> matchedFileCountThresholded = new TreeMap<Long, Long>();
        _matchedFileCountThresholded.forEach((t, c) -> {
            matchedFileCountThresholded.put(t, c.get());
        });
        return matchedFileCountThresholded;
    }

    @Override
    public long matchedFileSizeTotal() {
        return _matchedFileSizeTotal.get();
    }

    @Override
    public long matchedFileSizeMin() {
        return _matchedFileSizeMin.get();
    }

    @Override
    public long matchedFileSizeMax() {
        return _matchedFileSizeMax.get();
    }

    @Override
    public long regularFileCount() {
        return _regularFileCount.get();
    }

    @Override
    public long symlinkCount() {
        return _symlinkCount.get();
    }

    @Override
    public long nonRegularFileCount() {
        return _nonRegularFileCount.get();
    }

    @Override
    public long fileCount() {
        return _fileCount.get();
    }

    @Override
    public Map<Long, Long> fileCountThresholded() {
        if (_fileCountThresholded == null || _fileCountThresholded.isEmpty()) {
            return null;
        }
        final Map<Long, Long> fileCountThresholded = new TreeMap<Long, Long>();
        _fileCountThresholded.forEach((t, c) -> {
            fileCountThresholded.put(t, c.get());
        });
        return fileCountThresholded;
    }

    @Override
    public long fileSizeTotal() {
        return _fileSizeTotal.get();
    }

    @Override
    public long failedFileCount() {
        return _failedFileCount.get();
    }

    @Override
    public long fileSizeMin() {
        return _fileSizeMin.get();
    }

    @Override
    public long fileSizeMax() {
        return _fileSizeMax.get();
    }

    @Override
    public long directoryCount() {
        return _dirCount.get();
    }

    @Override
    public long failedDirectoryCount() {
        return _failedDirCount.get();
    }

    @Override
    public FileListSummary call() throws Exception {
        Files.walkFileTree(_options.rootDirectory(), _options.followLinks() ? EnumSet.of(FileVisitOption.FOLLOW_LINKS)
                : EnumSet.noneOf(FileVisitOption.class), Integer.MAX_VALUE, new SimpleFileVisitor<Path>() {
                    @Override
                    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                        _fileCount.getAndIncrement();
                        try {
                            if (Files.isSymbolicLink(file)) {
                                // ignore symbolic links
                                _symlinkCount.getAndIncrement();
                            } else {
                                long fileSize = Files.size(file);
                                _fileSizeTotal.getAndAdd(fileSize);
                                if (fileSize < _fileSizeMin.get()) {
                                    _fileSizeMin.set(fileSize);
                                }
                                if (fileSize > _fileSizeMax.get()) {
                                    _fileSizeMax.set(fileSize);
                                }
                                if (!Files.isRegularFile(file)) {
                                    // ignore non-regular files
                                    _nonRegularFileCount.getAndIncrement();
                                } else {
                                    _regularFileCount.getAndIncrement();
                                    updateFileCountThresholded(fileSize);
                                    boolean accept = true;
                                    if (_options.sizeFrom() != null || _options.sizeTo() != null) {
                                        if (_options.sizeFrom() != null && fileSize < _options.sizeFrom()) {
                                            accept = false;
                                        }
                                        if (_options.sizeTo() != null && fileSize > _options.sizeTo()) {
                                            accept = false;
                                        }
                                    }
                                    if (_options.hasIncRegex()) {
                                        accept = false;
                                        String path = file.toString();
                                        List<String> incRegex = _options.incRegex();
                                        for (String regex : incRegex) {
                                            if (path.matches(regex)) {
                                                accept = true;
                                                break;
                                            }
                                        }
                                    }
                                    if (_options.hasExcRegex()) {
                                        String path = file.toString();
                                        List<String> excRegex = _options.excRegex();
                                        for (String regex : excRegex) {
                                            if (path.matches(regex)) {
                                                accept = false;
                                                break;
                                            }
                                        }
                                    }
                                    if (accept) {
                                        _matchedFileCount.getAndIncrement();
                                        _matchedFileSizeTotal.getAndAdd(fileSize);
                                        updateMatchedFileCountThresholded(fileSize);
                                        if (fileSize < _matchedFileSizeMin.get()) {
                                            _matchedFileSizeMin.set(fileSize);
                                        }
                                        if (fileSize > _matchedFileSizeMax.get()) {
                                            _matchedFileSizeMax.set(fileSize);
                                        }
                                        _fh.handleFile(FileList.this, file, fileSize);
                                    }
                                }
                            }
                        } catch (Throwable e) {
                            _failedFileCount.getAndIncrement();
                            System.err.println("Failed to read file: '" + file + "'");
                            e.printStackTrace(System.err);
                        }
                        return FileVisitResult.CONTINUE;
                    }

                    @Override
                    public FileVisitResult visitFileFailed(Path file, IOException ioe) {
                        if (Files.isDirectory(file)) {
                            _failedDirCount.getAndIncrement();
                        } else {
                            _failedFileCount.getAndIncrement();
                        }
                        return FileVisitResult.CONTINUE;
                    }

                    @Override
                    public FileVisitResult postVisitDirectory(Path dir, IOException ioe) {
                        return FileVisitResult.CONTINUE;
                    }

                    @Override
                    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                        _dirCount.getAndIncrement();
                        return super.preVisitDirectory(dir, attrs);
                    }
                });
        return new FileListSummary(_options, matchedFileCount(), matchedFileCountThresholded(), matchedFileSizeTotal(),
                matchedFileSizeMin(), matchedFileSizeMax(), fileCount(), fileCountThresholded(), failedFileCount(),
                regularFileCount(), symlinkCount(), nonRegularFileCount(), fileSizeTotal(), fileSizeMin(),
                fileSizeMax(), directoryCount(), failedDirectoryCount());
    }

    private void updateFileCountThresholded(long fileSize) {
        if (_fileCountThresholded != null) {
            _fileCountThresholded.forEach((t, s) -> {
                if (fileSize <= t) {
                    s.getAndIncrement();
                }
            });
        }
    }

    private void updateMatchedFileCountThresholded(long fileSize) {
        if (_matchedFileCountThresholded != null) {
            _matchedFileCountThresholded.forEach((t, s) -> {
                if (fileSize <= t) {
                    s.getAndIncrement();
                }
            });
        }
    }

    private static long parseSize(String s) {
        long size;
        s = s.toLowerCase();
        if (s.endsWith("b")) {
            size = Long.parseLong(s.substring(0, s.length() - 1));
        } else if (s.endsWith("k")) {
            size = Long.parseLong(s.substring(0, s.length() - 1)) * 1000L;
        } else if (s.endsWith("m")) {
            size = Long.parseLong(s.substring(0, s.length() - 1)) * 1000000L;
        } else if (s.endsWith("g")) {
            size = Long.parseLong(s.substring(0, s.length() - 1)) * 1000000000L;
        } else if (s.endsWith("t")) {
            size = Long.parseLong(s.substring(0, s.length() - 1)) * 1000000000000L;
        } else {
            size = Long.parseLong(s);
        }
        if (size < 0) {
            throw new IllegalArgumentException("Invalid file size: " + s);
        }
        return size;
    }

    public static FileListSummary execute(Path rootDir, boolean followLinks, Long sizeFrom, Long sizeTo,
            Collection<String> incRegex, Collection<String> excRegex, FileHandler fh) throws Throwable {
        FileListOptions options = new FileListOptions.Builder().setRootDirectory(rootDir).setFollowLinks(followLinks)
                .setSizeFrom(sizeFrom).setSizeTo(sizeTo).setIncRegex(incRegex).setExcRegex(excRegex).build();
        return new FileList(options, fh).call();
    }

    public static FileListSummary execute(FileListOptions options, FileHandler fh) throws Throwable {
        return new FileList(options, fh).call();
    }

    public static int executeCommand(String[] args) throws Throwable {
        FileListOptions.Builder options = new FileListOptions.Builder();
        try {
            for (int i = 0; i < args.length;) {
                if ("--help".equalsIgnoreCase(args[i]) || "-h".equalsIgnoreCase(args[i])) {
                    printCommandUsage();
                    return 0;
                }
                if ("--follow-links".equalsIgnoreCase(args[i]) || "-l".equalsIgnoreCase(args[i])) {
                    if (options.followLinks() != null) {
                        throw new IllegalArgumentException("Multiple --follow-links found!");
                    }
                    options.setFollowLinks(true);
                    i += 1;
                } else if ("--size-from".equalsIgnoreCase(args[i])) {
                    if (options.sizeFrom() != null) {
                        throw new IllegalArgumentException("Multiple --size-from found!");
                    }
                    options.setSizeFrom(parseSize(args[i + 1]));
                    i += 2;
                } else if ("--size-to".equalsIgnoreCase(args[i])) {
                    if (options.sizeTo() != null) {
                        throw new IllegalArgumentException("Multiple --size-to found!");
                    }
                    options.setSizeTo(parseSize(args[i + 1]));
                    i += 2;
                } else if ("--include".equalsIgnoreCase(args[i])) {
                    options.addIncRegex(args[i + 1]);
                    i += 2;
                } else if ("--exclude".equalsIgnoreCase(args[i])) {
                    options.addExcRegex(args[i + 1]);
                    i += 2;
                } else if ("--thresholds".equalsIgnoreCase(args[i])) {
                    options.setThresholds(parseThresholds(args[i + 1]));
                    i += 2;
                } else {
                    if (args[i].startsWith("-")) {
                        throw new IllegalArgumentException("Unknown option: " + args[i]);
                    }
                    if (options.rootDirectory() != null) {
                        throw new IllegalArgumentException("Multiple root directory specified. Expects only one.");
                    }
                    options.setRootDirectory(Paths.get(args[i]).normalize().toAbsolutePath());
                    i++;
                }
            }
            if (options.rootDirectory() == null) {
                options.setRootDirectory(Paths.get(System.getProperty("user.dir")));
            }
            if (options.followLinks() == null) {
                options.setFollowLinks(false);
            }
        } catch (Throwable e) {
            throw new IllegalArgumentException(e);
        }
        execute(options.build(), new FileHandler() {

            @Override
            public void handleFile(FileListProgress progress, Path file, long size) {
                System.out.println(String.format("%-19d '%s'", size, file));
            }
        }).print(System.out);
        return 0;
    }

    private static long[] parseThresholds(String ts) {
        if (ts != null) {
            ts = ts.replace(" ", "").replaceAll(",$", "").replaceAll("^,", "");
            if (!ts.isEmpty()) {
                String[] ss = ts.split(",");
                long[] ls = new long[ss.length];
                for (int i = 0; i < ss.length; i++) {
                    ls[i] = parseSize(ss[i]);
                }
                return ls;
            }
        }
        throw new IllegalArgumentException("Failed to parse thresholds: " + ts);
    }

    public static void printCommandUsage() {
        // @formatter:off
        System.out.println();
        System.out.println("Usage: file-list [options] [directory]");
        System.out.println();
        System.out.println("Description: list files.");
        System.out.println();
        System.out.println("Options:");
        System.out.println("    -l, --follow-links                 follow symbolic links.");
        System.out.println("    --size-from size[k|b|m|g]          file size filter.");
        System.out.println("    --size-to   size[k|b|m|g]          file size filter.");
        System.out.println("    --include regex                    file path filter.");
        System.out.println("    --exclude regex                    file path filter.");
        System.out.println("    --thresholds  size[k|b|m|g]        count files by the specified thresholds. If multiple separate with commas. For example 1k,1m,10m,1g,5g");
        System.out.println("    -h, --help                         print command usage information.");
        System.out.println();
        // @formatter:on
    }

    public static void main(String[] args) {
        try {
            executeCommand(args);
        } catch (Throwable e) {
            e.printStackTrace();
            if (e instanceof IllegalArgumentException) {
                printCommandUsage();
            }
            System.exit(1);
        }
    }

}
