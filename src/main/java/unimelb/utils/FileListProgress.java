package unimelb.utils;

import java.util.Map;

public interface FileListProgress {

    FileListOptions options();

    long matchedFileCount();

    Map<Long, Long> matchedFileCountThresholded();

    long matchedFileSizeTotal();

    long matchedFileSizeMin();

    long matchedFileSizeMax();

    long fileCount();

    long regularFileCount();

    long symlinkCount();

    long nonRegularFileCount();

    long failedFileCount();

    Map<Long, Long> fileCountThresholded();

    long fileSizeTotal();

    long fileSizeMin();

    long fileSizeMax();

    long directoryCount();

    long failedDirectoryCount();

}
