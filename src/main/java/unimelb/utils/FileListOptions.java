package unimelb.utils;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class FileListOptions {

    private final Path _rootDirectory;
    private final boolean _followLinks;
    private final Long _sizeFrom;
    private final Long _sizeTo;
    private final List<String> _incRegex;
    private final List<String> _excRegex;
    private final List<Long> _thresholds;

    public FileListOptions(Path rootDir, boolean followLinks, Long sizeFrom, Long sizeTo, Collection<String> incRegex,
            Collection<String> excRegex, List<Long> thresholds) {
        _rootDirectory = rootDir;
        _followLinks = followLinks;
        _sizeFrom = sizeFrom;
        _sizeTo = sizeTo;
        _incRegex = incRegex == null ? null : new ArrayList<String>(incRegex);
        _excRegex = excRegex == null ? null : new ArrayList<String>(excRegex);
        _thresholds = thresholds == null ? null : new ArrayList<Long>(thresholds);
    }

    public final Path rootDirectory() {
        return _rootDirectory;
    }

    public final boolean followLinks() {
        return _followLinks;
    }

    public final Long sizeFrom() {
        return _sizeFrom;
    }

    public final Long sizeTo() {
        return _sizeTo;
    }

    public final List<String> incRegex() {
        return _incRegex == null ? null : Collections.unmodifiableList(_incRegex);
    }

    public final boolean hasIncRegex() {
        return _incRegex != null && !_incRegex.isEmpty();
    }

    public final List<String> excRegex() {
        return _excRegex == null ? null : Collections.unmodifiableList(_excRegex);
    }

    public final boolean hasExcRegex() {
        return _excRegex != null && !_excRegex.isEmpty();
    }

    public final boolean hasConstraints() {
        return _sizeFrom != null || _sizeTo != null || this.hasIncRegex() || this.hasExcRegex();
    }

    public final List<Long> thresholds() {
        return hasThresholds() ? Collections.unmodifiableList(_thresholds) : null;
    }

    public final boolean hasThresholds() {
        return _thresholds != null && !_thresholds.isEmpty();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("%50s: %26s", "root.directory", _rootDirectory)).append("\n");
        sb.append(String.format("%50s: %26b", "follow.symbolic.links", _followLinks)).append("\n");
        if (_sizeFrom != null && _sizeFrom >= 0) {
            sb.append(String.format("%50s: %,26d", "file.size >=", _sizeFrom)).append("\n");
        }
        if (_sizeTo != null && _sizeTo >= 0) {
            sb.append(String.format("%50s: %,26d", "file.size <=", _sizeTo)).append("\n");
        }
        if (this.hasIncRegex()) {
            for (String regex : _incRegex) {
                sb.append(String.format("%50s: %,26d", "Include(regex): ", regex)).append("\n");
            }
        }
        if (this.hasExcRegex()) {
            for (String regex : _excRegex) {
                sb.append(String.format("%50s  %,26d", "Exclude(regex): ", regex)).append("\n");
            }
        }
        return sb.toString();
    }

    public static class Builder {
        private Path _rootDirectory;
        private Boolean _followLinks;
        private Long _sizeFrom;
        private Long _sizeTo;
        private Set<String> _incRegex;
        private Set<String> _excRegex;
        private List<Long> _thresholds;

        public Builder(Path rootDir) {
            _rootDirectory = rootDir;
            _followLinks = false;
            _sizeFrom = null;
            _sizeTo = null;
            _incRegex = new LinkedHashSet<String>();
            _excRegex = new LinkedHashSet<String>();
            _thresholds = new LinkedList<Long>();
        }

        public Builder() {
            this(null);
        }

        public Path rootDirectory() {
            return _rootDirectory;
        }

        public Boolean followLinks() {
            return _followLinks;
        }

        public Long sizeFrom() {
            return _sizeFrom;
        }

        public Long sizeTo() {
            return _sizeTo;
        }

        public Collection<String> incRegex() {
            return _incRegex.isEmpty() ? null : Collections.unmodifiableCollection(_incRegex);
        }

        public Collection<String> excRegex() {
            return _excRegex.isEmpty() ? null : Collections.unmodifiableCollection(_excRegex);
        }

        public List<Long> thresholds() {
            return _thresholds.isEmpty() ? null : Collections.unmodifiableList(_thresholds);
        }

        public Builder setRootDirectory(Path rootDir) {
            _rootDirectory = rootDir;
            return this;
        }

        public Builder setFollowLinks(Boolean followLinks) {
            _followLinks = followLinks;
            return this;
        }

        public Builder setSizeFrom(Long sizeFrom) {
            _sizeFrom = sizeFrom;
            return this;
        }

        public Builder setSizeTo(Long sizeTo) {
            _sizeTo = sizeTo;
            return this;
        }

        public Builder addIncRegex(String regex) {
            _incRegex.add(regex);
            return this;
        }

        public Builder addExcRegex(String regex) {
            _excRegex.add(regex);
            return this;
        }

        public Builder setThresholds(long... thresholds) throws IllegalArgumentException {
            _thresholds.clear();
            if (thresholds != null) {
                for (long threshold : thresholds) {
                    if (threshold < 0) {
                        throw new IllegalArgumentException(
                                "File size threshold should be a positive number. Found:  " + threshold);
                    }
                    _thresholds.add(threshold);
                }
                if (!_thresholds.isEmpty()) {
                    Collections.sort(_thresholds);
                }
            }
            return this;
        }

        public FileListOptions build() {
            if (rootDirectory() == null) {
                setRootDirectory(Paths.get(System.getProperty("user.dir")));
            }
            if (followLinks() == null) {
                setFollowLinks(false);
            }
            return new FileListOptions(_rootDirectory, _followLinks, _sizeFrom, _sizeTo, _incRegex, _excRegex,
                    _thresholds);
        }

        public Builder setIncRegex(Collection<String> incRegex) {
            _incRegex.clear();
            if (incRegex != null) {
                _incRegex.addAll(incRegex);
            }
            return this;
        }

        public Builder setExcRegex(Collection<String> excRegex) {
            _excRegex.clear();
            if (excRegex != null) {
                _excRegex.addAll(excRegex);
            }
            return this;
        }

    }

}
