package unimelb.io;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class CountedOutputStream extends FilterOutputStream {

	private long _bytesCount = 0;

	public CountedOutputStream(OutputStream out) {
		super(out);
	}

	public synchronized long bytesCount() {
		return _bytesCount;
	}

	protected synchronized void incBytesCount(long n) {
		_bytesCount += n;
	}

	@Override
	public void write(final int b) throws IOException {
		incBytesCount(1);
		out.write(b);
	}

	@Override
	public void write(final byte[] b) throws IOException {
		incBytesCount(b == null ? 0 : b.length);
		out.write(b);
	}

	@Override
	public void write(final byte[] b, final int start, final int len) throws IOException {
		incBytesCount(len);
		out.write(b, start, len);
	}
}
