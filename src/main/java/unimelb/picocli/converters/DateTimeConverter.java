package unimelb.picocli.converters;

import java.text.SimpleDateFormat;
import java.util.Date;

import arc.utils.DateTime;
import picocli.CommandLine;

public class DateTimeConverter implements CommandLine.ITypeConverter<Date> {

    @Override
    public Date convert(String value) throws Exception {
        if (value.matches("^\\d{1,2}-[a-zA-Z]{3}-\\d{4}$")) {
            return new SimpleDateFormat("dd-MMM-yyyy").parse(value);
        } else if (value.matches("^\\d{1,2}-[a-zA-Z]{3}-\\d{4} \\d{2}:\\d{2}:\\d{2}$")) {
            return new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss").parse(value);
        } else if (value.matches("^\\d{1,2}-[a-zA-Z]{3}-\\d{4}.\\d{2}:\\d{2}:\\d{2}$")) {
            return new SimpleDateFormat("dd-MMM-yyyy.HH:mm:ss").parse(value);
        } else {
            try {
                return DateTime.parse(value);
            } catch (Throwable t) {
                throw new IllegalArgumentException("'" + value + "' is not a valid date/time.", t);
            }
        }
    }

}
