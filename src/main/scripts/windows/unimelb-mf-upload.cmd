@echo off

setlocal

pushd "%~dp0..\..\"
set "ROOT=%cd%"
popd

set "JRE=%ROOT%\jre"

if exist "%JRE%\" (
    set "JAVA=%JRE%\bin\java"
) else (
    set JAVA=java
)

set JAR=%ROOT%\lib\unimelb-mf-clients.jar

"%JAVA%" -XX:+UseG1GC -XX:MaxGCPauseMillis=200 -XX:+UseStringDeduplication -Xmx1g -cp "%JAR%" unimelb.mf.client.sync.cli.MFUpload %*

endlocal
