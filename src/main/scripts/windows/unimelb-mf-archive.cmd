@echo off

setlocal

pushd "%~dp0..\..\"
set "ROOT=%cd%"
popd

set "JRE=%ROOT%\jre"

if exist "%JRE%\" (
    set "JAVA=%JRE%\bin\java"
) else (
    set JAVA=java
)

set JAR=%ROOT%\lib\unimelb-mf-clients.jar

"%JAVA%" -Xmx200m -cp "%JAR%" unimelb.mf.client.archive.cli.MFArchiveCLI %*

endlocal
