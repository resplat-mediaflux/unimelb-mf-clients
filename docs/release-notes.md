# v0.5.3

##### Enhancements:

1. **unimelb-mf-check**: changed output CSV cell values from Y/N to true/false.

##### Binary releases:

* **Java:** 
  * https://gitlab.unimelb.edu.au/resplat-mediaflux/releases/raw/master/mediaflux/unimelb-mf-clients-0.5.3.zip
* **Windows 64bit:**
  * https://gitlab.unimelb.edu.au/resplat-mediaflux/releases/raw/master/mediaflux/unimelb-mf-clients-0.5.3-windows-x64.zip
* **Mac 64bit:** 
  * https://gitlab.unimelb.edu.au/resplat-mediaflux/releases/raw/master/mediaflux/unimelb-mf-clients-0.5.3-mac-x64.zip
* **Linux 64bit:**
  * https://gitlab.unimelb.edu.au/resplat-mediaflux/releases/raw/master/mediaflux/unimelb-mf-clients-0.5.3-linux-x64.zip


# v0.5.2

##### Enhancements:

1. **unimelb-mf-download**: added support to download individual assets (files).
2. Added support to connect Mediaflux server via HTTP proxy. The proxy settings can be specified by adding following
 line to the config file (mflux.cfg):
  * proxy=[USERNAME:PASSWORD@]PROYXY_HOST:PORT
  * For example:
    * **proxy=wwwproxy.unimelb.edu.au:8000**
3. **unimelb-mf-check** now includes file sizes and CRC32 checksums in the output CSV file.
4. **unimelb-mf-check** has been added a new option --compress-output to compress the output CSV to GZIP format(.csv
.gz).

# v0.5.1

##### Enhancements:

1. Added support to symbolic links on Linux/MacOS platforms:

    * **unimelb-mf-upload** Added **--follow-symlinks** option. If not specified, it will create a special symlink asset in Mediaflux to represent the symbolic link. 
    * **unimelb-mf-download** Added **--no-symlinks** option. If not specified, it will try to restore/create the symbolic link in the local file system from the source symlink asset.

