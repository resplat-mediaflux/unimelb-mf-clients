```
USAGE:
    unimelb-mf-upload [OPTIONS] --dest <dest-collection-path> [src-dir1 [src-dir2...]] [src-file1 [src-file2...]]
    unimelb-mf-upload --config <mf-upload-config.xml>

DESCRIPTION:
    Upload local files to Mediaflux.  If the file pre-exists in Mediaflux and is the same as that being uploaded, the Mediaflux asset is not modified. However, if the files differ, a new version of the asset will be created. In Daemon mode, the process will only upload new files since the process last executed.

OPTIONS:
    --config <config.xml>                     A single configuration file including all required settings (Mediaflux server details, user credentials, application settings). If supplied, all other configuration options are ignored.

    --mf.config <mflux.cfg>                   Path to the config file that contains Mediaflux server details and user credentials.
    --mf.host <host>                          Mediaflux server host.
    --mf.port <port>                          Mediaflux server port.
    --mf.transport <https|http|tcp/ip>        Mediaflux server transport, can be http, https or tcp/ip.
    --no-cluster-io                           Disable cluster I/O if applicable.
    --dest <dest-collection-path>             The destination collection in Mediaflux.
    --create-parents                          Create destination parent collection if it does not exist, including any necessary but nonexistent parent collections.
    --csum-check                              If enabled, computes the checksum from the uploaded file and compares with that computed by the server for the Mediaflux asset.
    --nb-queriers <n>                         Number of query threads. Defaults to 1. Maximum is 4
    --nb-workers <n>                          Number of concurrent worker threads to upload data. Defaults to 1. Maximum is 8
    --nb-retries <n>                          Retry times when error occurs. Defaults to 2
    --batch-size <size>                       Size of the query result. Defaults to 1000
    --daemon                                  Run as a daemon.
    --daemon-port <port>                      Daemon listener port if running as a daemon. Defaults to 9761
    --daemon-scan-interval <seconds>          Time interval (in seconds) between scans of source directories. Defaults to 60 seconds.
    --exclude-parent                          Exclude parent directory at the destination (Upload the contents of the directory) if the source path ends with trailing slash.
    --aggregate-transmission                  Aggregate transmission to improve the performance for large number of small files if file size is less than 1MB.
    --split                                   Split large files (size>1GB) into chunks and upload them in parallel. Ignored if single worker thread.
    --log-dir <dir>                           Path to the directory for log files. No logging if not specified.
    --log-file-size-mb <n>                    Log file size limit in MB. Defaults to 100MB
    --log-file-count <n>                      Log file count. Defaults to 2
    --notify <email-addresses>                When completes, send email notification to the recipients(comma-separated email addresses if multiple). Not applicable for daemon mode.
    --sync-delete-assets                      Delete assets that do not have corresponding local files exist.
    --hard-delete-assets                      Force the asset deletion (see --sync-delete-assets) process to hard delete assets.  Otherwise, the behaviour is controlled by server properties (whether a deletion is a soft or hard action).
    --follow-symlinks                         Follow symbolic links. If not specified, it will not follow symbolic links, instead, it will create special symbolic link assets in Mediaflux. When exported as NFS share these symoblic assets will be represented as symbolic links. If downloaded using the unimelb-mf-download tool on Linux/MacOS platforms, they can be restored as symbolic links.
    --worm                                    Set the WORM state for the uploaded assets.
    --worm-can-add-versions                   Allow to add new versions of metadata and content when in the WORM state. Ignored if --worm option is not specified.
    --worm-no-move                            Disallow to move the uploaded assets. Ignored if --worm option is not specified.
    --save-file-attrs                         If specified, save local file attributes, such as owner uid, gid, ctime and ACls, into Mediaflux asset metadata.
    --preserve-modified-time                  If specified, preserve the local file's modified time as corresponding asset modified time in Mediaflux.
    --quiet                                   Do not print progress messages.
    --help                                    Prints usage.
    --version                                 Prints version.

POSITIONAL ARGUMENTS:
    src-dir                                   Source directory to upload.
    src-file                                  Source file to upload.

EXAMPLES:
    unimelb-mf-upload --mf.config ~/.Arcitecta/mflux.cfg --nb-workers 4  --dest /projects/proj-1128.1.59 ~/Documents/foo ~/Documents/bar
    unimelb-mf-upload --config ~/.Arcitecta/mf-upload-config.xml
```
