```
USAGE:
    unimelb-mf-download [OPTIONS] --out <dst-dir> <src-asset-or-namespace-path> [<src-asset-or-namespace-path>...]
    unimelb-mf-download --config <mf-download-config.xml>

DESCRIPTION:
    Download assets (files)  from Mediaflux to the local file system.  Pre-existing files in the local file system can be skipped or overwritten. In Daemon mode, the process will only download new assets (files)  since the process last executed.

OPTIONS:
    --config <config.xml>                     A single configuration file including all required settings (Mediaflux server details, user credentials, application settings). If conflicts with all the other options.

    --mf.config <mflux.cfg>                   Path to the config file that contains Mediaflux server details and user credentials.
    --mf.host <host>                          Mediaflux server host.
    --mf.port <port>                          Mediaflux server port.
    --mf.transport <https|http|tcp/ip>        Mediaflux server transport, can be http, https or tcp/ip.
    --no-cluster-io                           Disable cluster I/O if applicable.
    -o, --out <dst-dir>                       The output/destination directory.
    --overwrite                               Overwrite if the dst file exists.
    --unarchive                               Extract Arcitecta .aar files.
    --csum-check                              Files are equated if the name and size are the same. In addition, with this argument, you can optionally compute the CRC32 checksumk to decide if two files are the same.
    --no-symlinks                             Do not restore symbolic links. If not specified, it will try to create (restore) symbolic links. Note: creating symbolic links works only the platforms that support symbolic links, such as Linux or MacOS.
    --nb-queriers <n>                         Number of query threads. Defaults to 1. Maximum is 4
    --nb-workers <n>                          Number of concurrent worker threads to download data. Defaults to 1. Maximum is 8
    --nb-retries <n>                          Retry times when error occurs. Defaults to 0
    --batch-size <size>                       Size of the query result. Defaults to 1000
    --daemon                                  Run as a daemon.
    --daemon-port <port>                      Daemon listener port if running as a daemon. Defaults to 9761
    --daemon-scan-interval <seconds>          Time interval (in seconds) between scans of source asset namespaces. Defaults to 60 seconds.
    --exclude-parent                          Exclude parent directory at the destination (Download the contents of the directory) if the source path ends with trailing slash.
    --log-dir <dir>                           Path to the directory for log files. No logging if not specified.
    --log-file-size-mb <n>                    Log file size limit in MB. Defaults to 100MB
    --log-file-count <n>                      Log file count. Defaults to 2
    --notify <email-addresses>                When completes, send email notification to the recipients(comma-separated email addresses if multiple). Not applicable for daemon mode.
    --sync-delete-files                       Delete local files that do not have corresponding assets exist on the server side.
    --quiet                                   Do not print progress messages.
    --help                                    Prints usage.
    --version                                 Prints version.

POSITIONAL ARGUMENTS:
    <src-asset-or-namespace-path>             The source asset path or asset namespace path.

EXAMPLES:
    unimelb-mf-download --mf.config ~/.Arcitecta/mflux.cfg --nb-workers 2  --out ~/Downloads /projects/proj-1128.1.59/foo /projects/proj-1128.1.59/bar
    unimelb-mf-download --mf.config ~/.Arcitecta/mflux.cfg --nb-workers 2  --out ~/Downloads /projects/proj-1128.1.15/sample.zip
    unimelb-mf-download --config ~/.Arcitecta/mf-download-config.xml
```
